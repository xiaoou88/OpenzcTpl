<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class ratingModel{
	function Run(&$atts, &$refObj, &$fields){
		global $product_class;
		$attlist = "star=,half=,empty=";
		
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE); 
		$rating_string="";
		$rating=0;
		
		if(is_array($fields) && isset($fields['products_id'])){
			if(isset($fields['reviews_rating'])){
				$rating=$fields['reviews_rating'];
			}else{
				$rating=$fields['products_rating'];
			}
		}else{
			if($_GET['main_page']==FILENAME_PRODUCT_INFO){
				global $products_info;
				$rating=$products_info['products_rating'][$_GET['products_id']];
			}
		}
		
		for($i=0;$i<5;$i++){
			$j=$rating-$i;
			if($j>0 && $j<1){
				$rating_string.=$half."\r\n";
			}else if($j<=0){
				$rating_string.=$empty."\r\n";
			}else{
				$rating_string.=$star."\r\n";
			}
		}
		
		return $rating_string;
    }
}

  
?>