<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class listbarModel{
	function Run(&$atts, &$refObj, &$fields){
		$attlist = "item=,flag=,start=10,space=5";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		$line = empty($row) ? 100 : $row;

		$listbar=array();
		switch($item){
			case "orderby":
			$str=$this->get_parameter_str("order");
			if(isset($_GET['orderby'])){$active=explode(",",$_GET['orderby']);}else{$default_status="active";}
			$flag=array_filter(explode(",",$flag));//sell,price,viewed,name
			$default=["orderby_link"=>zen_href_link($_GET['main_page'],$str),"orderby_name"=>"Order By Default","status"=>$default_status];
			$listbar[]=$default;
			foreach($flag as $k => $v){
				if(isset($active) && $active[0]==strtolower($v)){
					$status="active";
				}else{
					$status="";
				}
				if($v=="sell"){
					$array=array(
						"orderby_link"=>zen_href_link($_GET['main_page'],$str."&orderby=".strtolower($v).",desc"),
						"orderby_name"=>"Best Selling",
						"sort"=>"desc",
						"status"=>$status
					);
					$listbar[]=$array;
				}else{
					
					$sort=array("asc"=>"Ascending","desc"=>"Descending");
					$v=ucfirst($v);
					foreach($sort as $a => $b){
						if($active[1]==$a && $active[0]==strtolower($v)){$status="active";}else{$status="";}
						$array=array(
							"orderby_link"=>zen_href_link($_GET['main_page'],$str."&orderby=".strtolower($v).",".$a),
							"orderby_name"=>$v.",".$b,
							"sort"=>$a,
							"status"=>$status
						);
						$listbar[]=$array;
					}
					
				}
			}
	
			break;
			case "limit":
				$str=$this->get_parameter_str("limit");
				$start=array_filter(explode(",",$start));
				
				if(count($start)<=1){
					$init=$start[0];
					for($i=1;$i<=5;$i++){
						$start[$i]=($space*$i)+$init;
					}
				}
				if(array_key_exists("limit",$_GET)){$active_id=$_GET['limit'];}else{$active_id="";$default_status="active";}
				$default=["limit_link"=>zen_href_link($_GET['main_page'],$str),"limit_name"=>"Default Show List","status"=>$default_status];
				$listbar[]=$default;
				foreach($start as $k => $v){
					$listbar[$k+1]["limit_name"]=$v;
					$listbar[$k+1]["limit_link"]=zen_href_link($_GET['main_page'],$str."&limit=".$v);
					if($v==$active_id){
						$listbar[$k+1]["status"]="active";
					}else{
						$listbar[$k+1]["status"]="";
					}
				}

			break;
		}
		
		return $listbar;
	}
	private function get_parameter_str($bar){
	
		foreach($_GET as $k => $v){
			if($k!="main_page" && $k!=$bar && $k!="page" && $k!="sort" && $k!="order" && $k!="orderby"){$str.=$k."=".$v."&";}
		}
		$str.="#";
		$str=str_replace("&#","",$str);
		$str=str_replace("#","",$str);
		return $str;
	}
}
