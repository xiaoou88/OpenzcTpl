<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class reviewsModel{
	function Run(&$atts, &$refObj, &$fields){
	
		global $product_class;
       
		$attlist = "row=100,textlen=,orderby=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		
		$line = empty($row) ? 100 : $row;
        
		$reviews=array();
		
		if($_GET["main_page"]=="product_info" && isset($_GET['products_id'])){
			$sql="select r.*,rd.reviews_text from ".TABLE_REVIEWS." r,".TABLE_REVIEWS_DESCRIPTION." rd where r.reviews_id=rd.reviews_id and r.products_id=".$_GET['products_id']." and rd.languages_id=".(int)$_SESSION['languages_id']." and r.status=1 order by r.date_added desc limit ".$row;
			
			$data=openzcQuery($sql);
			$data=openzc_table_to_list($data);
			
			foreach($data as $k => $v){
				$reviews[$k]=$v;
				$reviews[$k]["reviews_link"]=zen_href_link(FILENAME_PRODUCT_REVIEWS_INFO,'&products_id='.$k.'&reviews_id='.$v['reviews_id']);
			}
		}
		if($_GET["main_page"]==FILENAME_BLOG_ARCHIVES && isset($_GET['archives_id'])){
			$sql="select r.*,rd.reviews_text from ".TABLE_BLOG_REVIEWS." r,".TABLE_BLOG_REVIEWS_DESCRIPTION." rd where r.reviews_id=rd.reviews_id and r.archives_id=".$_GET['archives_id']." and rd.language_id=".(int)$_SESSION['languages_id']." and r.reviews_status=1 order by r.date_added desc limit ".$row;
			$data=openzcQuery($sql);
			$data=openzc_table_to_list($data);
			
			foreach($data as $k => $v){
				$reviews[$k]=$v;
			}
		}
		
		return $reviews;
	}
}
?>