<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class sqlModel{
	function Run(&$atts, &$refObj, &$fields){
		
		$attlist = "table=,fkey=,where=,field=*,row=1,orderby=";	
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		
		$tables=explode(",",$table);
		
		if(count($tables)>1 && $fkey){
			end($tables);
			$end=key($tables);
			foreach($tables as $k => $v){
				$v=explode(" ",$v);
				$t=constant($v[0]);
				$table_sql.=$t." ".$v[1];
				if($k!=$end){$table_sql.=" left join ";}
			}
			$table_sql.=" on ".$fkey;
		}else{
			$table_sql=constant($table);
		}
		if(!$where){
			echo "SQL Query parameter '\$where' cannot be empty";exit;
		}else{
			$query="select $field from $table_sql where $where limit 0,$row";
		}
		
		if($orderby){$query.=" order by $orderby";}
		
		$data=openzcQuery($query);
		$data=openzc_table_to_list($data);
		
		return $data;
	}
}
