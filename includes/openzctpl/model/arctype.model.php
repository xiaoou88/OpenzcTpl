<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class arctypeModel{
	function Run(&$atts, &$refObj, &$fields){
		global $GET,$blog_class,$predata_class;

		$attlist = "cid=,parent=0,row=100,type=,current=,limit=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		$line = empty($row) ? 100 : $row;
		
		
	
		$categories=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
	
		if($cid=="current"){
			$cid=openzc_get_current_cpath($_GET['cPath']);
		}
		else if(($type=="top" && !$cid) || $cid=="all"){
			$cid=$blog_class->get_all_top();
		}
		else if($cid=="parent"){
			$current_id=$cid=openzc_get_current_cpath($GET['cPath']);
			$cid=$categories[$current_id]["parent_id"];
		}
		if($type=="son"){
			if($fields['categories_id']){
				$cid=$categories[$fields['categories_id']]['son'];
			}else{
				$cid=$categories[$cid]['son'];
			}
		}
		if($type=="top" && $cid=="current"){
			$current_id=$cid=openzc_get_current_cpath($GET['cPath']);
			$cid=openzc_get_top_id($categories,$current_id);
		}

		$parameter=array('cid'=>$cid,'row'=>$line,"type"=>$type,"limit"=>$limit);
		
		$categories=$blog_class->get_categories_list($_GET,$parameter);
		
		$atts="";
	
		return $categories;
		
	}
}
