<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class filterModel{
	function Run(&$atts, &$refObj, &$fields){
		global $_vars,$category_class,$product_class,$filter_class,$init;
		$attlist = "item=,space=0,options_id=,type=,mul=,values=,slide=false,min=,max=";
		FillAtts($atts,$attlist);
		//FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);

		unset($_GET['action'],$_GET['zenid']);
		
	    if($item){
			$GLOBALS['filter_item']=$item;
		}else{
			$item=$GLOBALS['filter_item'];
		}

		if($item=="brand"){
			$filter[0]['filter_item']="brand";
			if(array_key_exists("filter_brand",$GLOBALS)){
				$filter_brand_values=$GLOBALS['filter_brand'];
			}else{
				$filter_brand_values=$filter_class->get_brand_bar($_GET,$mul);
				$GLOBALS['filter_brand']=$filter_brand_values;
			}
			if(count($filter_brand_values)==0){
				$filter=array();
			}
			if($type=="son"){
				$filter=$filter_brand_values;
				foreach($filter as $k => $v){
					$filter[$k]['form_value']=$v['filter_id'];
				}
			}
		}
		
		if($item=="price"){
			switch($slide){
				case "true":
					$filter[0]['filter_item']="price";
					if($type=="son"){
						$filter=$filter_class->get_price_bar($_GET,false,false,true);
					}
				break;
				case "false":
					if($space){
						$GLOBALS['filter_price_space']=$space;
					}else{
						$space=$GLOBALS['filter_price_space'];
					}
					$filter[0]['filter_item']="price";
					if($type=="son"){
						$filter=$filter_class->get_price_bar($_GET,$space,false);
						foreach($filter as $k => $v){
							$filter[$k]['form_value']=$v['min'].",".$v['max'];
						}
					}
				break;
			}
			
		}
		if($item=="attr"){
			if(!$options_id){echo "“Error,请输入属性参数ID！”";return false;}
		
			$GLOBALS['attr_mul']=$mul;
			$filter=$filter_class->get_attr_bar($_GET,$options_id,$mul);
			
			if(count($filter)==0){return false;}
			if($type=="son"){
				$mul=$GLOBALS['attr_mul'];
				$options_id=$fields["options_id"];
				$filter=$filter[$options_id]['values'];
				if($values){
					$values=array_filter(explode(",",$values));
					foreach($values as $k => $v){
						$v=explode(":",$v);
						$filter[$v[0]]['filter_value']=$v[1];
					}
				}
				
				foreach($filter as $k => $v){
					$filter[$k]['form_value']=$v['options_values_id'];
				}
			}
			
		}
		if($item=="active"){
			if(array_key_exists("filter_active",$GLOBALS)){
				$filter=$GLOBALS['filter_active'];
				
			}else{
				$GLOBALS['filter_active']=$filter=$filter_class->get_active_bar($_GET);
			}
			
			if($type=="son"){
				$item_id=$fields["item_id"];
				$filter=$filter[$item_id]['values'];
			}
		}
		if($item=="clear"){
			$main_page=$_GET['main_page'];
			foreach($_GET as $k => $v){
				if($k!="brand" && $k!="price" && $k!="options" && $k!="main_page" && $k!="sort"){
					$str.="&".$k."=".$v;
				}else{
					$n++;
				}
			}
		
			if($n>1){
				$filter[0]['filter_name']="Clear Filter";
				$filter[0]['filter_link']=zen_href_link($main_page,$str);
			}else{
				$filter=array();
			}
		}
		
		return $filter;
	}
	
}

