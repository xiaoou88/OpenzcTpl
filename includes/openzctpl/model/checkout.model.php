<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class checkoutModel{
	function Run(&$atts, &$refObj, &$fields){
		global $payment_modules,$order_total_modules;
		$attlist = "item=,type=,field";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		switch($item){
			case "payment_method":
				$selection = $payment_modules->selection();
				if($type=="son" && $field){
					return $fields[$field];
				}
			break;
			case "shipping_method":
				global $quotes,$currencies;
				$selection=$quotes;
				if($type=="son"){
					foreach($fields['methods'] as $k => $v){
						$fields['methods'][$k]['pid']=$fields['id'];
					}
					return $fields['methods'];
				}
				foreach($selection as $k => $v){
					foreach($v['methods'] as $a => $b){
						$selection[$k]['methods'][$a]['cost']=$currencies->format(zen_add_tax($b['cost'], (isset($v['tax']) ? $v['tax'] : 0)));
						if($_SESSION["shipping"]['id']==$v['id']."_".$b['id']){
							$selection[$k]['methods'][$a]['status']="active";
						}else{
							$selection[$k]['methods'][$a]['status']="";
						}
					}
				}
			break;
			case "order_total_modules":
				$selection =  $order_total_modules->credit_selection();
				if($type=="son" && $field){
					foreach($fields[$field] as $k => $v){
						$v=str_replace("<input ","<input class='form-control' ",$v);
						$v=preg_replace("' onkeyup=\"(.*?)\"'is","",$v);
						$fields[$field][$k]=$v;
					}
					return $fields[$field];
				}
			break;
		}
		return $selection;
	}
}
  
  
?>