<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class varModel{
	function Run(&$atts, &$refObj, &$fields){
		
		$attlist = "name=,type=";	
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		if(!array_key_exists($name,$GLOBALS)){
			echo "Error: The variable name '\$".$name."' does not exist!";
			return false;
		}
		if(is_array($GLOBALS[$name])){
			$result=$GLOBALS[$name];
			if($type=='son'){
				$result=$fields;
			}
			return $result;
		}else{
			echo $GLOBALS[$name];
		}
	}
}
