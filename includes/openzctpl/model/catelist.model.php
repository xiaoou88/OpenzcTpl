<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class catelistModel{
	function Run(&$atts, &$refObj, &$fields){
		global $GET,$category_class,$product_class,$predata_class;

		$attlist = "cid=,parent=0,row=100,type=,current=,limit=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		$line = empty($row) ? 100 : $row;
		
		
	
		$categories=$predata_class->getPredata(TABLE_CATEGORIES);
	
		if($cid=="current"){
			$cid=openzc_get_current_cpath($_GET['cPath']);
		}
		else if(($type=="top" && !$cid) || $cid=="all"){
			$cid=$category_class->get_all_top();
		}
		else if($cid=="parent"){
			$current_id=$cid=openzc_get_current_cpath($GET['cPath']);
			$cid=$categories[$current_id]["parent_id"];
			if(!$cid || $cid==0){$cid="parent";}
		}
		if($type=="son"){
			if($cid=="parent"){
				$cid=$category_class->get_all_top();
			}else{
				if($fields['categories_id']){
					$cid=$categories[$fields['categories_id']]['son'];
				}else{
					$cid=$categories[$cid]['son'];
				}
			}
			
		}
		if($type=="top" && $cid=="current"){
			$current_id=$cid=openzc_get_current_cpath($GET['cPath']);
			$cid=openzc_get_top_id($categories,$current_id);
		}

		$parameter=array('cid'=>$cid,'row'=>$line,"type"=>$type,"limit"=>$limit);
		
		$categories=$category_class->get_categories_list($_GET,$parameter);
		
		$atts="";
	
		return $categories;
		
	}
}
