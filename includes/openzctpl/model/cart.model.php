<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class cartModel{
	function Run(&$atts, &$refObj, &$fields){
		global $_vars,$product_class,$predata_class,$category_class,$productArray,$currencies;
	
		$attlist = "item=,newcart=,imgsizer=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE); 
		
		$shopcart=$_SESSION['cart']->contents; 
		if($item){
			switch($item){
				case "attr":
					if($_GET['main_page']!=FILENAME_SHOPPING_CART){
						
						$options=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS);
						$options_values=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS_VALUES);
						$products_attributes=$predata_class->getPredata(TABLE_PRODUCGS_ATTRIBUTES);
						if(!$fields['attributes']){
							$fields['attributes']=$shopcart[$fields['id']]['attributes'];
						}
						foreach($fields['attributes'] as $k => $v){
							$attributes[$k]["products_options_name"]=$options[$k]["products_options_name"];
							$attributes[$k]["products_options_values_name"]=$options_values[$v]["products_options_values_name"];
							$attributes[$k]['options_values_price']=$products_attributes[$fields['products_id']]['options_values_price'];
						}
						$fields['attributes']=$attributes;
					}
					return $fields['attributes'];
				break;
				case "count":
					echo $_SESSION['cart']->count_contents();
				break;
				case "item":
					echo count($_SESSION['cart']->contents);
				break;
				case "total":
					echo $currencies->format($_SESSION['cart']->show_total());
				break;
				case "subtotal":
					echo $GLOBALS['ot_subtotal']->output[0]['text'];
				break;
				case "shipping_cost":
					echo $currencies->format($_SESSION['shipping']['cost']);
				break;
			}
		}
		else{
			
			$_SESSION['valid_to_checkout'] = true;
			$_SESSION['cart_errors'] = '';
			if(!$productArray){
				$productArray=$_SESSION['cart']->get_products(true);	
			}
			
			$pids=$this->get_products_ids($productArray);
			
			if($pids){
				
				$parameter=array("pid"=>$pids,"flag"=>true,"index_field"=>"products_id","imgsizer"=>$imgsizer);
				$products=$product_class->get_product_list($_GET,$parameter);
				
			}
			
			foreach($productArray as $k => $v){
				if($v['id']==$_SESSION['new_products_id_in_cart']){$new=$k;}
				$id=explode(":",$v['id'])[0];
				$productArray[$k]['products_image']=$products[$id]['products_image'];
				$productArray[$k]['products_name']=$products[$id]['products_name'];
				$productArray[$k]['products_link']=$products[$id]['products_link'];
				if(!$v['productsPriceEach']){
					$productArray[$k]['products_price']=$products[$id]['products_price'];
				}else{
					$productArray[$k]['products_price']=$v['productsPriceEach'];
				}
				
				$productArray[$k]['products_original_price']=$products[$id]["products_original_price"];
				$productArray[$k]['productPriceDiscount']=$products[$id]["productPriceDiscount"];
				$productArray[$k]['products_qty']=$_SESSION['cart']->contents[$v['id']]['qty'];
				$productArray[$k]['products_id']=$id;
				if(!isset($v['productsPrice'])){
					$pricenumber=$_SESSION['cart']->contents[$v['id']]['qty']*zen_get_products_base_price($id);
					$productArray[$k]['productsPrice']=$currencies->display_price($pricenumber,"");
				}
			}
			if($newcart==true){
				$result[]=$productArray[$new];
				return $result;
			}
			
		    return $productArray;
		}
	}
	
	private function get_products_ids($productArray){
		foreach($productArray as $k => $v){
		   $v['id']=explode(":",$v['id']);
		   $id[]=$v['id'][0];
		}
		return join(",",$id);
	}
	
}


  
?>