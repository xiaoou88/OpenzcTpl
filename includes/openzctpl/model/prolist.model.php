<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class prolistModel{
	function Run(&$atts, &$refObj, &$fields){
		global $product_class,$predata_class;
		
		$attlist = "cid=all,pid=,row=100,flag=true,titlelen=,desclen=,orderby=,saleid=,imgsizer=,bgcolor,type=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		
		switch($orderby){
			case "desc":
				$orderby=array("p","products_date_added","desc",TABLE_PRODUCTS);
			break;
			case "asc":
				$orderby=array("p","products_date_added","asc",TABLE_PRODUCTS);
			break;
			case "rand":
				$orderby=array("p","products_date_added","rand",TABLE_PRODUCTS);
			break;
			default:
				$orderby=array("p","products_date_added","desc",TABLE_PRODUCTS);
			break;
		}
		
		$categories=$predata_class->getPredata(TABLE_CATEGORIES);
	
		$line = empty($row) ? 100 : $row;
		if($cid!="all"){
			if($cid=="current" && array_key_exists('cPath',$_GET)){
				$cid=openzc_get_current_cpath($_GET['cPath']);
			}
			$cid=array_filter(explode(",",$cid));
			
			foreach($cid as $k => $v){
				@$ids.=openzc_get_son_ids($categories,$v,$v).",";
			}
			$cid=join("','",array_filter(explode(",",$ids)));
			
		}
		if($type=="compare" || $type=="wishlist"){
			if(isset($_SESSION[$type])){
				if($pid=="current"){$pid=$_SESSION[$type]['current'];}
				else{$pid=$_SESSION[$type]['list'];}
				if(!$_SESSION[$type]['list']){return false;}
			}else{
				return false;
			}
		}
		
		$parameter=array('cid'=>$cid,'pid'=>$pid,'row'=>$line,'flag'=>$flag,'titlelen'=>$titlelen,"desclen"=>$desclen,'orderby'=>$orderby,'saleid'=>$saleid,'imgsizer'=>$imgsizer,'bgcolor'=>$bgcolor,"type"=>$type);  
		$products=$product_class->get_product_list($_GET,$parameter);
		$atts="";
	
		return $products;
	}
	
	
}
  
?>