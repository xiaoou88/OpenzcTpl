<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class accountModel{
	function Run(&$atts, &$refObj, &$fields){
		global $_vars,$product_class,$category_class,$productArray,$currencies;
	
		$attlist = "item=,field=,default=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE); 
		
		switch($item){
			case "orders":
			global $order;
				if($_GET['main_page']=="account_history_info"){
					global $order,$statusArray,$currencies;
					$result=array();
					foreach($order->products as $k => $v){
						$ppe = zen_round(zen_add_tax($v['final_price'], $v['tax']), $currencies->get_decimal_places($order->info['currency']));
						$ppt = $ppe * $v['qty'];
						$v['price']=$currencies->format($v['price'], true, $order->info['currency'], $order->info['currency_value']);
						$v['final_price']=$currencies->format($ppt, true, $order->info['currency'], $order->info['currency_value']) . ($order->products[$i]['onetime_charges'] != 0 ? '<br />' . $currencies->format(zen_add_tax($v['onetime_charges'], $v['tax']), true, $order->info['currency'], $order->info['currency_value']) : '');
						$products[$k]=$v;
					}
					$result['orders_id']=$_GET['order_id'];
					$result['date_purchased']=zen_date_long($order->info['date_purchased']);
					$result['totals']=$order->totals;
					$result['products']=$products;
					$result['status_history']=$statusArray;
					$result['shipping_method']=$order->info['shipping_method'];
					$result['payment_method']=$order->info['payment_method'];
					$result['delivery']=zen_address_format($order->delivery['format_id'], $order->delivery, 1, ' ', '<br />');
					$result['billing']=zen_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />');
					if($field=="attr"){
						$result['attr']=$fields['attributes'];
					}
					
					if(!is_array($result[$field])){
						echo $result[$field];
						return false;
					}else{
						return $result[$field];
					}
				}
				if($field=="result_page"){
					global $history_split;
					$text=TEXT_RESULT_PAGE . $history_split->display_links($max_display_page_links, zen_get_all_get_params(array('page', 'info', 'x', 'y', 'main_page')), $paginateAsUL).$history_split->display_count(TEXT_DISPLAY_NUMBER_OF_ORDERS);
					echo $text;
				}else{
					global $ordersArray,$accountHistory;
					if(!$ordersArray){
						$ordersArray=$accountHistory;
					}
					foreach($ordersArray as $k => $v){
						$v['order_link']=zen_href_link(FILENAME_ACCOUNT_HISTORY_INFO, 'order_id=' . $v['orders_id'], 'SSL');
						$v['date_purchased']=zen_date_long($v['date_purchased']);
						$orders[$k]=$v;
					}
					
					return $orders;
				}
			break;
			case "nav":
				$nav=array();
				$nav["account"]["link"]=zen_href_link("account", '', 'SSL');
				$nav["account"]["page"]="account";
				$nav["account"]["text"]=HEADING_TITLE;
				$nav["account"]['title']=TITLE_ACCOUNT_DASHBOARD;
				$nav["orders"]["link"]=zen_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL');
				$nav["orders"]["page"]=FILENAME_ACCOUNT_HISTORY;
				$nav["orders"]["text"]=OVERVIEW_SHOW_ALL_ORDERS;
				$nav["orders"]['title']=TITLE_ACCOUNT_ORDERS;
				$nav["wishlist"]["link"]=zen_href_link(FILENAME_ACCOUNT_WISHLIST, '', 'SSL');
				$nav["wishlist"]["page"]=FILENAME_ACCOUNT_WISHLIST;
				$nav["wishlist"]["text"]="";
				$nav["wishlist"]['title']=TITLE_ACCOUNT_WISHLIST;
				$nav["edit"]["link"]=zen_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL');
				$nav["edit"]["page"]=FILENAME_ACCOUNT_EDIT;
				$nav["edit"]["text"]=MY_ACCOUNT_INFORMATION;
				$nav["edit"]["title"]=TITLE_ACCOUNT_INFORMATION;
				$nav["address"]["link"]=zen_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL');
				$nav["address"]["page"]=FILENAME_ADDRESS_BOOK;
				$nav["address"]["text"]=MY_ACCOUNT_ADDRESS_BOOK;
				$nav["address"]["title"]=TITLE_ACCOUNT_ADDRESS_BOOK;
				if($_SESSION['cart']->count_contents()>0){
					$nav["billing"]["link"]=zen_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL');
					$nav["billing"]["page"]=FILENAME_CHECKOUT_PAYMENT_ADDRESS;
					$nav["billing"]["text"]=TITLE_PAYMENT_ADDRESS;
					$nav["billing"]["title"]=TITLE_ACCOUNT_BILLING_ADDRESS;
				}
				$nav["password"]["link"]=zen_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL');
				$nav["password"]["page"]=FILENAME_ACCOUNT_PASSWORD;
				$nav["password"]["text"]=MY_ACCOUNT_PASSWORD;
				$nav["password"]["title"]=TITLE_ACCOUNT_PASSWORD;
				if ((int)ACCOUNT_NEWSLETTER_STATUS > 0 or CUSTOMERS_PRODUCTS_NOTIFICATION_STATUS !='0') {
					if ((int)ACCOUNT_NEWSLETTER_STATUS > 0) {
						$nav["notice_news"]["link"]=zen_href_link(FILENAME_ACCOUNT_NEWSLETTERS, '', 'SSL');
						$nav["notice_news"]["page"]=FILENAME_ACCOUNT_NEWSLETTERS;
						$nav["notice_news"]["text"]=EMAIL_NOTIFICATIONS_NEWSLETTERS;
						$nav["notice_news"]["title"]=TITLE_ACCOUNT_NOTIFICATIONS_NEWSLETTERS;
					}
					if (CUSTOMERS_PRODUCTS_NOTIFICATION_STATUS == '1') {
						$nav["notice_products"]["link"]=zen_href_link(FILENAME_ACCOUNT_NOTIFICATIONS, '', 'SSL');
						$nav["notice_products"]["page"]=FILENAME_ACCOUNT_NOTIFICATIONS;
						$nav["notice_products"]["text"]=EMAIL_NOTIFICATIONS_PRODUCTS;
						$nav["notice_products"]["title"]=TITLE_ACCOUNT_NOTIFICATIONS_PRODUCTS;
					}
				}
				if ($customer_has_gv_balance ) {
					$nav["gv"]["link"]=zen_href_link(FILENAME_GV_SEND, '', 'SSL');
					$nav["gv"]["page"]=FILENAME_GV_SEND;
					$nav["gv"]["text"]=BUTTON_SEND_A_GIFT_CERT_ALT;
					$nav["gv"]["title"]=TITLE_ACCOUNT_GVSEND;
				}
				$nav["logoff"]["link"]=zen_href_link(FILENAME_LOGOFF, '', 'SSL');
				$nav["logoff"]["page"]=FILENAME_LOGOFF;
				$nav["logoff"]["text"]=HEADER_TITLE_LOGOFF;
				$nav["logoff"]["title"]=HEADER_TITLE_LOGOFF;
				$navigation=count($_SESSION['navigation']->path)-1;
				foreach($nav as $k => $v){
					if($v['page']==$_SESSION['navigation']->path[$navigation]['page']){
						$nav[$k]['status']="active";
					}else{
						$nav[$k]['status']="";
					}
				}
				if($field){
					$field=explode(",",$field);
					foreach($field as $k => $v){
						if(array_key_exists($v,$nav)){
							$result[$k]=$nav[$v];
						}
					}
					return $result;
				}
				
				return $nav;
			break;
			case "address":
				
				if(!$_SESSION['customer_id']){return false;}
				if($_GET['main_page']=="address_book_process" && isset($_GET['edit'])){
					$address_id_sql=" and ab.address_book_id=".$_GET['edit'];
				}
				$addresses_query = "SELECT ab.*,ab.entry_firstname as firstname, ab.entry_lastname as lastname,
                           ab.entry_company as company, ab.entry_street_address as street_address,
                           ab.entry_suburb as suburb, ab.entry_city as city, ab.entry_postcode as postcode,
                           ab.entry_state as state, ab.entry_zone_id as zone_id, ab.entry_country_id as country_id,c.customers_telephone as telephone
                    FROM  " . TABLE_ADDRESS_BOOK . " ab left join ".TABLE_CUSTOMERS." c on c.customers_id=ab.customers_id
                    WHERE ab.customers_id = ".$_SESSION['customer_id'].@$address_id_sql."
                    ORDER BY firstname,lastname";
				$addresses = openzcQuery($addresses_query);
				
				if($_GET['main_page']=="address_book_process" && (isset($_GET['edit']) || isset($_GET['delete'])) && $field){
					if($field=="address"){
						$format_id = zen_get_address_format_id($addresses->fields['country_id']);
						echo zen_address_format($format_id, $addresses->fields, true, ' ', '<br />');
					}else if($field=="state"){
						echo zen_get_zone_name($addresses->fields['entry_country_id'], $addresses->fields['entry_zone_id'], $addresses->fields['entry_state']);
					}else{
						echo $addresses->fields[$field];
					}
					return false;
				}
				
				$addressArray = array();
				while (!$addresses->EOF) {
					$format_id = zen_get_address_format_id($addresses->fields['country_id']);
					$addressArray[] = array(
						'firstname'=>$addresses->fields['firstname'],
						'lastname'=>$addresses->fields['lastname'],
						'address_book_id'=>$addresses->fields['address_book_id'],
						'format_id'=>$format_id,
						'address'=>$addresses->fields,
					);
					$addresses->MoveNext();
				}
				if(isset($_SESSION['sendto']) && $_SESSION['sendto']){$default_address_id=$_SESSION['sendto'];}else{$default_address_id=$_SESSION['customer_default_address_id'];}
				
				foreach($addressArray as $k => $v){
					if($default==true && $v['address_book_id'] != $default_address_id){
						continue;
					}
					$address[$k]=$v['address'];
					$address[$k]['status']="";
					if($v['address_book_id'] == $_SESSION['customer_default_address_id']){
						$address[$k]['status']="active";
					}
					if(!$v['address']['entry_street_address'] || !$v['address']['entry_state'] || !$v['address']['entry_country_id']){$address[$k]['status']="off";}
					$address[$k]["edit_link"]=zen_href_link(FILENAME_ADDRESS_BOOK_PROCESS, 'edit=' . $v['address_book_id'], 'SSL');
					$address[$k]["edit_text"]=BUTTON_EDIT_SMALL_ALT;
					$address[$k]["delete_link"]=zen_href_link(FILENAME_ADDRESS_BOOK_PROCESS, 'delete=' . $v['address_book_id'], 'SSL');
					$address[$k]["delete_text"]=BUTTON_DELETE_SMALL_ALT;
					$address[$k]["address"]=zen_address_format($v['format_id'], $v['address'], true, ' ', '<br />');
				}
				
				return $address;
			break;
			default:
				global $account;
				if(!$account){
					$sql="select * from ".TABLE_CUSTOMERS." where customers_id=".$_SESSION['customer_id'];
					$account=openzcQuery($sql);
				}
				if($field){
					if($field=="customers_dob"){$result=zen_date_short($account->fields[$field]);}
					else{
						$result=$account->fields[$field];
					}
					echo $result;
				}
				else{$result[]=$account->fields;return $result;}
			break;
		}
		
	}
	
}

  
?>