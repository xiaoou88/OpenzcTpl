<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class msgModel{
	function Run(&$atts, &$refObj, &$fields){
		global $messageStack;
		$attlist = "type=,name=";//$type:error,warning,success,caution
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		$message=array();

		foreach($messageStack->messages as $k => $v){
			if(strstr($v['params'],"Error")){$v['type']="error";}
			if(strstr($v['params'],"Success")){$v['type']="success";}
			if(strstr($v['params'],"Warning")){$v['type']="warning";}
			if(strstr($v['params'],"Caution")){$v['type']="warning";}
			if($v['class']=="shopping_cart"){$v['type']="error";}
			switch($v['type']){
				case "error":$flag="<i class='fas fa-exclamation-triangle'></i> ";break;
				case "success":$flag="<i class='fas fa-check'></i> ";break;
				case "warning":$flag="<i class='fas fa-exclamation-triangle'></i> ";break;
			}
			$v['text']=preg_replace("'<img(.*?)>'is",$flag,$v['text']);
			$message[$v['class']][$v['type']][]=$v;
		}
		
		if($name){
			
			if(isset($_GET['action']) && $name=="contact_us" && $_GET['action']=="success"){
				$message[$name][$type]['text']=TEXT_SUCCESS;
				return $message[$name][$type];
			}

			if($type && array_key_exists($type,$message[$name])){
				$_SESSION['messageToStack'] = '';
				return $message[$name][$type];
			}
			return false;
		}
		return false;
	}
}
?>