<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class proattrModel{
	
	function Run(&$atts, &$refObj, &$fields){
		global $predata_class,$ImageSizer,$attributes_class,$currencies;
		$attlist = "item=,options_id=,type=,values=,imgsizer=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		$line = empty($row) ? 100 : $row;
		$options=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS);
		$options_values=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS_VALUES);
		$options_type=["select"=>0,"text"=>1,"radio"=>2,"checkbox"=>3,"file"=>4,"read"=>5];
		
		if($type=="son"){
			$options_values=$fields['values'];
			foreach($options_values as $k => $v){
				if($v['products_options_values_image'] && $imgsizer){
					$imgsizer=explode(",",$imgsizer);
					$img=$v['products_options_values_image'];
					$file=getCacheImgfile($img);
					$imgname=getCacheImgname(basename($img));
					$imgdir=IMGCACHE_DIR.$imgsizer[0]."-".$imgsizer[1]."/";
					
					$image_new=$imgdir.$imgname;
					if(!is_dir($imgdir)){
						mkdir($imgdir);
					}
					if(getCacheImgcheck($image_new)==true){
						$ImageSizer->Run(".".$file);
						$ImageSizer->resize($imgsizer[0],$imgsizer[1]);
						$ImageSizer->save($image_new);
					}
					$options_values[$k]["products_options_values_image_full"]="/images/".$img;
					$options_values[$k]["products_options_values_image"]="/".str_replace(ROOT_PATH,"",$imgdir).$imgname;
				}
			}

			return $options_values;
		}
		
		if($_GET['main_page']==FILENAME_PRODUCT_INFO){
			$products_id=$_GET['products_id'];
			$allpid="'".$products_id."'";
		}	
		if(is_array($fields) && isset($fields['products_id'])){
			$products_id=$fields['products_id'];
			$allpid=$fields['allpid'];
		}

		if(isset($GLOBALS['spec_'.md5($allpid)])){
			$specials_products=$GLOBALS['spec_'.md5($allpid)];
		}else{
			
			$specials_products = openzcQuery("select specials_new_products_price from " . TABLE_SPECIALS . " where products_id in(" .$allpid. ") and status='1'");
			$specials_products = openzc_table_to_list($specials_products,"products_id");
			$GLOBALS['spec_'.md5($allpid)]=$specials_products;
		}
		
		if(isset($GLOBALS['attr_'.md5($allpid)])){
			$products_attributes=$GLOBALS['attr_'.md5($allpid)];
		}else{
			
			$sql="select pa.*,p.products_tax_class_id,p.products_price,p.products_priced_by_attribute,p.product_is_free,p.product_is_call,p.products_model,p.master_categories_id,po.products_options_sort_order from ".TABLE_PRODUCTS_ATTRIBUTES." pa left join ".TABLE_PRODUCTS." p on pa.products_id=p.products_id ,".TABLE_PRODUCTS_OPTIONS." po where po.products_options_id=pa.options_id and p.products_id in(".$allpid.") and po.language_id='".(int)$_SESSION['languages_id']."' order by po.products_options_sort_order asc";
			
			$attributes=openzcQuery($sql);
			$attributes=openzc_table_to_list($attributes);
			foreach($attributes as $k => $v){
				$products_attributes[$v['products_id']][$v["products_attributes_id"]]=$v;
			}
			
			if(isset($fields['products_compare']) && isset($_SESSION['compare'])){
				$compare_id=str_replace(",","','",$_SESSION['compare']['list']);
				$sql="select pa.options_id,pa.products_id,po.products_options_sort_order from ".TABLE_PRODUCTS_ATTRIBUTES." pa left join ".TABLE_PRODUCTS_OPTIONS." po on po.products_options_id=pa.options_id where pa.products_id in('".$compare_id."') and po.language_id='".(int)$_SESSION['languages_id']."' order by po.products_options_sort_order asc";
				$compare_options=openzcQuery($sql);
				$compare_options=openzc_table_to_list($compare_options);
				foreach($compare_options as $k => $v){
					$all_options[$v['options_id']]=$v;
					$compare_attributes[$v['products_id']][$v['options_id']]=true;
				}
				$all_pids=array_filter(explode(",",str_replace("'","",$allpid)));
				foreach($all_pids as $k => $v){
					if(!isset($products_attributes[$v])){$products_attributes[$v]=array();}
				}
				foreach($all_options as $k => $v){
					foreach($products_attributes as $a => $b){
						if(!$compare_attributes[$a][$k]){
							$products_attributes[$a][]=['options_id'=>$v['options_id'],"products_options_sort_order"=>$v['products_options_sort_order']];
						}
					}
				}
				foreach($products_attributes as $k => $v){
					$products_attributes[$k]=multiSort($v,"products_options_sort_order","asc");
				}
				
			}
			
			$GLOBALS['attr_'.md5($allpid)]=$products_attributes;
		}
		
		if($options_id){
			$options_id=explode(",",$options_id);
			$options_id=array_filter($options_id);
		}
		
		
		if($item){
			$item=explode(",",$item);
			$item=array_filter($item);
		}
		
		
		foreach($products_attributes[$products_id] as $k => $v){
			if($item && $_GET['main_page']==FILENAME_PRODUCT_INFO){
				foreach($item as $a => $b){
					if($options[$v["options_id"]]['products_options_type']==$options_type[$b]){$break=false;}
				}
			}
			if($options_id){
				foreach($options_id as $a => $b){
					if($v["options_id"]==$b){$break=false;}
				}
			}
			
			if($break==false){
				$data[$v["options_id"]]['options_id']=$v["options_id"];
				$data[$v["options_id"]]['options_name']=$options[$v["options_id"]]['products_options_name'];
				$data[$v["options_id"]]['options_type']=$options[$v["options_id"]]['products_options_type'];
				$v['options_values_name']=$options_values[$v['options_values_id']]["products_options_values_name"];
				$v['products_options_values_image']=$options_values[$v['options_values_id']]["products_options_values_image"];
				if(!isset($specials_products[$products_id])){
					$specials_products[$products_id]=array();
				}
				$values_final_price=$attributes_class->get_attributes_price_final($v,$products_attributes[$products_id],$specials_products[$products_id]);
				$v['options_values_final_price']=number_format($values_final_price,2);
				if($v['price_prefix']=="+"){
					$v['products_options_values_final_price']=$currencies->display_price(($fields['products_price_number']+$values_final_price),'');
				}
				if($v['price_prefix']=="-"){
					$v['products_options_values_final_price']=$currencies->display_price(($fields['products_price_number']-$values_final_price),'');
				}
				$data[$v["options_id"]]['values'][$v['options_values_id']]=$v;
			}
		}
		
		return $data;
	}
	
	
}