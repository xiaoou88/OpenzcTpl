<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class proinfoModel{
	function Run(&$atts, &$refObj, &$fields){
		
		if(!$_GET['products_id']){echo "Error:{openzc:proinfo}该标签适用于商品详情页，<a href='http://www.openzc.com/' target='_blank'>查看说明</a>！";return false;}
		global $product_class,$category_class;
		$attlist = "field=,desclen=,imgsizer=";
		
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE); 
		$strlen = empty($strlen) ? 200 : $strlen;
		if(array_key_exists("products_info",$GLOBALS) && $field!="products_description" && !$desclen){
			$products=$GLOBALS["products_info"];
			$products_info=$GLOBALS["products_info"];
		}else{
			$products=$product_class->get_products_info($_GET['products_id']);
			$products['products_description']=$this->getDesview($products['products_description'],$desclen);
			$GLOBALS["products_info"]=$products;
		}
		
		if($field=="products_image" && $imgsizer){
			global $ImageSizer;
			if($imgsizer){
				$imgsizer=explode(",",$imgsizer);
				$imgname=getCacheImgname(basename($products[$field]));
				$imgdir=IMGCACHE_DIR.$imgsizer[0]."-".$imgsizer[1]."/";
				$image_new=$imgdir.$imgname;
				if(!is_dir($imgdir)){
					mkdir($imgdir);
				}
				if(getCacheImgcheck($image_new)==true){
					$ImageSizer->Run(".".$products[$field]);
					$ImageSizer->resize($imgsizer[0],$imgsizer[1]);
					$ImageSizer->save($image_new);
				}
				$products[$field]="/".str_replace(ROOT_PATH,"",$imgdir).$imgname;
			}
		}
		return $products[$field];
		
    }
	
	private function getDesview($str,$len){
		$pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png]))[\'|\"].*?[\/]?>/";
		preg_match_all($pattern,$str,$match);
		foreach($match[0] as $k => $v){
			$str=str_replace($v,"",$str);
		}
		if($len){
			$str=substr($str,0,$len);
		}
		return $str;
	}
}

  
?>