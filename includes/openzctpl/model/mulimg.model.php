<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class mulimgModel{
	function Run(&$atts, &$refObj, &$fields){
		global $product_class,$ImageSizer;
		$attlist = "row=100,imgsizer=,bigimgsizer=,bgcolor=,index=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		
		if($imgsizer){
			$imgsizer=explode(",",$imgsizer);
		}
		
		if($bigimgsizer){
			$bigimgsizer=explode(",",$bigimgsizer);
		}
		$line = empty($row) ? 100 : $row;
		$mulimg=array();
		
		if(array_key_exists("products_id",$_GET)){
			$products_id=$_GET["products_id"];
		}
		if($fields["products_id"]){
			$products_id=$fields["products_id"];
		}
		$data=$product_class->get_products_detail_image($products_id);
		
		if($index=="false"){
			unset($data[0]);
		}
		if(count($data)>0){
			foreach($data as $k => $v){
				$mulimg[$k]["sort_index"]=$k+1;
				if($imgsizer){
					$file=getCacheImgfile($v);
					$imgname=getCacheImgname(basename($v));
					$imgdir=IMGCACHE_DIR.$imgsizer[0]."-".$imgsizer[1]."/";
					$image_new=$imgdir.$imgname;
					if(!is_dir($imgdir)){
						mkdir($imgdir);
					}
					if(getCacheImgcheck($image_new)==true){
						$ImageSizer->Run(".".$file);
						$ImageSizer->resize($imgsizer[0],$imgsizer[1]);
						$ImageSizer->save($image_new);
					}
					$mulimg[$k]["products_image_detail_full"]="/images/".$v;
					$mulimg[$k]["products_image_detail"]="/".str_replace(ROOT_PATH,"",$imgdir).$imgname;
				}
				if($bigimgsizer){
					$file=getCacheImgfile($v);
					$imgname=getCacheImgname(basename($v));
					$imgdir=IMGCACHE_DIR.$bigimgsizer[0]."-".$bigimgsizer[1]."/";
					$image_new=$imgdir.$imgname;
					if(!is_dir($imgdir)){
						mkdir($imgdir);
					}
					if(getCacheImgcheck($image_new)==true){
						$ImageSizer->Run(".".$file);
						$ImageSizer->resize($bigimgsizer[0],$bigimgsizer[1]);
						$ImageSizer->save($image_new);
					}
					$mulimg[$k]["products_image_detail_big_full"]="/images/".$v;
					$mulimg[$k]["products_image_detail_big"]="/".str_replace(ROOT_PATH,"",$imgdir).$imgname;
				}
			}
		}
		
		return $mulimg;
	}
	
}
?>