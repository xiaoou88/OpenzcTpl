<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class bannerModel{
	function Run(&$atts, &$refObj, &$fields){
		global $predata_class;
		$attlist = "bid=,group=";	
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		$banners=$predata_class->getPredata(TABLE_BANNERS);
		$result=array();
		if(!empty($bid)){
			$bid=array_filter(explode(",",$bid));
			foreach($bid as $k => $v){
				$result[$v]=$banners[$v];
			}
			return $result;
		}
		
		if(!empty($group)){
			$group=array_filter(explode(",",$group));
			foreach($banners as $k => $v){
				foreach($group as $a => $b){
					if($v['banners_group']==$b){
						$result[$a]=$v;
					}
				}
			}
			return $result;
		}
		
	}
}
