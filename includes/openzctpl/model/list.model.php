<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class listModel{
	function Run(&$atts, &$refObj, &$fields){
		global $tpl,$_vars,$category_class,$blog_class,$product_class,$search_class,$predata_class;
		
		$attlist = "pagesize=100,noflag=,flag=,type=,titlelen=,desclen=,imgsizer=,bgcolor=";
		extract($atts, EXTR_OVERWRITE);
		if($_GET['main_page']=="blog" && isset($_GET['cPath'])){
			$orderby=array("a","archives_date_added","desc",TABLE_BLOG_ARCHIVES);
			$cacheData[$_GET['main_page']]=array("pagesize"=>$pagesize);
			tplCacheCount($cacheData);
			$line = empty($pagesize) ? 100 : $pagesize;
			if(isset($_GET['limit'])){
				$pagesize=$_GET['limit'];
			}
			$cid=openzc_get_current_cpath($_GET['cPath']);
			$categories=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
			$cid=openzc_get_son_ids($categories,$cid,$cid);
			$parameter=array('cid'=>$cid,'row'=>$line,'titlelen'=>$titlelen,'desclen'=>$desclen,'flag'=>$flag,'orderby'=>$orderby,"list"=>"true","imgsizer"=>$imgsizer,"bgcolor"=>$bgcolor,"page"=>true);
			$archives=$blog_class->get_archives_list($_GET,$parameter);
			
			return $archives;
		}else{
			$orderby=array("p","products_date_added","desc",TABLE_PRODUCTS);
			if(isset($_GET['orderby'])){
				//*price,name,sell,view*//
				$order=explode(",",strtolower($_GET['orderby']));
				switch($order[0]){
					case "price":
						$orderby=array("p","products_price_sorter",$order[1],TABLE_PRODUCTS);
					break;
					case "name":
						$orderby=array("pd","products_name",$order[1],TABLE_PRODUCTS_DESCRIPTION);
					break;
					case "viewed":
						$orderby=array("pd","products_viewed",$order[1],TABLE_PRODUCTS_DESCRIPTION);
					break;
					case "sell":
						$orderby=array("op","products_orders_count",$order[1],TABLE_ORDERS_PRODUCTS);
					break;
					default:
						$orderby=array("p","products_date_added","desc",TABLE_PRODUCTS);
					break;
				}
			}
			$cacheData[$_GET['main_page']]=array("pagesize"=>$pagesize);
			tplCacheCount($cacheData);
			if(isset($_GET['limit'])){
				$pagesize=$_GET['limit'];
			}
			
			$line = empty($pagesize) ? 100 : $pagesize;
			$cid=openzc_get_current_cpath($_GET['cPath']);
			$categories=$predata_class->getPredata(TABLE_CATEGORIES);
			$cid=openzc_get_son_ids($categories,$cid,$cid);
			switch($_GET['main_page']){
				case "specials": 				$flag="specials"; 	break;
				case "featured_products": 		$flag="featured";	break;
				case "advanced_search_result":	$flag="search";		break;
				case "advanced_search":			$flag="search";		break;
			}
			$parameter=array('cid'=>$cid,'row'=>$line,'titlelen'=>$titlelen,'desclen'=>$desclen,'flag'=>$flag,'orderby'=>$orderby,"list"=>"true","imgsizer"=>$imgsizer,"bgcolor"=>$bgcolor,"page"=>true);
		
		
			$products=$product_class->get_product_list($_GET,$parameter);
			//$products=array();
			
			return $products;
		}
		
	}
}
  
?>