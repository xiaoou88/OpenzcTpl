<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class topbarModel{
	function Run(&$atts, &$refObj, &$fields){
		$attlist = "item=";	
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		
		if($item=="currency"){
			if(isset($_GET)){
				foreach($_GET as $k => $v){
					if($k!="currency" && $k!="language" && $k!="main_page"){
						$parameter.="&".$k."=".$v;
					}
				}
			}
		
			
			$currency=openzcQuery("select * from ".TABLE_CURRENCIES);
			$currency=openzc_table_to_list($currency);
			foreach($currency as $k => $v){
				$currency[$k]['status']="";
				if(isset($_COOKIE) && $_COOKIE['currency']==$v['code']){
					$currency[$k]['status']="active";
				}
				$currency[$k]['link']=zen_href_link($_GET['main_page'],$parameter."&currency=".$v['code']);
			}
			
			return $currency;
		}
		if($item=="language"){
			
			if(isset($_GET)){
					foreach($_GET as $k => $v){
						if($k!="currency" && $k!="language" && $k!="main_page"){
						$parameter.="&".$k."=".$v;
						}
					}
			}
			
			$languages=openzcQuery("select * from ".TABLE_LANGUAGES);
			$languages=openzc_table_to_list($languages);
			
			foreach($languages as $k => $v){
				$languages[$k]['status']="";
				if(isset($_COOKIE) && $_COOKIE['language']==$v['code']){
					$languages[$k]['status']="active";
				}
				$languages[$k]['title']=$v['name'];
				$languages[$k]['image']="/".DIR_WS_LANGUAGES.$v['directory']."/images/icon.gif";
				$languages[$k]['link']=zen_href_link($_GET['main_page'],$parameter."&language=".$v['code']);
			}
		
			return $languages;
		}
	}
	
}
