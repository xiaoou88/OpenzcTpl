<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class pagelistModel{
	function Run(&$atts, &$refObj, &$fields){
		global $_vars,$product_class;
		$attlist = "listitem=,listsize=,total=,pernum=,custom=,tpl=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE); 
		
		if(!$total){
			$total=$product_class->get_pagelist_total();
			
			$pernum=$product_class->get_pagelist_pernum();
		}
		$parameter=array('total'=>$total,'listsize'=>$listsize,'listitem'=>$listitem,'pernum'=>$pernum,"info"=>true);
		
		$pagelist = $product_class->get_products_list_page($_GET,$parameter);
		
		$html=file_get_contents(TPL_MODULES."tpl/pagelist.tpl");
	    $GLOBALS["tplfile"][]=str_replace(TEMPLATE_TPL,"",TPL_MODULES."tpl/pagelist.tpl");
		$listitem=explode(",",$listitem);
		
        if(count($listitem)>0){
			foreach($listitem as $k => $v){
				if($v=="list"){
					$tpls=$this->get_pagelist_tpl($html,$v);
					foreach($pagelist[$v] as $a => $b){
						if(array_key_exists("status",$b)){
							$content=$this->get_pagelist_tpl($html,"active");
						}else{
							$content=$tpls;
						}
						$str=str_replace("[field:page_link/]",$b["link"],$content);
						$str=str_replace("[field:page_name/]",$b["text"],$str);
						$result.=$str;
					}
				}
				else{
					$content=$this->get_pagelist_tpl($html,$v);
					$str=str_replace("[field:page_link/]",$pagelist[$v]["link"],$content);
					$str=str_replace("[field:page_name/]",$pagelist[$v]["text"],$str);
					foreach($pagelist['info'] as $a => $b){
						$str=str_replace("[field:page_".$a."/]",$b,$str);
					}
					$result.=$str;
				}
			}
		}

		$atts='';
		
		return $result;
    }
    private function get_pagelist_tpl($html,$name){
		$start=strpos($html,"{type:".$name."}")+strlen("{type:".$name."}");
		$end=strpos($html,"{/type:".$name."}");
		$length=$end-$start;
		if($length<0){
			return false;
		}
		$content=substr($html,$start,$length);
		return $content;
    }
}
  
  
?>