<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class arclistModel{
	function Run(&$atts, &$refObj, &$fields){
		global $blog_class,$predata_class;
		$attlist = "cid=all,aid=,row=100,flag=,titlelen=,desclen=,orderby=,imgsizer=,bgcolor,noflag=,type";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		
		switch($orderby){
			case "desc":
				$orderby=array("a","archives_date_added","desc",TABLE_BLOG_ARCHIVES);
			break;
			case "asc":
				$orderby=array("a","archives_date_added","asc",TABLE_BLOG_ARCHIVES);
			break;
			case "rand":
				$orderby=array("a","archives_date_added","rand",TABLE_BLOG_ARCHIVES);
			break;
			default:
				$orderby=array("a","archives_date_added","desc",TABLE_BLOG_ARCHIVES);
			break;
		}
		if(!empty($noflag)){$flag='false';}
		$categories=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
		
		$line = empty($row) ? 100 : $row;
		if($cid!="all"){
			if($cid=="current" && array_key_exists('cPath',$_GET)){
				$cid=openzc_get_current_cpath($_GET['cPath']);
			}
			$cid=array_filter(explode(",",$cid));
			
			foreach($cid as $k => $v){
				@$ids.=openzc_get_son_ids($categories,$v,$v).",";
			}
			$cid=join("','",array_filter(explode(",",$ids)));
			
		}
		if(isset($_GET['archives_id']) && $_GET['main_page']==FILENAME_BLOG_ARCHIVES){
			switch($type){
				case "related":
					$cid=openzc_get_current_cpath($_GET['cPath']);
				break;
				case "arcprev":
					$cid=openzc_get_current_cpath($_GET['cPath']);
					$sql="select archives_id from ".TABLE_BLOG_ARCHIVES." where archives_id = (select archives_id from ".TABLE_BLOG_ARCHIVES." where archives_id < ".$_GET['archives_id']." and master_categories_id='".$cid."' order by archives_id desc limit 1)";   
					$aid=openzcQuery($sql);
					$aid=$aid->fields['archives_id'];
					if(!$aid){return false;}
				break;
				case "arcnext":
					$cid=openzc_get_current_cpath($_GET['cPath']);
					$sql="select archives_id from ".TABLE_BLOG_ARCHIVES." where archives_id = (select archives_id from ".TABLE_BLOG_ARCHIVES." where archives_id > ".$_GET['archives_id']." and master_categories_id='".$cid."' order by archives_id asc limit 1)"; 
					$aid=openzcQuery($sql);
					$aid=$aid->fields['archives_id'];
					if(!$aid){return false;}
				break;
			}
		}
		$parameter=array('cid'=>$cid,'aid'=>$aid,'row'=>$line,'flag'=>$flag,'titlelen'=>$titlelen,"desclen"=>$desclen,'orderby'=>$orderby,'imgsizer'=>$imgsizer,'bgcolor'=>$bgcolor,"type"=>$type);  
		$archives=$blog_class->get_archives_list($_GET,$parameter);
		
		$atts="";
	
		return $archives['datalist'];
	}
}
  
?>