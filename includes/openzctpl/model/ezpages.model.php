<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class ezpagesModel{
	function Run(&$atts, &$refObj, &$fields){
	
		global $category_class,$product_class;
       
		$attlist = "id=,row=,orderby=desc,field=,module=pages_id";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		
		$ezpages=array();
		$line = empty($row) ? 100 : $row;
		//EZPAGE页面内容调用
		if($field && $_GET['main_page']=="page"){
			$sql="select * from ".TABLE_EZPAGES." where pages_id=".$_GET['id']." and languages_id=".(int)$_SESSION['languages_id'];
			$data=openzcQuery($sql);
			$data=openzc_table_to_list($data)[0];
			if(array_key_exists($field,$data)){
				echo $data[$field];
			}
			return false;
		}
		if($field && $_GET['main_page']!="page"){
			$sql="select * from ".TABLE_EZPAGES." where pages_id=".$id." and languages_id=".(int)$_SESSION['languages_id'];
			$data=openzcQuery($sql);
			$data=openzc_table_to_list($data)[0];
			if(array_key_exists($field,$data)){
				echo $data[$field];
			}
			return false;
		}
		
		//EZPAGE导航调用
		

		if(EXPECTED_DATABASE_VERSION_MAJOR.EXPECTED_DATABASE_VERSION_MINOR=="15.7"){
			if($module!="pages_id"){
				$status="e.status_".$module;
				$module.="_sort_order";
				$status=" and ".$status."=1";
			}
			$sql="select * from ".TABLE_EZPAGES." e left join ".TABLE_EZPAGES_CONTENT." ec on ec.pages_id=e.pages_id where ec.languages_id='".(int)$_SESSION['languages_id']."'".$status." order by e.".$module." ".$orderby." limit ".$line;
		}else{
			if($module!="pages_id"){
				$status="status_".$module;
				$module.="_sort_order";
				$status=" and ".$status."=1";
			}
			$sql="select * from ".TABLE_EZPAGES." where languages_id='".(int)$_SESSION['languages_id']."'".$status." order by ".$module." ".$orderby." limit ".$line;
		}
		
	
		$data=openzcQuery($sql);
		$data=openzc_table_to_list($data,$module);
		
		if($id){
			$id=explode(",",$id);
			foreach($id as $k => $v){
				$ezpages[]=$data[$v];
			}
		}else{
			$ezpages=$data;
		}
		
		$ezpages=array_filter($ezpages);
		if($_GET['main_page']=="page"){
			$pageid=$_GET['id'];
		}

		foreach($ezpages as $k => $v){
			if(is_numeric($v['pages_id'])){
				if($v['alt_url']){
					$ezpages[$k]["pages_link"]="/".$v['alt_url'];
				}else if($v['alt_url_external']){
					$ezpages[$k]["pages_link"]=$v['alt_url_external'];
				}else{
					$ezpages[$k]["pages_link"]=zen_href_link(FILENAME_EZPAGES,'id='.$v['pages_id']);
				}
				if($v['pages_id']==$pageid){
					$ezpages[$k]['status']="active";
				}else{
					$ezpages[$k]['status']="";
				}
			}else{
				$ezpages[$k]["pages_link"]=zen_href_link($v['file_name']);
			}
		}
	
		return $ezpages;
	}
	
}
?>