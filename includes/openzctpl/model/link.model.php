<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class linkModel{
	
	function Run(&$atts, &$refObj, &$fields){
		
		$attlist = "name=,parameter=";
		FillAtts($atts,$attlist);
		FillFields($atts,$fields,$refObj);
		extract($atts, EXTR_OVERWRITE);
		$array=array_filter(explode(",",$name));
		if(count($array)>1){
			foreach($array as $k => $v){
				$link[$k]['link']=$this->getLink($v,$parameter);
				$link[$k]['title']=constant($v."_TITLE");
			}
			return $link;
		}else{
			echo $this->getLink($name,$parameter);
		}
	}
	private function getLink($name,$parameter){
		if($name=="FILENAME_BACK"){
			$link=zen_back_link(true);
		}else{
			if(strstr($parameter,"+")){
				$parameter=explode("+",$parameter);
				$str="";
				foreach($parameter as $k => $v){
					if(strstr($v,"$")){
						$n=eval('return '. $v . ';');
						$str.=$n;
					}else{$str.=$v;}
				}
				$parameter=$str;
			}
			$link=zen_href_link(constant($name),$parameter,'SSL');
		}
		return $link;
	}
	
}
?>