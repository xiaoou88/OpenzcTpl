<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
$main_page=$_GET["main_page"];
isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'false';
$referer=$_SERVER['HTTP_REFERER'];
$variable_class->init_static();
$categories=$predata_class->getPredata(TABLE_CATEGORIES);
if(isset($_GET['cPath'])){
	$current=openzc_get_current_cpath($_GET['cPath']);
	$current_top=openzc_get_top_id($categories,$current);
}

switch($main_page){
	case FILENAME_BLOG_ARCHIVES:
		$sql="select count(r.reviews_id) as count from ".TABLE_BLOG_REVIEWS." r,".TABLE_BLOG_REVIEWS_DESCRIPTION." rd where r.reviews_id=rd.reviews_id and r.archives_id=".$_GET['archives_id']." and rd.language_id=".(int)$_SESSION['languages_id']." and r.reviews_status=1 ";
		$data=openzcQuery($sql);
		define("BLOG_ARCHOVES_REVIEWS_COUNT",$data->fields['count']);
	break;
	case FILENAME_BLOG_CATEGORIES:
		$categories=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
		$current_top=openzc_get_top_id($categories,$current);
	break;
	case FILENAME_PRODUCT_INFO:
		$products_info=$product_class->get_products_info($_GET['products_id']);
		$data[]=$products_info;
		$products_info['products_rating']=$product_class->get_products_reviews_rating($data);
	break;
	case FILENAME_CREATE_ACCOUNT:
		$text_origin_login=str_replace("%s","/index.php?main_page=login",TEXT_ORIGIN_LOGIN);
	break;
	case FILENAME_SHOPPING_CART:
		if(isset($_COOKIE['checkout_login']) && strstr($_SERVER['HTTP_REFERER'],FILENAME_LOGIN)){
			zen_redirect(zen_href_link(FILENAME_CHECKOUT_SHIPPING,'','SSL'));
		}
		require_once(DIR_WS_TEMPLATE.'templates/tpl_popup_shipping_estimator_default.php');
	break;
	case FILENAME_SHIPPING_ESTIMATOR:
		
	break;
	case FILENAME_LOGIN:
		$path=$_SESSION['navigation']->path;
		if($_SESSION['navigation']->path[(key($path)-1)]['page']==FILENAME_CHECKOUT_SHIPPING || strstr($_SERVER['HTTP_REFERER'],FILENAME_SHOPPING_CART)){
			setcookie("checkout_login","true");
			if($_SESSION['cart']->count_contents()==0){
				zen_redirect(zen_href_link(FILENAME_SHOPPING_CART,'','SSL'));
			}else{
				$checkout_login_file=DIR_WS_TEMPLATE."templates/tpl_checkout_login_default.php";
				if(file_exists($checkout_login_file)){
					require($checkout_login_file);
				}
				if(array_key_exists("tpl_checkout_login_default.php",$tplpage_route)){
					$tpl_page_body="tpl_checkout_login_default.php";
				}
			}
		}
	break;
	case FILENAME_CHECKOUT_SHIPPING:
	break;
	case FILENAME_CHECKOUT_SHIPPING_ADDRESS:
	break;
	case FILENAME_ADDRESS_BOOK_PROCESS:
		if(isset($_GET['edit'])){
			$address_book['primary']=false;
			$address_book = openzcQuery("SELECT * FROM " . TABLE_ADDRESS_BOOK . " WHERE customers_id = ".$_SESSION['customer_id']." AND address_book_id = ".$_GET['edit']);
			$address_book = openzc_table_to_list($address_book)[0];
			if($address_book['address_book_id']==$_SESSION['customer_default_address_id']){
				$address_book['primary']=true;
			}
		}
	break;
	case FILENAME_TIME_OUT:
		header("location:/index.php");
	break;
	
}

$checkout_status['logged']=false;
$checkout_status['shipping_address']=false;
$checkout_status['shipping_method']=false;
$checkout_status['checkout_payment']=false;
$checkout_step=1;
if(isset($_SESSION['customer_id'])){
	$checkout_status['logged']=true;
	if(!isset($_SESSION['sendto'])){
		$sendto=$_SESSION['customer_default_address_id'];
	}else{
		$sendto=$_SESSION['sendto'];
	}
	$sql="select * from ".TABLE_ADDRESS_BOOK." where address_book_id = ".$sendto;
	$data=openzcQuery($sql);
	$data=openzc_table_to_list($data)[0];
	$selected_country=$data['entry_country_id'];
	$selected_state=$data['entry_zone_id'];
	
	if(!$data['entry_country_id'] || !$data['entry_state'] || !$data['zone_id']|| !$data['entry_city'] || !$data['entry_street_address']){
		$checkout_step=2;
	}
	if(isPost() && $_POST['action']=="shipping_address" && isset($_POST['action'])){
		if(count($_POST)<=2){
			$checkout_status['shipping_address']=true;
		}else{
			$checkout_status['shipping_address']=$variable_class->shippingAddress($_POST);
		}
	}

	if($main_page==FILENAME_CHECKOUT_SHIPPING && $checkout_status['shipping_address']==true){
		$checkout_step=3;
		$checkout_status['shipping_method']=true;
	}
	
	if($main_page==FILENAME_CHECKOUT_PAYMENT){
		$order_total=$order_total_modules->process();
		$checkout_step=4;
		$checkout_status['checkout_payment']=true;
	}
	
	if($main_page==FILENAME_CHECKOUT_CONFIRMATION){
		$order_total=$order_total_modules->process();
		$variable_class->getCheckoutConfirmation();
	}
	
}
$variable_class->getFilterStatus();
$countries = $variable_class->get_country_list($selected_country);
$states = $variable_class->get_state_list($selected_country,$selected_state);
if($_SESSION['customer_id']){
	$sql="select count(orders_id) as count from ".TABLE_ORDERS." where customers_id = ".$_SESSION['customer_id'];
	$data=openzcQuery($sql);
	define("ORDERS_COUNT",openzc_table_to_list($data)[0]['count']);
}

//print_r($_SESSION);EXIT;

?>