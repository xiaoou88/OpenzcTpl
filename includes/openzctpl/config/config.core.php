<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
$autoLoadOpenzc[]=array("autoType"=>"config","loadFile"=>CONFIG_DIR."cache_file.inc.php");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."predata.class.php","className"=>"predata","Name"=>"predata_class");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."category.class.php","className"=>"category","Name"=>"category_class");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."product.class.php","className"=>"product","Name"=>"product_class");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."attributes.class.php","className"=>"attributes","Name"=>"attributes_class");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."blog.class.php","className"=>"blog","Name"=>"blog_class");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."ctag.class.php","className"=>"cTag","Name"=>"cTag");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."filter.class.php","className"=>"filter","Name"=>"filter_class");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."imagesizer.class.php","className"=>"ImageSizer","Name"=>"ImageSizer");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."ajax.class.php","className"=>"openzcajax","Name"=>"ajax_class");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."variable.class.php","className"=>"variable","Name"=>"variable_class");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."tag.class.php");
$autoLoadOpenzc[]=array("autoType"=>"class","loadFile"=>CLASS_DIR."tpl.class.php","className"=>"OpenzcTemplate","Name"=>"OpenzcTpl");

foreach(scandir(MODEL_DIR) as $k => $v){
	if($v!="." && $v!=".."){
		$name=explode(".",$v)[0];
		$autoLoadOpenzc[]=array("autoType"=>"model","loadFile"=>MODEL_DIR.$v,"className"=>$name."Model","Name"=>$name."Model");
	}
}

$sysAjaxAction=array('addCart','addWishlist','addCompare','delWishlist','delCompare','writeReviews');
?>