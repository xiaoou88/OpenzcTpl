<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
	date_default_timezone_set('Asia/Shanghai');
	require_once(INCLUDES_PATH."configure.php");
	require_once(INCLUDES_PATH."database_tables.php");
	
	$db = new mysqli(DB_SERVER,DB_SERVER_USERNAME,DB_SERVER_PASSWORD,DB_DATABASE);
	if($db->connect_error) {die("数据库连接失败: " . $db->connect_error);}
	
	
	require(CLASS_DIR."init.class.php");
	$init = new opencinit();
	openzc_init($_GET);
	if(!defined("OPENZC_TEMPLATE_DIR")) openzc_get_template();
	
	if(is_file(INCLUDES_PATH."templates/".OPENZC_TEMPLATE_DIR."/template_info.php")){
		require(INCLUDES_PATH."templates/".OPENZC_TEMPLATE_DIR."/template_info.php");
		if(!isset($openzc_template_engine) || $openzc_template_engine!=true){
			require(CONFIG_DIR."zcindex.php");
			exit();
		}
	}else{
		echo "未找到模板信息文件“template_info.php”！";
		exit();
	}
	
?>