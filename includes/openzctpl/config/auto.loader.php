<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */

if(defined("DIR_WS_TEMPLATE")){
	if(is_file(DIR_WS_TEMPLATE."page_route.php")){
		$autoLoadOpenzc[]=array("autoType"=>"require","loadFile"=>DIR_WS_TEMPLATE."page_route.php");
	}else{
		echo "OpenzcTpl模板引擎映射路由文件：“".DIR_WS_TEMPLATE."page_route.php"."”不存在，请补全！";exit;
	}
	if(is_file(DIR_WS_TEMPLATE."helper.php")){
		$autoLoadOpenzc[]=array("autoType"=>"require","loadFile"=>DIR_WS_TEMPLATE."helper.php");
	}else{
		fopen(DIR_WS_TEMPLATE."helper.php", "w");
	}
	if(!is_dir(DIR_WS_TEMPLATE."languages/".$_SESSION['language']."/")){
		mkdir(DIR_WS_TEMPLATE."languages/".$_SESSION['language']."/",0777,true); 
	}
	if(is_file(DIR_WS_TEMPLATE."languages/".$_SESSION['language'].".php")){
		$autoLoadOpenzc[]=array("autoType"=>"require","loadFile"=>DIR_WS_TEMPLATE."languages/".$_SESSION['language'].".php");
	}
	if(is_file(DIR_WS_TEMPLATE."languages/".$_SESSION['language']."/".$current_page_base.".php")){
		$autoLoadOpenzc[]=array("autoType"=>"require","loadFile"=>DIR_WS_TEMPLATE."languages/".$_SESSION['language']."/".$current_page_base.".php");
	}
}

foreach($autoLoadOpenzc as $k => $v){
	if($v["autoType"]!="function"){	require($v['loadFile']);}
}

foreach($autoLoadOpenzc as $k => $v){

	switch($v['autoType']){
		case 'class':
		    if(isset($v['className']))
		    ${$v['Name']} = new $v['className']();
		break;
		case 'model':
		    if(class_exists($v['className'], false)) {
				${$v['Name']} = new $v['className']();
            }else{
				echo "sorry,Class name ".$v['className']." does not exist!";
			}
		break;
		
	}
}
foreach($autoLoadOpenzc as $k => $v){if($v["autoType"]=="function"){require_once($v['loadFile']);}}
$OpenzcTpl->OpenzcTemplate(DIR_WS_TEMPLATE);
?>