<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
    if(count($_GET)==0){
		$_GET['main_page']="index";
	}
	
	require(CONFIG_DIR."cache_file.inc.php");
	
	$getHtml  = $init->getHtml($_GET);
	
	if($getHtml){
		echo $getHtml;
		exit();
	}
?> 