<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
  function strRequest(&$str)
  {
    
    if( is_array($str) ){
    	foreach($str as $key => $val) $str[$key] = strRequest($val);
    }else{
    	$str = addslashes($str);
    }
    
    return $str;
  }
  foreach(Array('_GET','_POST','_COOKIE') as $_request)
  {
    foreach(${$_request} as $_k => $_v) ${$_k} = strRequest($_v);
  }
  
  function isAjax(){
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		return true;
	}else{
		return false;
	}
  }

  function isGet(){
    return $_SERVER['REQUEST_METHOD'] == 'GET' ? true : false;
  }

  function isPost() {
	return $_SERVER['REQUEST_METHOD'] == 'POST' ? true : false;
	//return ($_SERVER['REQUEST_METHOD'] == 'POST' && checkurlHash($GLOBALS['verify']) && (empty($_SERVER['HTTP_REFERER']) || preg_replace("~https?:\/\/([^\:\/]+).*~i", "\\1", $_SERVER['HTTP_REFERER']) == preg_replace("~([^\:]+).*~", "\\1", $_SERVER['HTTP_HOST']))) ? 1 : 0;
  }
?>