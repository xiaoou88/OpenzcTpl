<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
	DEFINE('ROOT_PATH',str_replace('\\','/',dirname(dirname(dirname(__FILE__))).'/'));
	DEFINE('OPENZCTPL',preg_replace("#[\\\\\/]install#", '', dirname(__FILE__)));
	DEFINE('INSTALL_DIR',str_replace('\\',"/",dirname(__FILE__)."/"));
	DEFINE('TEMPLATES_STYLE',"/includes/openzctpl/install/style/");
	DEFINE('INIT_INCLUDES',ROOT_PATH."init_includes/");
	DEFINE('INSTALL_TEMPLATES',INSTALL_DIR."templates/");
	
	require_once(INSTALL_DIR.'install.inc.php');
	foreach(Array('_GET','_POST','_COOKIE') as $_request){
		foreach($$_request as $_k => $_v) ${$_k} = RunMagicQuotes($_v);
	}
	if(file_exists(INSTALL_DIR."/install.lock")){
		include(INSTALL_TEMPLATES.'installed.html');
		exit();
	}
	if(empty($step)){
		include(INSTALL_TEMPLATES.'step-1.html');
		exit();
	}
	if($step==1){
		include(INSTALL_TEMPLATES.'step-1.html');
		exit();
	}else if($step==2){
		$phpv = phpversion();
		$sp_os = PHP_OS;
		$sp_gd = gdversion();
		$sp_server = $_SERVER['SERVER_SOFTWARE'];
		$sp_host = (empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_HOST'] : $_SERVER['REMOTE_ADDR']);
		$sp_name = $_SERVER['SERVER_NAME'];
		$sp_max_execution_time = ini_get('max_execution_time');
		$sp_allow_reference = (ini_get('allow_call_time_pass_reference') ? '<font >[√]On</font>' : '<font color=red>[×]Off</font>');
		$sp_allow_url_fopen = (ini_get('allow_url_fopen') ? '<font >[√]On</font>' : '<font color=red>[×]Off</font>');
		$sp_safe_mode = (ini_get('safe_mode') ? '<font color=red>[×]On</font>' : '<font >[√]Off</font>');
		$sp_gd = ($sp_gd>0 ? '<font >[√]On</font>' : '<font color=red>[×]Off</font>');
		$sp_mysql = (function_exists('mysql_connect') ? '<font >[√]On</font>' : '<font color=red>[×]Off</font>');
		if($phpv>=7){
			$sp_mysql = (function_exists('mysqli_connect') ? '<font >[√]On</font>' : '<font color=red>[×]Off</font>');
		}
		if($sp_mysql=='<font color=red>[×]Off</font>')
		$sp_mysql_err = TRUE;
		else
		$sp_mysql_err = FALSE;
		include(INSTALL_TEMPLATES.'step-2.html');
		exit();
	}else if($step==3){
		if(empty($action)){$action='';}
		require(ROOT_PATH.'configure.php');
		require(ROOT_PATH.'database_tables.php');
		require(ROOT_PATH.'version.php');
		define("TABLE_OPENZC_TRIGGER",DB_PREFIX."openzc_trigger");
		if($action=='install'){
			
			include(INSTALL_TEMPLATES.'step-install.html');
			$db = new mysqli(DB_SERVER,DB_SERVER_USERNAME,DB_SERVER_PASSWORD,DB_DATABASE);
			if($db->connect_error){die("数据库连接失败: " . $db->connect_error);}
			require(INSTALL_DIR.'upgrade.class.php');
			$upgrade=new upgrade();
			$upgrade->Run();
			exit();
		}
	
		include(INSTALL_TEMPLATES.'step-3.html');
		exit();
	}else if($step=4){
		fopen(INSTALL_DIR."install.lock", "w");
		include(INSTALL_TEMPLATES.'step-4.html');
		exit();
	}
?>