<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: product.php 15881 2010-04-11 16:32:39Z wilt $
 */

  require('includes/application_top.php');

  require(DIR_WS_MODULES . '/blog/prod_cat_header_code.php');
  require(DIR_WS_MODULES . '/blog/general.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (zen_not_null($action)) {
    switch ($action) {
		case "setflag_reviews":
			$sql="update ".TABLE_BLOG_REVIEWS." set reviews_status='$_GET[flag]' where reviews_id='$_GET[rID]'";
			$db->Execute($sql);
			zen_redirect(zen_href_link(FILENAME_BLOG_REVIEWS));
		break;
		case "delete_reviews":
			require(DIR_WS_MODULES . '/blog/sidebox_delete_reviews.php');
		break;
		case "delete_reviews_confirm":
			$sql="delete from ".TABLE_BLOG_REVIEWS." where reviews_id='".$_POST['reviews_id']."'";
			$db->Execute($sql);
			$sql="delete from ".TABLE_BLOG_REVIEWS_DESCRIPTION." where reviews_id='".$_POST['reviews_id']."'";
			$db->Execute($sql);
			zen_redirect(zen_href_link(FILENAME_BLOG_REVIEWS));
		break;
    }
	
  }


?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
<script language="javascript" src="includes/menu.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript">
  <!--
  function init()
  {
    cssjsmenu('navbar');
    if (document.getElementById)
    {
      var kill = document.getElementById('hoverJS');
      kill.disabled = true;
    }
if (typeof _editor_url == "string") HTMLArea.replaceAll();
 }
 // -->
</script>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="init()">
<div id="spiffycalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
<?php


  require(DIR_WS_MODULES . 'blog_reviews_listing.php');

if ( (zen_not_null($heading)) && (zen_not_null($string)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $string);

      echo '            </td>' . "\n";
    }

   
?>

          </tr>
		  
          <tr>
<?php
// Split Page
if ($products_query_numrows > 0) {
  if (empty($pInfo->archives_id)) {
    $pInfo->archives_id= $pID;
  }
?>
            <td class="smallText" align="right"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_RESULTS_CATEGORIES, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS) . '<br>' . $products_split->display_links($products_query_numrows, MAX_DISPLAY_RESULTS_CATEGORIES, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], zen_get_all_get_params(array('page', 'info', 'x', 'y')) ); ?></td>

<?php
}
// Split Page
?>
          </tr>
        </table></td>
      </tr>
    </table>

    </td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
