<?php
/**
 * @package admin
 * @copyright Copyright 2003-2009 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: categories.php 14139 2009-08-10 13:46:02Z wilt $
 */

define('HEADING_TITLE', 'Blog Seo Setting');
define('HEADING_TITLE_GOTO', 'Go To:');

define('TABLE_HEADING_ID', 'Language ID');
define('TABLE_HEADING_ARCHIVES_REVEIWS', 'Blog SEO');

define('TABLE_HEADING_SEO_TITLE', 'Blog Seo Title');
define('TABLE_HEADING_SEO_KEYWORDS', 'Blog Seo Keywords');
define('TABLE_HEADING_SEO_DESCRIPTION', 'Blog Seo Description');