<?php
/**
 * @package admin
 * @copyright Copyright 2003-2007 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: catalog_dhtml.php 6050 2007-03-24 03:20:50Z ajeh $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}
  $za_contents = array();
  $za_heading = array('text' => BOX_HEADING_BLOG, 'link' => zen_href_link(FILENAME_ALT_NAV, '', 'NONSSL'));
  
  $za_contents[] = array('text' => BOX_BLOG_TYPE_ARCHIVES, 'link' => zen_href_link(FILENAME_BLOG, '', 'NONSSL'));
  $za_contents[] = array('text' => BOX_BLOG_REVIEWS, 'link' => zen_href_link(FILENAME_BLOG_REVIEWS, '', 'NONSSL'));
  $za_contents[] = array('text' => BOX_BLOG_SEO, 'link' => zen_href_link(FILENAME_BLOG_SEO, '', 'NONSSL'));


if ($za_dir = @dir(DIR_WS_BOXES . 'extra_boxes')) {
  while ($zv_file = $za_dir->read()) {
    if (preg_match('/blog_dhtml.php$/', $zv_file)) {
      require(DIR_WS_BOXES . 'extra_boxes/' . $zv_file);
    }
  }
  $za_dir->close();
}

?>
<!-- catalog //-->
<?php
echo zen_draw_admin_box($za_heading, $za_contents);
?>
<!-- catalog_eof //-->
