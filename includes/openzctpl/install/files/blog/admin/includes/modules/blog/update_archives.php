<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: update_product.php 15636 2010-03-07 07:00:40Z drbyte $
 */
  
  if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
  }
 
  if (isset($_GET['pID'])) $archives_id = zen_db_prepare_input($_GET['pID']);
  if (isset($_POST['edit_x']) || isset($_POST['edit_y'])) {
    $action = 'new_archives';
	
  } elseif($_POST['archives_name'] . $_POST['archives_description'] != '') {
  
    // Data-cleaning to prevent MySQL5 data-type mismatch errors:
    
    $sql_data_array = array(
        'archives_type' => zen_db_prepare_input($_POST['archives_type']),
        'archives_status' => zen_db_prepare_input((int)$_POST['archives_status']),
        'archives_sort_order' => (int)zen_db_prepare_input($_POST['archives_sort_order']),
    );

    // when set to none remove from database
    // is out dated for browsers use radio only
      $sql_data_array['archives_image'] = zen_db_prepare_input($_POST['archives_image']);
      $new_image= 'true';

    if ($_POST['image_delete'] == 1) {
      $sql_data_array['archives_image'] = '';
      $new_image= 'false';
    }

if ($_POST['image_delete'] == 1) {
      $sql_data_array['archives_image'] = '';
      $new_image= 'false';
}

    if ($action == 'insert_archives') {
      $insert_sql_data = array( 'archives_date_added' => 'now()',
                                'master_categories_id' => (int)$current_category_id,
								"related_products_id"=>zen_db_prepare_input($_POST['related_products_id']),
								"related_categories_id"=>zen_db_prepare_input($_POST['related_categories_id'])
							);

      $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

      zen_db_perform(TABLE_BLOG_ARCHIVES, $sql_data_array);
      $archives_id = zen_db_insert_id();

      $db->Execute("insert into " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " (archives_id,categories_id) values ('" . (int)$archives_id . "', '" . (int)$current_category_id . "')");
	  
      ////    *END OF PRODUCT-TYPE-SPECIFIC INSERTS* ////////
      ///////////////////////////////////////////////////////
    } elseif ($action == 'update_archives') {
      $update_sql_data = array( 'archives_last_modified' => 'now()',
                                'master_categories_id' => ($_POST['master_categories_id'] > 0 ? zen_db_prepare_input($_POST['master_categories_id']) : zen_db_prepare_input($_POST['master_categories_id'])));

      $sql_data_array = array_merge($sql_data_array, $update_sql_data);

      zen_db_perform(TABLE_BLOG_ARCHIVES, $sql_data_array, 'update', "archives_id = '" . (int)$archives_id . "'");

      ///////////////////////////////////////////////////////
      //// INSERT PRODUCT-TYPE-SPECIFIC *UPDATES* HERE //////


      ////    *END OF PRODUCT-TYPE-SPECIFIC UPDATES* ////////
      ///////////////////////////////////////////////////////
    }

    $languages = zen_get_languages();
	
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
      $language_id = $languages[$i]['id'];

      $sql_data_array = array('archives_name' => zen_db_prepare_input($_POST['archives_name'][$language_id]),
                              'archives_description' => zen_db_prepare_input($_POST['archives_description'][$language_id]),
							  'archives_shorttitle' => zen_db_prepare_input($_POST['archives_shorttitle'][$language_id]),
							  'archives_keywords' => zen_db_prepare_input($_POST['archives_keywords'][$language_id]),
							  'archives_body' => zen_db_prepare_input($_POST['archives_body'][$language_id]),
							  'archives_tag' => zen_db_prepare_input($_POST['archives_tag'][$language_id])
							  );
							 
      if ($action == 'insert_archives') {
        $insert_sql_data = array('archives_id' => $archives_id,'language_id' => $language_id);

        $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
 
        zen_db_perform(TABLE_BLOG_ARCHIVES_DESCRIPTION, $sql_data_array);
      } elseif ($action == 'update_archives') {
		 
        zen_db_perform(TABLE_BLOG_ARCHIVES_DESCRIPTION, $sql_data_array, 'update', "archives_id = '" . (int)$archives_id . "' and language_id = '" . (int)$language_id . "'");
      }
    }

    

    

    zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&pID=' . $archives_id . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '') . (isset($_POST['search']) ? '&search=' . $_POST['search'] : '') ));
  } else {
    $messageStack->add_session(ERROR_NO_DATA_TO_SAVE, 'error');
    zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&pID=' . $archives_id . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '') . (isset($_POST['search']) ? '&search=' . $_POST['search'] : '') ));
  }