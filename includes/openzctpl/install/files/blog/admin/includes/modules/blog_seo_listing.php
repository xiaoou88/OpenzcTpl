<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: category_product_listing.php 15359 2010-01-28 19:52:09Z drbyte $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}


?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2" >
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo zen_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" width="100"><?php echo TABLE_HEADING_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_TITLE; ?></td>
				<td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_KEYWORDS; ?></td>
				<td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_DESCRIPTION; ?></td>
				<td class="dataTableHeadingContent"></td>
              </tr>
<?php
   
    $categories_count = 0;
    $rows = 0;
	$languages = zen_get_languages();
	
    $blog_seo = $db->Execute("select * from " . TABLE_BLOG_SEO);
    while (!$blog_seo->EOF) {
      $seo_count++;
      $rows++;
	  
	  foreach($blog_seo->fields as $k => $v){
		  $blogseo[$blog_seo->fields['language_id']][$k]=$v;
	  }
	 
	  $blog_seo->MoveNext();
	}
	
	foreach($languages as $k => $v){
	echo zen_draw_form('blog_seo', FILENAME_BLOG_SEO, 'action=update_blog_seo', 'post');
	zen_hide_session_id();
?>
<tr>
	<td class="dataTableContent" width="100">
		<input name="language_id" type="hidden" value="<?php echo $v['id']; ?>"/>
		<?php echo $v['id']; ?>
	</td>
    <td class="dataTableContent">
		<input name="seo_title" style="width:95%" type="text" value="<?php echo $blogseo[$v['id']]['seo_title']; ?>"/>
	</td>
    <td class="dataTableContent">
		<input name="seo_keywords" style="width:95%" type="text" value="<?php echo $blogseo[$v['id']]['seo_keywords']; ?>"/>
	</td>
    <td class="dataTableContent">
		<input name="seo_description" style="width:95%" type="text" value="<?php echo $blogseo[$v['id']]['seo_description']; ?>"/>
	</td>
	<td><?php echo zen_image_submit('button_save.gif', IMAGE_SAVE);?></td>
</tr>
<?php
	echo '</form>';
	}
    
// Split Page
// reset page when page is unknown
    $cPath_back = '';
    if (sizeof($cPath_array) > 0) {
      for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
        if (empty($cPath_back)) {
          $cPath_back .= $cPath_array[$i];
        } else {
          $cPath_back .= '_' . $cPath_array[$i];
        }
      }
    }

    $cPath_back = (zen_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
?>
            </table></td>
