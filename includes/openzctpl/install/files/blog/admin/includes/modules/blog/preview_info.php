<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: preview_info.php 15884 2010-04-11 16:45:00Z wilt $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}
    if (zen_not_null($_POST)) {
      $pInfo = new objectInfo($_POST);
      $archives_name = $_POST['archives_name'];
      $archives_description = $_POST['archives_description'];
	  $archives_shorttitle = $_POST['archives_shorttitle'];
	  $archives_body = $_POST['archives_body'];
	  $archives_keywords = $_POST['archives_keywords'];
	  $archives_tag = $_POST['archives_tag'];
    } else {
      $archives = $db->Execute("select p.archives_id, pd.language_id, pd.archives_name,
                                      pd.archives_description,p.archives_image, p.archives_date_added, p.archives_last_modified,
                                      p.archives_status,p.archives_sort_order
                               from " . TABLE_BLOG_ARCHIVES . " p, " . TABLE_BLOG_ARCHIVES_DESCRIPTION . " pd
                               where p.archives_id = pd.archives_id
                               and p.archives_id = '" . (int)$_GET['pID'] . "'");

      $pInfo = new objectInfo($archives->fields);
      $archives_image_name = $pInfo->archives_image;
    }

    $form_action = (isset($_GET['pID'])) ? 'update_archives' : 'insert_archives';

    $languages = zen_get_languages();
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
      if (isset($_GET['read']) && ($_GET['read'] == 'only')) {
        $pInfo->archives_name = zen_get_archives_name($pInfo->archives_id, $languages[$i]['id']);
        $pInfo->archives_description = zen_get_archives_description($pInfo->archives_id, $languages[$i]['id']);
        $pInfo->archives_shorttitle = zen_get_archives_field($pInfo->archives_id, $languages[$i]['id'],"archives_shorttitle");
		$pInfo->archives_body= zen_get_archives_field($pInfo->archives_id, $languages[$i]['id'],"archives_body");
		$pInfo->archives_keywords = zen_get_archives_field($pInfo->archives_id, $languages[$i]['id'],"archives_keywords");
		$pInfo->archives_tag = zen_get_archives_field($pInfo->archives_id, $languages[$i]['id'],"archives_tag");

      } else {
        $pInfo->archives_name = zen_db_prepare_input($archives_name[$languages[$i]['id']]);
        $pInfo->archives_description = zen_db_prepare_input($archives_description[$languages[$i]['id']]);
        $pInfo->archives_shorttitle = zen_db_prepare_input($archives_shorttitle[$languages[$i]['id']]);
		$pInfo->archives_body = zen_db_prepare_input($archives_body[$languages[$i]['id']]);
		$pInfo->archives_keywords = zen_db_prepare_input($archives_keywords[$languages[$i]['id']]);
		$pInfo->archives_tag = zen_db_prepare_input($archives_tag[$languages[$i]['id']]);
		
      }

      
?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . $pInfo->archives_name; ?></td>
           
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td class="main">
          <?php
//auto replace with defined missing image
            if ($_POST['archives_image_manual'] != '') {
              $archives_image_name = $_POST['img_dir'] . $_POST['archives_image_manual'];
              $pInfo->archives_name = $archives_image_name;
            }
            if ($_POST['image_delete'] == 1 || $archives_image_name == '' and PRODUCTS_IMAGE_NO_IMAGE_STATUS == '1') {
              echo zen_image(DIR_WS_CATALOG_IMAGES . PRODUCTS_IMAGE_NO_IMAGE, $pInfo->archives_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'align="right" hspace="5" vspace="5"') . $pInfo->archives_description;
            } else {
              echo zen_image(DIR_WS_CATALOG_IMAGES . $archives_image_name, $pInfo->archives_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'align="right" hspace="5" vspace="5"') . $pInfo->archives_description;
            }
          ?>
        </td>
      </tr>

      <tr>
        <td><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>

      <tr>
        <td align="center" class="smallText"><?php echo sprintf(TEXT_ARCHIVES_DATE_ADDED, zen_date_long($pInfo->archives_date_added)); ?></td>
      </tr>

      <tr>
        <td><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
<?php
    }

    if (isset($_GET['read']) && ($_GET['read'] == 'only')) {
      if (isset($_GET['origin'])) {
        $pos_params = strpos($_GET['origin'], '?', 0);
        if ($pos_params != false) {
          $back_url = substr($_GET['origin'], 0, $pos_params);
          $back_url_params = substr($_GET['origin'], $pos_params + 1);
        } else {
          $back_url = $_GET['origin'];
          $back_url_params = '';
        }
      } else {
        $back_url = FILENAME_BLOG;
        $back_url_params = 'cPath=' . $cPath . '&pID=' . $pInfo->archives_id;
      }
?>
      <tr>
        <td align="right"><?php echo '<a href="' . zen_href_link($back_url, $back_url_params . (isset($_POST['search']) ? '&search=' . $_POST['search'] : ''), 'NONSSL') . '">' . zen_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
      </tr>
<?php
    } else {
      echo zen_draw_form($form_action, FILENAME_BLOG_ARCHIVES, 'cPath=' . $cPath . (isset($_GET['archives_type']) ? '&archives_type=' . $_GET['archives_type'] : '') . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . '&action=' . $form_action . (isset($_GET['page']) ? '&page=' . $_GET['page'] : ''), 'post', 'enctype="multipart/form-data"');
?>
      <tr>
        <td align="right" class="smallText">
<?php
/* Re-Post all POST'ed variables */
      reset($_POST);
      while (list($key, $value) = each($_POST)) {
        if (!is_array($_POST[$key])) {
          echo zen_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
        }
      }

      $languages = zen_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        echo zen_draw_hidden_field('archives_name[' . $languages[$i]['id'] . ']', $archives_name[$languages[$i]['id']]);
        echo zen_draw_hidden_field('archives_description[' . $languages[$i]['id'] . ']', htmlspecialchars(stripslashes($archives_description[$languages[$i]['id']])));
		echo zen_draw_hidden_field('archives_shorttitle[' . $languages[$i]['id'] . ']', $archives_shorttitle[$languages[$i]['id']]);
		echo zen_draw_hidden_field('archives_keywords[' . $languages[$i]['id'] . ']', $archives_keywords[$languages[$i]['id']]);
		echo zen_draw_hidden_field('archives_body[' . $languages[$i]['id'] . ']', htmlspecialchars(stripslashes($archives_body[$languages[$i]['id']])));
		echo zen_draw_hidden_field('archives_tag[' . $languages[$i]['id'] . ']', htmlspecialchars(stripslashes($archives_tag[$languages[$i]['id']])));
      }
      echo zen_draw_hidden_field('archives_image', stripslashes($archives_image_name));
      echo ( (isset($_GET['search']) && !empty($_GET['search'])) ? zen_draw_hidden_field('search', $_GET['search']) : '') . ( (isset($_POST['search']) && !empty($_POST['search']) && empty($_GET['search'])) ? zen_draw_hidden_field('search', $_POST['search']) : '');
      echo zen_image_submit('button_back.gif', IMAGE_BACK, 'name="edit"') . '&nbsp;&nbsp;';

      if (isset($_GET['pID'])) {
        echo zen_image_submit('button_update.gif', IMAGE_UPDATE);
      } else {
        echo zen_image_submit('button_insert.gif', IMAGE_INSERT);
      }
     echo '&nbsp;&nbsp;<a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '') . (isset($_GET['search']) ? '&search=' . $_GET['search'] : '')) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>';
?>
        </td>
      </tr>
    </table></form>
<?php
    }
?>