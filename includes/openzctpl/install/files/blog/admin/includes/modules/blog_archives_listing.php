<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: category_product_listing.php 15359 2010-01-28 19:52:09Z drbyte $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}

if (isset($_GET['set_display_categories_dropdown'])) {
  $_SESSION['display_categories_dropdown'] = $_GET['set_display_categories_dropdown'];
}
if (!isset($_SESSION['display_categories_dropdown'])) {
  $_SESSION['display_categories_dropdown'] = 0;
}
?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?>&nbsp;-&nbsp;<?php echo zen_output_generated_archives_category_path($current_category_id); ?></td>
            <td class="pageHeading" align="right"><?php echo zen_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText" align="right">
<?php
    echo zen_draw_form('search', FILENAME_BLOG, '', 'get');
// show reset search
    if (isset($_GET['search']) && zen_not_null($_GET['search'])) {
      echo '<a href="' . zen_href_link(FILENAME_BLOG) . '">' . zen_image_button('button_reset.gif', IMAGE_RESET) . '</a>&nbsp;&nbsp;';
    }
    echo HEADING_TITLE_SEARCH_DETAIL . ' ' . zen_draw_input_field('search') . zen_hide_session_id();
    if (isset($_GET['search']) && zen_not_null($_GET['search'])) {
      $keywords = zen_db_input(zen_db_prepare_input($_GET['search']));
      echo '<br />' . TEXT_INFO_SEARCH_DETAIL_FILTER . $keywords;
    }
    echo '</form>';
?>
                </td>
              </tr>
              <tr>
                <td class="smallText" align="right">
<?php  
  if ($_SESSION['display_categories_dropdown'] == 0) {
    echo '<a href="' . zen_href_link(FILENAME_BLOG, 'set_display_categories_dropdown=1&cID=' . $categories->fields['categories_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_ICONS . 'cross.gif', IMAGE_ICON_STATUS_OFF) . '</a>&nbsp;&nbsp;';
    echo zen_draw_form('goto', FILENAME_BLOG, '', 'get');
    echo zen_hide_session_id();
    echo HEADING_TITLE_GOTO . ' ' . zen_draw_pull_down_menu('cPath', zen_get_archives_category_tree(), $current_category_id, 'onChange="this.form.submit();"');
    echo '</form>';
  } else {
    echo '<a href="' . zen_href_link(FILENAME_BLOG, 'set_display_categories_dropdown=0&cID=' . $categories->fields['categories_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_ICONS . 'tick.gif', IMAGE_ICON_STATUS_ON) . '</a>&nbsp;&nbsp;';
    echo HEADING_TITLE_GOTO;
  }
?>
                </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
<?php if ($action == '') { ?>
                <td class="dataTableHeadingContent" width="20" align="right"><?php echo TABLE_HEADING_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CATEGORIES_ARCHIVES; ?></td>
				<td class="dataTableHeadingContent">&nbsp;&nbsp;&nbsp;</td>
				<td class="dataTableHeadingContent">&nbsp;&nbsp;&nbsp;</td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_QUANTITY; ?>&nbsp;&nbsp;&nbsp;</td>
                <td class="dataTableHeadingContent" width="50" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_CATEGORIES_SORT_ORDER; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
<?php } // action == '' ?>
              </tr>
<?php
    switch ($_SESSION['categories_products_sort_order']) {
      case (0):
        $order_by = " order by c.sort_order, cd.categories_name";
        break;
      case (1):
        $order_by = " order by cd.categories_name";
      case (2);
      case (3);
      case (4);
      case (5);
      case (6);
      }

    $categories_count = 0;
    $rows = 0;
    if (isset($_GET['search'])) {
      $search = zen_db_prepare_input($_GET['search']);

      $categories = $db->Execute("select c.categories_id, cd.categories_name, cd.categories_description, c.categories_image,
                                         c.parent_id, c.sort_order, c.date_added,
                                         c.categories_status
                                  from " . TABLE_BLOG_CATEGORIES . " c, " . TABLE_BLOG_CATEGORIES_DESCRIPTION . " cd
                                  where c.categories_id = cd.categories_id
                                  and cd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                                  and cd.categories_name like '%" . zen_db_input($search) . "%'" .
                                  $order_by);
    } else {
      $categories = $db->Execute("select c.categories_id, cd.categories_name, cd.categories_description, c.categories_image,
                                         c.parent_id, c.sort_order, c.date_added,
                                         c.categories_status
                                  from " . TABLE_BLOG_CATEGORIES . " c, " . TABLE_BLOG_CATEGORIES_DESCRIPTION . " cd
                                  where c.parent_id = '" . (int)$current_category_id . "'
                                  and c.categories_id = cd.categories_id
                                  and cd.language_id = '" . (int)$_SESSION['languages_id'] . "'" .
                                  $order_by);
    }
    while (!$categories->EOF) {
      $categories_count++;
      $rows++;

// Get parent_id for subcategories if search
      if (isset($_GET['search'])) $cPath = $categories->fields['parent_id'];

      if ((!isset($_GET['cID']) && !isset($_GET['pID']) || (isset($_GET['cID']) && ($_GET['cID'] == $categories->fields['categories_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
       
        $cInfo = new objectInfo($categories->fields);
      }

      if (isset($cInfo) && is_object($cInfo) && ($categories->fields['categories_id'] == $cInfo->categories_id) ) {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\''  . zen_href_link(FILENAME_BLOG, zen_get_archives_path($categories->fields['categories_id'])) . '\'">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . zen_href_link(FILENAME_BLOG, zen_get_archives_path($categories->fields['categories_id'])) . '\'">' . "\n";
      }
?>
<?php if ($action == '') { ?>
                <td class="dataTableContent" width="20" align="right"><?php echo $categories->fields['categories_id']; ?></td>
                <td class="dataTableContent"><?php echo '<a href="' . zen_href_link(FILENAME_BLOG, zen_get_archives_path($categories->fields['categories_id'])) . '">' . zen_image(DIR_WS_ICONS . 'folder.gif', ICON_FOLDER) . '</a>&nbsp;<b>' . $categories->fields['categories_name'] . '</b>'; ?></td>
                
                <td class="dataTableContent" align="right">&nbsp;</td>
                <td class="dataTableContent" align="center">&nbsp;</td>
                <td class="dataTableContent" align="right" valign="bottom">
                  <?php
                    // show counts
                    $total_archives = zen_get_archives_to_categories($categories->fields['categories_id'], true);
                    $total_archives_on = zen_get_archives_to_categories($categories->fields['categories_id'], false);
                    echo $total_archives_on . TEXT_PRODUCTS_STATUS_ON_OF . $total_archives . TEXT_PRODUCTS_STATUS_ACTIVE;
                  ?>
                  &nbsp;&nbsp;
                </td>
                <td class="dataTableContent" width="50" align="left">
<?php

      if ($categories->fields['categories_status'] == '1') {
        echo '<a href="' . zen_href_link(FILENAME_BLOG, 'action=setflag_categories&flag=0&cID=' . $categories->fields['categories_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_IMAGES . 'icon_green_on.gif', IMAGE_ICON_STATUS_ON) . '</a>';
      } else {
        echo '<a href="' . zen_href_link(FILENAME_BLOG, 'action=setflag_categories&flag=1&cID=' . $categories->fields['categories_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_IMAGES . 'icon_red_on.gif', IMAGE_ICON_STATUS_OFF) . '</a>';
      }
      if (zen_get_products_to_categories($categories->fields['categories_id'], true, 'products_active') == 'true') {
        echo '&nbsp;&nbsp;' . zen_image(DIR_WS_IMAGES . 'icon_yellow_on.gif', IMAGE_ICON_LINKED);
      }
?>
                </td>
                <td class="dataTableContent" align="right"><?php echo $categories->fields['sort_order']; ?></td>
                <td class="dataTableContent" align="right">
                  <?php echo '<a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&cID=' . $categories->fields['categories_id'] . '&action=edit_category' . '&search=' . $_GET['search']) . '">' . zen_image(DIR_WS_IMAGES . 'icon_edit.gif', ICON_EDIT) . '</a>'; ?>
                  <?php echo '<a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&cID=' . $categories->fields['categories_id'] . '&action=delete_category') . '">' . zen_image(DIR_WS_IMAGES . 'icon_delete.gif', ICON_DELETE) . '</a>'; ?>
                  <?php echo '<a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&cID=' . $categories->fields['categories_id'] . '&action=move_category') . '">' . zen_image(DIR_WS_IMAGES . 'icon_move.gif', ICON_MOVE) . '</a>'; ?>

                </td>
<?php } // action == '' ?>
              </tr>
<?php
      $categories->MoveNext();
    }


    switch ($_SESSION['categories_products_sort_order']) {
      case (0):
        $order_by = " order by p.archives_sort_order, pd.archives_name";
        break;
      case (1):
        $order_by = " order by pd.archives_name";
        break;
      case (3);
        $order_by = " order by p.archives_chick, pd.archives_name";
        break;
      case (4);
        $order_by = " order by p.archives_chick DESC, pd.archives_name";
        break;
    
      }

    $archives_count = 0;
	
    if (isset($_GET['search'])) {
// fix duplicates and force search to use master_categories_id
      $archives_query_raw = ("select p.*,pd.*
                                from " . TABLE_BLOG_ARCHIVES . " p, " . TABLE_BLOG_ARCHIVES_DESCRIPTION . " pd, "
                                       . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " p2c
                                where p.archives_id = pd.archives_id
                                and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                                and (p.archives_id = p2c.archives_id
                                and p.master_categories_id = p2c.categories_id)
                                and (
                                pd.archives_name like '%" . zen_db_input($_GET['search']) . "%'
                                or pd.archives_description like '%" . zen_db_input($_GET['search']) . "%'".$order_by);
    } else {
		
      $archives_query_raw = ("select p.*,pd.*
                                from " . TABLE_BLOG_ARCHIVES . " p, " . TABLE_BLOG_ARCHIVES_DESCRIPTION . " pd, " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " p2c
                                where p.archives_id = pd.archives_id
                                and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                                and p.archives_id = p2c.archives_id
                                and p2c.categories_id = '" . (int)$current_category_id . "'" .
                                $order_by);
    }
// Split Page
// reset page when page is unknown

if (($_GET['page'] == '1' or $_GET['page'] == '') and $_GET['pID'] != '') {
  $old_page = $_GET['page'];
  $check_page = $db->Execute($archives_query_raw);
  if ($check_page->RecordCount() > MAX_DISPLAY_RESULTS_CATEGORIES) {
    $check_count=1;
    while (!$check_page->EOF) {
      if ($check_page->fields['archives_id'] == $_GET['pID']) {
        break;
      }
      $check_count++;
      $check_page->MoveNext();
    }
    $_GET['page'] = round((($check_count/MAX_DISPLAY_RESULTS_CATEGORIES)+(fmod_round($check_count,MAX_DISPLAY_RESULTS_CATEGORIES) !=0 ? .5 : 0)),0);
    $page = $_GET['page'];
    if ($old_page != $_GET['page']) {
//      zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $_GET['cPath'] . '&pID=' . $_GET['pID'] . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')));
    }
  } else {
    $_GET['page'] = 1;
  }
}

	if($action!="edit_category"){
		$archives_split = new splitPageResults($_GET['page'], MAX_DISPLAY_RESULTS_CATEGORIES, $archives_query_raw, $archives_query_numrows);
		$archives = $db->Execute($archives_query_raw);
	
    

    
// Split Page

    while (!$archives->EOF) {
      $archives_count++;
      $rows++;

// Get categories_id for product if search
      if (isset($_GET['search'])) $cPath = $archives->fields['categories_id'];
// Split Page
      $type_handler = $zc_products->get_admin_handler($archives->fields['archives_type']);
	  
      if (isset($pInfo) && is_object($pInfo) && ($archives->fields['archives_id'] == $pInfo->archives_id) ) {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . zen_href_link($type_handler , 'page=' . $_GET['page'] . '&archives_type=' . $archives->fields['archives_type'] . '&cPath=' . $cPath . '&pID=' . $archives->fields['archives_id'] . '&action=new_archives' . (isset($_GET['search']) ? '&search=' . $_GET['search'] : '')) . '\'">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . zen_href_link(FILENAME_BLOG_ARCHIVES , 'page=' . $_GET['page'] . '&cPath=' . $cPath . '&pID=' . $archives->fields['archives_id'] . '&action=new_archives' . (isset($_GET['search']) ? '&search=' . $_GET['search'] : '')) . '\'">' . "\n";
      }
// Split Page
?>
                <td class="dataTableContent" width="20" align="right"><?php echo $archives->fields['archives_id']; ?></td>
                <td class="dataTableContent"><?php echo '<a href="' . zen_href_link(FILENAME_BLOG_ARCHIVES, 'cPath=' . $cPath . '&pID=' . $archives->fields['archives_id'] . '&action=new_archives_preview&read=only' . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $archives->fields['archives_name']; ?></td>
                <td class="dataTableHeadingContent">&nbsp;&nbsp;&nbsp;</td>
                <td class="dataTableHeadingContent">&nbsp;&nbsp;&nbsp;</td>
                <td class="dataTableContent" align="right"></td>
                <td class="dataTableContent" width="50" align="left">
<?php
      if ($archives->fields['archives_status'] == '1') {
        echo '<a href="' . zen_href_link(FILENAME_BLOG, 'action=setflag&flag=0&pID=' . $archives->fields['archives_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_IMAGES . 'icon_green_on.gif', IMAGE_ICON_STATUS_ON) . '</a>';
      } else {
        echo '<a href="' . zen_href_link(FILENAME_BLOG, 'action=setflag&flag=1&pID=' . $archives->fields['archives_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_IMAGES . 'icon_red_on.gif', IMAGE_ICON_STATUS_OFF) . '</a>';
      }
      if (zen_get_archives_is_linked($archives->fields['archives_id']) == 'true') {
        echo '&nbsp;&nbsp;' . zen_image(DIR_WS_IMAGES . 'icon_yellow_on.gif', IMAGE_ICON_LINKED) . '<br>';
      }
?>
                </td>
<?php
if($_GET['action']=="delete_reviews"){
	echo "<td>fsdafdsafds</td>";
}
?>
<?php if ($action == '') { ?>
                <td class="dataTableContent" align="right"><?php echo $archives->fields['archives_sort_order']; ?></td>
                <td class="dataTableContent" align="right">
        <?php echo '<a href="' . zen_href_link("blog_archives.php", 'cPath=' . $cPath . '&pID=' . $archives->fields['archives_id']  . '&action=new_archives' . (isset($_GET['search']) ? '&search=' . $_GET['search'] : '')) . '">' . zen_image(DIR_WS_IMAGES . 'icon_edit.gif', ICON_EDIT) . '</a>'; ?>
        <?php echo '<a href="' . zen_href_link("blog_archives.php", 'cPath=' . $cPath . '&archives_type=' . $archives->fields['archives_type'] . '&pID=' . $archives->fields['archives_id'] . '&action=delete_archives') . '">' . zen_image(DIR_WS_IMAGES . 'icon_delete.gif', ICON_DELETE) . '</a>'; ?>
        <?php echo '<a href="' . zen_href_link("blog_archives.php", 'cPath=' . $cPath . '&archives_type=' . $archives->fields['archives_type'] . '&pID=' . $archives->fields['archives_id'] . '&action=move_archives') . '">' . zen_image(DIR_WS_IMAGES . 'icon_move.gif', ICON_MOVE) . '</a>'; ?>
        
<?php
// BOF: Attribute commands
//if (!empty($archives->fields['archives_id']) && zen_has_product_attributes($archives->fields['archives_id'], 'false')) {
?>

<?php
//} // EOF: Attribute commands
?>
<?php
        

        
?>
<?php } // action == '' ?>

                </td>
              </tr>
<?php
      $archives->MoveNext();
    }
	}
	
    $cPath_back = '';
    if (sizeof($cPath_array) > 0) {
      for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
        if (empty($cPath_back)) {
          $cPath_back .= $cPath_array[$i];
        } else {
          $cPath_back .= '_' . $cPath_array[$i];
        }
      }
    }

    $cPath_back = (zen_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
?>
<?php
 
 if ($action == '') { ?>
              <tr>
                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText"><?php echo TEXT_CATEGORIES . '&nbsp;' . $categories_count . '<br />' . TEXT_PRODUCTS . '&nbsp;' . $archives_count; ?></td>
                    <td align="right" class="smallText"><?php if (sizeof($cPath_array) > 0) echo '<a href="' . zen_href_link(FILENAME_BLOG, $cPath_back . 'cID=' . $current_category_id) . '">' . zen_image_button('button_back.gif', IMAGE_BACK) . '</a>&nbsp;'; if (!isset($_GET['search'])) echo (!$zc_skip_categories ? '<a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&action=new_category') . '">' . zen_image_button('button_new_category.gif', IMAGE_NEW_CATEGORY) . '</a>&nbsp;' : ''); ?>

<?php if ($zc_skip_archives == false) { ?>


<?php echo zen_draw_form('newarchives', FILENAME_BLOG_ARCHIVES, 'action=new_archives', 'post', 'class="form-horizontal"'); ?>
             <?php echo (empty($_GET['search']) ? zen_image_submit('button_new_product.gif', IMAGE_NEW_PRODUCT) : ''); ?>
<?php
  $archives_restrict_types_array=array(
		"0"=>array("id"=>"text","text"=>"Archives Text"),
		"1"=>array("id"=>"image","text"=>"Archives Image"),
		"2"=>array("id"=>"vedio","text"=>"Archives Vedio")
  );
 
?>
<?php echo '&nbsp;&nbsp;' . zen_draw_pull_down_menu('archives_type', $archives_restrict_types_array);
echo zen_hide_session_id(); ?>
           <input type="hidden" name="cPath" value="<?php echo $cPath; ?>">
           <input type="hidden" name="action" value="new_archives">
          </form>
<?php
  } else {
    echo BLOG_CATEGORY_HAS_SUBCATEGORIES;
?>
<?php } // hide has cats?>
          &nbsp;</td>
                  </tr>
                </table></td>
              </tr>
<?php } // turn off when editing ?>
            </table></td>
