<?php
/**
 * @package admin
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: prod_cat_header_code.php 3009 2006-02-11 15:41:10Z wilt $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();


  if (isset($_GET['archives_type'])) {
    $archives_type = zen_db_prepare_input($_GET['archives_type']);
  } else {
    $archives_type='text';
  }
  
  $type_admin_handler=explode("/",$_SERVER['PHP_SELF']);
	$type_admin_handler=$type_admin_handler[count($type_admin_handler)-1];
  

  function zen_reset_page() {
    global $db, $current_category_id;
    $look_up = $db->Execute("select p.*,pd.* from " . TABLE_BLOG_ARCHIVES . " p, " . TABLE_BLOG_ARCHIVES_DESCRIPTION . " pd, " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " p2c where p.archives_id = pd.archives_id and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' and p.archives_id = p2c.archives_id and p2c.categories_id = '" . $current_category_id . "' order by pd.archives_name");
    while (!$look_up->EOF) {
      $look_count ++;
      if ($look_up->fields['archives_id']== $_GET['pID']) {
        exit;
      } else {
        $look_up->MoveNext();
      }
    }
    return round( ($look_count+.05)/MAX_DISPLAY_RESULTS_CATEGORIES);
  }
		
?>