<?php
/**
 * @package admin
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: new_product_preview.php 3009 2006-02-11 15:41:10Z wilt $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}
// copy image only if modified
        if (!isset($_GET['read']) || $_GET['read'] == 'only') {
          $archives_image = new upload('archives_image');
          $archives_image->set_destination(DIR_FS_CATALOG_IMAGES . $_POST['img_dir']);
          if ($archives_image->parse() && $archives_image->save($_POST['overwrite'])) {
            $archives_image_name = $_POST['img_dir'] . $archives_image->filename;
          } else {
            $archives_image_name = (isset($_POST['archives_previous_image']) ? $_POST['archives_previous_image'] : '');
          }
        }
?>