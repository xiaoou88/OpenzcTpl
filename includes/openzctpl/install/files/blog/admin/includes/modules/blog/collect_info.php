<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: collect_info.php 15884 2010-04-11 16:45:00Z wilt $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}
    $parameters = array('archives_name' => '',
                        'archives_description' => '',
                        'archives_id' => '',
                        'archives_image' => '',
                        'archives_date_added' => '',
                        'archives_status' => '',
                        'archives_sort_order' => '0',
                        'master_categories_id' => ''
                       );

    $pInfo = new objectInfo($parameters);

    if (isset($_GET['pID']) && empty($_POST)) {
      $archives = $db->Execute("select a.*,ad.archives_name, ad.archives_description
                              from " . TABLE_BLOG_ARCHIVES . " a, " . TABLE_BLOG_ARCHIVES_DESCRIPTION . " ad
                              where a.archives_id = '" . (int)$_GET['pID'] . "'
                              and a.archives_id = ad.archives_id
                              and ad.language_id = '" . (int)$_SESSION['languages_id'] . "'");

      $pInfo->objectInfo($archives->fields);
    } elseif (zen_not_null($_POST)) {
      $pInfo->objectInfo($_POST);
      $archives_name = $_POST['archives_name'];
      $archives_description = $_POST['archives_description'];
    }

	

    $languages = zen_get_languages();

    if (!isset($pInfo->archives_status)) $pInfo->archives_status = '1';
    switch ($pInfo->archives_status) {
      case '0': $in_status = false; $out_status = true; break;
      case '1':
      default: $in_status = true; $out_status = false;
      break;
    }
// set to out of stock if categories_status is off and new product or existing archives_status is off
    if (zen_get_archives_categories_status($current_category_id) == '0' and $pInfo->archives_status != '1') {
      $pInfo->archives_status = 0;
      $in_status = false;
      $out_status = true;
    }
	
	$archives_type=array(
			"0"=>array("id"=>"text","text"=>"Archives Text"),
			"1"=>array("id"=>"image","text"=>"Archives Image"),
			"2"=>array("id"=>"vedio","text"=>"Archives Vedio"),
		);
	$archives_flag=array(
			"0"=>array("id"=>"","text"=>"Please Select"),
			"1"=>array("id"=>"p","text"=>"Archives Flag p"),
			"2"=>array("id"=>"v","text"=>"Archives Flag v"),
			"3"=>array("id"=>"r","text"=>"Archives Flag r"),
			"4"=>array("id"=>"f","text"=>"Archives Flag f"),
			"5"=>array("id"=>"s","text"=>"Archives Flag s"),
			"6"=>array("id"=>"h","text"=>"Archives Flag h"),
			"7"=>array("id"=>"c","text"=>"Archives Flag c")
	);

// set image overwrite
  $on_overwrite = true;
  $off_overwrite = false;
// set image delete
  $on_image_delete = false;
  $off_image_delete = true;
?>
<link rel="stylesheet" type="text/css" href="includes/javascript/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="includes/javascript/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript"><!--
  var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "new_archives", "archives_date_added","btnDate1","<?php echo $pInfo->archives_date_added; ?>",scBTNMODE_CUSTOMBLUE);
//--></script>

    <?php
//  echo $type_admin_handler;
echo zen_draw_form('new_archives', FILENAME_BLOG_ARCHIVES, 'cPath=' . $cPath . (isset($_GET['archives_type']) ? '&archives_type=' . $_GET['archives_type'] : '') . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . '&action=new_archives_preview' . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '') . ( (isset($_GET['search']) && !empty($_GET['search'])) ? '&search=' . $_GET['search'] : '') . ( (isset($_POST['search']) && !empty($_POST['search']) && empty($_GET['search'])) ? '&search=' . $_POST['search'] : ''), 'post', 'enctype="multipart/form-data"');
    ?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo sprintf(TEXT_NEW_ARCHIVES, zen_output_generated_archives_category_path($current_category_id)); ?></td>
            <td class="pageHeading" align="right"><?php echo zen_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td class="main" align="right"><?php echo zen_draw_hidden_field('archives_date_added', (zen_not_null($pInfo->date_added) ? $pInfo->date_added : date('Y-m-d'))) . zen_image_submit('button_preview.gif', IMAGE_PREVIEW) . '&nbsp;&nbsp;<a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '') . ( (isset($_GET['search']) && !empty($_GET['search'])) ? '&search=' . $_GET['search'] : '') . ( (isset($_POST['search']) && !empty($_POST['search']) && empty($_GET['search'])) ? '&search=' . $_POST['search'] : '')) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
      </tr>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
<?php
// show when product is linked
if (zen_get_archives_is_linked($_GET['pID']) == 'true' and $_GET['pID'] > 0) {
?>
          <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_MASTER_CATEGORIES_ID; ?></td>
            <td class="main">
              <?php
                // echo zen_draw_pull_down_menu('archives_tax_class_id', $tax_class_array, $pInfo->archives_tax_class_id);
                echo zen_image(DIR_WS_IMAGES . 'icon_yellow_on.gif', IMAGE_ICON_LINKED) . '&nbsp;&nbsp;';
                echo zen_draw_pull_down_menu('master_category', zen_get_archives_master_categories_pulldown($_GET['pID']), $pInfo->master_categories_id); ?>
            </td>
          </tr>
<?php } else { ?>
          <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_MASTER_CATEGORIES_ID; ?></td>
            <td class="main"><?php echo TEXT_INFO_ID . ($_GET['pID'] > 0 ? $pInfo->master_categories_id  . ' ' . zen_get_archives_category_name($pInfo->master_categories_id, $_SESSION['languages_id']) : $current_category_id  . ' ' . zen_get_archives_category_name($current_category_id, $_SESSION['languages_id'])); ?>
          </tr>
<?php } ?>
          
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '100%', '2'); ?></td>
          </tr>
<?php
// hidden fields not changeable on products page
echo zen_draw_hidden_field('master_categories_id', $pInfo->master_categories_id);

?>
          <tr>
            <td colspan="2" class="main" align="center"><?php echo (zen_get_archives_categories_status($current_category_id) == '0' ? TEXT_CATEGORIES_STATUS_INFO_OFF : '') . ($out_status == true ? ' ' . TEXT_PRODUCTS_STATUS_INFO_OFF : ''); ?></td>
          <tr>
          <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_STATUS; ?></td>
            <td class="main">
				<?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . zen_draw_radio_field('archives_status', '1', $in_status) . '&nbsp;' . TEXT_ARCHIVES_AVAILABLE . '&nbsp;' . zen_draw_radio_field('archives_status', '0', $out_status) . '&nbsp;' . TEXT_ARCHIVES_NOT_AVAILABLE; ?>
			</td>
          </tr>
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_ADD_DATE; ?><br /><small>(YYYY-MM-DD)</small></td>
            <td class="main">
			<?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;'; ?>
			<script language="javascript">dateAvailable.writeControl(); dateAvailable.dateFormat="yyyy-MM-dd";</script>
			</td>
          </tr>
		  <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
		  <tr>
			<td class="main"><?php echo TEXT_ARCHIVES_TYPE; ?>&nbsp;</td>
			<td class="main"><?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;'; ?><?php echo zen_draw_pull_down_menu('archives_type', $archives_type, $archives_type); ?></td>
		  </tr>
		  <tr>
			<td class="main"><?php echo TEXT_ARCHIVES_FLAG; ?>&nbsp;</td>
			<td class="main"><?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;'; ?><?php echo zen_draw_pull_down_menu('archives_flag', $archives_flag, $archives_flag); ?> &nbsp;<?php echo TEXT_ARCHIVES_FLAG_INFO;?></td>
		  </tr>
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
		  <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_SHORT_TITLE; ?></td>
            <td class="main"><?php echo zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . zen_draw_input_field('archives_shorttitle[' . $languages[$i]['id'] . ']', (isset($archives_shorttitle[$languages[$i]['id']]) ? stripslashes($archives_shorttitle[$languages[$i]['id']]) : zen_get_archives_field($pInfo->archives_id, $languages[$i]['id'],"archives_shorttitle")), zen_set_field_length(TABLE_BLOG_ARCHIVES_DESCRIPTION, 'archives_shorttitle')); ?></td>
          </tr>
		  <tr>
            <td class="main"><?php  echo TEXT_ARCHIVES_KEYWORDS; ?></td>
            <td class="main"><?php echo zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . zen_draw_input_field('archives_keywords[' . $languages[$i]['id'] . ']', (isset($archives_keywords[$languages[$i]['id']]) ? stripslashes($archives_keywords[$languages[$i]['id']]) : zen_get_archives_field($pInfo->archives_id, $languages[$i]['id'],"archives_keywords")), zen_set_field_length(TABLE_BLOG_ARCHIVES_DESCRIPTION, 'archives_keywords')); ?></td>
          </tr>
		  <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_DESCRIPTION; ?></td>
            <td class="main"><?php echo zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . zen_draw_textarea_field('archives_description[' . $languages[$i]['id'] . ']', 'soft', '100%', '3', (isset($archives_description[$languages[$i]['id']])) ? stripslashes($archives_description[$languages[$i]['id']]) : zen_get_archives_field($pInfo->archives_id, $languages[$i]['id'],"archives_description")); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_NAME; ?></td>
            <td class="main"><?php echo zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . zen_draw_input_field('archives_name[' . $languages[$i]['id'] . ']', (isset($archives_name[$languages[$i]['id']]) ? stripslashes($archives_name[$languages[$i]['id']]) : zen_get_archives_name($pInfo->archives_id, $languages[$i]['id'])), zen_set_field_length(TABLE_BLOG_ARCHIVES_DESCRIPTION, 'archives_name')); ?></td>
          </tr>
<?php
    }
?>
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<script language="javascript"><!--
updateGross();
//--></script>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
          <tr>
            <td class="main" valign="top"><?php echo TEXT_ARCHIVES_BODY; ?></td>
            <td colspan="2"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="main" width="25" valign="top"><?php echo zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                <td class="main" width="100%">
        <?php if ($_SESSION['html_editor_preference_status']=="FCKEDITOR") {
                $oFCKeditor = new FCKeditor('archives_body[' . $languages[$i]['id'] . ']') ;
                $oFCKeditor->Value = (isset($archives_body[$languages[$i]['id']])) ? stripslashes($archives_body[$languages[$i]['id']]) : zen_get_archives_description($pInfo->archives_id, $languages[$i]['id']) ;
                $oFCKeditor->Width  = '99%' ;
                $oFCKeditor->Height = '350' ;
//                $oFCKeditor->Config['ToolbarLocation'] = 'Out:xToolbar' ;
//                $oFCKeditor->Create() ;
                $output = $oFCKeditor->CreateHtml() ;  echo $output;
          } else { // using HTMLAREA or just raw "source"

          echo zen_draw_textarea_field('archives_body[' . $languages[$i]['id'] . ']', 'soft', '100%', '30', (isset($archives_body[$languages[$i]['id']])) ? stripslashes($archives_body[$languages[$i]['id']]) : zen_get_archives_field($pInfo->archives_id, $languages[$i]['id'],"archives_body"));
          } ?>
        </td>
              </tr>
            </table></td>
          </tr>
<?php
    }
?>
          
<?php
  $dir = @dir(DIR_FS_CATALOG_IMAGES);
  $dir_info[] = array('id' => '', 'text' => "Main Directory");
  while ($file = $dir->read()) {
    if (is_dir(DIR_FS_CATALOG_IMAGES . $file) && strtoupper($file) != 'CVS' && $file != "." && $file != "..") {
      $dir_info[] = array('id' => $file . '/', 'text' => $file);
    }
  }
  $dir->close();
  sort($dir_info);

  $default_directory = substr( $pInfo->archives_image, 0,strpos( $pInfo->archives_image, '/')+1);
?>

          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_black.gif', '100%', '3'); ?></td>
          </tr>

          <tr>
            <td class="main" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="main"><?php echo TEXT_ARCHIVES_IMAGE; ?></td>
                <td class="main"><?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . zen_draw_file_field('archives_image') . '&nbsp;' . ($pInfo->archives_image !='' ? TEXT_IMAGE_CURRENT . $pInfo->archives_image : TEXT_IMAGE_CURRENT . '&nbsp;' . NONE) . zen_draw_hidden_field('archives_previous_image', $pInfo->archives_image); ?></td>
                <td valign = "center" class="main"><?php echo TEXT_ARCHIVES_IMAGE_DIR; ?>&nbsp;<?php echo zen_draw_pull_down_menu('img_dir', $dir_info, $default_directory); ?></td>
			  </tr>
              <tr>
                <td class="main"><?php echo zen_draw_separator('pixel_trans.gif', '24', '15'); ?></td>
                <td class="main" valign="top"><?php echo TEXT_IMAGES_DELETE_ARCHIVES . ' ' . zen_draw_radio_field('image_delete', '0', $off_image_delete) . '&nbsp;' . TABLE_HEADING_NO . ' ' . zen_draw_radio_field('image_delete', '1', $on_image_delete) . '&nbsp;' . TABLE_HEADING_YES; ?></td>
	  	      </tr>
              <tr>
                <td class="main"><?php echo zen_draw_separator('pixel_trans.gif', '24', '15'); ?></td>
                <td colspan="3" class="main" valign="top"><?php echo TEXT_IMAGES_OVERWRITE  . ' ' . zen_draw_radio_field('overwrite', '0', $off_overwrite) . '&nbsp;' . TABLE_HEADING_NO . ' ' . zen_draw_radio_field('overwrite', '1', $on_overwrite) . '&nbsp;' . TABLE_HEADING_YES; ?>
                  <?php echo '<br />' . TEXT_PRODUCTS_IMAGE_MANUAL . '&nbsp;' . zen_draw_input_field('archives_image_manual'); ?></td>
              </tr>
            </table></td>
          </tr>
          
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_SORT_ORDER; ?></td>
            <td class="main"><?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . zen_draw_input_field('archives_sort_order', $pInfo->archives_sort_order); ?></td>
          </tr>
		  <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_RELATED_PRODUCTS_ID; ?></td>
            <td class="main">
				<?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . zen_draw_input_field('related_products_id', $pInfo->related_products_id); ?>
				&nbsp;&nbsp;<?php echo TEXT_ARCHIVES_RELATED_PRODUCTS_ID_INFO;?>
			</td>
		  </tr>
		  <tr>
            <td class="main"><?php echo TEXT_ARCHIVES_RELATED_CATEGORIES_ID; ?></td>
            <td class="main">
				<?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . zen_draw_input_field('related_categories_id', $pInfo->related_categories_id); ?>
				&nbsp;&nbsp;<?php echo TEXT_ARCHIVES_RELATED_CATEGORIES_ID_INFO;?>
			</td>
		  </tr>
        </table>
		</td>
      </tr>
      <tr>
        <td><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
	  
      <tr>
        <td class="main" align="right"><?php echo zen_draw_hidden_field('archives_date_added', (zen_not_null($pInfo->archives_date_added) ? $pInfo->archives_date_added : date('Y-m-d'))) . ( (isset($_GET['search']) && !empty($_GET['search'])) ? zen_draw_hidden_field('search', $_GET['search']) : '') . ( (isset($_POST['search']) && !empty($_POST['search']) && empty($_GET['search'])) ? zen_draw_hidden_field('search', $_POST['search']) : '') . zen_image_submit('button_preview.gif', IMAGE_PREVIEW) . '&nbsp;&nbsp;<a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '') . ( (isset($_GET['search']) && !empty($_GET['search'])) ? '&search=' . $_GET['search'] : '') . ( (isset($_POST['search']) && !empty($_POST['search']) && empty($_GET['search'])) ? '&search=' . $_POST['search'] : '')) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
      </tr>
    </table></form>
