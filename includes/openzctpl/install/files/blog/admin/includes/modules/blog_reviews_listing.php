<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: category_product_listing.php 15359 2010-01-28 19:52:09Z drbyte $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}


?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2" >
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo zen_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText" align="right">
<?php
    echo zen_draw_form('search', FILENAME_REVIEWS, '', 'get');
// show reset search
    if (isset($_GET['search']) && zen_not_null($_GET['search'])) {
      echo '<a href="' . zen_href_link(FILENAME_REVIEWS) . '">' . zen_image_button('button_reset.gif', IMAGE_RESET) . '</a>&nbsp;&nbsp;';
    }
    echo HEADING_TITLE_SEARCH_DETAIL . ' ' . zen_draw_input_field('search') . zen_hide_session_id();
    if (isset($_GET['search']) && zen_not_null($_GET['search'])) {
      $keywords = zen_db_input(zen_db_prepare_input($_GET['search']));
      echo '<br />' . TEXT_INFO_SEARCH_DETAIL_FILTER . $keywords;
    }
    echo '</form>';
?>
                </td>
              </tr>
              <tr>
                <td class="smallText" align="right">
<?php  
 
    echo '<a href="' . zen_href_link(FILENAME_REVIEWS, 'set_display_categories_dropdown=0&cID=' . $categories->fields['categories_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' ;
    
  
?>
                </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" <?php if($_GET['action']=="delete_reviews"){echo "style='display:none'";}?>><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
<?php if ($action == '') { ?>
                <td class="dataTableHeadingContent" width="20" align="right"><?php echo TABLE_HEADING_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ARCHIVES_REVEIWS; ?></td>
				<td class="dataTableHeadingContent">&nbsp;&nbsp;&nbsp;</td>
				<td class="dataTableHeadingContent">&nbsp;&nbsp;&nbsp;</td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_DATE; ?>&nbsp;&nbsp;&nbsp;</td>
                <td class="dataTableHeadingContent" width="50" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
<?php } // action == '' ?>
              </tr>
<?php
   

    $categories_count = 0;
    $rows = 0;
    if (isset($_GET['search'])) {
      $search = zen_db_prepare_input($_GET['search']);

      $reviews = $db->Execute("select r.*,rd.*
                                  from " . TABLE_BLOG_REVIEWS . " r, " . TABLE_BLOG_REVIEWS_DESCRIPTION . " rd
                                  where r.reviews_id = rd.reviews_id
                                  and rd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                                  and rd.reviews_text like '%" . zen_db_input($search) . "%'" .
                                  $order_by);
    } else {
      $reviews = $db->Execute("select r.*,rd.*
                                  from " . TABLE_BLOG_REVIEWS . " r, " . TABLE_BLOG_REVIEWS_DESCRIPTION . " rd
                                  where r.reviews_id = rd.reviews_id
                                  and rd.language_id = '" . (int)$_SESSION['languages_id'] . "'" .
                                  $order_by);
    }
    while (!$reviews->EOF) {
      $reviews_count++;
      $rows++;


?>
<?php if ($action == '') { ?>
                <td class="dataTableContent" width="20" align="right"><?php echo $reviews->fields['reviews_id']; ?></td>
                <td class="dataTableContent"><?php echo '<a href="' . zen_href_link(FILENAME_BLOG_REVIEWS,$reviews->fields['reviews_id']) . '">' . zen_image(DIR_WS_ICONS . 'preview.gif', ICON_FOLDER) . '</a>&nbsp;<b>&nbsp;<b>'.$reviews->fields['customers_name'].':</b>&nbsp;&nbsp;' . $reviews->fields['reviews_text'] . '</b>'; ?></td>
                
                <td class="dataTableContent" align="right">&nbsp;</td>
                <td class="dataTableContent" align="center">&nbsp;</td>
                <td class="dataTableContent" align="right" valign="bottom">
                  <?php echo $reviews->fields['date_added'];?>
                </td>
                <td class="dataTableContent" width="50" align="left">
<?php

      if ($reviews->fields['reviews_status'] == '1') {
        echo '<a href="' . zen_href_link(FILENAME_BLOG_REVIEWS, 'action=setflag_reviews&flag=0&rID=' . $reviews->fields['reviews_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_IMAGES . 'icon_green_on.gif', IMAGE_ICON_STATUS_ON) . '</a>';
      } else {
        echo '<a href="' . zen_href_link(FILENAME_BLOG_REVIEWS, 'action=setflag_reviews&flag=1&rID=' . $reviews->fields['reviews_id'] . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image(DIR_WS_IMAGES . 'icon_red_on.gif', IMAGE_ICON_STATUS_OFF) . '</a>';
      }
?>
                </td>
                <td class="dataTableContent" align="right">
                  <?php echo '<a href="' . zen_href_link(FILENAME_BLOG_REVIEWS, '&rID=' . $reviews->fields['reviews_id'] . '&action=delete_reviews') . '">' . zen_image(DIR_WS_IMAGES . 'icon_delete.gif', ICON_DELETE) . '</a>'; ?>
                </td>
<?php } // action == '' ?>
              </tr>
<?php
      $reviews->MoveNext();
    }
// Split Page
// reset page when page is unknown
    $cPath_back = '';
    if (sizeof($cPath_array) > 0) {
      for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
        if (empty($cPath_back)) {
          $cPath_back .= $cPath_array[$i];
        } else {
          $cPath_back .= '_' . $cPath_array[$i];
        }
      }
    }

    $cPath_back = (zen_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
?>
            </table></td>
