<?php
/**
 * @package admin
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: sidebox_delete_product.php 3358 2006-04-03 04:33:32Z ajeh $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}

$sql="select * from ".TABLE_BLOG_ARCHIVES_DESCRIPTION." where archives_id='".$_GET['pID']."' and language_id='".(int)$_SESSION['languages_id']."'";

$pInfo=$db->Execute($sql)->fields;


        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_PRODUCT . '</b>');

        $contents = array('form' => zen_draw_form('archives', FILENAME_BLOG_ARCHIVES, 'action=delete_archives_confirm&archives_type=' . $archives_type . '&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . zen_draw_hidden_field('archives_id', $pInfo['archives_id']));
        $contents[] = array('text' => TEXT_DELETE_PRODUCT_INTRO);
        $contents[] = array('text' => '<br /><b>' . $pInfo['archives_name'] . ' ID#' . $pInfo['archives_id'] . '</b>');

       

        $archives_categories_string = '';
        $archives_categories = zen_generate_archives_category_path($pInfo['archives_id'], 'archives');

        if (sizeof($archives_categories) > 1) {
          $contents[] = array('text' => '<br /><b><span class="alert">' . TEXT_MASTER_CATEGORIES_ID . '</span>' . '</b>');
        }
        for ($i = 0, $n = sizeof($archives_categories); $i < $n; $i++) {
          $category_path = '';
          for ($j = 0, $k = sizeof($archives_categories[$i]); $j < $k; $j++) {
            $category_path .= $archives_categories[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
          }
          $category_path = substr($category_path, 0, -16);
          if (sizeof($archives_categories) > 1 && zen_get_parent_category_id($pInfo['archives_id']) == $archives_categories[$i][sizeof($archives_categories[$i])-1]['id']) {
            $archives_categories_string .= '<strong><span class="alert">' . zen_draw_checkbox_field('archives_categories[]', $archives_categories[$i][sizeof($archives_categories[$i])-1]['id'], true) . '&nbsp;' . $category_path . '</strong></span><br />';
          } else {
            $archives_categories_string .= zen_draw_checkbox_field('archives_categories[]', $archives_categories[$i][sizeof($archives_categories[$i])-1]['id'], true) . '&nbsp;' . $category_path . '<br />';
          }
        }
        $archives_categories_string = substr($archives_categories_string, 0, -4);

        $contents[] = array('text' => '<br />' . $archives_categories_string);
        $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&pID=' . $pInfo['archives_id'] . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
?>