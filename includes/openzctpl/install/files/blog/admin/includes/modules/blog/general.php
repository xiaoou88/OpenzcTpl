<?php
function zen_output_generated_archives_category_path($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = zen_generate_archives_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
//        $calculated_category_path_string .= $calculated_category_path[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
        $calculated_category_path_string = $calculated_category_path[$i][$j]['text'] . '&nbsp;&gt;&nbsp;' . $calculated_category_path_string;
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -16) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;
	
    return $calculated_category_path_string;
  }
  
function zen_generate_archives_category_path($id, $from = 'category', $categories_array = '', $index = 0) {
    global $db;

    if (!is_array($categories_array)) $categories_array = array();

    if ($from == 'archives') {
      $categories = $db->Execute("select categories_id
                                  from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . "
                                  where archives_id = '" . (int)$id . "'");

      while (!$categories->EOF) {
        if ($categories->fields['categories_id'] == '0') {
          $categories_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
        } else {
          $category = $db->Execute("select cd.categories_name, c.parent_id
                                    from " . TABLE_BLOG_CATEGORIES . " c, " . TABLE_BLOG_CATEGORIES_DESCRIPTION . " cd
                                    where c.categories_id = '" . (int)$categories->fields['categories_id'] . "'
                                    and c.categories_id = cd.categories_id
                                    and cd.language_id = '" . (int)$_SESSION['languages_id'] . "'");

          $categories_array[$index][] = array('id' => $categories->fields['categories_id'], 'text' => $category->fields['categories_name']);
          if ( (zen_not_null($category->fields['parent_id'])) && ($category->fields['parent_id'] != '0') ) $categories_array = zen_generate_archives_category_path($category->fields['parent_id'], 'category', $categories_array, $index);
          $categories_array[$index] = array_reverse($categories_array[$index]);
        }
        $index++;
        $categories->MoveNext();
      }
    } elseif ($from == 'category') {
      $category = $db->Execute("select cd.categories_name, c.parent_id
                                from " . TABLE_BLOG_CATEGORIES . " c, " . TABLE_BLOG_CATEGORIES_DESCRIPTION . " cd
                                where c.categories_id = '" . (int)$id . "'
                                and c.categories_id = cd.categories_id
                                and cd.language_id = '" . (int)$_SESSION['languages_id'] . "'");
      if (!$category->EOF) {
        $categories_array[$index][] = array('id' => $id, 'text' => $category->fields['categories_name']);
        if ( (zen_not_null($category->fields['parent_id'])) && ($category->fields['parent_id'] != '0') ) $categories_array = zen_generate_archives_category_path($category->fields['parent_id'], 'category', $categories_array, $index);
      }
    }

    return $categories_array;
  }
  
  function zen_get_archives_category_name($category_id, $language_id) {
    global $db;
    $category = $db->Execute("select categories_name
                              from " . TABLE_BLOG_CATEGORIES_DESCRIPTION . "
                              where categories_id = '" . (int)$category_id . "'
                              and language_id = '" . (int)$language_id . "'");

    return $category->fields['categories_name'];
  }
  function zen_get_archives_category_field($category_id, $language_id,$field) {
    global $db;
    $category = $db->Execute("select $field
                              from " . TABLE_BLOG_CATEGORIES_DESCRIPTION . "
                              where categories_id = '" . (int)$category_id . "'
                              and language_id = '" . (int)$language_id . "'");

    return $category->fields[$field];
  }
  function zen_get_archives_name($archives_id, $language_id = 0) {
    global $db;
    if ($language_id == 0) $language_id = $_SESSION['languages_id'];
    $archives = $db->Execute("select archives_name
                             from " . TABLE_BLOG_ARCHIVES_DESCRIPTION . "
                             where archives_id = '" . (int)$archives_id . "'
                             and language_id = '" . (int)$language_id . "'");

    return $archives->fields['archives_name'];
  }
  function zen_get_archives_description($archives_id, $language_id) {
    global $db;
    $archives = $db->Execute("select archives_body
                             from " . TABLE_BLOG_ARCHIVES_DESCRIPTION . "
                             where archives_id = '" . (int)$archives_id . "'
                             and language_id = '" . (int)$language_id . "'");

    return $archives->fields['archives_description'];
  }
  function zen_get_archives_field($archives_id, $language_id,$field) {
    global $db;
    $archives = $db->Execute("select $field
                             from " . TABLE_BLOG_ARCHIVES_DESCRIPTION . "
                             where archives_id = '" . (int)$archives_id . "'
                             and language_id = '" . (int)$language_id . "'");

    return $archives->fields[$field];
  }
  function zen_get_archives_is_linked($archives_id, $show_count = 'false') {
    global $db;

    $sql = "select * from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . (zen_not_null($archives_id) ? " where archives_id=" . $archives_id : "");
    $check_linked = $db->Execute($sql);
    if ($check_linked->RecordCount() > 1) {
      if ($show_count == 'true') {
        return $check_linked->RecordCount();
      } else {
        return 'true';
      }
    } else {
      return 'false';
    }
  }
  function zen_get_archives_master_categories_pulldown($archives_id) {
    global $db;

    $master_category_array = array();

    $master_categories_query = $db->Execute("select ptc.archives_id, cd.categories_name, cd.categories_id
                                    from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " ptc
                                    left join " . TABLE_BLOG_CATEGORIES_DESCRIPTION . " cd
                                    on cd.categories_id = ptc.categories_id
                                    where ptc.archives_id='" . $archives_id . "'
                                    and cd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                                    ");

    $master_category_array[] = array('id' => '0', 'text' => TEXT_INFO_SET_MASTER_CATEGORIES_ID);
    while (!$master_categories_query->EOF) {
      $master_category_array[] = array('id' => $master_categories_query->fields['categories_id'], 'text' => $master_categories_query->fields['categories_name'] . TEXT_INFO_ID . $master_categories_query->fields['categories_id']);
      $master_categories_query->MoveNext();
    }

    return $master_category_array;
  }
  function zen_get_archives_categories_status($categories_id) {
    global $db;
    $sql = "select categories_status from " . TABLE_BLOG_CATEGORIES . (zen_not_null($categories_id) ? " where categories_id=" . $categories_id : "");
    $check_status = $db->Execute($sql);
    return $check_status->fields['categories_status'];
  }
  function zen_set_archives_status($archives_id, $status) {
    global $db;
    if ($status == '1') {
      return $db->Execute("update " . TABLE_BLOG_ARCHIVES . "
                           set archives_status = 1, archives_last_modified = now()
                           where archives_id = '" . (int)$archives_id . "'");

    } elseif ($status == '0') {
      return $db->Execute("update " . TABLE_BLOG_ARCHIVES . "
                           set archives_status = 0, archives_last_modified = now()
                           where archives_id = '" . (int)$archives_id . "'");

    } else {
      return -1;
    }
  }
  function zen_get_archives_category_tree($parent_id = '0', $spacing = '', $exclude = '', $category_tree_array = '', $include_itself = false, $category_has_products = false, $limit = false) {
    global $db;

    if ($limit) {
      $limit_count = " limit 1";
    } else {
      $limit_count = '';
    }

    if (!is_array($category_tree_array)) $category_tree_array = array();
    if ( (sizeof($category_tree_array) < 1) && ($exclude != '0') ) $category_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {
      $category = $db->Execute("select cd.categories_name
                                from " . TABLE_BLOG_CATEGORIES_DESCRIPTION . " cd
                                where cd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                                and cd.categories_id = '" . (int)$parent_id . "'");

      $category_tree_array[] = array('id' => $parent_id, 'text' => $category->fields['categories_name']);
    }

    $categories = $db->Execute("select c.categories_id, cd.categories_name, c.parent_id
                                from " . TABLE_BLOG_CATEGORIES . " c, " . TABLE_BLOG_CATEGORIES_DESCRIPTION . " cd
                                where c.categories_id = cd.categories_id
                                and cd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                                and c.parent_id = '" . (int)$parent_id . "'
                                order by c.sort_order, cd.categories_name");

    while (!$categories->EOF) {
      if ($category_has_products == true and zen_products_in_category_count($categories->fields['categories_id'], '', false, true) >= 1) {
        $mark = '*';
      } else {
        $mark = '&nbsp;&nbsp;';
      }
      if ($exclude != $categories->fields['categories_id']) $category_tree_array[] = array('id' => $categories->fields['categories_id'], 'text' => $spacing . $categories->fields['categories_name'] . $mark);
      $category_tree_array = zen_get_archives_category_tree($categories->fields['categories_id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $category_tree_array, '', $category_has_products);
      $categories->MoveNext();
    }

    return $category_tree_array;
  }
 function zen_get_archives_path($current_category_id = '') {
    global $cPath_array, $db;

// set to 0 if Top Level
    if ($current_category_id == '') {
      if (empty($cPath_array)) {
        $cPath_new= '';
      } else {
        $cPath_new = implode('_', $cPath_array);
      }
    } else {
      if (sizeof($cPath_array) == 0) {
        $cPath_new = $current_category_id;
      } else {
        $cPath_new = '';
        $last_category = $db->Execute("select parent_id
                                       from " . TABLE_BLOG_CATEGORIES . "
                                       where categories_id = '" . (int)$cPath_array[(sizeof($cPath_array)-1)] . "'");

        $current_category = $db->Execute("select parent_id
                                          from " . TABLE_BLOG_CATEGORIES . "
                                           where categories_id = '" . (int)$current_category_id . "'");

        if ($last_category->fields['parent_id'] == $current_category->fields['parent_id']) {
          for ($i = 0, $n = sizeof($cPath_array) - 1; $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        } else {
          for ($i = 0, $n = sizeof($cPath_array); $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        }

        $cPath_new .= '_' . $current_category_id;

        if (substr($cPath_new, 0, 1) == '_') {
          $cPath_new = substr($cPath_new, 1);
        }
      }
    }

    return 'cPath=' . $cPath_new;
  }
 
  function zen_get_archives_category_description($category_id, $language_id) {
    global $db;
    $category = $db->Execute("select categories_description
                              from " . TABLE_BLOG_CATEGORIES_DESCRIPTION . "
                              where categories_id = '" . (int)$category_id . "'
                              and language_id = '" . (int)$language_id . "'");

    return $category->fields['categories_description'];
  }
  
  function zen_get_archives_to_categories($category_id, $include_inactive = false, $counts_what = 'archives') {
    global $db;

    $archives_count = 0;
    if ($include_inactive == true) {
      switch ($counts_what) {
        case ('archives'):
        $cat_archives_query = "select count(*) as total
                           from " . TABLE_BLOG_ARCHIVES . " p, " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " p2c
                           where p.archives_id = p2c.archives_id
                           and p2c.categories_id = '" . (int)$category_id . "'";
        break;
        case ('archives_active'):
        $cat_archives_query = "select p.archives_id
                           from " . TABLE_BLOG_ARCHIVES . " p, " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " p2c
                           where p.archives_id = p2c.archives_id
                           and p2c.categories_id = '" . (int)$category_id . "'";
        break;
      }

    } else {
      switch ($counts_what) {
        case ('archives'):
          $cat_archives_query = "select count(*) as total
                             from " . TABLE_BLOG_ARCHIVES . " p, " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " p2c
                             where p.archives_id = p2c.archives_id
                             and p.archives_status = 1
                             and p2c.categories_id = '" . (int)$category_id . "'";
        break;
        case ('archives_active'):
          $cat_archives_query = "select p.archives_id
                             from " . TABLE_BLOG_ARCHIVES . " p, " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " p2c
                             where p.archives_id = p2c.archives_id
                             and p.archives_status = 1
                             and p2c.categories_id = '" . (int)$category_id . "'";
        break;
      }

    }
    $cat_archives = $db->Execute($cat_archives_query);
      switch ($counts_what) {
        case ('archives'):
          $cat_archives_count += $cat_archives->fields['total'];
          break;
        case ('archives_active'):
        while (!$cat_archives->EOF) {
          if (zen_get_archives_is_linked($cat_archives->fields['archives_id']) == 'true') {
            return $archives_linked = 'true';
          }
          $cat_archives->MoveNext();
        }
          break;
      }

    $cat_child_categories_query = "select categories_id
                               from " . TABLE_BLOG_CATEGORIES . "
                               where parent_id = '" . (int)$category_id . "'";

    $cat_child_categories = $db->Execute($cat_child_categories_query);

    if ($cat_child_categories->RecordCount() > 0) {
      while (!$cat_child_categories->EOF) {
      switch ($counts_what) {
        case ('archives'):
          $cat_archives_count += zen_get_archives_to_categories($cat_child_categories->fields['categories_id'], $include_inactive);
          break;
        case ('archives_active'):
          if (zen_get_archives_to_categories($cat_child_categories->fields['categories_id'], true, 'archives_active') == 'true') {
            return $archives_linked = 'true';
          }
          break;
        }
        $cat_child_categories->MoveNext();
      }
    }


      switch ($counts_what) {
        case ('archives'):
          return $cat_archives_count;
          break;
        case ('archives_active'):
          return $archives_linked;
          break;
      }
  }
  function zen_get_generated_archives_category_path_ids($id, $from = 'category') {
    global $db;
    $calculated_category_path_string = '';
    $calculated_category_path = zen_generate_archives_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['id'] . '_';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -1) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }
  
  function zen_remove_archives_category($category_id) {
    if ((int)$category_id == 0) return;
    global $db;
    $category_image = $db->Execute("select categories_image
                                    from " . TABLE_BLOG_CATEGORIES . "
                                    where categories_id = '" . (int)$category_id . "'");

    $duplicate_image = $db->Execute("select count(*) as total
                                     from " . TABLE_BLOG_CATEGORIES . "
                                     where categories_image = '" .
                                           zen_db_input($category_image->fields['categories_image']) . "'");
    if ($duplicate_image->fields['total'] < 2) {
      if (file_exists(DIR_FS_CATALOG_IMAGES . $category_image->fields['categories_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $category_image->fields['categories_image']);
      }
    }

    $db->Execute("delete from " . TABLE_BLOG_CATEGORIES . "
                  where categories_id = '" . (int)$category_id . "'");

    $db->Execute("delete from " . TABLE_BLOG_CATEGORIES_DESCRIPTION . "
                  where categories_id = '" . (int)$category_id . "'");

    $db->Execute("delete from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . "
                  where categories_id = '" . (int)$category_id . "'");

   
  }
  
  function zen_remove_archives($archives_id, $ptc = 'true') {
    global $db;
    $archives_image = $db->Execute("select archives_image
                                   from " . TABLE_BLOG_ARCHIVES . "
                                   where archives_id = '" . (int)$archives_id . "'");

    $duplicate_image = $db->Execute("select count(*) as total
                                     from " . TABLE_BLOG_ARCHIVES . "
                                     where archives_image = '" . zen_db_input($archives_image->fields['archives_image']) . "'");

    if ($duplicate_image->fields['total'] < 2 and $archives_image->fields['archives_image'] != '') {
      $archives_image = $archives_image->fields['archives_image'];
      $archives_image_extension = substr($archives_image, strrpos($archives_image, '.'));
			$archives_image_base = preg_replace('/' . $archives_image_extension . '/', '', $archives_image);

      $filename_medium = 'medium/' . $archives_image_base . IMAGE_SUFFIX_MEDIUM . $archives_image_extension;
			$filename_large = 'large/' . $archives_image_base . IMAGE_SUFFIX_LARGE . $archives_image_extension;

      if (file_exists(DIR_FS_CATALOG_IMAGES . $archives_image->fields['archives_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $archives_image->fields['archives_image']);
      }
      if (file_exists(DIR_FS_CATALOG_IMAGES . $filename_medium)) {
        @unlink(DIR_FS_CATALOG_IMAGES . $filename_medium);
      }
      if (file_exists(DIR_FS_CATALOG_IMAGES . $filename_large)) {
        @unlink(DIR_FS_CATALOG_IMAGES . $filename_large);
      }
    }

   

    $db->Execute("delete from " . TABLE_BLOG_ARCHIVES . "
                  where archives_id = '" . (int)$archives_id . "'");

//    if ($ptc == 'true') {
      $db->Execute("delete from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . "
                    where archives_id = '" . (int)$archives_id . "'");
//    }

    $db->Execute("delete from " . TABLE_BLOG_ARCHIVES_DESCRIPTION . "
                  where archives_id = '" . (int)$archives_id . "'");


    $archives_reviews = $db->Execute("select reviews_id
                                     from " . TABLE_BLOG_REVIEWS . "
                                     where archives_id = '" . (int)$archives_id . "'");

    while (!$archives_reviews->EOF) {
      $db->Execute("delete from " . TABLE_BLOG_REVIEWS_DESCRIPTION . "
                    where reviews_id = '" . (int)$archives_reviews->fields['reviews_id'] . "'");
      $archives_reviews->MoveNext();
    }
    $db->Execute("delete from " . TABLE_BLOG_REVIEWS . "
                  where archives_id = '" . (int)$archives_id . "'");

 

  }
  
?>