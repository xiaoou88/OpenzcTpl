<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: categories.php 15880 2010-04-11 16:24:30Z wilt $
 */

  require('includes/application_top.php');

  require(DIR_WS_MODULES . '/blog/prod_cat_header_code.php');
  require(DIR_WS_MODULES . '/blog/general.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');
 
  
  if (!isset($_SESSION['categories_products_sort_order'])) {
    $_SESSION['categories_products_sort_order'] = CATEGORIES_ARCHIVES_SORT_ORDER;
  }

  if (!isset($_GET['reset_categories_products_sort_order'])) {
    $reset_categories_products_sort_order = $_SESSION['categories_products_sort_order'];
  }

  if (zen_not_null($action)) {
    switch ($action) {
      case 'set_categories_archives_sort_order':
      $_SESSION['set_categories_archives_sort_order'] = $_GET['reset_categories_archives_sort_order'];
      $action='';
      zen_redirect(zen_href_link(FILENAME_BLOG,  'cPath=' . $_GET['cPath'] . ((isset($_GET['pID']) and !empty($_GET['pID'])) ? '&pID=' . $_GET['pID'] : '') . ((isset($_GET['page']) and !empty($_GET['page'])) ? '&page=' . $_GET['page'] : '')));
      break;
      case 'set_editor':
      // Reset will be done by init_html_editor.php. Now we simply redirect to refresh page properly.
      $action='';
      zen_redirect(zen_href_link(FILENAME_BLOG,  'cPath=' . $_GET['cPath'] . ((isset($_GET['pID']) and !empty($_GET['pID'])) ? '&pID=' . $_GET['pID'] : '') . ((isset($_GET['page']) and !empty($_GET['page'])) ? '&page=' . $_GET['page'] : '')));
      break;

      case 'update_category_status':
      // disable category and products including subcategories
      if (isset($_POST['categories_id'])) {
        $categories_id = zen_db_prepare_input($_POST['categories_id']);

        $categories = zen_get_archives_category_tree($categories_id, '', '0', '', true);

        for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
          $archives_ids = $db->Execute("select archives_id
                                         from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . "
                                         where categories_id = '" . (int)$categories[$i]['id'] . "'");

          while (!$archives_ids->EOF) {
            $archives[$archives_ids->fields['archives_id']]['categories'][] = $categories[$i]['id'];
            $archives_ids->MoveNext();
          }
        }

        // change the status of categories and products
        zen_set_time_limit(600);
        for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
          if ($_POST['categories_status'] == '1') {
            $categories_status = '0';
            $archives_status = '0';
          } else {
            $categories_status = '1';
            $archives_status = '1';
          }

          $sql = "update " . TABLE_BLOG_CATEGORIES . " set categories_status='" . $categories_status . "'
                  where categories_id='" . $categories[$i]['id'] . "'";
          $db->Execute($sql);

          // set products_status based on selection
          if ($_POST['set_archives_status'] == 'set_archives_status_nochange') {
            // do not change current product status
          } else {
            if ($_POST['set_archives_status'] == 'set_archives_status_on') {
              $archives_status = '1';
            } else {
              $archives_status = '0';
            }

            $sql = "select archives_id from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " where categories_id='" . $categories[$i]['id'] . "'";
            $category_archives = $db->Execute($sql);

            while (!$category_archives->EOF) {
              $sql = "update " . TABLE_PRODUCTS . " set archives_status='" . $archives_status . "' where archives_id='" . $category_archives->fields['archives_id'] . "'";
              $db->Execute($sql);
              $category_archives->MoveNext();
            }
          }
        } // for

      }
      zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $_GET['cPath'] . '&cID=' . $_GET['cID']));
      break;

     
      case 'insert_category':
      case 'update_category':
     
      if (isset($_POST['categories_id'])) $categories_id = zen_db_prepare_input($_POST['categories_id']);
      $sort_order = zen_db_prepare_input($_POST['sort_order']);

      $sql_data_array = array('sort_order' => (int)$sort_order);

      if ($action == 'insert_category') {
        $insert_sql_data = array('parent_id' => $current_category_id,
                                 'date_added' => 'now()');

        $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

        zen_db_perform(TABLE_BLOG_CATEGORIES, $sql_data_array);

        $categories_id = zen_db_insert_id();
        // check if [arent is restricted
        $sql = "select parent_id from " . TABLE_BLOG_CATEGORIES . " where categories_id = '" . $categories_id . "'";

        $parent_cat = $db->Execute($sql);
        
      } elseif ($action == 'update_category') {
        $update_sql_data = array('last_modified' => 'now()');

        $sql_data_array = array_merge($sql_data_array, $update_sql_data);

        zen_db_perform(TABLE_BLOG_CATEGORIES, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "'");
      }

      $languages = zen_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $categories_name_array = $_POST['categories_name'];
        $categories_description_array = $_POST['categories_description'];
        $language_id = $languages[$i]['id'];

        // clean $categories_description when blank or just <p /> left behind
        $sql_data_array = array('categories_name' => zen_db_prepare_input($categories_name_array[$language_id]),
                                'categories_description' => ($categories_description_array[$language_id] == '<p />' ? '' : zen_db_prepare_input($categories_description_array[$language_id])));

        if ($action == 'insert_category') {
          $insert_sql_data = array('categories_id' => $categories_id,
                                   'language_id' => $languages[$i]['id']);

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

          zen_db_perform(TABLE_BLOG_CATEGORIES_DESCRIPTION, $sql_data_array);
        } elseif ($action == 'update_category') {
          zen_db_perform(TABLE_BLOG_CATEGORIES_DESCRIPTION, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "' and language_id = '" . (int)$languages[$i]['id'] . "'");
        }
      }

      if ($_POST['categories_image_manual'] != '') {
        // add image manually
        $categories_image_name = $_POST['img_dir'] . $_POST['categories_image_manual'];
        $db->Execute("update " . TABLE_BLOG_CATEGORIES . "
                      set categories_image = '" . $categories_image_name . "'
                      where categories_id = '" . (int)$categories_id . "'");
      } else {
        if ($categories_image = new upload('categories_image')) {
          $categories_image->set_destination(DIR_FS_CATALOG_IMAGES . $_POST['img_dir']);
          if ($categories_image->parse() && $categories_image->save()) {
            $categories_image_name = $_POST['img_dir'] . $categories_image->filename;
          }
          if ($categories_image->filename != 'none' && $categories_image->filename != '' && $_POST['image_delete'] != 1) {
            // save filename when not set to none and not blank
            $db->Execute("update " . TABLE_BLOG_CATEGORIES . "
                          set categories_image = '" . $categories_image_name . "'
                          where categories_id = '" . (int)$categories_id . "'");
          } else {
            // remove filename when set to none and not blank
            if ($categories_image->filename != '' || $_POST['image_delete'] == 1) {
              $db->Execute("update " . TABLE_BLOG_CATEGORIES . "
                            set categories_image = ''
                            where categories_id = '" . (int)$categories_id . "'");
            }
          }
        }
      }

      zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&cID=' . $categories_id));
      break;

      case 'delete_category_confirm_old':
      // demo active test
      if (zen_admin_demo()) {
        $_GET['action']= '';
        $messageStack->add_session(ERROR_ADMIN_DEMO, 'caution');
        zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath));
      }
      if (isset($_POST['categories_id'])) {
        $categories_id = zen_db_prepare_input($_POST['categories_id']);

        $categories = zen_get_archives_category_tree($categories_id, '', '0', '', true);
        $archives = array();
        $archives_delete = array();

        for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
          $archives_ids = $db->Execute("select archives_id
                                           from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . "
                                           where categories_id = '" . (int)$categories[$i]['id'] . "'");

          while (!$archives_ids->EOF) {
            $archives[$archives_ids->fields['archives_id']]['categories'][] = $categories[$i]['id'];
            $archives_ids->MoveNext();
          }
        }

        reset($archives);
        while (list($key, $value) = each($archives)) {
          $category_ids = '';

          for ($i=0, $n=sizeof($value['categories']); $i<$n; $i++) {
            $category_ids .= "'" . (int)$value['categories'][$i] . "', ";
          }
          $category_ids = substr($category_ids, 0, -2);

          $check = $db->Execute("select count(*) as total
                                           from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . "
                                           where archives_id = '" . (int)$key . "'
                                           and categories_id not in (" . $category_ids . ")");
          if ($check->fields['total'] < '1') {
            $products_delete[$key] = $key;
          }
        }

        // removing categories can be a lengthy process
        zen_set_time_limit(600);
        for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
          zen_remove_category($categories[$i]['id']);
        }

        reset($archives_delete);
        while (list($key) = each($archives_delete)) {
          zen_remove_archives($key);
        }
      }


      zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath));
      break;

      //////////////////////////////////
      // delete new

      case 'delete_category_confirm':
      // demo active test
      if (zen_admin_demo()) {
        $_GET['action']= '';
        $messageStack->add_session(ERROR_ADMIN_DEMO, 'caution');
        zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath));
      }

      // future cat specific deletion
      $delete_linked = 'true';
      if ($_POST['delete_linked'] == 'delete_linked_no') {
        $delete_linked = 'false';
      } else {
        $delete_linked = 'true';
      }

      // delete category and products
      if (isset($_POST['categories_id']) && $_POST['categories_id'] != '' && is_numeric($_POST['categories_id']) && $_POST['categories_id'] != 0) {
        $categories_id = zen_db_prepare_input($_POST['categories_id']);

        // create list of any subcategories in the selected category,
        $categories = zen_get_archives_category_tree($categories_id, '', '0', '', true);

        zen_set_time_limit(600);

        // loop through this cat and subcats for delete-processing.
        for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
          $sql = "select archives_id from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " where categories_id='" . $categories[$i]['id'] . "'";
          $category_archives = $db->Execute($sql);

          while (!$category_archives->EOF) {
            $cascaded_prod_id_for_delete = $category_archives->fields['archives_id'];
            $cascaded_prod_cat_for_delete = array();
            $cascaded_prod_cat_for_delete[] = $categories[$i]['id'];
          
            require(DIR_WS_MODULES . '/blog/delete_archives_confirm.php');
            
            // THIS LINE COMMENTED BECAUSE IT'S DONE ALREADY DURING DELETE_PRODUCT_CONFIRM.PHP:
            //zen_remove_product($category_products->fields['products_id'], $delete_linked);
            $category_archives->MoveNext();
          }

          zen_remove_archives_category($categories[$i]['id']);

        } // end for loop
      }
      zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath));
      break;

      

      case 'move_category_confirm':
      if (isset($_POST['categories_id']) && ($_POST['categories_id'] != $_POST['move_to_category_id'])) {
        $categories_id = zen_db_prepare_input($_POST['categories_id']);
        $new_parent_id = zen_db_prepare_input($_POST['move_to_category_id']);

        $path = explode('_', zen_get_generated_archives_category_path_ids($new_parent_id));

        if (in_array($categories_id, $path)) {
          $messageStack->add_session(ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT, 'error');

          zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath));
        } else {

          $sql = "select count(*) as count from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " where categories_id='" . (int)$new_parent_id . "'";
          $zc_count_archives = $db->Execute($sql);

          if ( $zc_count_archives->fields['count'] > 0) {
            $messageStack->add_session(ERROR_CATEGORY_HAS_PRODUCTS, 'error');
          } else {
            $messageStack->add_session(SUCCESS_CATEGORY_MOVED, 'success');
          }

          $db->Execute("update " . TABLE_BLOG_CATEGORIES . "
                            set parent_id = '" . (int)$new_parent_id . "', last_modified = now()
                            where categories_id = '" . (int)$categories_id . "'");

          // fix here - if this is a category with subcats it needs to know to loop through
          

          zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $new_parent_id));
        }
      } else {
        $messageStack->add_session(ERROR_CANNOT_MOVE_CATEGORY_TO_CATEGORY_SELF . $cPath, 'error');
        zen_redirect(zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath));
      }

      break;
      // @@TODO where is move_product_confirm
      // @@TODO where is insert_product
      // @@TODO where is update_product

      case 'new_archives':
      if (isset($_GET['archives_type'])) {
        // see if this category is restricted
        $pieces = explode('_',$_GET['cPath']);
        $cat_id = $pieces[sizeof($pieces)-1];
        //	echo $cat_id;
        $sql = "select product_type_id from " . TABLE_PRODUCT_TYPES_TO_CATEGORY . " where category_id = '" . $cat_id . "'";
        $product_type_list = $db->Execute($sql);
        $sql = "select product_type_id from " . TABLE_PRODUCT_TYPES_TO_CATEGORY . " where category_id = '" . $cat_id . "' and product_type_id = '" . $_GET['product_type'] . "'";
        $product_type_good = $db->Execute($sql);
        if ($product_type_list->RecordCount() < 1 || $product_type_good->RecordCount() > 0) {
          $url = zen_get_all_get_params();
          $sql = "select type_handler from " . TABLE_PRODUCT_TYPES . " where type_id = '" . $_GET['product_type'] . "'";
          $handler = $db->Execute($sql);
          zen_redirect(zen_href_link($handler->fields['type_handler'] . '.php', zen_get_all_get_params()));
        } else {
          $messageStack->add(ERROR_CANNOT_ADD_PRODUCT_TYPE, 'error');
        }
      }
      break;
    }
  }

  // check if the catalog image directory exists
  if (is_dir(DIR_FS_CATALOG_IMAGES)) {
    if (!is_writeable(DIR_FS_CATALOG_IMAGES)) $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
  } else {
    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
<script language="javascript" src="includes/menu.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript">
<!--
function init()
{
  cssjsmenu('navbar');
  if (document.getElementById)
  {
    var kill = document.getElementById('hoverJS');
    kill.disabled = true;
  }
  if (typeof _editor_url == "string") HTMLArea.replaceAll();
}
// -->
</script>
<?php if ($action != 'edit_category_meta_tags') { // bof: categories meta tags ?>
<?php if ($editor_handler != '') include ($editor_handler); ?>
<?php } // meta tags disable editor eof: categories meta tags?>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="init()">
<div id="spiffycalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
<?php if ($action == '') { ?>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="smallText" align="center" width="100" valign="top"><?php echo TEXT_LEGEND; ?></td>
            <td class="smallText" align="center" width="100" valign="top"><?php echo TEXT_LEGEND_STATUS_OFF . '<br />' . zen_image(DIR_WS_IMAGES . 'icon_red_on.gif', IMAGE_ICON_STATUS_OFF); ?></td>
            <td class="smallText" align="center" width="100" valign="top"><?php echo TEXT_LEGEND_STATUS_ON . '<br />' . zen_image(DIR_WS_IMAGES . 'icon_green_on.gif', IMAGE_ICON_STATUS_ON); ?></td>
            <td class="smallText" align="center" width="100" valign="top"><?php echo TEXT_LEGEND_LINKED . '<br />' . zen_image(DIR_WS_IMAGES . 'icon_yellow_on.gif', IMAGE_ICON_LINKED); ?></td>
            <td class="smallText" align="center" width="150" valign="top"><?php echo TEXT_LEGEND_META_TAGS . '<br />' . TEXT_YES . '&nbsp;' . TEXT_NO . '<br />' . zen_image(DIR_WS_IMAGES . 'icon_edit_metatags_on.gif', ICON_METATAGS_ON) . '&nbsp;' . zen_image(DIR_WS_IMAGES . 'icon_edit_metatags_off.gif', ICON_METATAGS_OFF); ?></td>
          </tr>
        </table></td>
      </tr>
  <tr>
    <td class="smallText" width="100%" align="right">
<?php
      // toggle switch for editor
      echo TEXT_EDITOR_INFO . zen_draw_form('set_editor_form', FILENAME_BLOG, '', 'get') . '&nbsp;&nbsp;' . zen_draw_pull_down_menu('reset_editor', $editors_pulldown, $current_editor_key, 'onChange="this.form.submit();"') . zen_hide_session_id() .
            zen_draw_hidden_field('cID', $cPath) .
            zen_draw_hidden_field('cPath', $cPath) .
            zen_draw_hidden_field('pID', $_GET['pID']) .
            zen_draw_hidden_field('page', $_GET['page']) .
            zen_draw_hidden_field('action', 'set_editor') .
      '</form>';
?>
    </td>
  </tr>

  <tr>
    <td class="smallText" width="100%" align="right">
      <?php
      // check for which buttons to show for categories and products
	  $sql="select count(categories_id) as count from ".TABLE_BLOG_CATEGORIES." where parent_id='".$current_category_id."'";
      $check_categories = $db->Execute($sql);
	  $check_categories = $check_categories->fields['count'];
	
	  $sql="select count(archives_id) as count from ".TABLE_BLOG_ARCHIVES_TO_CATEGORIES." where categories_id='".$current_category_id."'";
      $check_archives = $db->Execute($sql);
	  $check_archives = $check_archives->fields['count'];
		
      $zc_skip_archives = false;
      $zc_skip_categories = false;

      if ($check_archives == 0) {
        $zc_skip_archives = false;
        $zc_skip_categories = false;
      }
      if ($check_categories > 0) {
        $zc_skip_archives = true;
        $zc_skip_categories = false;
      }
      if ($check_archives > 0) {
        $zc_skip_archives = false;
        $zc_skip_categories = true;
      }

      if ($zc_skip_archives == true) {
        // toggle switch for display sort order
        $categories_archives_sort_order_array = array(array('id' => '0', 'text' => TEXT_SORT_CATEGORIES_SORT_ORDER_ARCHIVES_NAME),
        array('id' => '1', 'text' => TEXT_SORT_CATEGORIES_NAME)
        );
      } else {
        // toggle switch for display sort order
        $categories_archives_sort_order_array = array(array('id' => '0', 'text' => TEXT_SORT_ARCHIVES_SORT_ORDER_ARCHIVES_NAME),
        array('id' => '1', 'text' => TEXT_SORT_ARCHIVES_NAME)
        );
      }

      echo TEXT_CATEGORIES_ARCHIVES_SORT_ORDER_INFO. zen_draw_form('set_categories_products_sort_order_form', FILENAME_BLOG, '', 'get') . '&nbsp;&nbsp;' . zen_draw_pull_down_menu('reset_categories_products_sort_order', $categories_archives_sort_order_array, $reset_categories_products_sort_order, 'onChange="this.form.submit();"') . zen_hide_session_id() .
            zen_draw_hidden_field('cID', $cPath) .
            zen_draw_hidden_field('cPath', $cPath) .
            zen_draw_hidden_field('pID', $_GET['pID']) .
            zen_draw_hidden_field('page', $_GET['page']) .
            zen_draw_hidden_field('action', 'set_categories_products_sort_order') .
      '</form>';
      ?>
    </td>
  </tr>

<?php } ?>
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
<?php
  require(DIR_WS_MODULES . 'blog_archives_listing.php');

  $heading = array();
  $contents = array();
  // Make an array of product types
  $sql = "select type_id, type_name from " . TABLE_PRODUCT_TYPES;
  $product_types = $db->Execute($sql);
  while (!$product_types->EOF) {
    $type_array[] = array('id' => $product_types->fields['type_id'], 'text' => $product_types->fields['type_name']);
    $product_types->MoveNext();
  }

  if (isset($_GET['cPath'])) {
    $cPath = $_GET['cPath'];
  }

  switch ($action) {
    case 'setflag_categories':
    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_STATUS_CATEGORY . '</b>');
    $contents = array('form' => zen_draw_form('categories', FILENAME_BLOG, 'action=update_category_status&cPath='.$_GET['cPath'].'&cID=' . $_GET['cID'], 'post', 'enctype="multipart/form-data"') . zen_draw_hidden_field('categories_id', $cInfo->categories_id) . zen_draw_hidden_field('categories_status', $cInfo->categories_status));
    $contents[] = array('text' => zen_get_archives_category_name($cInfo->categories_id, $_SESSION['languages_id']));
    $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_STATUS_WARNING . '<br /><br />');
    $contents[] = array('text' => TEXT_CATEGORIES_STATUS_INTRO . ' ' . ($cInfo->categories_status == '1' ? TEXT_CATEGORIES_STATUS_OFF : TEXT_CATEGORIES_STATUS_ON));
    if ($cInfo->categories_status == '1') {
      $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_STATUS_INFO . ' ' . TEXT_PRODUCTS_STATUS_OFF . zen_draw_hidden_field('set_products_status_off', true));
    } else {
      $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_STATUS_INFO . '<br />' .
      zen_draw_radio_field('set_products_status', 'set_products_status_on', true) . ' ' . TEXT_PRODUCTS_STATUS_ON . '<br />' .
      zen_draw_radio_field('set_products_status', 'set_products_status_off') . ' ' . TEXT_PRODUCTS_STATUS_OFF . '<br />' .
      zen_draw_radio_field('set_products_status', 'set_products_status_nochange') . ' ' . TEXT_PRODUCTS_STATUS_NOCHANGE);
    }

    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
    break;

    case 'new_category':
    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW_CATEGORY . '</b>');

    $contents = array('form' => zen_draw_form('newcategory', FILENAME_BLOG, 'action=insert_category&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"'));
    $contents[] = array('text' => TEXT_NEW_CATEGORY_INTRO);

    $category_inputs_string = '';
    $languages = zen_get_languages();
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . zen_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', '', zen_set_field_length(TABLE_CATEGORIES_DESCRIPTION, 'categories_name'));
    }

    $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_NAME . $category_inputs_string);
    $category_inputs_string = '';

    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;';
      if ($_SESSION['html_editor_preference_status']=='FCKEDITOR') {
                $oFCKeditor = new FCKeditor('categories_description[' . $languages[$i]['id']  . ']') ;
                $oFCKeditor->Value = zen_get_archives_category_description($cInfo->categories_id, $languages[$i]['id']);
                $oFCKeditor->Width  = '97%' ;
                $oFCKeditor->Height = '200' ;
//                $oFCKeditor->Config['ToolbarLocation'] = 'Out:xToolbar' ;
//                $oFCKeditor->Create() ;
                $output = $oFCKeditor->CreateHtml() ;
        $category_inputs_string .= '<br />' . $output;

//        $category_inputs_string .= '<IFRAME src= "' . DIR_WS_CATALOG . 'FCKeditor/fckeditor.html?FieldName=categories_description[' . $languages[$i]['id']  . ']&Upload=false&Browse=false&Toolbar=Short" width="97%" height="200" frameborder="no" scrolling="yes"></IFRAME>';
//        $category_inputs_string .= '<INPUT type="hidden" name="categories_description[' . $languages[$i]['id']  . ']" ' . 'value=' . "'" . zen_get_archives_category_description($cInfo->categories_id, $languages[$i]['id']) . "'>";
      } else {
        $category_inputs_string .= zen_draw_textarea_field('categories_description[' . $languages[$i]['id'] . ']', 'soft', '100%', '20', zen_get_archives_category_description($cInfo->categories_id, $languages[$i]['id']));
      }
    }
    $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_DESCRIPTION . $category_inputs_string);
    $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_IMAGE . '<br />' . zen_draw_file_field('categories_image'));
    $dir = @dir(DIR_FS_CATALOG_IMAGES);
    $dir_info[] = array('id' => '', 'text' => "Main Directory");
    while ($file = $dir->read()) {
      if (is_dir(DIR_FS_CATALOG_IMAGES . $file) && strtoupper($file) != 'CVS' && $file != "." && $file != "..") {
        $dir_info[] = array('id' => $file . '/', 'text' => $file);
      }
    }
    $dir->close();
    sort($dir_info);
    $default_directory = 'categories/';

    $contents[] = array('text' => TEXT_CATEGORIES_IMAGE_DIR . ' ' . zen_draw_pull_down_menu('img_dir', $dir_info, $default_directory));
    $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_IMAGE_MANUAL . '&nbsp;' . zen_draw_input_field('categories_image_manual'));

    $contents[] = array('text' => '<br />' . TEXT_SORT_ORDER . '<br />' . zen_draw_input_field('sort_order', '', 'size="6"'));
    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
    break;
    case 'edit_category':
    // echo 'I SEE ' . $_SESSION['html_editor_preference_status'];
    // set image delete
    $on_image_delete = false;
    $off_image_delete = true;
    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_CATEGORY . '</b>');

    $contents = array('form' => zen_draw_form('categories', FILENAME_BLOG, 'action=update_category&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"') . zen_draw_hidden_field('categories_id', $cInfo->categories_id));
    $contents[] = array('text' => TEXT_EDIT_INTRO);

    $languages = zen_get_languages();

    $category_inputs_string = '';
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . zen_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', zen_get_archives_category_name($cInfo->categories_id, $languages[$i]['id']), zen_set_field_length(TABLE_BLOG_CATEGORIES_DESCRIPTION, 'categories_name'));
    }
	
    $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_NAME . $category_inputs_string);
	
	$category_inputs_string = '';
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . zen_draw_input_field('categories_keywords[' . $languages[$i]['id'] . ']', zen_get_archives_category_field($cInfo->categories_id, $languages[$i]['id'],"categories_keywords"), zen_set_field_length(TABLE_BLOG_CATEGORIES_DESCRIPTION, 'categories_keywords'));
    }
	
    $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_KEYWORDS . $category_inputs_string);
	
	$category_inputs_string = '';
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . zen_draw_input_field('categories_seotitle[' . $languages[$i]['id'] . ']', zen_get_archives_category_field($cInfo->categories_id, $languages[$i]['id'],"categories_seotitle"), zen_set_field_length(TABLE_BLOG_CATEGORIES_DESCRIPTION, 'categories_seotitle'));
    }
	
    $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_SEOTITLE . $category_inputs_string);
	
	
    $category_inputs_string = '';
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' ;
      if ($_SESSION['html_editor_preference_status']=='FCKEDITOR') { 
                $oFCKeditor = new FCKeditor('categories_description[' . $languages[$i]['id']  . ']') ;
                $oFCKeditor->Value = zen_get_archives_category_description($cInfo->categories_id, $languages[$i]['id']);
                $oFCKeditor->Width  = '97%' ;
                $oFCKeditor->Height = '200' ;
//                $oFCKeditor->Config['ToolbarLocation'] = 'Out:xToolbar' ;
//                $oFCKeditor->Create() ;
                $output = $oFCKeditor->CreateHtml() ;
        $category_inputs_string .= '<br />' . $output;

      } else {
        $category_inputs_string .= zen_draw_textarea_field('categories_description[' . $languages[$i]['id'] . ']', 'soft', '100%', '20', zen_get_archives_category_description($cInfo->categories_id, $languages[$i]['id']));
      }
    }
    $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_DESCRIPTION . $category_inputs_string);
    $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_IMAGE . '<br />' . zen_draw_file_field('categories_image'));

    $dir = @dir(DIR_FS_CATALOG_IMAGES);
    $dir_info[] = array('id' => '', 'text' => "Main Directory");
    while ($file = $dir->read()) {
      if (is_dir(DIR_FS_CATALOG_IMAGES . $file) && strtoupper($file) != 'CVS' && $file != "." && $file != "..") {
        $dir_info[] = array('id' => $file . '/', 'text' => $file);
      }
    }
    $dir->close();
    sort($dir_info);
    $default_directory = substr( $cInfo->categories_image, 0,strpos( $cInfo->categories_image, '/')+1);

    $contents[] = array('text' => TEXT_CATEGORIES_IMAGE_DIR . ' ' . zen_draw_pull_down_menu('img_dir', $dir_info, $default_directory));

    $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_IMAGE_MANUAL . '&nbsp;' . zen_draw_input_field('categories_image_manual'));

    $contents[] = array('text' => '<br />' . zen_info_image($cInfo->categories_image, $cInfo->categories_name));
    $contents[] = array('text' => '<br />' . $cInfo->categories_image);
    $contents[] = array('text' => '<br />' . TEXT_IMAGES_DELETE . ' ' . zen_draw_radio_field('image_delete', '0', $off_image_delete) . '&nbsp;' . TABLE_HEADING_NO . ' ' . zen_draw_radio_field('image_delete', '1', $on_image_delete) . '&nbsp;' . TABLE_HEADING_YES);

    $contents[] = array('text' => '<br />' . TEXT_EDIT_SORT_ORDER . '<br />' . zen_draw_input_field('sort_order', $cInfo->sort_order, 'size="6"'));
    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
    

    break;
    case 'delete_category':
    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_CATEGORY . '</b>');

    $contents = array('form' => zen_draw_form('categories', FILENAME_BLOG, 'action=delete_category_confirm&cPath=' . $cPath) . zen_draw_hidden_field('categories_id', $cInfo->categories_id));
    $contents[] = array('text' => TEXT_DELETE_CATEGORY_INTRO);
    $contents[] = array('text' => '<br />' . TEXT_DELETE_CATEGORY_INTRO_LINKED_ARCHIVES);
    $contents[] = array('text' => '<br /><b>' . $cInfo->categories_name . '</b>');
    if ($cInfo->childs_count > 0) $contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_CHILDS, $cInfo->childs_count));
    if ($cInfo->products_count > 0) $contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_PRODUCTS, $cInfo->products_count));
  
    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
    break;

    // bof: categories meta tags
    case 'edit_category_meta_tags':
    $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_CATEGORY_META_TAGS . '</strong>');

    $contents = array('form' => zen_draw_form('categories', FILENAME_BLOG, 'action=update_category_meta_tags&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"') . zen_draw_hidden_field('categories_id', $cInfo->categories_id));
    $contents[] = array('text' => TEXT_EDIT_CATEGORIES_META_TAGS_INTRO . ' - <strong>' . $cInfo->categories_id . ' ' . $cInfo->categories_name . '</strong>');

    $languages = zen_get_languages();

    $category_inputs_string_metatags_title = '';
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string_metatags_title .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['metatags_title']) . '&nbsp;' . zen_draw_input_field('metatags_title[' . $languages[$i]['id'] . ']', zen_get_category_metatags_title($cInfo->categories_id, $languages[$i]['id']), zen_set_field_length(TABLE_METATAGS_CATEGORIES_DESCRIPTION, 'metatags_title'));
    }
    $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_META_TAGS_TITLE . $category_inputs_string_metatags_title);

    $category_inputs_string_metatags_keywords = '';
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string_metatags_keywords .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['metatags_keywords']) . '&nbsp;' ;
      $category_inputs_string_metatags_keywords .= zen_draw_textarea_field('metatags_keywords[' . $languages[$i]['id'] . ']', 'soft', '100%', '20', zen_get_category_metatags_keywords($cInfo->categories_id, $languages[$i]['id']));
    }
    $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_META_TAGS_KEYWORDS . $category_inputs_string_metatags_keywords);

    $category_inputs_string_metatags_description = '';
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $category_inputs_string_metatags_description .= '<br />' . zen_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' ;
      $category_inputs_string_metatags_description .= zen_draw_textarea_field('metatags_description[' . $languages[$i]['id'] . ']', 'soft', '100%', '20', zen_get_category_metatags_description($cInfo->categories_id, $languages[$i]['id']));
    }
    $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_META_TAGS_DESCRIPTION . $category_inputs_string_metatags_description);

    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_save.gif', IMAGE_SAVE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
	
    break;
    // eof: categories meta tags

    case 'move_category':
    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_MOVE_CATEGORY . '</b>');

    $contents = array('form' => zen_draw_form('categories', FILENAME_BLOG, 'action=move_category_confirm&cPath=' . $cPath) . zen_draw_hidden_field('categories_id', $cInfo->categories_id));
    $contents[] = array('text' => sprintf(TEXT_MOVE_CATEGORIES_INTRO, $cInfo->categories_name));
    $contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $cInfo->categories_name) . '<br />' . zen_draw_pull_down_menu('move_to_category_id', zen_get_archives_category_tree(), $current_category_id));
    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_move.gif', IMAGE_MOVE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
    break;
    case 'delete_product':
    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_PRODUCT . '</b>');

    $contents = array('form' => zen_draw_form('products', FILENAME_BLOG, 'action=delete_product_confirm&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . zen_draw_hidden_field('products_id', $pInfo->products_id));
    $contents[] = array('text' => TEXT_DELETE_PRODUCT_INTRO);
    $contents[] = array('text' => '<br /><b>' . $pInfo->products_name . ' ID#' . $pInfo->products_id . '</b>');

    $product_categories_string = '';
    $product_categories = zen_generate_category_path($pInfo->products_id, 'product');
    for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
      $category_path = '';
      for ($j = 0, $k = sizeof($product_categories[$i]); $j < $k; $j++) {
        $category_path .= $product_categories[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
      }
      $category_path = substr($category_path, 0, -16);
      $product_categories_string .= zen_draw_checkbox_field('product_categories[]', $product_categories[$i][sizeof($product_categories[$i])-1]['id'], true) . '&nbsp;' . $category_path . '<br />';
    }
    $product_categories_string = substr($product_categories_string, 0, -4);

    $contents[] = array('text' => '<br />' . $product_categories_string);
    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
    break;
    case 'move_product':
    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_MOVE_PRODUCT . '</b>');

    $contents = array('form' => zen_draw_form('products', FILENAME_BLOG, 'action=move_product_confirm&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . zen_draw_hidden_field('products_id', $pInfo->products_id));
    $contents[] = array('text' => sprintf(TEXT_MOVE_PRODUCTS_INTRO, $pInfo->products_name));
    $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><b>' . zen_output_generated_category_path($pInfo->products_id, 'product') . '</b>');
    $contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $pInfo->products_name) . '<br />' . zen_draw_pull_down_menu('move_to_category_id', zen_get_category_tree(), $current_category_id));
    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_move.gif', IMAGE_MOVE) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
    break;
    case 'copy_to':
    $copy_attributes_delete_first = '0';
    $copy_attributes_duplicates_skipped = '0';
    $copy_attributes_duplicates_overwrite = '0';
    $copy_attributes_include_downloads = '1';
    $copy_attributes_include_filename = '1';

    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_COPY_TO . '</b>');
    // WebMakers.com Added: Split Page
    if (empty($pInfo->products_id)) {
      $pInfo->products_id= $pID;
    }

    $contents = array('form' => zen_draw_form('copy_to', FILENAME_BLOG, 'action=copy_to_confirm&cPath=' . $cPath . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . zen_draw_hidden_field('products_id', $pInfo->products_id));
    $contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
    $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_PRODUCT . '<br /><b>' . $pInfo->products_name  . ' ID#' . $pInfo->products_id . '</b>');
    $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><b>' . zen_output_generated_category_path($pInfo->products_id, 'product') . '</b>');
    $contents[] = array('text' => '<br />' . TEXT_CATEGORIES . '<br />' . zen_draw_pull_down_menu('categories_id', zen_get_category_tree(), $current_category_id));
    $contents[] = array('text' => '<br />' . TEXT_HOW_TO_COPY . '<br />' . zen_draw_radio_field('copy_as', 'link', true) . ' ' . TEXT_COPY_AS_LINK . '<br />' . zen_draw_radio_field('copy_as', 'duplicate') . ' ' . TEXT_COPY_AS_DUPLICATE);

    // only ask about attributes if they exist
    if (zen_has_product_attributes($pInfo->products_id, 'false')) {
      $contents[] = array('text' => '<br />' . zen_image(DIR_WS_IMAGES . 'pixel_black.gif','','100%','3'));
      $contents[] = array('text' => '<br />' . TEXT_COPY_ATTRIBUTES_ONLY);
      $contents[] = array('text' => '<br />' . TEXT_COPY_ATTRIBUTES . '<br />' . zen_draw_radio_field('copy_attributes', 'copy_attributes_yes', true) . ' ' . TEXT_COPY_ATTRIBUTES_YES . '<br />' . zen_draw_radio_field('copy_attributes', 'copy_attributes_no') . ' ' . TEXT_COPY_ATTRIBUTES_NO);
      // future          $contents[] = array('align' => 'center', 'text' => '<br />' . ATTRIBUTES_NAMES_HELPER . '<br />' . zen_draw_separator('pixel_trans.gif', '1', '10'));
      $contents[] = array('text' => '<br />' . zen_image(DIR_WS_IMAGES . 'pixel_black.gif','','100%','3'));
    }

    $contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_copy.gif', IMAGE_COPY) . ' <a href="' . zen_href_link(FILENAME_BLOG, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id . (isset($_GET['page']) ? '&page=' . $_GET['page'] : '')) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');

    break;
 
    
  

  } // switch

  if ( (zen_not_null($heading)) && (zen_not_null($contents)) ) {
    echo '            <td valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);
 
    echo '            </td>' . "\n";
  }
?>

          </tr>
          <tr>
            <td class="alert" colspan="3" width="100%" align="center">
<?php
  
  // warning if products are in top level categories
  $check_products_top_categories = $db->Execute("select count(*) as products_errors from " . TABLE_BLOG_ARCHIVES_TO_CATEGORIES . " where categories_id = 0");
  if ($check_products_top_categories->fields['products_errors'] > 0) {
    echo WARNING_PRODUCTS_IN_TOP_INFO . $check_products_top_categories->fields['products_errors'] . '<br />';
  }
?>
            </td>
          </tr>
          <tr>
<?php
// Split Page
if ($products_query_numrows > 0) {
  if (empty($pInfo->products_id)) {
    $pInfo->products_id= $pID;
  }
?>
            <td class="smallText" align="center"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_RESULTS_CATEGORIES, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS) . '<br>' . $products_split->display_links($products_query_numrows, MAX_DISPLAY_RESULTS_CATEGORIES, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], zen_get_all_get_params(array('page', 'info', 'x', 'y', 'pID')) ); ?></td>

<?php
}
// Split Page
?>
          </tr>
        </table></td>
      </tr>
    </table>
    </td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
