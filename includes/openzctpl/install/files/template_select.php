<?php
/**
 * @package admin
 * @copyright Copyright 2003-2016 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: Author: DrByte  Sun Oct 18 02:12:50 2015 -0400 Modified in v1.5.5 $
 */

  require('includes/application_top.php');
// get an array of template info
  $dir = @dir(DIR_FS_CATALOG_TEMPLATES);
  if (!$dir) die('DIR_FS_CATALOG_TEMPLATES NOT SET');
  while ($file = $dir->read()) {
    if (is_dir(DIR_FS_CATALOG_TEMPLATES . $file) && $file != 'template_default') {
      if (file_exists(DIR_FS_CATALOG_TEMPLATES . $file . '/template_info.php')) {
        require(DIR_FS_CATALOG_TEMPLATES . $file . '/template_info.php');
        $template_info[$file] = array(
			'name' => $template_name,'version' => $template_version,'author' => $template_author,'description' => $template_description,'screenshot' => $template_screenshot,'template_openzc'=>$openzc_template_engine
		);
      }
    }
  }
  $dir->close();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (zen_not_null($action)) {
    switch ($action) {
      case 'insert':
        // @TODO: add duplicate-detection and empty-submission detection
        $sql = "select * from " . TABLE_TEMPLATE_SELECT . " where template_language = :lang:";
        $sql = $db->bindVars($sql, ':lang:', $_POST['lang'], 'string');
        $check_query = $db->Execute($sql);
        if ($check_query->RecordCount() < 1 ) {
          $sql = "insert into " . TABLE_TEMPLATE_SELECT . " (template_dir, template_language,template_device) values (:tpl:, :lang:,:dev:)";
          $sql = $db->bindVars($sql, ':tpl:', $_POST['ln'], 'string');
          $sql = $db->bindVars($sql, ':lang:', $_POST['lang'], 'string');
		  $sql = $db->bindVars($sql, ':dev:', $_POST['de'], 'string');
          $db->Execute($sql);
          $_GET['tID'] = $db->Insert_ID();
        }
        $action="";
        break;
      case 'save':
        $sql = "update " . TABLE_TEMPLATE_SELECT . " set template_dir = :tpl:,template_device = :dev: where template_id = :id:";
        $sql = $db->bindVars($sql, ':tpl:', $_POST['ln'], 'string');
		$sql = $db->bindVars($sql, ':dev:', $_POST['de'], 'string');
        $sql = $db->bindVars($sql, ':id:', $_GET['tID'], 'integer');
        $db->Execute($sql);
        break;
      case 'deleteconfirm':
        $check_query = $db->Execute("select template_language from " . TABLE_TEMPLATE_SELECT . " where template_id = '" . (int)$_POST['tID'] . "'");
        if ( $check_query->fields['template_language'] != 0 ) {
          $db->Execute("delete from " . TABLE_TEMPLATE_SELECT . " where template_id = '" . (int)$_POST['tID'] . "'");
          zen_redirect(zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page']));
        }
        $action="";
      break;
	  case "setroute":
		if(is_file("../".DIR_WS_TEMPLATE."page_route.php")){
			require("../".DIR_WS_TEMPLATE."page_route.php");
			require("../".DIR_WS_INCLUDES."openzctpl/config/tplpage.php");
		}
		if(is_array($custom_page)){
			foreach($custom_page as $k => $v){
				$custom[$v]=true;
			}
		}
		if($_POST){
			if(isset($_POST['delete'])){
				foreach($custom_page as $k => $v){if($v==$_POST['file']){unset($custom_page[$k]);}}
				unset($tplpage_route[$_POST['file']]);
			}else{
				$tplpage_route[$_POST['key']][0]=$_POST['tpl'];
				$tplpage_route[$_POST['key']][1]=$_POST['cache_status'];
			}
			
			$string="<?php \r\n";
			$string.="\$custom_page=[\r\n";
			foreach($custom_page as $k => $v){
				$string.='"'.$k.'"=>'.'"'.$v.'",'."\r\n";
			}
			$string.="];\r\n";
			$string.="\$tplpage_route=[\r\n";
			foreach($tplpage_route as $k => $v){
				if(!isset($v[1])){$v[1]="off";}
				$string.='"'.$k.'"=>'.'["'.$v[0].'","'.$v[1].'"],'."\r\n";
			}
			$string.="];\r\n";
			$route = fopen("../".DIR_WS_TEMPLATE."page_route.php", "w") or die("Unable to open file!");
			fwrite($route, $string);
			fclose($route);
			zen_redirect(zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'].'&tID='.$_GET['tID'].'&action=setroute'));
		}
	  break;
	  case "newtpl":
		if(is_file("../".DIR_WS_TEMPLATE."page_route.php")){
			require("../".DIR_WS_TEMPLATE."page_route.php");
			require("../".DIR_WS_INCLUDES."openzctpl/config/tplpage.php");
		}
		$error_php=$error_tpl=$error_name="";
		if($_POST){
			if(!strstr($_POST['phpfile'],".php")){$error_php="Please enter the correct php file name!";}
			if(!strstr($_POST['tplfile'],".tpl")){$error_tpl="Please enter the correct tpl file name!";}
			if(!$_POST['pagename']){$error_name="Can not be empty!";}
		if($error_php || $error_tpl || $error_name){
			//nothing
		}else{
			$custom_page[$_POST['pagename']]=$_POST['phpfile'];
			$tplpage_route[$_POST['phpfile']][0]=$_POST['tplfile'];
			$tplpage_route[$_POST['phpfile']][1]=$_POST['cache_status'];
			$string="<?php \r\n";
			$string.="\$custom_page=[\r\n";
			foreach($custom_page as $k => $v){
				if($v==$_POST['phpfile'] && $k!=$_POST['pagename']){
					unset($custom_page[$k]);
				}else{
					$string.='"'.$k.'"=>'.'"'.$v.'",'."\r\n";
				}
			}
			$string.="];\r\n";
			$string.="\$tplpage_route=[\r\n";
			foreach($tplpage_route as $k => $v){
				if(!isset($v[1])){$v[1]="off";}
				$string.='"'.$k.'"=>'.'["'.$v[0].'","'.$v[1].'"],'."\r\n";
			}
			$string.="];\r\n";
			$route = fopen("../".DIR_WS_TEMPLATE."page_route.php", "w") or die("Unable to open file!");
			fwrite($route, $string);
			fclose($route);
			$file=fopen("../".DIR_WS_TEMPLATE.$_POST['phpfile'], "w") or die("Unable to open file!");
			fwrite($file,"");
			fclose($file);
			zen_redirect(zen_href_link(FILENAME_TEMPLATE_SELECT,'page='.$_GET['page'].'&tID='.$_GET['tID'].'&action=setroute'));
		}
		}
	  break;
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
<style>tr.tpl:hover{background:#e7e6e0}</style>
<script language="javascript" src="includes/menu.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript">
  <!--
  function init()
  {
    cssjsmenu('navbar');
    if (document.getElementById)
    {
      var kill = document.getElementById('hoverJS');
      kill.disabled = true;
    }
  }
  // -->
</script>
</head>
<body onLoad="init()">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->
<!-- body //-->
<?php 
 
 if($action=="setroute"){ 
	$template_name=str_replace(DIR_WS_INCLUDES."templates/","",DIR_WS_TEMPLATE);
	$template_name=str_replace("/","",$template_name);
	
	if($template_info[$template_name]['template_openzc']){
		
		
	?>
		<table border="0" width="100%" cellspacing="2" cellpadding="2">
		<tr>
			<!-- body_text //-->
			<td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
				  <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
					  <tr>
						<td class="pageHeading"><?php echo "Page Route Setting: (".$template_name.")"; ?> 
						<button><a href="<?php echo zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $_GET['tID']."&action=newtpl");?>">Create New Template Page</a></button>
						</td>
						<td class="pageHeading" align="right"><?php echo zen_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
					  </tr>
					</table></td>
				</tr>
			</td>
		</tr>
		<tr>
          <td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                    <tr class="dataTableHeadingRow">
                      <td class="dataTableHeadingContent">PHP Template File</td>
                      <td class="dataTableHeadingContent">TPL Template File</td>
					  <td class="dataTableHeadingContent">Cache status</td>
                      <td class="dataTableHeadingContent">Template description</td>
					  <td class="dataTableHeadingContent">Action</td>
                    </tr>
				</td>
			 </tr>
			
			 <?php
			 foreach($tplpage_route as $k => $v){
			  $str=zen_draw_form('setroute', FILENAME_TEMPLATE_SELECT, 'page='.$_GET['page'].'&tID='.$_GET['tID'].'&action=setroute');
			  $str.="<tr class='tpl' height='30px'>\r\n";
			  $str.="<td>".$k."</td>\r\n";
			  $str.="<input type='hidden' name='key' value='".$k."'/>";
			  $str.="<td><input size='50' type='text' style='padding-left:10px' name='tpl' value='".$v[0]."'/></td>\r\n";
			  if($v[1]=="on"){
				  $str.="<td><select name='cache_status'><option value='on' selected=''>on</option><option value='off'>off</option></select></td>\r\n";
			  }else{
				  $str.="<td><select name='cache_status'><option value='on'>on</option><option value='off' selected=''>off</option></select></td>\r\n";
			  }
			  
			  $str.="<td>".constant($tplpage[$k])."</td>\r\n";
			  $str.="<td><input type='submit' value='save'/>";
			  if(is_array($custom)){
				  if(isset($custom[$k])){
					  $str.="  ".zen_draw_form('setroute', FILENAME_TEMPLATE_SELECT, 'page='.$_GET['page'].'&tID='.$_GET['tID'].'&action=setroute')."<input type='hidden' name='delete' value='true'/><input type='hidden' name='file' value='".$k."'/><input type='submit' value='delete'/></form></td>\r\n";
				  }
			  }
			  $str.="</td>\r\n";
			  $str.="</tr></form>\r\n";
			  echo $str;
			}?>
			
			</table>
			</td>
		</tr>
		
		</table>
 <?php }}else if($action=="newtpl"){
	 $str=zen_draw_form('newtpl', FILENAME_TEMPLATE_SELECT, 'page='.$_GET['page'].'&tID='.$_GET['tID'].'&action=newtpl');
	 $str.='<table border="0" width="100%" cellspacing="0" cellpadding="2"><tbody><tr><td class="infoBoxContent">Please fill out the following information for the new template page</td></tr>';
	 $str.='<tr><td class="infoBoxContent"><br>Template page name:'."<b style='color:red'>".$error_name."</b>".'<br><input type="text" name="pagename" size="51" maxlength="96"> eg:custome_page</td></tr>';
     $str.='<tr><td class="infoBoxContent"><br>Template page php file:'."<b style='color:red'>".$error_php."</b>".'<br><input type="text" name="phpfile" size="51" maxlength="96"> eg: tpl_index_default.php</td></tr>';
	 $str.='<tr><td class="infoBoxContent"><br>Template page tpl file:'."<b style='color:red'>".$error_tpl."</b>".'<br><input type="text" name="tplfile" size="51" maxlength="96"> eg: index/index.tpl</td></tr>';
	 $str.='<tr><td class="infoBoxContent"><br>Template page cache status: <select name="cache_status"><option value="on">on</option><option value="off">off</option></select> * Whether to allow caching of pages bound to this template</td></tr>';
	 $str.='<tr><td align="center" class="infoBoxContent">'.zen_image_submit('button_save.gif', IMAGE_SAVE).'</td></tr>';
     $str.='</tbody></table></form>';
	 echo $str;
 }else{?>
<table border="0" width="100%" cellspacing="2" cellpadding="2" <?php if($action=="setroute"){echo "style='display:none;'";}?>>
  <tr>
    <!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                <td class="pageHeading" align="right"><?php echo zen_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                    <tr class="dataTableHeadingRow">
                      <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LANGUAGE; ?></td>
                      <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                      <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DIRECTORY; ?></td>
                      <td class="dataTableHeadingContent" align="center"><?php echo "Template Device"; ?>&nbsp;</td>
					  <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                    </tr>
                    <?php
  $template_query_raw = "select * from " . TABLE_TEMPLATE_SELECT;
  $template_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $template_query_raw, $template_query_numrows);
  $templates = $db->Execute($template_query_raw);
  while (!$templates->EOF) {
    if ((!isset($_GET['tID']) || (isset($_GET['tID']) && ($_GET['tID'] == $templates->fields['template_id']))) && !isset($tInfo) && (substr($action, 0, 3) != 'new')) {
      $tInfo = new objectInfo($templates->fields);
	  
    }

    if (isset($tInfo) && is_object($tInfo) && ($templates->fields['template_id'] == $tInfo->template_id)) {
      echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $tInfo->template_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $templates->fields['template_id']) . '\'">' . "\n";
    }
    if ($templates->fields['template_language'] == 0) {
      $template_language = "Default(All)";
    } else {
      $ln = $db->Execute("select name from " . TABLE_LANGUAGES . " where languages_id = '" . $templates->fields['template_language'] . "'");
      $template_language = $ln->fields['name'];
    }
	
?>
                    <td class="dataTableContent"><?php echo $template_language; ?></td>
                    <td class="dataTableContent"><?php echo $template_info[$templates->fields['template_dir']]['name']; ?></td>
                    <td class="dataTableContent" align="center"><?php echo $templates->fields['template_dir']; ?></td>
					<td class="dataTableContent" align="center">
						<?php 
						if($templates->fields['template_device']==0){
							echo "Computer(PC)";
						}else{
							echo "Mobile devices";
						}
						
						?>
					</td>
                    <td class="dataTableContent" align="right">
                      <?php if (isset($tInfo) && is_object($tInfo) && ($templates->fields['template_id'] == $tInfo->template_id) ) { echo zen_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $templates->fields['template_id']) . '">' . zen_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>
                      &nbsp;</td>
                    </tr>
<?php
    $templates->MoveNext();
  }
?>
                    <tr>
                      <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                          <tr>
                            <td class="smallText" valign="top">
							<?php echo $template_split->display_count($template_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_TEMPLATES); ?>
							</td>
                            <td class="smallText" align="right">
							<?php echo $template_split->display_links($template_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
							</td>
                          </tr>
                          <?php
  if (empty($action)) {
?>
                          <tr>
                            <td colspan="2" align="right">
							<?php echo '<a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&action=new') . '">' . zen_image_button('button_new_language.gif', IMAGE_NEW_TEMPLATE) . '</a>'; ?>
							</td>
                          </tr>
                          <?php
  }
?>
                        </table></td>
                    </tr>
                  </table></td>
                <?php
			
  $heading = array();
  $contents = array();
  $template_device=array(
		"0"=>array("id"=>0,"text"=>"Computer(PC)"),
		"1"=>array("id"=>1,"text"=>"Mobile device")
	  );
  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW_TEMPLATE . '</b>');

      $contents = array('form' => zen_draw_form('zones', FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);
      while (list ($key, $value) = each($template_info) ) {
        $template_array[] = array('id' => $key, 'text' => $value['name']);
      }
      $lns = $db->Execute("select name, languages_id from " . TABLE_LANGUAGES);
      while (!$lns->EOF) {
        $language_array[] = array('text' => $lns->fields['name'], 'id' => $lns->fields['languages_id']);
        $lns->MoveNext();
      }
      $contents[] = array('text' => '<br>' . TEXT_INFO_TEMPLATE_NAME . '<br>' . zen_draw_pull_down_menu('ln', $template_array));
	  $contents[] = array('text' => '<br>Template Device<br>' . zen_draw_pull_down_menu('de', $template_device,""));
      $contents[] = array('text' => '<br>' . TEXT_INFO_LANGUAGE_NAME . '<br>' . zen_draw_pull_down_menu('lang', $language_array, $_POST['lang']));
      $contents[] = array('align' => 'center', 'text' => '<br>' . zen_image_submit('button_insert.gif', IMAGE_INSERT) . '&nbsp;<a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page']) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    case 'edit':
      $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_TEMPLATE . '</b>');
      $contents = array('form' => zen_draw_form('templateselect', FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $tInfo->template_id . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
      reset($template_info);
      while (list ($key, $value) = each($template_info) ) {
        $template_array[] = array('id' => $key, 'text' => $value['name']);
      }
      $contents[] = array('text' => '<br>' . TEXT_INFO_TEMPLATE_NAME . '<br>' . zen_draw_pull_down_menu('ln', $template_array, $templates->fields['template_dir']));
	  
	 
	  $contents[] = array('text' => '<br>Template Device<br>' . zen_draw_pull_down_menu('de', $template_device, $tInfo->template_device));
      $contents[] = array('align' => 'center', 'text' => '<br>' . zen_image_submit('button_update.gif', IMAGE_UPDATE) . '&nbsp;<a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $tInfo->template_id) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
	  
      break;
	
    case 'delete':
      $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_TEMPLATE . '</b>');

      $contents = array('form' => zen_draw_form('zones', FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&action=deleteconfirm') . zen_draw_hidden_field('tID', $tInfo->template_id));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br><b>' . $template_info[$tInfo->template_dir]['name'] . '</b>');
      $contents[] = array('align' => 'center', 'text' => '<br>' . zen_image_submit('button_delete.gif', IMAGE_DELETE) . '&nbsp;<a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $tInfo->template_id) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    default:
	
      if (isset($tInfo) && is_object($tInfo)) {
		
        $heading[] = array('text' => '<b>' . $template_info[$tInfo->template_dir]['name'] . '</b>');
        if ($tInfo->template_language == 0) {
          $contents[] = array('align' => 'center', 'text' => '<a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $tInfo->template_id . '&action=edit') . '">' . zen_image_button('button_edit.gif', IMAGE_EDIT) . '</a>');
        } else {
          $contents[] = array('align' => 'center', 'text' => '<a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $tInfo->template_id . '&action=edit') . '">' . zen_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $tInfo->template_id . '&action=delete') . '">' . zen_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
        }
		
		
		if($template_info[$tInfo->template_dir]['template_openzc']==true){
			$contents[] = array('align' => 'center', 'text' => '<button><a href="' . zen_href_link(FILENAME_TEMPLATE_SELECT, 'page=' . $_GET['page'] . '&tID=' . $tInfo->template_id . '&action=setroute') . '">' ."Page Route Setting" . '</a></button>');
		}
        $contents[] = array('text' => '<br>' . TEXT_INFO_TEMPLATE_AUTHOR  . $template_info[$tInfo->template_dir]['author']);
        $contents[] = array('text' => '<br>' . TEXT_INFO_TEMPLATE_VERSION  . $template_info[$tInfo->template_dir]['version']);
        $contents[] = array('text' => '<br>' . TEXT_INFO_TEMPLATE_DESCRIPTION  . '<br />' . $template_info[$tInfo->template_dir]['description']);
        $contents[] = array('text' => '<br>' . TEXT_INFO_TEMPLATE_INSTALLED  . '<br />');
        while (list ($key, $value) = each($template_info) ) {
          $contents[] = array('text' => '<a href="' . DIR_WS_CATALOG_TEMPLATE . $key . '/images/' . $value['screenshot'] . '" target = "_blank">' . zen_image_button('button_preview.gif', IMAGE_PREVIEW) . '</a>&nbsp;&nbsp;' . $value['name']);
        }
      }
      break;
  }

  if ( (zen_not_null($heading)) && (zen_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
              </tr>
            </table></td>
        </tr>
      </table></td>
    <!-- body_text_eof //-->
  </tr>
 </table><?php }?>
<!-- body_eof //-->
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
