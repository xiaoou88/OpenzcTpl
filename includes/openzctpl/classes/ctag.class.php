<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class cTag{
    function tag_field($GET,$field){
        global $currencies,$blog_class,$product_class;
				
		if($_GET['main_page']==FILENAME_BLOG_ARCHIVES && !empty($_GET['archives_id'])){
		
			if(isset($GLOBALS['archives_info'])){$archives_info=$GLOBALS['archives_info'];}
			else{
				$sql="select a.*,ad.* from ".TABLE_BLOG_ARCHIVES." a left join ".TABLE_BLOG_ARCHIVES_DESCRIPTION." ad on a.archives_id=ad.archives_id where a.archives_id='".$_GET['archives_id']."' and ad.language_id='".(int)$_SESSION['languages_id']."'";
				$archives=openzcQuery($sql);
				$archives_info=openzc_table_to_list($archives)[0];
				$archives_info["archives_image"]=$blog_class->get_archives_image($archives_info["archives_image"]);
				$GLOBALS['archives_info']=$archives_info;
			}
			
			foreach($archives_info as $k => $v){
				if($field==$k){return $v;}
			} 
		}
		
        switch ($field) {
			case "securityToken":
				return $_SESSION['securityToken'];
			break;
			case 'hidden':
				foreach($GET as $k => $v){
					if($v){$str.="<input type='hidden' name='".$k."' value='".$v."'/>\r\n";}
                }
				return $str;
			break;
			case 'position':
				global $breadcrumb;
				if(strstr($_GET['main_page'],"blog")){
					$position=$this->get_archives_path(BREAD_CRUMBS_SEPARATOR);
				}else{
					$position=$breadcrumb->trail(BREAD_CRUMBS_SEPARATOR);
					$position=str_replace("NAVBAR_TITLE",NAVBAR_TITLE,$position);
				}

				return $position;
			break;
			case 'template':
				$str="/".DIR_WS_TEMPLATE;
				return $str;
			break;
			case 'dirpath':
				global $tpl;
				$array=explode("/",$tpl);
				$dir=str_replace($array[count($array)-1],"",$tpl);
				$str="/".TEMPLATE_TPL.$dir."/";
				$str=str_replace("//","/",$str);
				return $str;
			break;
			case "assets":
				$str="/".TPL_MODULES."assets/";
				return $str;
			break;
			case "weburl":
				$http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
				return $http_type . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			break;
			
        }
        
    }
    private function tag_meta($GET,$field){
        switch($field){
            case "global_title":return TITLE;break;
            case "global_site_tagline":return SITE_TAGLINE;break;
            case "global_custom_keywords":return CUSTOM_KEYWORDS;break;
            case "home_title":return HOME_PAGE_TITLE;break;
            case "home_keywords":return HOME_PAGE_META_KEYWORDS;break;
            case "home_description":return HOME_PAGE_META_DESCRIPTION;break;
            case "title":return ;break;
            case "keywords":return ;break;
            case "description":return ;break;
        }
    }
    private function get_archives_path($separator){
		global $predata_class,$blog_class;
		$categories=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
		
		$archives=$blog_class->get_archives_list($_GET,["aid"=>$_GET['archives_id']]);
		$archives=$archives['datalist'][0];
		$cpath=array_filter(explode("_",$_GET['cPath']));
		$data[]=array("link"=>HTTP_SERVER,"title"=>HEADER_TITLE_CATALOG);
		foreach($cpath as $k => $v){
			if(isset($categories[$v])){
				$link=$categories[$v]['categories_link'];
				$title=$categories[$v]['categories_name'];
				$data[]=array("link"=>$link,"title"=>$title);
			}
		}
		
		$data[]=array("link"=>$archives['archives_link'],"title"=>$archives['archives_name']);
		end($data);$key=key($data);
		foreach($data as $k => $v){
			if($k==$key){
				@$string.="<a href='".$v['link']."'>".$v['title']."</a>\r\n";
			}else{
				@$string.="<a href='".$v['link']."'>".$v['title']."</a>".$separator."\r\n";
			}
			
		}
		return $string;
	}
	
}
?>