<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class variable{
	private $init_static;
	function page_var_handle(){
		global $product_class;
		if(array_key_exists("main_page",$_GET)){
			$main_page=$_GET['main_page'];
			switch($main_page){
				case "create_account":
					$GLOBALS['selected_state']=zen_prepare_country_zones_pull_down($GLOBALS['selected_country']);
					$GLOBALS['country_list']=$this->get_country_list($GLOBALS['selected_country']);
				break;
				
			}
			if($_SESSION['customer_id']){
				$account['order_count']=zen_count_customer_orders();
			}
		}
	}
	
	function checkDirectory($GET){
		if($GET['main_page']=="advanced_search_result"){
			return false;
		}else{
			return true;
		}
	}
	function shippingAddress($data){
		
		$error=false;
		$required=explode(",","zone_id,country_id,city,street_address,firstname,lastname,postcode,sendto");
		foreach($required as $k => $v){
			if(!isset($data[$v]) || !$data[$v]){
				$error=true;
			}
		}
		if($error==true){return false;}
		$address=openzcQuery("SELECT * FROM " . TABLE_ADDRESS_BOOK . " WHERE customers_id = ".$_SESSION['customer_id']." AND address_book_id = ".$data['sendto']);
		$address=openzc_table_to_list($address)[0];
		foreach($address as $k => $v){
			$key=str_replace("entry_","",$k);
			if($address[$k]!=$data[$key] && isset($data[$key])){
				@$values.=$k."='".$data[$key]."',";
			}
		}
		if($values){$values.="#";$values=str_replace(",#","",$values);}
		if($values){
			$sql="update ".TABLE_ADDRESS_BOOK." set ".$values." where address_book_id=".$data['sendto'];
			openzcQuery($sql);
		}
		return true;
	}
	function getCheckoutConfirmation(){
		global $payment_modules,$order;
		
		$billing=zen_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />');
		define("TEXT_BILLING_ADDRESS",$billing);
		
		define("TEXT_PAYMENT_METHOD",$GLOBALS[$_SESSION['payment']]->title);
		$delivery=zen_address_format($order->delivery['format_id'], $order->delivery, 1, ' ', '<br />');
		define("TEXT_DELIVERY_ADDRESS",$delivery);
		define("TEXT_SHIPPING_METHOD",$order->info['shipping_method']);
		define("TEXT_ORDER_COMMENTS",$order->info['comments']);
	}
	function get_state_list($selected_country,$selected_state=''){
		if($_GET['main_page']=="address_book_process" && isset($_GET['edit']) && is_numeric($_GET['edit'])){
			$entry = openzcQuery("SELECT * FROM " . TABLE_ADDRESS_BOOK . " WHERE customers_id = ".$_SESSION['customer_id']." AND address_book_id = ".$_GET['edit']);
			$selected_country=$entry->fields['entry_country_id'];
		}
		if(!$selected_country){$selected_country=STORE_COUNTRY;}
		
		$state=zen_prepare_country_zones_pull_down($selected_country);
		
		foreach($state as $k => $v){
			$state[$k]['status']="";
			if($v['id']==$selected_state){$state[$k]['status']="active";}
		}
		return $state;
	}
	function get_country_list($selected_country) {
		
		if($_GET['main_page']=="address_book_process" && isset($_GET['edit']) && is_numeric($_GET['edit'])){
			$entry = openzcQuery("SELECT * FROM " . TABLE_ADDRESS_BOOK . " WHERE customers_id = ".$_SESSION['customer_id']." AND address_book_id = ".$_GET['edit']);
			$selected_country=$entry->fields['entry_country_id'];
		}
		if(!$selected_country){
			if($_GET['main_page']=="checkout_shipping"){}
		}
		$countriesAtTopOfList = array();
		$countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
		$countries = zen_get_countries();

		// Set some default entries at top of list:
		if (STORE_COUNTRY != SHOW_CREATE_ACCOUNT_DEFAULT_COUNTRY) $countriesAtTopOfList[] = SHOW_CREATE_ACCOUNT_DEFAULT_COUNTRY;
		$countriesAtTopOfList[] = $selected_country;
		
		foreach ($countriesAtTopOfList as $key=>$val) {
			if($val==$selected_country){
				$countries_array[] = array('id' => $val, 'text' => zen_get_country_name($val),'status'=>"active");
			}else{
				$countries_array[] = array('id' => $val, 'text' => zen_get_country_name($val),'status'=>"");
			}
		}
		// now add anything not in the defaults list:
		for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
			$alreadyInList = FALSE;
			foreach($countriesAtTopOfList as $key=>$val) {
				if ($countries[$i]['countries_id'] == $val){
					$alreadyInList = TRUE;
					continue;
				}
			}
			if (!$alreadyInList) $countries_array[] = array('id' => $countries[$i]['countries_id'], 'text' => $countries[$i]['countries_name'],'status'=>"");
		}
		
		return $countries_array;
	}
	//*自带CSS框架，针对公共通用模板文件使用*//
	function init_static(){
		$error=false;
		$this->init_assets_file(BASIC_PATH."assets");
		$dest_dir=TPL_MODULES."assets";
		$source_dir=BASIC_PATH."assets";
		
		if(!is_dir($dest_dir)){mkdir($dest_dir);}
		
		foreach($this->init_static as $k => $v){
			if(!is_file($dest_dir.$v['file'])){
				$dir=$dest_dir.$v['dir'];
				if(!is_dir($dir)){
					mkdir($dir);
					if(!is_dir($dir)){mkdirs($dir);}
				}
				copy($source_dir.$v["file"],$dest_dir.$v['file']);
			}
		}
	}
	function init_assets_file($dir){
        $temp=scandir($dir);
		
        foreach($temp as $v){
            $a=$dir.'/'.$v;
			if(!strstr($a,BASIC_PATH."assets")){$a=BASIC_PATH."assets".$a;}
			if(is_dir($a)){
				if($v=='.' || $v=='..'){continue;}
				$this->init_assets_file($a);
			}else{
				$type=pathinfo($a)['extension'];
				$dir=str_replace(BASIC_PATH."assets","",$dir);
				$a=str_replace(BASIC_PATH."assets","",$a);
                $this->init_static[]=array("type"=>$type,"file"=>$a,"dir"=>$dir);
            }
        }
		
    }
	function init_helper($fun){
		if(!strstr($fun,"(") || !strstr($fun,"@me")){return false;}
		$fun=explode("(",$fun);
		if(function_exists($fun[0])){
			return true;
		}
		return false;
	}
	function getFilterStatus(){
		$GLOBALS['filterStatus']=false;
		if(isset($_GET['brand']) || isset($_GET['price']) || isset($_GET['options'])){
			$GLOBALS['filterStatus']=true;
		}
		
	}
	
}

?>