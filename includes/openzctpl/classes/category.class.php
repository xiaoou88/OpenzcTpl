<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class category{
	function get_category_tree(){
		global $predata_class;
		$check=$predata_class->checkUpdatedata();
		if(!is_dir(TPLCACHE_TPL)){mkdir(TPLCACHE_TPL);}
		$file=TPLCACHE_TPL."categoryTree_".$_SESSION['languages_id'].md5(DB_DATABASE).".inc";
		if($check==true || !is_file($file)){
			$categories=openzcQuery("select c.categories_status,c.categories_id,c.parent_id,c.categories_image,cd.categories_name,cd.categories_description from ".TABLE_CATEGORIES." c left join ".TABLE_CATEGORIES_DESCRIPTION." cd on cd.categories_id=c.categories_id and cd.language_id=".(int)$_SESSION['languages_id']);
			$categories=openzc_table_to_list($categories,"categories_id");
			$categories_images=$this->get_categories_images($categories);
			$procount=openzcQuery("select count(DISTINCT products_id) as total,categories_id from ".TABLE_PRODUCTS_TO_CATEGORIES." group by categories_id");
			$procount=openzc_table_to_list($procount,"categories_id");
			
			$sub=openzcQuery("select parent_id,group_concat(categories_id separator ',') as sub from ".TABLE_CATEGORIES." group by parent_id");
			$sub=openzc_table_to_list($sub,"parent_id");
	
			foreach($categories as $k => $v){
				$v['son']=$sub[$k]['sub'];
				if($v['son']){$v['has_sub_cat']=1;}else{$v['has_sub_cat']=0;}
				if($v['has_sub_cat']==1){
					$v['count']=$this->get_product_count($v['son']);
					$v['count']+=$procount[$k]['total'];
				}else{
					$v['count']=$procount[$k]['total'];
				}
				$v['path']=zen_get_generated_category_path_rev($k);
				$v['categories_link']=zen_href_link(FILENAME_DEFAULT, "cPath=".$v['path']);
				$v['categories_image']=$categories_images[$k];
				$result[$k]=$v;
			}
			$myfile = fopen($file, "w") or die("Unable to open file:".$file."!");
			fwrite($myfile, json_encode($result));
			fclose($myfile);
		}else{
			$result=json_decode(file_get_contents($file),true);
		}
		return $result;
	}
	
	function get_product_count($ids){
		$ids="'".str_replace(",","','",$ids)."'";
		$procount=openzcQuery("select count(products_id) as total from ".TABLE_PRODUCTS_TO_CATEGORIES." where categories_id in(".$ids.")");
		$procount=openzc_table_to_list($procount);
		return $procount[0]['total'];
	}
	
  
    function show_category_tree($categories,$products_count,$level=0){
		foreach($categories as $k => $v){
			if(file_exists("./images/".$v['categories_image'])){
				$v['categories_image']="/images/".$v['categories_image'];
			}else if(file_exists("./".$v['categories_image'])){
				$v['categories_image']="/".$v['categories_image'];
			}
			
			$categories[$k]['path']="cPath=".$this->get_category_parent($v['categories_id'],$categories,$v['categories_id']);
			$categories[$k]['has_sub_cat']=$this->get_category_has_sub($k,$categories);
			if($v['parent_id']==0){
				$categories[$k]['top']="true";
				
			}else{
				$categories[$k]['top']="false";
			}
			$categories[$k]['categories_link']=zen_href_link(FILENAME_DEFAULT, $categories[$k]['path']);
			$categories[$k]['level']=substr_count($categories[$k]['path'],'_');
			if($categories[$k]['level']>$level){
				$level=$categories[$k]['level'];
			}
			
		}
		foreach($categories as $k => $v){
			$son_ids=explode(",",openzc_get_son_ids($categories,$k,$k));
			
			foreach($son_ids as $a => $b){
				$count+=count($products_count[$b]);
			}
		
			$categories[$k]["products_count"]=$count;
			$count="";
		}
		
		return $categories;
    }
  
    function get_category_parent($id,$categories,$str){
		$parent_id=$categories[$id]['parent_id'];
		if($categories[$id]['parent_id']!=0){
			$str=$this->get_category_parent($parent_id,$categories).$parent_id."_".$str;
		}
		return $str;
    }
  
    function get_category_has_sub($id,$categories,$n=0){
		foreach($categories as $k=> $v){
			if($v['parent_id']==$id){
				$n=1;
				return $n;
			}
		}
		return $n;
	}
	function get_category_get_current_son($categories,$id,$str){
		foreach($categories as $k => $v){
			if($v['parent_id']==$id){
				if($str){
					$str.=",".$v['categories_id'];
				}else{
					$str=$v['categories_id'];
				}
				if(array_key_exists("has_sub_cat",$v)){
					$str=$this->get_category_get_current_son($categories,$v['categories_id'],$str);
				}
			}
		}
		return $str;
	}
	
	
	function get_categories_list($GET,$parameter){
		global $predata_class;
	
		$parameter['cid']=explode(",",$parameter['cid']);
		$parameter['cid']=array_filter($parameter['cid']);
		if($parameter['row']){
			$i=0;
			foreach($parameter['cid'] as $k => $v){
				if($i>=$parameter['row']){unset($parameter['cid'][$k]);}
				$i++;
			}
		}
	
		return $this->show_categories_list($GET,$parameter);
	}
  
	function show_categories_list($GET,$parameter){
		global $predata_class;
		$categories_tree=$predata_class->getPredata(TABLE_CATEGORIES);
		
		if($parameter['limit']){$limit=explode(",",$parameter['limit']);$row=0;}
		foreach($parameter['cid'] as $k => $v){
			$status=true;
			if($parameter['limit']){
				if($row>=$limit[0] && $row < $limit[1]){
					$status=true;
				}else{
					$status=false;
				}
				$row++;
			}
			if($status==true){
				$datalist[$k]=$categories_tree[$v];
				$datalist[$k]['sort_index']=$k+1;
				$datalist[$k]['current_status']=0;
				if($GET['cPath']){
					if($GET['cPath']==$v){$datalist[$k]['current_status']=1;}
				}	
			}
		}
		return $datalist;
	}
	function get_all_top($result=array()){
		global $predata_class;
		$categories=$predata_class->getPredata(TABLE_CATEGORIES);
		
		foreach($categories as $k => $v){
			if($v['parent_id']==0){
				$result[$k]=$k;
			}
		}
		return join(",",$result);
	}
	function get_categories_images($data){
		$result=array();
		foreach($data as $k => $v){
			if(is_file("./images/".$v['categories_image'])){
				$result[$k]="/images/".$v['categories_image'];
			}else if(is_file("./".$v['categories_image'])){
				$result[$k]=$v['categories_image'];
			}
		}
		return $result;
	}
	function checkDataUpdate(){
		define("TABLE_OPENZC_TRIGGER",DB_PREFIX."openzc_trigger");
		$sql="select last_modified from ".TABLE_OPENZC_TRIGGER." where table_name in ('".TABLE_CATEGORIES."','".TABLE_CATEGORIES_DESCRIPTION."','".TABLE_PRODUCTS_TO_CATEGORIES."') order by last_modified desc limit 1";
		$result=openzcQuery($sql);
		return $result->fields['last_modified'];
	}

	
}
?>