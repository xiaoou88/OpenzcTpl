<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class attributes {
    private $attributes=array();
	private $intact_data=array();
	private $specials=array();
	private $products_id="";
	
	function get_attributes_price_final($attributes,$intact_data,$specials,$include_onetime='false'){
		
		$this->attributes=$attributes;
		$this->intact_data=$intact_data;
		$this->specials=$specials;
		$this->products_id=$attributes['products_id'];
		$attributes_price_final = 0;
		
		// normal attributes price
		if($this->attributes['price_prefix']=="-"){
			$attributes_price_final -= $this->attributes["options_values_price"];
		}else{
			$attributes_price_final += $this->attributes["options_values_price"];
		}
		// qty discounts
		$attributes_price_final += $this->get_attributes_qty_prices_onetime();
		// price factor
		$display_normal_price = $this->get_products_actual_price();
		$display_special_price = $this->get_products_special_price();
		
		$attributes_price_final += $this->get_attributes_price_factor($display_normal_price, $display_special_price, $this->attributes["attributes_price_factor"], $this->attributes["attributes_price_factor_offset"]);
		
		// onetime charges
		if ($include_onetime == 'true') {
			$attributes_price_final += $this->get_attributes_price_final_onetime($this->attributes["products_attributes_id"], 1,$this->attributes);
		}
		$attributes_price_final=zen_get_discount_calc((int)$this->attributes['products_id'], true, $attributes_price_final);
		if ($attributes_price_final < 0) {$attributes_price_final = -$attributes_price_final;}
		
		
		return $attributes_price_final;
	}
	
	// return attributes_qty_prices or attributes_qty_prices_onetime based on qty
	function get_attributes_qty_prices_onetime() {
		$string=$this->attributes["attributes_qty_prices"];
		$attribute_qty = preg_split("/[:,]/" , str_replace(' ', '', $string));
		$new_price = 0;
		$size = sizeof($attribute_qty);
		// if an empty string is passed then $attributes_qty will consist of a 1 element array
		if ($size > 1) {
			for ($i=0, $n=$size; $i<$n; $i+=2) {
				$new_price = $attribute_qty[$i+1];
				if ($attribute_qty[$i]>=1) {
					$new_price = $attribute_qty[$i+1];
					break;
				}
			}
		}
		return $new_price;
	}
	// Actual Price Retail
	// Specials and Tax Included
	function get_products_actual_price() {

		$show_display_price = '';
		$display_normal_price = $this->get_products_base_price();
		$display_special_price = $this->get_products_special_price(true);
		$display_sale_price = $this->get_products_special_price( false);

		$products_actual_price = $display_normal_price;

		if ($display_special_price) {
			$products_actual_price = $display_special_price;
		}
		if ($display_sale_price) {
			$products_actual_price = $display_sale_price;
		}

		// If Free, Show it
		if ($this->attributes['product_is_free'] == '1') {
			$products_actual_price = 0;
		}

		return $products_actual_price;
	}
	
	function get_products_base_price() {
		$products_price = $this->attributes['products_price'];
		foreach($this->intact_data as $k => $v){
			if($v['attributes_display_only']!="1" && $v['attributes_price_base_included']=='1'){
				$product_att_query[]=$v;
			}
		}
		$the_options_id= 'x';
		$the_base_price= 0;
		if($this->attributes['products_priced_by_attribute'] == '1' && count($product_att_query)>=1){
			foreach($product_att_query as $k => $v){
				if($the_options_id != $v['options_id']){
					$the_options_id = $v['options_id'];
					$the_base_price += (($v['price_prefix'] == '-') ? -1 : 1) * $v['options_values_price'];
				}
			}
		}else{
			$the_base_price = $products_price;
		}
		return $the_base_price;
	}
	
	function get_products_special_price($specials_price_only=false) {
		global $db;
		if (count($this->intact_data)> 0) {
			$product_price = $this->get_products_base_price();
		} else {
			return false;
		}
		
		
		if (count($this->specials) > 0) {
			$special_price = $this->specials['specials_new_products_price'];
		} else {
			$special_price = false;
		}

		if(substr($this->attributes['products_model'], 0, 4) == 'GIFT') {  
			if (zen_not_null($special_price)) {
				return $special_price;
			} else {
				return false;
			}
		}

		if ($specials_price_only==true) {
			if (zen_not_null($special_price)) {
				return $special_price;
			} else {
				return false;
			}
		} else {
		
			$category = $this->attributes['master_categories_id'];
	
			$sale = $db->Execute("select sale_specials_condition, sale_deduction_value, sale_deduction_type from " . TABLE_SALEMAKER_SALES . " where sale_categories_all like '%," . $category . ",%' and sale_status = '1' and (sale_date_start <= now() or sale_date_start = '0001-01-01') and (sale_date_end >= now() or sale_date_end = '0001-01-01') and (sale_pricerange_from <= '" . $product_price . "' or sale_pricerange_from = '0') and (sale_pricerange_to >= '" . $product_price . "' or sale_pricerange_to = '0')");
			if ($sale->RecordCount() < 1) {
				return $special_price;
			}

			if (!$special_price) {
				$tmp_special_price = $product_price;
			} else {
				$tmp_special_price = $special_price;
			}
			switch ($sale->fields['sale_deduction_type']) {
				case 0:
					$sale_product_price = $product_price - $sale->fields['sale_deduction_value'];
					$sale_special_price = $tmp_special_price - $sale->fields['sale_deduction_value'];
				break;
				case 1:
					$sale_product_price = $product_price - (($product_price * $sale->fields['sale_deduction_value']) / 100);
					$sale_special_price = $tmp_special_price - (($tmp_special_price * $sale->fields['sale_deduction_value']) / 100);
				break;
				case 2:
					$sale_product_price = $sale->fields['sale_deduction_value'];
					$sale_special_price = $sale->fields['sale_deduction_value'];
				break;
				default:
					return $special_price;
			}

			if ($sale_product_price < 0) {
				$sale_product_price = 0;
			}

			if ($sale_special_price < 0) {
				$sale_special_price = 0;
			}

			if (!$special_price) {
				return number_format($sale_product_price, 4, '.', '');
			} else {
				switch($sale->fields['sale_specials_condition']){
					case 0:
						return number_format($sale_product_price, 4, '.', '');
					break;
					case 1:
						return number_format($special_price, 4, '.', '');
					break;
					case 2:
						return number_format($sale_special_price, 4, '.', '');
					break;
					default:
						return number_format($special_price, 4, '.', '');
				}
			}
		}
	}
	// return attributes_price_factor
	function get_attributes_price_factor($price, $special, $factor, $offset) {
		if (ATTRIBUTES_PRICE_FACTOR_FROM_SPECIAL =='1' and $special) {
			// calculate from specials_new_products_price
			$calculated_price = $special * ($factor - $offset);
		} else {
		// calculate from products_price
			$calculated_price = $price * ($factor - $offset);
		}
		return $calculated_price;
	}
	// attributes final price onetime
	function get_attributes_price_final_onetime($attribute, $qty= 1, $pre_selected_onetime) {
	
		$attributes_price_final_onetime += $pre_selected_onetime["attributes_price_onetime"];

		// price factor
		$display_normal_price = $this->get_products_actual_price();
		$display_special_price = $this->get_products_special_price();

		// price factor one time
		$attributes_price_final_onetime += $this->get_attributes_price_factor($display_normal_price, $display_special_price, $pre_selected_onetime["attributes_price_factor_onetime"], $pre_selected_onetime["attributes_price_factor_onetime_offset"]);

		// onetime charge qty price
		$attributes_price_final_onetime += $this->get_attributes_qty_prices_onetime($pre_selected_onetime["attributes_qty_prices_onetime"], 1);

		return $attributes_price_final_onetime;
    }
}
?>