<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class predata{
    //执行数据缓存
    function getPredata($table){
		if(isset($GLOBALS[$table."_predata"])){
			return $GLOBALS[$table."_predata"];
		}else{
			switch($table){
				case TABLE_CATEGORIES:
					global $category_class;
					$data= $category_class->get_category_tree();
				break;
				case TABLE_CATEGORIES_DESCRIPTION:
				
					$sql="select * from ".$table." where language_id='".(int)$_SESSION['languages_id']."'";
					$data=openzcQuery($sql);
					$data=openzc_table_to_list($data,"categories_id");
				break;
				case TABLE_PRODUCTS_OPTIONS:
					$sql="select * from ".$table." where language_id='".(int)$_SESSION['languages_id']."'";
					$data=openzcQuery($sql);
					$data=openzc_table_to_list($data,"products_options_id");
				break;
				case TABLE_PRODUCTS_OPTIONS_VALUES:
					$sql="select v.*,i.products_options_values_image,i.products_options_values_color from ".$table." v left join ".TABLE_PRODUCTS_OPTIONS_VALUES_IMAGE." i on v.products_options_values_id=i.products_options_values_id where v.language_id='".(int)$_SESSION['languages_id']."'";
					$data=openzcQuery($sql);
					$data=openzc_table_to_list($data,"products_options_values_id");
				break;
				case TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS:
					$sql="select * from ".$table;
					$data=openzcQuery($sql);
					$data=openzc_table_to_list($data,"products_options_values_id");
				break;
				case TABLE_MANUFACTURERS:
					$sql="select * from ".$table;
					$data=openzcQuery($sql);
					$data=openzc_table_to_list($data,"manufacturers_id");
				break;
				case TABLE_BANNERS:
					$sql="select * from ".$table." where status='1' order by banners_sort_order desc";
					$data=openzcQuery($sql);
					$data=openzc_table_to_list($data,"banners_id");
				break;
				case TABLE_BLOG_CATEGORIES:
					global $blog_class;
					$data = $blog_class->get_category_tree();
				break;
				case TABLE_BLOG_REVIEWS:
					global $blog_class;
					$data = $blog_class->get_archives_reviews();
				break;
				case TABLE_CURRENCIES:
					$sql = "select * from ".$table;
					$data=openzcQuery($sql);
					$data=openzc_table_to_list($data,"code");
				break;
			}
			$GLOBALS[$table."_predata"]=$data;
			return $data;
		}
	}
	function checkUpdatedata(){
		global $cacheTablelist;
		
		define("TABLE_OPENZC_TRIGGER",DB_PREFIX."openzc_trigger");
		$count=openzcQuery("select count(id) as count from ".TABLE_OPENZC_TRIGGER);
		$count=openzc_table_to_list($count)[0]['count'];

		if($count>1000){
			openzcQuery("truncate table ".TABLE_OPENZC_TRIGGER);
			openzcQuery("alter table ".TABLE_OPENZC_TRIGGER." auto_increment=1");
		}
		$sql="select o.table_name,o.last_modified as Update_time from (select * from ".TABLE_OPENZC_TRIGGER." order by last_modified desc) o group by o.table_name";
		$update=openzcQuery($sql);
		$update=openzc_table_to_list($update,"table_name");
		$now=strtotime('now');
        foreach($cacheTablelist as $k => $v){
			if(!array_key_exists($k,$update)){
				$update[$k]['Update_time']=$now;
				$sql="insert into ".TABLE_OPENZC_TRIGGER."(table_name,action,field,value,last_modified) values ('".$k."','openzc','openzc','openzc','".$now."')";
				openzcQuery($sql);
			}
		}
		$recorder_file=CACHE_DIR.md5(DB_DATABASE).".inc";
		
		if(is_file($recorder_file)){
			$recorder=json_decode(file_get_contents($recorder_file),true);
			foreach($update as $k => $v){
				if(($v['Update_time']!=$recorder[$k]['Update_time'] && array_key_exists($k,$recorder)) || count($recorder)==0){
					$recorder_file = fopen($recorder_file, "w") or die("Unable to open file recorder!");
					fwrite($recorder_file, json_encode($update));
					fclose($recorder_file);
					return true;
				}
			}
		}else{
			$recorder_file = fopen($recorder_file, "w") or die("Unable to open file recorder!");
			fwrite($recorder_file, json_encode($update));
			fclose($recorder_file);
			return true;
		}
		
		
        return false;
    }
}
?>