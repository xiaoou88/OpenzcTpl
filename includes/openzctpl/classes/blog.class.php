<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
class blog {
	/*-----------------------------------------------------------Blog Categories Function-----------------------------------------------------------*/
    function get_category_tree(){
		global $predata_class;
		$check=$predata_class->checkUpdatedata();
		if(!is_dir(TPLCACHE_TPL)){mkdir(TPLCACHE_TPL);}
		$file=TPLCACHE_TPL."blogCategoryTree_".$_SESSION['languages_id'].md5(DB_DATABASE).".inc";
		if($check==true || !is_file($file)){
			$categories=openzcQuery("select c.categories_status,c.categories_id,c.parent_id,c.categories_image,cd.categories_name,cd.categories_description from ".TABLE_BLOG_CATEGORIES." c left join ".TABLE_BLOG_CATEGORIES_DESCRIPTION." cd on cd.categories_id=c.categories_id and cd.language_id=".(int)$_SESSION['languages_id']);
			$categories=openzc_table_to_list($categories,"categories_id");
			$categories_images=$this->get_categories_images($categories);
			$arccount=openzcQuery("select count(DISTINCT archives_id) as total,categories_id from ".TABLE_BLOG_ARCHIVES_TO_CATEGORIES." group by categories_id");
			$arccount=openzc_table_to_list($arccount,"categories_id");
			
			$sub=openzcQuery("select parent_id,group_concat(categories_id separator ',') as sub from ".TABLE_BLOG_CATEGORIES." group by parent_id");
			$sub=openzc_table_to_list($sub,"parent_id");
	
			foreach($categories as $k => $v){
				$v['son']=$sub[$k]['sub'];
				if($v['son']){$v['has_sub_cat']=1;}else{$v['has_sub_cat']=0;}
				if($v['has_sub_cat']==1){
					$v['count']=$this->get_archives_count($v['son']);
					$v['count']+=$arccount[$k]['total'];
				}else{
					$v['count']=$arccount[$k]['total'];
				}
				$v['path']=$this->get_generated_category_path_rev($k);
				$v['categories_link']=zen_href_link(FILENAME_BLOG, "cPath=".$v['path']);
				$v['categories_image']=$categories_images[$k];
				$result[$k]=$v;
			}
			$myfile = fopen($file, "w") or die("Unable to open file:".$file."!");
			fwrite($myfile, json_encode($result));
			fclose($myfile);
		}else{
			$result=json_decode(file_get_contents($file),true);
		}
		return $result;
	}
	function get_archives_count($ids){
		$ids="'".str_replace(",","','",$ids)."'";
		$arccount=openzcQuery("select count(archives_id) as total from ".TABLE_BLOG_ARCHIVES_TO_CATEGORIES." where categories_id in(".$ids.")");
		$arccount=openzc_table_to_list($arccount);
		return $arccount[0]['total'];
	}
	function get_categories_images($data){
		$result=array();
		foreach($data as $k => $v){
			if(is_file("./images/".$v['categories_image'])){
				$result[$k]="/images/".$v['categories_image'];
			}else if(is_file("./".$v['categories_image'])){
				$result[$k]=$v['categories_image'];
			}
		}
		return $result;
	}
	function get_generated_category_path_rev($this_categories_id) {
		$categories=array();
		$categories=$this->get_parent_categories($categories, $this_categories_id);
		$categories = array_reverse($categories);
		$categories_imploded = implode('_', $categories);
		if (zen_not_null($categories_imploded)) $categories_imploded .= '_';
		$categories_imploded .= $this_categories_id;
		return $categories_imploded;
	}
	function get_parent_categories(&$categories, $categories_id) {
		global $db;
		$parent_categories_query = "select parent_id
									from " . TABLE_BLOG_CATEGORIES . "
									where categories_id = '" . (int)$categories_id . "'";

		$parent_categories = $db->Execute($parent_categories_query);

		while (!$parent_categories->EOF) {
		  if ($parent_categories->fields['parent_id'] == 0) return true;
		  $categories[sizeof($categories)] = $parent_categories->fields['parent_id'];
		  if ($parent_categories->fields['parent_id'] != $categories_id) {
			$this->get_parent_categories($categories, $parent_categories->fields['parent_id']);
		  }
		  $parent_categories->MoveNext();
		}
		return $categories;
    }
	function get_categories_list($GET,$parameter){

		$parameter['cid']=explode(",",$parameter['cid']);
		$parameter['cid']=array_filter($parameter['cid']);
		if($parameter['row']){
			$i=0;
			foreach($parameter['cid'] as $k => $v){
				if($i>=$parameter['row']){unset($parameter['cid'][$k]);}
				$i++;
			}
		}
	
		return $this->show_categories_list($GET,$parameter);
	}
	function show_categories_list($GET,$parameter){
		global $predata_class;
		$categories_tree=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
		
		if($parameter['limit']){$limit=explode(",",$parameter['limit']);$row=0;}
		foreach($parameter['cid'] as $k => $v){
			$status=true;
			if($parameter['limit']){
				if($row>=$limit[0] && $row < $limit[1]){
					$status=true;
				}else{
					$status=false;
				}
				$row++;
			}
			if($status==true){
				$datalist[$k]=$categories_tree[$v];
				$datalist[$k]['sort_index']=$k+1;
				$datalist[$k]['current_status']=0;
				if($GET['cPath']){
					if($GET['cPath']==$v){$datalist[$k]['current_status']=1;}
				}	
			}
		}
		return $datalist;
	}
	function get_all_top($result=array()){
		global $predata_class;
		$categories=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
		
		foreach($categories as $k => $v){
			if($v['parent_id']==0){
				$result[$k]=$k;
			}
		}
		return join(",",$result);
	}
	/*-----------------------------------------------------------Blog Archives Function-----------------------------------------------------------*/
	/*获取商品列表*/
	function get_archives_list($GET,$parameter){
		
		if($parameter['page']!=true){
			$sql=$this->get_flag_archives($GET,$parameter);
		}else{
			$sql=$this->get_archives_search($GET,$parameter);
		}
	
		return $this->show_archives_list($GET,$sql,$parameter);
	}
	
	//调用普通文章和标签文章
	function get_flag_archives($GET,$parameter){
		global $predata_class;
		$orderby=$parameter['orderby'][1]." ".$parameter['orderby'][2];				
		$limit=$parameter['row'];
		$field="ad.archives_name,a.*";
		if($parameter['desclen']){
			$field.=",left(ad.archives_description,".$parameter['desclen'].") as archives_description";
		}
		if($parameter['aid']){
			$aid=str_replace(",","','",$parameter['aid']);
			$sql="select a.*,ad.archives_name from ".TABLE_BLOG_ARCHIVES." a left join ".TABLE_BLOG_ARCHIVES_DESCRIPTION." ad on a.archives_id=ad.archives_id where a.archives_id in('".$aid."') and ad.language_id='".(int)$_SESSION['languages_id']."'";
			return $sql;
		}
		
		$categories=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
		
		if($parameter['cid']!="all" && $parameter['cid'] ){
			
			$cid_where=" and atc.categories_id in('".$parameter['cid']."')";
		}else{
			$cid_where=" and atc.categories_id>0";
		}
		
		if($parameter['flag']){
			$flag_where="a.archives_flag='".$parameter['flag']."' and ";
		}
		if($parameter['flag']==='false'){
			$flag_where="a.archives_flag=null and ";
		}
		$sql="select ".$field." from ".TABLE_BLOG_ARCHIVES." a ,".TABLE_BLOG_ARCHIVES_DESCRIPTION." ad,".TABLE_BLOG_ARCHIVES_TO_CATEGORIES." atc where a.archives_id=ad.archives_id and ".$flag_where."ad.language_id='".(int)$_SESSION['languages_id']."' and atc.archives_id=a.archives_id ".$cid_where." and a.archives_status='1' order by a.".$orderby." limit ".$limit;
	
		return $sql;
	}
	//调用Blog列表页
	function get_archives_search($GET,$parameter){
		global $predata_class;
		$categories=$predata_class->getPredata(TABLE_CATEGORIES);

		$limit=$parameter['row'];
		$field="ad.archives_name,a.*";
		if($parameter['desclen']){
			$field.=",left(ad.archives_description,".$parameter['desclen'].") as archives_description";
		}
		$table="";
	
		$where="archives_id>0";
		
		

		
		if($parameter['cid']!="all" && $parameter['cid']){
			$cid=str_replace(",","','",$parameter['cid']);
			$where.=" and master_categories_id in('".$cid."') and archives_status='1'";
		}
		if($parameter['page']==true && array_key_exists("page",$_GET)){
			$limit=($parameter['row']*($_GET['page']-1)).",".$parameter['row'];
		}
		
		$orderby=$parameter['orderby'];
		
		if($where){$where=" where ".$where;}
		
		switch($orderby[3]){
			case TABLE_BLOG_ARCHIVES:
				$table.="(select * from ".TABLE_BLOG_ARCHIVES.$where." order by ".$orderby[1]." ".$orderby[2]." limit ".$limit.") a left join ".TABLE_BLOG_ARCHIVES_DESCRIPTION." ad on ad.archives_id=a.archives_id";
			break;
			case TABLE_BLOG_ARCHIVES_DESCRIPTION:
				$table.="(select * from ".TABLE_BLOG_ARCHIVES.$where.") a left join ".TABLE_BLOG_ARCHIVES_DESCRIPTION." ad on ad.archives_id=a.archives_id ";
			break;
		}
		$GLOBALS['page_sql']=$where;
		
		$sql="select ".$field." from ".$table." where".$between." ad.language_id='".(int)$_SESSION['languages_id']."' group by a.archives_id order by ".$orderby[0].".".$orderby[1]." ".$orderby[2];
		
		return $sql;
	}
	
	//BLOG列表展示
	function show_archives_list($GET,$sql,$parameter){
		global $predata_class;
	
		$data=openzcQuery($sql);
		
		if($parameter['index_field']){
			$data=openzc_table_to_list($data,$parameter['index_field']);
		}else{
			$data=openzc_table_to_list($data);
		}
		
		$categories=$predata_class->getPredata(TABLE_BLOG_CATEGORIES);
		$reviews=$predata_class->getPredata(TABLE_BLOG_REVIEWS);
		foreach($data as $k => $v){
			$v["sort_index"]=$k+1;
			$v['categories_name']=$categories[$v['master_categories_id']]['categories_name'];
			$v['categories_link']=$categories[$v['master_categories_id']]['categories_link'];
			$v['reviews_count']=count($reviews[$v['archives_id']]);
			//**archives_image
			$v['archives_image']=$this->get_archives_image($v['archives_image'],$parameter['imgsizer'],$parameter['bgcolor']);
			$v['archives_image_full']=$v['archives_image'];
			
			$cPath=$categories[$v['master_categories_id']]['path'];
			$v['archives_link']=zen_href_link(FILENAME_BLOG_ARCHIVES,'cPath='.$cPath.'&archives_id='.$v['archives_id']);
			if(isset($v['archives_description'])){
				$v['archives_description']=strip_tags($v['archives_description']);
			}
			$datalist[$k]=$v;
		}
		$sql="select count(distinct archives_id) as total from ".TABLE_BLOG_ARCHIVES.$GLOBALS['page_sql'];
		$total=openzcQuery($sql);
		$list['total']=$total->fields['total'];
		$list['datalist']=$datalist;
		
		return $list;
	}
	
	
	//获取主图
	function get_archives_image($images,$sizer,$bgcolor){
		global $ImageSizer;
		if(!is_dir(IMGCACHE_DIR)){mkdir(IMGCACHE_DIR);}
		$images=explode("|||",$images);
		if(is_file("./images/".$images[0])){
			$result="/images/".$images[0];
		}else if(is_file("./".$images[0])){
			$result="/".$images[0];
		}
		if($sizer){
			$sizer=explode(",",$sizer);
			$imgname=getCacheImgname(basename($result));
			$imgdir=IMGCACHE_DIR.$sizer[0]."-".$sizer[1]."/";
			$image_new=$imgdir.$imgname;
			if(!is_dir($imgdir)){
				mkdir($imgdir);
			}
			if(getCacheImgcheck($image_new)==true){
				$ImageSizer->Run(".".$result);
				$ImageSizer->resize($sizer[0],$sizer[1]);
				$ImageSizer->save($image_new);
			}
			$result="/".str_replace(ROOT_PATH,"",$imgdir).$imgname;
		}
		return $result;
	}
	//商品列表分页
	function get_archives_list_page($GET,$parameter){
		
		if(array_key_exists("limit",$GET)){
			$pernum=$GET['limit'];
		}else{
			$pernum=$parameter['pernum'];
		}
		
		$total=$parameter['total'];
		
		$rows=ceil($total / $pernum);//获取页数
        if(!array_key_exists("page",$GET)){$GET['page']=1;}
		if($parameter['listitem']){
			foreach($GET as $k => $v){
				if($k!="main_page" && $k!="page" && $k != "sort"){$str.=$k."=".$v."&";}
			}
			
			$str.="#";
			$str=str_replace("&#","",$str);
			$str=str_replace("#","",$str);
			if(isset($parameter['listsize'])){
				$listsize=$parameter["listsize"];
				if($listsize<5) $listsize=5;
			}else{
				$listsize="5";
			}
			if($listsize%2==0){
				$n1=($listsize/2)-1;
				$n2=$listsize/2;
			}else{
				$n1=$n2=($listsize-1)/2;
			}
			
			$a=$GET['page']-$n1;
			if($a<=0){$n2=$n2+abs($a)+$GET['page']+1;$n1=1;}
			else{$n1=$GET['page']-$n1;$n2=$n2+$GET['page']+1;}
		    if($rows<=$listsize){
				$n1=1;$n2=$rows;
			}
			
			for($i=1;$i<=$rows;$i++){
				if( $n1 <= $i && $i <= $n2 ){
					$list['list'][$i]['text']=$i;
					$list['list'][$i]['link']=zen_href_link($GET['main_page'],$str."&page=".$i);
					if($i==$GET['page']){
						$list['list'][$i]['status']="active";
					}
				}
			}
			if($GET['page']==1){
				$list['next']['text']="Next";
				$list['next']['link']=$list['list'][$GET['page']+1]['link'];
			}
			else if($GET['page']>1 && $GET['page']<$rows){
				$list['prev']['text']="Previous";
				$list['prev']['link']=$list['list'][$GET['page']-1]['link'];
				$list['next']['text']="Next";
				$list['next']['link']=$list['list'][$GET['page']+1]['link'];
			}
			else if($GET['page']==$rows){
				$list['prev']['text']="Previou";
				$list['prev']['link']=$list['list'][$GET['page']-1]['link'];
			}
		
			$list['first']=array('text'=>'Home page','link'=>zen_href_link($GET['main_page'],$str."&page=1"));
			$list['last']=array('text'=>'Last page','link'=>zen_href_link($GET['main_page'],$str."&page=".$rows));
		
			if($GET['page']==1){unset($list['first']);unset($list['prev']);}
			if($GET['page']==$rows){unset($list['last']);unset($list['next']);}
			
		}
		if($parameter['info']){
			$list['info']['pageno']=$rows;
			$list['info']['total']=$total;
			$list['info']['pernum']=$pernum;
			if($total < $GET['page']*$pernum){
				$list['info']['range']=((($GET['page']-1)*$pernum)+1)."-".$total;
			}else{
				$list['info']['range']=((($GET['page']-1)*$pernum)+1)."-".$GET['page']*$pernum;
			}
		}
		return $list;
	}
	
	function get_pagelist_total(){
		global $predata_class;
		$parameter=array('cid'=>"all","table"=>TABLE_BLOG_ARCHIVES,"as"=>"a","status"=>"archives_status");

		$sql=$this->get_pagelist_sql($_GET,$parameter);
		
		$count=openzcQuery($sql);
		return $count->fields['count'];
	}
	function get_pagelist_sql($GET,$parameter){
		global $predata_class;
		$as=$parameter['as'];
		$table=$parameter["table"]." ".$as;
		$where=" ".$as.".".$parameter["status"]."=1";
		if($parameter['cid']!=="all"){
			$where.=" and a.master_categories_id in('".$parameter['cid']."')";
		}
		if($parameter["table"]!=TABLE_BLOG_ARCHIVES){
			$table.=" left join ".TABLE_BLOG_ARCHIVES." a on a.archives_id=".$as.".archives_id";
		}
		foreach($GET as $k => $v){
			switch($k){
				case "keyword":
					$table.=" left join ".TABLE_BLOG_ARCHIVES_DESCRIPTION." ad on ad.archives_id=a.archives_id";
					$key=" and ad.archives_name like '%".$v."%' and ad.language_id='".(int)$_SESSION['languages_id']."'";
					$where.=$key;
				break;
				case "categories_id":
					$cid=" and a.master_categories_id in('".$v."')";
				break;
			}
		}
		$sql="select count(distinct a.archives_id) as count from ".$table." where ".$where;
		
		return $sql;
	}
	function get_pagelist_pernum(){
		if(array_key_exists("limit",$_GET)){
			$pernum=$_GET['limit'];
		}else{
			$file=TPLCACHE_TPL."tplCacheCount_".md5("tplBlogCacheCount").".inc";
			$json=file_get_contents($file);
			$json=json_decode($json,true);
			$pernum=$json[$_GET['main_page']]['pagesize'];
		}
		return $pernum;
	}
	function get_archives_reviews(){
		$sql="select r.*,rd.* from ".TABLE_BLOG_REVIEWS." r,".TABLE_BLOG_REVIEWS_DESCRIPTION." rd where r.reviews_id=rd.reviews_id and rd.language_id='".(int)$_SESSION['languages_id']."' and r.reviews_status=1";
		$data=openzcQuery($sql);
		$data=openzc_table_to_list($data);
		foreach($data as $k => $v){
			$result[$v['archives_id']][]=$v;
		}
		return $result;
	}
	
}
?>