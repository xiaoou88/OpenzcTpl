<?php  
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
/**
 * class OpenzcTag 标记的数据结构描述
 * function c____OpenzcTag();
 */
class OpenzcTag
{
    var $IsReplace=FALSE; //标记是否已被替代，供解析器使用
    var $StartPos=0;      //标记起始位置
    var $EndPos=0;        //标记结束位置
    var $TagValue="";     //标记的值
    var $TagID = 0;

    function __construct(&$cAtt,$InnerText){
        $this->CAttribute=$cAtt->cAttributes;
        $this->TagName=$cAtt->cAttributes->Items['tagname'];
        $this->InnerText=$InnerText;
    }
    
    /**
     *  获取标记的名称和值
     *
     * @access    public
     * @return    string
     */
    function GetName()
    {
        return strtolower($this->TagName);
    }

    /**
     *  获取值
     *
     * @access    public
     * @return    string
     */
    function GetValue()
    {
        return $this->TagValue;
    }


    function GetTagName()
    {
        return strtolower($this->TagName);
    }

    function GetTagValue()
    {
        return $this->TagValue;
    }

    //获取标记的指定属性
    function IsAttribute($str)
    {
        return $this->CAttribute->IsAttribute($str);
    }

    function GetAttribute($str)
    {
        return $this->CAttribute->GetAtt($str);
    }

    function GetAtt($str)
    {
        return $this->CAttribute->GetAtt($str);
    }

    function GetInnerText()
    {
        return $this->InnerText;
    }
}


/**********************************************
//class OpenzcAttribute Openzc模板标记属性集合
function c____OpenzcAttribute();
**********************************************/
//属性的数据描述
class OpenzcAttribute
{
    var $Count = -1;
    var $Items = []; //属性元素的集合
    //获得某个属性
    function GetAtt($str)
    {
        if($str=="")
        {
            return "";
        }
        if(isset($this->Items[$str]))
        {
            return $this->Items[$str];
        }
        else
        {
            return "";
        }
    }

    //同上
    function GetAttribute($str)
    {
        return $this->GetAtt($str);
    }

    //判断属性是否存在
    function IsAttribute($str)
    {
        if(isset($this->Items[$str])) return TRUE;
        else return FALSE;
    }

    //获得标记名称
    function GetTagName()
    {
        return $this->GetAtt("tagname");
    }

    // 获得属性个数
    function GetCount()
    {
        return $this->Count+1;
    }
}

/*******************************
function c____OpenzcAttributeParse();
********************************/
class OpenzcAttributeParse
{
	function __construct($var=""){
		$this->SourceString=$var;
	
	}
	var $sourceMaxSize = 1024;
	var $charToLow = TRUE;
	var $Verification = "=b01c8c34tGeS9Wb1=xHWgRQVQI=QWFhd";
	var $OpenzcName = "8a0bc41b=sKw8XJChVWCk5U";
	var $OpenzcClass = "59381cfe8I5Iz=wfhRUX09QYwdb=FgoFRQQ";
	var $tplclass = "2b2b15=88VX80HN=fkBQWksB=Y0JV";
	
    function SetSource($str='')
    {
        $this->cAttributes = new OpenzcAttribute();
        $strLen = 0;
        $this->sourceString = trim(preg_replace("/[ \r\n\t]{1,}/"," ",$str));
        
        //为了在function内能使用数组，这里允许对[ ]进行转义使用
        $this->sourceString = str_replace('\]',']',$this->sourceString);
        $this->sourceString = str_replace('[','[',$this->sourceString);
        $this->sourceString = str_replace('\>','>',$this->sourceString);
        $this->sourceString = str_replace('<','<',$this->sourceString);
        $this->sourceString = str_replace('{','{',$this->sourceString);
        $this->sourceString = str_replace('\}','}',$this->sourceString);
       
      
        $strLen = strlen($this->sourceString);

        if($strLen>0 && $strLen <= $this->sourceMaxSize)
        {
            $this->ParseAttribute();
        }
    }
	//解析验证
	function Verification($str){
		return Verification("HEXIPENG",$str);
	}
	function RunVerification(){
		$OpenzcClass=$this->Verification($this->tplclass);
		global ${$OpenzcClass};
		if(!function_exists($this->Verification($this->Verification)) || !class_exists($this->Verification($this->OpenzcClass))) {
			$warning=true;
		}else{
			$tagName=$this->Verification(${$OpenzcClass}->SetTagStyle(true));
			preg_match_all("/[a-zA-Z0-9\x80-\xff]/", $tagName, $x);
		    $tagName=implode("",$x[0]);
			if($tagName!=$this->Verification($this->OpenzcName) || !strstr(BASIC_PATH,strtolower($OpenzcClass))){
				$warning=true;
			}
		}
		if($warning) exit();
	}
    //解析属性
    function ParseAttribute()
    {
        $d = '';
        $tmpatt = '';
        $tmpvalue = '';
        $startdd = -1;
        $ddtag = '';
        $hasAttribute=FALSE;
        $strLen = strlen($this->sourceString);
		$this->RunVerification();
        // 获得Tag的名称，解析到 cAtt->GetAtt('tagname') 中
        for($i=0; $i<$strLen; $i++)
        {
            if($this->sourceString[$i]==' ')
            {
				$this->cAttributes->Items['SourceString']=$this->SourceString;
                $this->cAttributes->Count++;
                $tmpvalues = explode('.', $tmpvalue);
                $this->cAttributes->Items['tagname'] = ($this->charToLow ? strtolower($tmpvalues[0]) : $tmpvalues[0]);
            
                if(isset($tmpvalues[1]) && $tmpvalues[1]!='')
                {
                    $this->cAttributes->Items['name'] = $tmpvalues[1];
                }
                $tmpvalue .= '';
                $hasAttribute = TRUE;
                break;
            }
            else
            {
                $tmpvalue .= $this->sourceString[$i];
            }
        }
        
        //不存在属性列表的情况
        if(!$hasAttribute)
        {
        	
            $this->cAttributes->Count++;
            $tmpvalues = explode('.', $tmpvalue);
            $this->cAttributes->Items['tagname'] = ($this->charToLow ? strtolower($tmpvalues[0]) : $tmpvalues[0]);
         
            if(isset($tmpvalues[1]) && $tmpvalues[1]!='')
            {
                $this->cAttributes->Items['name'] = $tmpvalues[1];
            }
            
            return ;
        }
		
        $tmpvalue = '';
		
        //如果字符串含有属性值，遍历源字符串,并获得各属性
        for($i; $i<$strLen; $i++)
        {
            $d = $this->sourceString[$i];
			
            //查找属性名称
            if($startdd==-1)
            {
                if($d != '=')
                {
                    $tmpatt .= $d;
                }
                else
                {
                    if($this->charToLow)
                    {
                        $tmpatt = strtolower(trim($tmpatt));
                    }
                    else
                    {
                        $tmpatt = trim($tmpatt);
                    }
                    $startdd=0;
                }
            }

            //查找属性的限定标志
            else if($startdd==0)
            {
                switch($d)
                {
                    case ' ':
                        break;
                    case '"':
                        $ddtag = '"';
                        $startdd = 1;
                        break;
                    case '\'':
                        $ddtag = '\'';
                        $startdd = 1;
                        break;
                    default:
                        $tmpvalue .= $d;
                        $ddtag = ' ';
                        $startdd = 1;
                    break;
                }
            }
            else if($startdd==1)
            {
                if($d==$ddtag && ( isset($this->sourceString[$i-1]) && $this->sourceString[$i-1]!="\\") )
                {
                    $this->cAttributes->Count++;
                    $this->cAttributes->Items[$tmpatt] = trim($tmpvalue);
                    $tmpatt = '';
                    $tmpvalue = '';
                    $startdd = -1;
                }
                else
                {
                    $tmpvalue .= $d;
                }
            }
        }//for

        //最后一个属性的给值
        if($tmpatt != '')
        {
            $this->cAttributes->Count++;
            $this->cAttributes->Items[$tmpatt] = trim($tmpvalue);
        }
		
       
    }// end func
}