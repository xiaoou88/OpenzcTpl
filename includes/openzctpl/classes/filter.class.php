<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
//商品筛选过滤导航类库
class filter{
	//获取已选导航
	function get_active_bar($GET){
		global $predata_class,$currencies;
		$symbol=$currencies->currencies[$_SESSION['currency']]['symbol_left'];
		$row=0;
		foreach($GET as $k => $v){
			switch($k){
				case "brand":
					$rs[$row]['item_id']=$row;
					$rs[$row]['filter_item']=$k;
					$manufacturers=$predata_class->getPredata(TABLE_MANUFACTURERS);
					$parameter=$this->get_page_parameter($GET,"brand");
					$array=explode(",",$GET['brand']);
					foreach($array as $a => $b){
						$brand=$array;
						unset($brand[$a]);
						$brand_ids=join(",",$brand);
						$bar['filter_name']=$manufacturers[$b]['manufacturers_name'];
						if($brand_ids){$bar['filter_link']=zen_href_link($GET['main_page'],$parameter."&brand=".$brand_ids);}
						else{$bar['filter_link']=zen_href_link($GET['main_page'],$parameter);}
						$rs[$row]['values'][]=$bar;
					}
					$row++;
				break;
				case "price":
					$rs[$row]['item_id']=$row;
					$rs[$row]['filter_item']=$k;
					$parameter=$this->get_page_parameter($GET,"price");
					$array=explode("|",$GET['price']);
					//$array=$this->price_exchange_rate($array);
					
					foreach($array as $a => $b){
						$price=$array;
						unset($price[$a]);
						$price_ids=openzc_get_each_ids($price,'',"|");
						$b=$this->price_exchange_rate(explode(",",$b));
						$b=$symbol.$b[0].",".$symbol.$b[1];
						$bar['filter_name']=str_replace(","," to ",$b);
						if($price_ids){$bar['filter_link']=zen_href_link($GET['main_page'],$parameter."&price=".$price_ids);}
						else{$bar['filter_link']=zen_href_link($GET['main_page'],$parameter);}
						$rs[$row]['values'][]=$bar;
					}
					$row++;
				break;
				case "options":
					$options=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS);
					$options_values=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS_VALUES);
					$values_to_options=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS);
					$parameter=$this->get_page_parameter($GET,"options");
					$array=explode(",",$GET['options']);
					
					foreach($array as $a => $b){
						$options_id=$values_to_options[$b]['products_options_id'];
						$arrays=$array;
						unset($arrays[$a]);
						$options_ids=openzc_get_each_ids($arrays);
						$bar['filter_name']=$options_values[$b]['products_options_values_name'];
						if($options_ids){$bar['filter_link']=zen_href_link($GET['main_page'],$parameter."&options=".$options_ids);}
						else{$bar['filter_link']=zen_href_link($GET['main_page'],$parameter);}
						$data[$options_id][]=$bar;
					}
					foreach($data as $a => $b){
						$rs[$row]['item_id']=$row;
						$rs[$row]['filter_item']=$options[$a]['products_options_name'];
						foreach($b as $c => $d){
							$rs[$row]['values'][]=$d;
						}
						$row++;
					}
				break;
			}
		}
		
		return $rs;
	}
	//根据品牌筛选商品
	function get_brand_bar($GET,$mul){
		global $predata_class;
		
		$manufacturers=$predata_class->getPredata(TABLE_MANUFACTURERS);
	
		$parameter=$this->get_page_parameter($GET,"brand");
		
		$sql=$this->get_filter_sql("brand",$GET);
		
		$data=openzcQuery($sql);
		
		$data=openzc_table_to_list($data,"manufacturers_id");
		unset($data[0]);
	
		$filtered=array_filter(explode(",",$GET['brand']));
		$filtered=openzc_field_to_key($filtered);
		
		if(count($data)>0){
			foreach($manufacturers as $k => $v){
				if(!$data[$k]['filter_count']){break;}
				$filtereds=$filtered;
				
				if($mul!='true' && !array_key_exists($k,$filtered) && is_array($filtered)){
					continue;
				}
				if(array_key_exists($k,$filtereds)){
					$rs[$k]['status']="active";
					unset($filtereds[$k]);
				}else{
					$rs[$k]['status']="";
					$filtereds[$k]=$k;
				}

				$rs[$k]['filter_id']=$k;
				$rs[$k]['filter_count']=$data[$k]['filter_count'];
				$rs[$k]['filter_name']=$v['manufacturers_name'];
				$rs[$k]['filter_image']=$v['manufacturers_image'];
				if(count($filtereds)>0){
					$rs[$k]['filter_link']=zen_href_link($GET['main_page'],$parameter."&brand=".join(",",$filtereds));
				}else{
					$rs[$k]['filter_link']=zen_href_link($GET['main_page'],$parameter);
				}
			}
	
			return $rs;
		}
		
		return false;
		
		
	}
	
	//根据商品价格筛选
	function get_price_bar($GET,$space='',$mul=true,$slide=''){
		global $Predata,$currencies;
		$cid=$this->get_current_cid($GET);
		$cid=str_replace(",","','",$cid);
		$parameter=$this->get_page_parameter($GET,"price");
		
		switch($_GET['main_page']){
			case "specials":
				$sql="select max(specials_new_products_price) as max,min(specials_new_products_price) as min from ".TABLE_SPECIALS." where status=1";
			break;
			case "featured_products":
				$sql="select max(p.products_price_sorter) as max,min(p.products_price_sorter) as min from ".TABLE_FEATURED." f left join ".TABLE_PRODUCTS." p on p.products_id=f.products_id where f.status=1";
			break;
			case "advanced_search_result":
				$sql="select max(p.products_price_sorter) as max,min(p.products_price_sorter) as min from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_DESCRIPTION." pd on p.products_id=pd.products_id left join ".TABLE_SPECIALS." s on s.products_id=p.products_id where pd.products_name like '%".$_GET['keyword']."%' and if(s.status,p.products_price_sorter=s.specials_new_products_price,p.products_price_sorter) and pd.language_id=".(int)$_SESSION['languages_id'];
			break;
			case "advanced_search":
				$sql="select max(p.products_price_sorter) as max,min(p.products_price_sorter) as min from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_DESCRIPTION." pd on p.products_id=pd.products_id left join ".TABLE_SPECIALS." s on s.products_id=p.products_id where pd.products_name like '%".$_GET['keyword']."%' and if(s.status,p.products_price_sorter=s.specials_new_products_price,p.products_price_sorter) and pd.language_id=".(int)$_SESSION['languages_id'];
			break;
			default:
				$cid_where=" and p.master_categories_id in('".$cid."')";
				if($_GET['main_page']=="products_all"){
					$cid_where="";
				}
				$sql="select max(p.products_price_sorter) as max,min(p.products_price_sorter) as min from ".TABLE_PRODUCTS." p left join ".TABLE_SPECIALS." s on p.products_id=s.products_id where if(s.status=1,p.products_price_sorter=s.specials_new_products_price,p.products_price_sorter)".$cid_where;
			break;
		}
			
		$range=openzcQuery($sql);
		$max=$range->fields['max'];
		$min=$range->fields['min'];
		
	
		$data=$this->get_price_section($min,$max,$space,$slide);
	
		foreach($data as $k => $v){
			$sql=$this->get_filter_sql("price",$GET,[$v['min'],$v['max']],$mul); 
			//echo $sql."\r\n";
			$count=openzcQuery($sql);
			$data[$k]['filter_count']=$count->fields['filter_count'];
		}
		if($slide==true){
			$rate=$currencies->currencies[$_SESSION['currency']]['value'];
			foreach($data as $k => $v){
				$v['min']=floor($min*$rate);
				$v['max']=ceil($max*$rate);
				if(isset($_SESSION["filter_price_min"])){
					$v['min']=$_SESSION["filter_price_min"];
					unset($_SESSION["filter_price_min"]);
				}
				if(isset($_SESSION["filter_price_max"])){
					$v['max']=$_SESSION["filter_price_max"];
					unset($_SESSION["filter_price_max"]);
				}
				
				$v['filter_link']=zen_href_link($GET['main_page'],$parameter);
				$rs[]=$v;
			}
			return $rs;
		}
		foreach($data as $k => $v){
			if(array_key_exists("price",$GET)){
				$price=explode("|",$GET['price']);
				$price=array_filter($price);
			}else{
				$price=array();
			}
			
			if($v['filter_count']>0){
				$limits=$v['min'].",".$v['max'];
				if(strstr("#".$GET['price']."#","#".$limits."#") && array_key_exists("price",$GET)){
					$v['status']='active';
					foreach($price as $a => $b){
						if($b==$limits)unset($price[$a]);
					}
					if(count($price)==0){
						$v['filter_link']=zen_href_link($GET['main_page'],$parameter);
					}else{
						$v['filter_link']=zen_href_link($GET['main_page'],$parameter."&price=".join("|",$price));
					}
				}else{
					$v['status']='';
					if($mul==true && count($price)>0){
						$price[]=$limits;
						$v['filter_link']=zen_href_link($GET['main_page'],$parameter."&price=".join("|",$price));
					}else{
						$v['filter_link']=zen_href_link($GET['main_page'],$parameter."&price=".$limits);
					}
				}
			
				$rs[]=$v;
			}
		}
		
		
		return $rs;
	}
	
	//根据商品属性筛选
	function get_attr_bar($GET,$options_id='',$mul=false){
		global $predata_class;
		if($options_id){
			$options_id=explode(",",$options_id);
			$options_id=openzc_field_to_key($options_id);
		}else{
			$options_id=array();
		}
	
		$options=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS);
		$options_values=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS_VALUES);
		$parameter=$this->get_page_parameter($GET,"options");
		
		
		$cid=$this->get_current_cid($GET);
		$cid=str_replace(",","','",$cid);
		switch($_GET['main_page']){
			case "specials":
				$sql="select pa.options_id from ".TABLE_SPECIALS." s left join ".TABLE_PRODUCTS." p on p.products_id=s.products_id left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=s.products_id where s.status=1 group by pa.options_id";
			break;
			case "featured_products":
				$sql="select pa.options_id from ".TABLE_FEATURED." f left join ".TABLE_PRODUCTS." p on p.products_id=f.products_id left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=f.products_id where f.status=1 group by pa.options_id";
			break;
			case "advanced_search_result":
				if(isset($_GET['keyword'])){
					$sql="pd.products_name like '%".$_GET['keyword']."%'";
					if(isset($_GET['categories_id'])){
						$sql.=" and p.master_categories_id in('".$_GET['categories_id']."')";
					}
					$sql="select pa.options_id from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_DESCRIPTION." pd on p.products_id=pd.products_id left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id where ".$sql." and pd.language_id=".(int)$_SESSION['languages_id']." group by pa.options_id";
				}
			break;
			case "products_all":
				$sql="select pa.options_id from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id group by pa.options_id";
			break;
			default:
				$sql="select pa.options_id from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id where p.master_categories_id in('".$cid."') group by pa.options_id";
			break;
		}
		
		$options_ids=openzcQuery($sql);
		$options_ids=array_filter(openzc_table_to_list($options_ids,"options_id"));
		
		
		$data=array();
	
		$result=array();
		
		$modules_options=TPL_MODULES."json/options.json";
		if(is_file($modules_options)){
			$modules_options=json_decode(file_get_contents($modules_options),true);
		}else{
			$modules_options=array();
		}
	
		foreach($options_ids as $k => $v){
			
			if(count($options_id)>0){
				if(!array_key_exists($k,$options_id)){
					continue;
				}
			}
			
			$field="DISTINCT pa.options_values_id as options_values_id";
			
			switch($_GET['main_page']){
				case "specials":
					$sql="select ".$field." from ".TABLE_SPECIALS." s left join ".TABLE_PRODUCTS." p on p.products_id=s.products_id left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=s.products_id where s.status=1 and pa.options_id=".$k." group by pa.options_values_id";
				break;
				case "featured_products":
					$sql="select ".$field." from ".TABLE_FEATURED." f left join ".TABLE_PRODUCTS." p on p.products_id=f.products_id left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=f.products_id where f.status=1 and pa.options_id=".$k." group by pa.options_values_id";
				break;
				case "advanced_search":
					if(isset($_GET['keyword'])){
						$sql="pd.products_name like '%".$_GET['keyword']."%'";
						if(isset($_GET['categories_id'])){
							$sql.=" and p.master_categories_id in('".$_GET['categories_id']."')";
						}
						$sql="select ".$field." from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_DESCRIPTION." pd on p.products_id=pd.products_id left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id where ".$sql." and pd.language_id=".(int)$_SESSION['languages_id']." and pa.options_id=".$k." group by pa.options_values_id";
					}
				break;
				case "advanced_search_result":
					if(isset($_GET['keyword'])){
						$sql="pd.products_name like '%".$_GET['keyword']."%'";
						if(isset($_GET['categories_id'])){
							$sql.=" and p.master_categories_id in('".$_GET['categories_id']."')";
						}
						$sql="select ".$field." from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_DESCRIPTION." pd on p.products_id=pd.products_id left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id where ".$sql." and pd.language_id=".(int)$_SESSION['languages_id']." and pa.options_id=".$k." group by pa.options_values_id";
					}
				break;
				case "products_all":
					$sql="select ".$field." from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id where pa.options_id=".$k." group by pa.options_values_id";
				break;
				default:
					$sql="select ".$field." from ".TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id where p.master_categories_id in('".$cid."') and pa.options_id=".$k." group by pa.options_values_id";
				break;
			}
			
			$options_values_ids=openzcQuery($sql);
			$options_values_ids=openzc_table_to_list($options_values_ids);
			$opvids=array();
			foreach($options_values_ids as $a => $b){
				$opvids[]=$b['options_values_id'];
			}
			$opvids="'".join("','",$opvids)."'";
		
			
			$sql=$this->get_filter_sql("options",$GET,$k,"",$opvids);
		
			$data=openzcQuery($sql);
			$data=openzc_table_to_list($data,"options_values_id");
			
			$filtered=array_filter(explode(",",$GET['options']));
			$filtered=openzc_field_to_key($filtered);
			if(count($filtered)>0){
				foreach($filtered as $a => $b){
					if(array_key_exists($b,$data)){
						$data[$b]['status']="active";
						if(!$mul){
							$array[$b]=$data[$b];
							$data=array();
							$data[$b]=$array[$b];
						}
					}
				}
			}
			
			
			
			foreach($data as $a => $b){
				$filtereds=$filtered;
				if($b['status']=="active"){
					unset($filtereds[$a]);
				}else{
					$filtereds[]=$a;
				}
				$b['filter_name']=$options_values[$a]['products_options_values_name'];
				$b['filter_image']=$options_values[$a]['products_options_values_image'];
				$b['filter_color']=$options_values[$a]['products_options_values_color'];
				
				if(isset($modules_options[$a])){
					if(isset($modules_options[$a]['image']) && $modules_options[$a]['image']){$b['filter_image']=$modules_options[$a]['image'];}
					if(isset($modules_options[$a]['color']) && $modules_options[$a]['color']){$b['filter_color']=$modules_options[$a]['color'];}
				}
				
				if(count($filtereds)>0){
					$b['filter_link']=zen_href_link($GET['main_page'],$parameter."&options=".join(",",$filtereds));
				}else{
					$b['filter_link']=zen_href_link($GET['main_page'],$parameter);
				}
				$data[$a]=$b;
			}
		
			$result[$k]['filter_item']=$options[$k]['products_options_name'];
			$result[$k]['options_id']=$k;
			$result[$k]['values']=$data;
		
		}
		
		return $result;
	}
  
	
	function get_table_ids($array,$v){
		if($v=="true"){
			foreach($array as $k => $v){$str.=$v.",";}
		}else{
			foreach($array as $k => $v){$str.=$k.",";}
		}
		$str.="#";$str=str_replace(",#","",$str);
		$str.="#";$str=str_replace("#","",$str);
		return $str;
	}
	
	function get_options_values_ids($str,$values_id,$options_id,$mul){
		global $predata_class;
		$products_options_values_to_products_options=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS);
		$str=explode(",",$str);
		$str=array_filter($str);
		if(!$mul){
			foreach($str as $k => $v){
				if($products_options_values_to_products_options[$v]['products_options_id']==$options_id){$str[$k]=$values_id;$n++;break;}
			}
			if($n==0){$str[]=$values_id;}
		}
	
		if($mul==true){$str[]=$values_id;}
	  
		if(count($str)==1){$ids=$str[0];}
		else{
			$ids=array_unique($str);
			$ids=$this->get_table_ids($ids,"true");
		}
	
		return $ids;
	}
	
	function get_page_parameter($GET,$parameter){
		
		foreach($GET as $k => $v){
			
			if($k!="main_page" && $k!=$parameter && $k!="page" && $k!="sort" && $k!="order" && $k!="disp_order"){$str.=$k."=".$v."&";}
		}
		$str.="#";
		$str=str_replace("&#","",$str);
		$str=str_replace("#","",$str);
		return $str;
	}
	
	function get_price_section($min,$max,$space,$slide){
		global $currencies;
		$symbol=$currencies->currencies[$_SESSION['currency']]['symbol_left'];
		if($slide==true){
			$rs[0]['symbol']=$symbol;
			
		}else{
			if(!$space){$space=$max;}
			$space=number_format($space,2);
			$space_rate=number_format($this->price_exchange_rate($space),2);
			$n=$max/$space;
			if($n<1){$n=1;}
			$rs=array();
			$j=$l=0;
		
			for($i=0;$i<$n;$i++){
				$rs[$i]['filter_name']=$symbol.$j." - ".$symbol.($j+$space_rate);
				$rs[$i]['min']=$l;
				$rs[$i]['max']=$l+$space;
				$j=$j+$space_rate;
				$l=$l+$space;
			}
		}
		return $rs;
	}
	
	function price_exchange_rate($data){
		global $currencies;
	  
		if(is_array($data)){
			foreach($data as $k => $v){
				$rs[]=floor($v*($currencies->currencies[$_SESSION['currency']]['value']));
			}
		}else{
			$rs=floor($data*($currencies->currencies[$_SESSION['currency']]['value'])); 
		}
		
		return $rs;
	}
	private function get_current_cid($GET){
		global $predata_class;
		$categories=$predata_class->getPredata(TABLE_CATEGORIES);
		$cid=openzc_get_current_cpath($GET['cPath']);
		$cids=openzc_get_son_ids($categories,$cid,$cid);
		return $cids;
	}
	private function get_filter_sql($item,$GET,$range='',$mul=false,$opids=''){
		global $predata_class;
		$cid=$this->get_current_cid($GET);
		$cid=str_replace(",","','",$cid);
		$table=TABLE_PRODUCTS." p left join ".TABLE_PRODUCTS_DESCRIPTION." pd on p.products_id=pd.products_id";
		$field="count(distinct p.products_id) as filter_count,p.manufacturers_id";
		if($_GET['main_page']=="products_all"){
			$where="p.master_categories_id > 0";
		}else{
			$where="p.master_categories_id in('".$cid."')";
		}
		
		switch($_GET['main_page']){
			case "specials":
				$table=TABLE_SPECIALS." s left join ".TABLE_PRODUCTS." p on p.products_id=s.products_id left join ".TABLE_PRODUCTS_DESCRIPTION." pd on s.products_id=pd.products_id";
				$where="s.status=1";
			break;
			case "featured_products":
				$table=TABLE_FEATURED." f left join ".TABLE_PRODUCTS." p on p.products_id=f.products_id left join ".TABLE_PRODUCTS_DESCRIPTION." pd on f.products_id=pd.products_id";
				$where="f.status=1";
			break;
			case "advanced_search_result":
				if(isset($_GET['keyword'])){
					$where="pd.products_name like '%".$_GET['keyword']."%'";
					if(isset($_GET['categories_id'])){
						$where.=" and p.products_id in('".$_GET['categories_id']."')";
					}
				}
			break;
			case "advanced_search":
				if(isset($_GET['keyword'])){
					$where="pd.products_name like '%".$_GET['keyword']."%'";
					if(isset($_GET['categories_id'])){
						$where.=" and p.products_id in('".$_GET['categories_id']."')";
					}
				}
			break;
		}
		
		foreach($GET as $k => $v){
			$sql="";
			switch($k){
				case "brand":
					$v=explode(",",$v);
					$v=array_filter($v);
					if($item!=$k){
						
						$v="'".join("','",$v)."'";
						$sql.=" and p.manufacturers_id in(".$v.")";
					}
					$where.=$sql;
				break;
				case "price":
					$v=explode("|",$v);
					$v=array_filter($v);
					if($range && $item==$k){
						if($_GET['main_page']=="specials"){
							$sql.="s.specials_new_products_price>='".$range[0]."' and s.specials_new_products_price < '".$range[1]."'";
						}else{
							$sql.="p.products_price_sorter>='".$range[0]."' and p.products_price_sorter < '".$range[1]."'";
						}
					}else{
						$first=key($v);
						foreach($v as $a => $b){
							$b=explode(",",$b);
							if(count($v)>1){
								if($first==$a){
									$sql.="(p.products_price_sorter>='".$b[0]."' and p.products_price_sorter < '".$b[1]."') ";
								}else{
									$sql.=" or (p.products_price_sorter>='".$b[0]."' and p.products_price_sorter < '".$b[1]."')";
								}
							}else{
								if($_GET['main_page']=="specials"){
									$sql.="s.specials_new_products_price>='".$b[0]."' and s.specials_new_products_price < '".$b[1]."'";
								}else{
									$sql.="p.products_price_sorter>='".$b[0]."' and p.products_price_sorter < '".$b[1]."'";
								}
							}
						}
					}
					$where.=" and (".$sql.")";
				break;
				case "options":
					$v=explode(",",$v);
					$v=array_filter($v);
					$vtp=$predata_class->getPredata(TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS);
					foreach($v as $a => $b){
						$options[$vtp[$b]['products_options_id']]['values'][]=$b;
					}
					
				
					if($range && $item==$k){
						unset($options[$range]);
						foreach($options as $a => $b){
							$vid="'".join("','",$b['values'])."'";
							$sql.=" and p.products_id in ( select products_id from ".TABLE_PRODUCTS_ATTRIBUTES." where options_values_id in(".$vid.") )";
						}
					
					}else{
						$table.=" left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id";
						foreach($options as $a => $b){
							$vid="'".join("','",$b['values'])."'";
							$sql.=" and p.products_id in ( select products_id from ".TABLE_PRODUCTS_ATTRIBUTES." where options_values_id in(".$vid.") )";
						}
						$where.=" ";
					}
					$where.=$sql;
				break;
			}
		}
		if($item=="brand"){
			$where.=" group by p.manufacturers_id";
		}
		else if($item=="price" && !array_key_exists("price",$GET)){
			if($_GET['main_page']=="specials"){
				$where .= " and s.specials_new_products_price>='".$range[0]."' and s.specials_new_products_price < '".$range[1]."'";
			}else{
				$table.=" left join ".TABLE_SPECIALS." s on p.products_id=s.products_id";
				$where .= " and if(s.status,s.specials_new_products_price>='".$range[0]."' and s.specials_new_products_price <'".$range[1]."',p.products_price_sorter>='".$range[0]."' and p.products_price_sorter < '".$range[1]."')";
			}
		}
		else if($item=="options"){
			$field.=",pa.options_values_id";
			$table.=" left join ".TABLE_PRODUCTS_ATTRIBUTES." pa on pa.products_id=p.products_id";
			$where .=" and pa.options_id='".$range."' group by pa.options_values_id";
		}
		$sql="select ".$field." from ".$table." where pd.language_id='".(int)$_SESSION['languages_id']."' and ".$where;
		
		return $sql;
	}
}
?>