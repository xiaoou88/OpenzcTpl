<?php
/**
 * @link         http://www.openzc.cn/
 * @author       hexipeng | e-mail:2017412656@qq.com hxpjason@gmail.com
 * @copyright    Copyright (c) 2020, DesDev, Inc.
 * @license      http://www.openzc.cn/license.txt
 */
header("Content-type: text/html; charset=utf-8");
define('ROOT_PATH',str_replace('\\','/',dirname(dirname(dirname(__FILE__))).'/'));
define('INCLUDES_PATH',str_replace('\\','/',dirname(dirname(__FILE__))).'/');
define('BASIC_PATH',str_replace('\\','/',dirname(__FILE__)).'/');

define('CACHE_DIR',     ROOT_PATH  .'cache/tplcache/'); 	//网页缓存目录
define('IMGCACHE_DIR',	ROOT_PATH  .'images/imgcache/');	//商品图片缓存目录
define('CONFIG_DIR',    BASIC_PATH .'config/');       		//配置目录
define('MODEL_DIR',     BASIC_PATH .'model/');        		//模型目录
define('FUNCTION_DIR',	BASIC_PATH .'functions/');	  		//函数库目录
define('CLASS_DIR',		BASIC_PATH .'classes/');	  		//工具类目录

require(FUNCTION_DIR.'common.function.php');
require(CONFIG_DIR.'config.php');
require(CONFIG_DIR."config.core.php");
require(FUNCTION_DIR.'common.request.php');

if(isGet()){$GET=$_GET;}else{$GET=[];}
  
if(empty($sessions)){$sessions='';}
if(!isPost() && !isAjax() && !$sessions){
	require(CONFIG_DIR."cache.speed.php");
}
//OpenzcAjax
if(isAjax()){
	$parse=parse_url($_SERVER['HTTP_REFERER']);
	if(isset($parse['query'])){
		$query=explode("&",$parse['query']);
		foreach($query as $k => $v){
			$v=explode("=",$v);
			if($_COOKIE['html']==true){
				$_GET[$v[0]]=$v[1];
			}else{
				if($v[0]=="main_page"){$_GET['main_page']=$v[1];break;}
			}
		}
	}
}
require(INCLUDES_PATH.'application_top.php');


/*模板路径设置*/
define("TEMPLATE_TPL",DIR_WS_TEMPLATE."html/");
define("TPLCACHE_TPL",DIR_WS_TEMPLATE."tplcache/");
define("TPL_MODULES" ,TEMPLATE_TPL."modules/");
define("TPL_AJAX_BOX" ,TEMPLATE_TPL."ajax/");

if(!empty($_COOKIE['currency'])){
	$_SESSION['currency']=$_COOKIE['currency'];
}

require(CONFIG_DIR."auto.loader.php");
//OpenzcAjax
if(isAjax()){
	foreach($sysAjaxAction as $k => $v){
		if($_POST['action']==$v){$ajax_class->ajaxRun($_POST);exit(0);}
	}
}
//页面映射
$body_code=$OpenzcTpl->TplRoute($current_page_base);

//语言文件加载
if(file_exists(DIR_WS_LANGUAGES.$_SESSION['language']."/".$current_page_base.".php")){
	require(DIR_WS_LANGUAGES.$_SESSION['language']."/".$current_page_base.".php");
}

$directory_array = $template->get_template_part($code_page_directory, '/^header_php/');

foreach ($directory_array as $value) {
	if($variable_class->checkDirectory($GET)==true){
		require($code_page_directory . '/' . $value);
	}
}

if(!strstr($current_page_base,"page_not_found")){require($body_code);}

$tpl_page_body=str_replace("/","",$tpl_page_body);

//自定义页面
if(strstr($current_page_base,"page_not_found") && $custom_page[$GET['main_page']]){
	$tpl_page_body=$custom_page[$GET['main_page']];
	$custom_php_file=$template->get_template_dir("",DIR_WS_TEMPLATE,'templates').$tpl_page_body;
	if(file_exists($custom_php_file)){
		header('HTTP/1.1 200 OK');
		require($custom_php_file);
	}
}

require(CONFIG_DIR."variable.php");
if(!$tpl_page_body){
	foreach($tplpage_route as $k => $v){
		if(strstr($body_code,$k)){$tpl_page_body=$k;break;}
	}
}
$tpl=$tplpage_route[$tpl_page_body][0];
if(!isAjax()){
	$OpenzcTpl->LoadTemplate($tpl);
	$OpenzcTpl->Display();
}else{
	$ajax_class->ajaxRun($_POST);
	exit;
}

$init->saveHtml($_GET,ob_get_contents());

?>