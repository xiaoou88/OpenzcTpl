<div class="menu vertical-menu js-top-menu position-static hidden-md-down">
                    <div id="czverticalmenublock" class="block verticalmenu-block open">
                      <h4 class="expand-more title h3 block_title" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="verticalmenu-dropdown"> Shop By Category <span class="dropdown-arrow"></span> </h4>
                      <div class="block_content verticalmenu_block dropdown-menu" aria-labelledby="verticalmenu-dropdown" id="_desktop_top_menu">
                        <ul class="top-menu" id="top-menu" data-depth="0">
                         {openzc:catelist type="top"}
                          <li class="category menu_icon0" id="czcategory-[field:categories_id/]">
                           {openzc:if $field['has_sub_cat']==1}
                           <a href="[field:categories_link/]" class="dropdown-item" data-depth="0" > 
                           <span class="pull-xs-right hidden-lg-up">
                           	<span data-target="#top_sub_menu_[field:categories_id/]" data-toggle="collapse" class="navbar-toggler collapse-icons">
                           		<i class="fa-icon add">&nbsp;</i><i class="fa-icon remove">&nbsp;</i>
                           	</span>
                           </span> 
                           <span class="pull-xs-right sub-menu-arrow"></span>[field:categories_name/]
                           </a>
                            <div  class="popover sub-menu js-sub-menu collapse   moreColumn" id="top_sub_menu_[field:categories_id/]">
                              <ul class="top-menu"  data-depth="1">
                               {openzc:catelist type="son"}
                                <li class="category  " id="czcategory-4">
                                 <a href="[field:categories_link/]" class="dropdown-item dropdown-submenu" data-depth="1" > 
                                 <span class="pull-xs-right hidden-lg-up">
                                 <span data-toggle="collapse" class="navbar-toggler collapse-icons"><i class="fa-icon add">&nbsp;</i><i class="fa-icon remove">&nbsp;</i></span></span>[field:categories_name/]</a>
                                  <div  class="collapse" id="top_sub_menu_[field:categories_id/]">
                                    <ul class="top-menu"  data-depth="2">
                                     {openzc:catelist type="son"}
                                      <li class="category  " id="czcategory-[field:categories_id/]">
                                      <a href="[field:categories_link/]" class="dropdown-item" data-depth="2" >[field:categories_name/]</a>
                                      </li>
                                     {/openzc:catelist}
                                    </ul>
                                    <div class="menu-images-container"></div>
                                  </div>
                                </li>
                               {/openzc:catelist}
                                
                              </ul>
                              <div class="menu-images-container"><img src="[field:categories_image/]"></div>
                            </div>
                            {else}
							<a href="[field:categories_link/]" class="dropdown-item" data-depth="0" > 
                           <span class="pull-xs-right hidden-lg-up">
                           	<span data-target="#top_sub_menu_[field:categories_id/]" data-toggle="collapse" class="navbar-toggler collapse-icons">
                           		<i class="fa-icon add">&nbsp;</i><i class="fa-icon remove">&nbsp;</i>
                           	</span>
                           </span> 
                          [field:categories_name/]
                           </a>
                            {/openzc:if}
                          </li>
                          {/openzc:catelist}
                          
                        </ul>
                      </div>
                    </div>
                  </div>