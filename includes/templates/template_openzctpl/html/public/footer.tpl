<footer id="footer">
  <div class="footer-before">
    <div class="container"> </div>
  </div>
  <div class="footer-container">
    <div class="container">
      <div class="row footer">
        <div class="col-md-4 links block">
          <h3 class="h3 hidden-md-down">Products</h3>
          <div class="title h3 block_title hidden-lg-up" data-target="#footer_sub_menu_20687" data-toggle="collapse"> <span class="">Products</span> <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="fa-icon add"></i> <i class="fa-icon remove"></i> </span> </span> </div>
          <ul id="footer_sub_menu_20687" class="collapse block_content">
            <li> 
               <a id="link-product-page-prices-drop-1" class="cms-page-link" href="#" > Prices drop </a> </li>
            <li> 
               <a id="link-product-page-new-products-1" class="cms-page-link" href="#" title="Our new products"> New products </a> 
            </li>
            <li> 
               <a id="link-product-page-best-sales-1" class="cms-page-link" href="" title="Our best sales"> Best sales </a> </li>
            <li> 
               <a id="link-static-page-contact-1" class="cms-page-link" href="" title="Use our form to contact us"> Contact us </a> </li>
            <li> 
            	<a id="link-static-page-sitemap-1" class="cms-page-link" href="" title="Lost ? Find what your are looking for"> Sitemap </a> 
            </li>
          </ul>
        </div>
        <div class="col-md-4 links block">
          <h3 class="h3 hidden-md-down">Our company</h3>
          <div class="title h3 block_title hidden-lg-up" data-target="#footer_sub_menu_29294" data-toggle="collapse"> <span class="">Our company</span> <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="fa-icon add"></i> <i class="fa-icon remove"></i> </span> </span> </div>
          <ul id="footer_sub_menu_29294" class="collapse block_content">
            <li> 
               <a id="link-cms-page-1-2" class="cms-page-link" href="" title="Our terms and conditions of delivery"> Delivery </a> </li>
            <li> 
               <a id="link-cms-page-2-2" class="cms-page-link" href="" title="Legal notice"> Legal Notice </a> 
            </li>
            <li> 
               <a id="link-cms-page-3-2" class="cms-page-link" href="" title="Our terms and conditions of use"> Terms and conditions of use </a> 
            </li>
            <li> 
               <a id="link-cms-page-4-2"
                class="cms-page-link"
                href=""
                title="Learn more about us"> About us </a> </li>
            <li> <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href=""
                title="Our secure payment method"> Secure payment </a> </li>
          </ul>
        </div>
        <div class="block-contact col-md-4 links wrapper">
          <h3 class="text-uppercase block-contact-title hidden-sm-down"><a href="">Store information</a></h3>
          <div class="title clearfix hidden-md-up" data-target="#block-contact_list" data-toggle="collapse"> <span class="h3">Store information</span> <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="fa-icon add"></i> <i class="fa-icon remove"></i> </span> </span> </div>
          <ul id="block-contact_list" class="collapse">
            <li> <i class="fa fa-map-marker"></i> <span>Maxcart - Mega Store<br />
              United States</span> </li>
            <li> <i class="fa fa-phone"></i> <span>000-000-0000</span> </li>
            <li> <i class="fa fa-fax"></i> <span>123456</span> </li>
            <li> <i class="fa fa-envelope-o"></i> <span>sales@yourcompany.com</span> </li>
          </ul>
        </div>
        <div class="block_newsletter">
          <div class="row">
            <h4 class="sub_heading title"><span class="news1">Join Our Mailing List</span></h4>
            <div class="sub-title">There are many variations of passages of Lorem Ipsum available</div>
            <div class="block_content">
              <form action="" method="post">
                <div class="newsletter-form">
                  <input class="btn btn-primary pull-xs-right hidden-xs-down" name="submitNewsletter" type="submit" value="Subscribe">
                  <input class="btn btn-primary pull-xs-right hidden-sm-up" name="submitNewsletter" type="submit" value="OK">
                  <div class="input-wrapper">
                    <input name="email" type="text" value="" placeholder="Your email address">
                  </div>
                  <input type="hidden" name="action" value="0">
                  <div class="clearfix"></div>
                </div>
                <div class="newsletter-message">
                  <p>You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.</p>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="block-social">
          <h3 class="block-social-title title"><span>Follow Us</span></h3>
          <ul>
            <li class="facebook"><a href="#" target="_blank"><span>Facebook</span></a></li>
            <li class="twitter"><a href="#" target="_blank"><span>Twitter</span></a></li>
            <li class="youtube"><a href="#" target="_blank"><span>YouTube</span></a></li>
            <li class="googleplus"><a href="#" target="_blank"><span>Google +</span></a></li>
            <li class="instagram"><a href="#" target="_blank"><span>Instagram</span></a></li>
          </ul>
        </div>
        <div id="czfootercmsblock" class="footer-cms-block col-md-4 links block">
          <div id="footerlogo">
            <div class="footerdiv">
              <div class="footercms-inner"><a href="#" class="footercms"><img src="{openzc:field.template/}style/img/cms-footer1.png" alt="cms-footer1" class="footercms-image1" /></a> <a href="#" class="footercms"><img src="{openzc:field.template/}style/img/cms-footer2.png" alt="cms-footer2" class="footercms-image2" /></a> <a href="#" class="footercms"><img src="{openzc:field.template/}style/img/cms-footer3.png" alt="cms-footer3" class="footercms-image2" /></a></div>
            </div>
          </div>
        </div>
        <!-- Block payment logo module -->
        <div id="payement_logo_block_left" class="payement_logo_block">
          <h4 class="payement_logo title"><span> Payement Accept </span></h4>
          <a href=""> <img src="{openzc:field.template/}style/img/visa.png" alt="visa" width="34" height="23" /> <img src="{openzc:field.template/}style/img/discover.png" alt="discover" width="34" height="23" /> <img src="{openzc:field.template/}style/img/american_express.png" alt="american_express" width="34" height="23" /> <img src="{openzc:field.template/}style/img/master_card.png" alt="master_card" width="34" height="23" /> <img src="{openzc:field.template/}style/img/paypal.png" alt="paypal" width="34" height="23" /> </a> </div>
        <!-- /Block payment logo module --> 
        
      </div>
    </div>
  </div>
  </div>
  <div class="footer-after">
    <div class="container">
      <div class="copyright"> <a class="_blank" href="http://www.openzc.com" target="_blank"> 2020 - Ecommerce software by OpenzcTpl. </a> </div>
    </div>
  </div>
  <a class="top_button" href="#" style="">&nbsp;</a> </footer>
