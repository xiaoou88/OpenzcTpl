 <a class="shopping-cart" rel="nofollow" data-toggle="dropdown">
 <span class="hidden-sm-down cart-headding">My Cart</span> <span class="mobile_count">{openzc:php}echo count($_SESSION['cart']->contents);{/openzc:php}</span>
 {openzc:if count($_SESSION['cart']->contents) > 0}
<div class="cart_block block exclusive dropdown-menu">
                  <div class="block_content">
                    <div class="cart_block_list">
                     {openzc:cart imgsizer="80,76"}
                      <div class="cart-item">
                        <div class="cart-image"> 
                        <a href="[field:products_link/]"> 
                        <img src="[field:products_image/]" alt="[field:products_name/]"> 
                        </a>
                        </div>
                        <div class="cart-info">
						<span class="product-quantity">[field:products_qty/]</span>x 
						<span class="product-name">
						<a href="[field:products_link/]"> [field:products_name/]</a></span> 
						<span class="product-price">[field:products_price/]</span> <a class="remove-from-cart openzc-btn" data-id="[field:id/]" data-action="delCart"> &nbsp; 
						</a> 
						</div>
                      </div>
                      {/openzc:cart}
                    </div>
                    <div class="card cart-summary">
                      <div class="card-block">
                        <div class="cart-summary-line" id="cart-subtotal-products"> <span class="label js-subtotal"> {openzc:cart item="count"/} item </span> <span class="value">{openzc:cart item="total"/}</span> </div>
                        <div class="cart-summary-line" id="cart-subtotal-shipping"> <span class="label"> Shipping </span> <span class="value">Free</span>
                          <div><small class="value"></small></div>
                        </div>
                      </div>
                      <div class="card-block">
                        <div class="cart-summary-line cart-total"> <span class="label">Total</span> <span class="value">{openzc:cart item="total"/}</span> </div>
                        <div class="cart-summary-line"> <small class="label"></small> <small class="value"></small> </div>
                      </div>
                    </div>
                    <div class="checkout card-block"> <a rel="nofollow" href="/index.php?main_page=shopping_cart" class="viewcart">
                      <button type="button" class="btn btn-primary">View Cart</button>
                      </a> </div>
                  </div>
                </div>
{/openzc:if}
</a>