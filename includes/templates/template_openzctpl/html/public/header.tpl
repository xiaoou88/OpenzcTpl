
<header id="header">
  <div class="header-banner"> </div>
  <nav class="header-nav">
    <div class="container">
      <div class="left-nav">
        <div class="language-selector dropdown js-dropdown"> 
         {openzc:topbar item="language"}
          {openzc:if $field['status']=='active'} <span class="expand-more" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="language-dropdown"> [field:name/] <span class="dropdown-arrow"></span> </span> {/openzc:if}
          {/openzc:topbar}
          <ul class="dropdown-menu" aria-labelledby="language-dropdown">
            {openzc:topbar item="language"}
            {openzc:if $field['status']=='active'}
            <li class="current" > <a href="[field:link/]" class="dropdown-item">[field:name/]</a> </li>
            {else}
            <li> <a href="[field:link/]" class="dropdown-item">[field:name/]</a> </li>
            {/openzc:if}
            {/openzc:topbar}
          </ul>
        </div>
        <div class="currency-selector dropdown js-dropdown"> {openzc:topbar item="currency"}
          {openzc:if $field['status']=="active"} <span class="expand-more _gray-darker" data-toggle="dropdown" id="currency-dropdown"> [field:title/] <span class="expand-more dropdown-arrow"></span> </span> {/openzc:if}
          {/openzc:topbar}
          <ul class="dropdown-menu" aria-labelledby="currency-dropdown">
            {openzc:topbar item="currency"}
            {openzc:if $field['status']=="active"}
            <li class="current"><a title="[field:title/]" rel="nofollow" href="[field:link/]" class="dropdown-item">[field:title/]</a> </li>
            {else}
            <li><a title="[field:title/]" rel="nofollow" href="[field:link/]" class="dropdown-item">[field:title/]</a> </li>
            {/openzc:if}
            {/openzc:topbar}
          </ul>
        </div>
      </div>
      <div class="right-nav">
        <div class="text-xs-left mobile hidden-lg-up mobile-menu">
          <div class="menu-icon">
            <div class="cat-title">Menu</div>
          </div>
          <div id="mobile_top_menu_wrapper" class="row hidden-lg-up">
            <div class="mobile-menu-inner">
              <div class="menu-icon">
                <div class="cat-title">Menu</div>
              </div>
              <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
              <div class="js-top-menu mobile" id="_mobile_main_menu"></div>
            </div>
          </div>
        </div>
        <div class="menu main-menu col-lg-12 js-top-menu position-static hidden-md-down" id="_desktop_main_menu">
          <ul class="top-menu  container" id="top-menu" data-depth="0">
            <li class="link" id="lnk-home"> <a class="dropdown-item" href="/" data-depth="0"> Home </a> </li>
            {openzc:ezpages id="1,2,5,specials,featured,all_products,new_products"}
            <li class="cms-page" id="cms-page-[field:pages_id/]"> <a class="dropdown-item" href="[field:pages_link/]" data-depth="0"> [field:pages_title/] </a> </li>
            {/openzc:ezpages}
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <div class="header-top">
    <div class="container">
      <div class="header-inner-content">
        <div class="header_logo"> <a href="/"> <img class="logo img-responsive" src="{openzc:field.template/}style/img/martech-demo-store-logo-1549613849.png" alt="Maxcart - Mega Store"> </a> </div>
        <div class="header-right">
          <div id="desktop_cart">
            <div class="blockcart cart-preview inactive">
              <div id="viewcart" class="header blockcart-header dropdown js-dropdown"> 
                {openzc:include filename="public/viewcart.tpl" ajax="viewcart"/}
              </div>
            </div>
          </div>
          <div class="user-info dropdown js-dropdown"> <span class="user-info-title expand-more _gray-darker" data-toggle="dropdown"> 
           <span class="account_text">Sign In</span> <span class="account_desc">&amp; Register</span> </span>
            <ul class="dropdown-menu">
              {openzc:if isset($_SESSION['customer_id'])}
              <li> <a class="dropdown-item" href="/index.php?main_page=account" title="Log in to your customer account"rel="nofollow"> <span>My Account</span> </a> </li>
              {else}
              <li> <a class="dropdown-item" href="/index.php?main_page=login" title="Log in to your customer account"rel="nofollow"> <span>Sign in</span> </a> </li>
              {/openzc:if}
            </ul>
          </div>
          <!-- Block search module TOP -->
          <div id="search_widget" class="col-lg-4 col-md-5 col-sm-12 search-widget"> <span class="search_button"></span>
            <div class="search_toggle">
              <form method="get" action="">
               <input type="hidden" name="main_page" value="advanced_search_result">
                <input type="text" name="keyword" value="" placeholder="Search our catalog">
                <button type="submit">Search</button>
              </form>
            </div>
          </div>
          <!-- /Block search module TOP --> 
          
        </div>
      </div>
    </div>
    <div class="header-top-inner">
      <div class="container"> </div>
    </div>
  </div>
</header>

