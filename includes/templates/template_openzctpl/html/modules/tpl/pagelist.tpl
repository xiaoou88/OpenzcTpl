
{type:prev}
<li> <a rel="prev" href="[field:page_link/]" class="previous"> <i class="fa fa-long-arrow-left"></i> </a> </li>
{/type:prev}

{type:next}
<li> <a rel="next" href="[field:page_link/]" class="next"> <i class="fa fa-long-arrow-right"></i> </a> </li>
{/type:next}

{type:first}
  <li><a href="[field:page_link/]">[field:page_name/]</a></li>
{/type:first}

{type:last}
  <li><a href="[field:page_link/]">[field:page_name/]</a></li>
{/type:last}

{type:list}
<li> <a rel="nofollow" href="[field:page_link/]"> [field:page_name/] </a> </li>
{/type:list} 

{type:active}
<li class="current"> 
	<a rel="nofollow" href="[field:page_link/]" class="disabled js-search-link"> [field:page_name/] </a> 
</li>                   
{/type:active} 

{type:info}
Showing [field:page_range/] of [field:page_total/] item(s)
{/type:info}

{type:text}
There are [field:page_total/] products.
{/type:text}

{type:info1}
Showing [field:page_range/] of [field:page_total/] item(s)
{/type:info1}