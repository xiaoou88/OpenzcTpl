{openzc:if TEMPLATE_DEVICE==0}

<div id="openzcBox">
<div id="msgCompare">
<div class="col-xs-11 col-sm-3 alert alert-success alert-dismissible fade show animated" data-notify-position="top-right" role="alert" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out 0s; z-index: 2031; top: 30px; right: 20px;">
	{openzc:prolist type="compare" pid="current"}
  Success: You have added <a class="text-reset font-weight-bold" href="[field:products_link/]">[field:products_name/]</a> to your <a class="text-reset font-weight-bold" href="{openzc:link name='FILENAME_COMPARE'/}">product comparison</a>!
  {/openzc:prolist}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
</div>
</div>
<script>
setTimeout(function(){$("#msgCompare").hide(600)}, 3000);
</script>
{/openzc:if}
