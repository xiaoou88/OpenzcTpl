
<div id="openzcBox">
<div id="msgReviews">
{openzc:php}
foreach($messageStack->messages as $k => $v){
	if($v['class']=="review_success"){$reviews_status="alert-success";}else{$reviews_status="alert-warning";}
	$reviews_text=$v['text'];
}
{/openzc:php}
<div class="col-xs-11 col-sm-3 alert {openzc:echo}$reviews_status;{/openzc:echo} alert-dismissible fade show animated" data-notify-position="top-right" role="alert" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out 0s; z-index: 2031; top: 30px; right: 20px;">
 {openzc:echo}$reviews_text;{/openzc:echo}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

</div>
</div>
<script>
setTimeout(function(){$("#msgReviews").hide(600)}, 3000);
</script>