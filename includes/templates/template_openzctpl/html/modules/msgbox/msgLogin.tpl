
<div id="openzcBox">
<button id="msglogin" class="hide" data-toggle="modal" data-backdrop="false" data-target="#msgLogin"></button>
<div class="modal fade show" id="msgLogin" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true"  data-pause=1500 style="transition: all 0.5s ease-in-out 0s;">
  <div class="modal-dialog" >
    <div class="modal-content ">
      <div class="modal-header border-bottom-0" style="height:0px">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body p-5 pt-0">
        <div class="tt-login-wishlist">
          <p class="mb-4 font-weight-bold text-secondary ">Please login and you will add product to your wishlist</p>
          <div class="row-btn">
            <a href="{openzc:link name='FILENAME_LOGIN'/}" class="btn btn-small btn-primary waves-effect waves-light mr-4">SIGN IN</a>
            <a href="{openzc:link name='FILENAME_CREATE_ACCOUNT'/}" class="btn btn-small btn-outline-primary waves-effect waves-light border-2">REGISTER</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>$("#msglogin").trigger("click");</script>