(function($) {
    $.Openzc = function() {
		this.Run =function () {
			Openzc.msgBox();
			$('.openzc-input').on('change', function(){
				Openzc.ajaxLoding("on");
				var othis = $(this);
				Openzc.active(othis);
			});
			$('.openzc-btn').unbind('click').bind('click',function(){
				Openzc.ajaxLoding("on");
				
				var othis = $(this);
				Openzc.active(othis);
			});
			$('.openzc-form').unbind('submit').bind('submit',function(e){
				Openzc.ajaxLoding("on");
				//$(this).unbind();
				var FormData=$(this).serializeArray();
				Openzc.openzcForm(FormData);
				return false;
			});
			$('.openzc-select').on('change', function(){
				Openzc.ajaxLoding("on");
				var othis = $(this);
				var data = othis.data();
				if(data['action']=="autoSubmit"){
					$("#"+data['form']).submit();
				}
				if(data['action']=="autoLink"){
					self.location.href=othis.val();
					return false;
					
				}
				Openzc.active(othis);
			});
	    },
		this.active =function (othis) {
			
			var data=othis.data();
			if(data['action']=="filterPrice"){
				data['min']="";
				data['max']="";
				data['securityToken']=securityToken;
				$(".openzc-input").each(function(){
					var name=$(this).attr('name');
					switch(name){
						case "filter-price-min":
							data['min']=$(this).val();
						break;
						case "filter-price-max":
							data['max']=$(this).val();
						break;
						case "filter-price-link":
							data['url']=$(this).val();
						break;
					}
				});
				data['url']=Openzc.ajaxpost("index.php","post",data);
				
			}else{
				data['value']=othis.val();
			}
			var position=Openzc.getCookie("position");
			var url="index.php";
			data['securityToken']=securityToken;
			data['position']=position;
			data["config"]=Openzc.config;
			
			if(data['url']){
				url=data['url'];
				if(data['action']!="filterPrice"){history.pushState(null,null,url);}
			}
			Openzc.ajaxpost(url,"post",data);
		},
		this.token=function(){
			var data={};
			data['action']="securityToken";
			var token=Openzc.ajaxpost("index.php?sessions=true","get",data);
			return token;
			return false;
		},
		this.openzcForm=function(Formdata){
			var data={};
			$.each(Formdata,function(){data[this.name]=this.value;});
			data["securityToken"]=securityToken;
			var position=Openzc.getCookie("position");
			data["position"]=position;
			data["config"]=Openzc.config;
			
			jQuery.ajax({
				type:'post',
				url:"index.php",
				data:data,
				async:false,
				success:function (result){
					result=JSON.stringify(result);
					Openzc.ajaxRun(result);
				}
			});
			return false;
		},
		this.ajaxpost=function(url,method,data){
			jQuery.ajax({
				type:method,
				url:url,
				data:data,
				async:false,
				success:function (result){
					rs=result;
					if(data['action']!='securityToken' && rs['action']!="getUrl"){
						rs=JSON.stringify(result);
						Openzc.ajaxRun(rs);
					}
					
				}
			}); 
			switch(data['action']){
				case "securityToken":
					return rs['data'];
				break;
				case "filterPrice":
					if(rs['action']=="getUrl"){
						return rs['url'];
					}
				break;
			}
			return false;
		},
		this.ajaxRun=function(data){
			data=JSON.parse(data);
		
			$.each(data,function(action,list){
				switch(action){
					case "reload":
						if(list=="null"){
							window.location.reload();
							return false;
						}
						$.each(list,function(key,value){
							$(value["div"]).html(value["html"]);
							if(value['custom_status']==true && data['autoReload']==false){
								if($.isFunction(Openzc.customReload)){
									Openzc.customReload(key,value);
								}
							}
						});
					break;
				}
			});
			$.each(data["jquery"],function(key,value){
				$.getScript(value);
			});
			Openzc.ajaxLoding("off");
			if(data["action"]=="addCart" || data["action"]=="writeReviews"){
				Openzc.Run();
			}
			return false;
			
		},
		this.getCookie=function(cookieName){
        	var cookieValue="";  
			if (document.cookie && document.cookie != '') {   
				var cookies = document.cookie.split(';');  
				for (var i = 0; i < cookies.length; i++) {   
					 var cookie = cookies[i];  
					 if (cookie.substring(0, cookieName.length + 2).trim() == cookieName.trim() + "=") {  
						   cookieValue = cookie.substring(cookieName.length + 2, cookie.length);   
						   break;  
					 }  
				 }  
			}   
			return cookieValue;
		},
		//**Ajax自定义自动刷新内容
		this.autoReload=function(){
			$(document).ready(function() {
				var html=Openzc.getCookie("html");
				if(Openzc.config){
				if(Openzc.config.customAutoload != "undefined" && html==true){
					Openzc.ajaxLoding("on");
					var position=Openzc.getCookie("position");
					var data={"reload":Openzc.config.customAutoload,"action":"getHtml","securityToken":securityToken,"config":Openzc.config,"position":position,'autoReload':true};
					Openzc.ajaxpost("index.php","post",data);
				}
				}
				return false;
			});
		},
		//**添加信息弹窗box
		this.msgBox=function(){
			if($("#msgOpenzc").length == 0){
				$('body').prepend('<div id="msgOpenzc"></div>'+"\r\n");
			}
		},
		//**以下函数Loding函数
		this.ajaxLoding=function(type){
			switch(type){
				case "on":
					loading.showLoading({type:1});
				break;
				case "off":
					loading.hideLoading();
				break;
			}
		}
	};
	//**Openzc-Loading
	const loading={_loadItem:null,_anItem:null,_tipItem:null,_tipLabel:null,_showTip:true,_type:1,showLoading(config){if(this._loadItem){this._loadItem.remove();this._loadItem=null;this._anItem=null;this._tipItem=null;this._showTip=true;this._type=1}if(typeof config=="string"){this._tipLabel=config;this._type=1}else if(typeof config=="object"){this._tipLabel=typeof config.tip=="string"?config.tip:"loading...";this._type=typeof config.type=="number"?config.type:1;this._showTip=typeof config.showTip=="boolean"?config.showTip:true}else{this._tipLabel="loading...";this._type=1}this.createEle()},createEle(){this._loadItem=$(`<div class="load-view"></div>`);this._anItem=$(`<div class="load-an-view"></div>`);this._tipItem=$(`<div class="load-tip">${this._tipLabel}</div>`);let load1=$(`<div class="load-circle"><div class="load-container load-container1"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="load-container load-container2"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="load-container load-container3"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div></div><style>.load-view{width:100px;height:100px;background:rgba(0,0,0,.8);border-radius:8px;position:fixed;left:0;top:0;right:0;bottom:0;margin:auto;box-sizing:border-box;display:flex;flex-direction:column;justify-content:space-around;padding:6px 0 0;-webkit-transition:height 1s linear 0s;-moz-transition:height 1s linear 0s;-o-transition:height 1s linear 0s;transition:height 1s linear 0s}.load-an-view{width:100%;height:auto;box-pack:center;box-align:center;display:-moz-box;-moz-box-pack:center;-moz-box-align:center;display:-webkit-box;-webkit-box-pack:center;-webkit-box-align:center;-o-box-pack:center;-o-box-align:center}.load-tip{width:100%;text-align:center;font-size:16px;line-height:16px;color:#fff;font-family:Trebuchet MS,Verdana,Helvetica,Arial,sans-serif}.load-circle{min-width:40px;min-height:40px;position:relative}.load-container1>div,.load-container2>div,.load-container3>div{width:20%;height:20%;background-color:#fff;border-radius:100%;position:absolute;-webkit-animation:bouncedelay 1.2s infinite ease-in-out;-moz-animation:bouncedelay 1.2s infinite ease-in-out;-o-animation:bouncedelay 1.2s infinite ease-in-out;animation:bouncedelay 1.2s infinite ease-in-out;-webkit-animation-fill-mode:both;-moz-animation-fill-mode:both;-o-animation-fill-mode:both;animation-fill-mode:both}.load-circle .load-container{position:absolute;width:100%;height:100%}.load-container2{-webkit-transform:rotateZ(45deg);-moz-transform:rotateZ(45deg);-o-transform:rotateZ(45deg);transform:rotateZ(45deg)}.load-container3{-webkit-transform:rotateZ(90deg);-moz-transform:rotateZ(90deg);-o-transform:rotateZ(90deg);transform:rotateZ(90deg)}.circle1{top:0;left:0}.circle2{top:0;right:0}.circle3{right:0;bottom:0}.circle4{left:0;bottom:0}.load-container2 .circle1{-webkit-animation-delay:-1.1s;-moz-animation-delay:-1.1s;-o-animation-delay:-1.1s;animation-delay:-1.1s}.load-container3 .circle1{-webkit-animation-delay:-1s;-moz-animation-delay:-1s;-o-animation-delay:-1s;animation-delay:-1s}.load-container1 .circle2{-webkit-animation-delay:-.9s;-moz-animation-delay:-.9s;-o-animation-delay:-.9s;animation-delay:-.9s}.load-container2 .circle2{-webkit-animation-delay:-.8s;-moz-animation-delay:-.8s;-o-animation-delay:-.8s;animation-delay:-.8s}.load-container3 .circle2{-webkit-animation-delay:-.7s;-moz-animation-delay:-.7s;-o-animation-delay:-.7s;animation-delay:-.7s}.load-container1 .circle3{-webkit-animation-delay:-.6s;-moz-animation-delay:-.6s;-o-animation-delay:-.6s;animation-delay:-.6s}.load-container2 .circle3{-webkit-animation-delay:-.5s;-moz-animation-delay:-.5s;-o-animation-delay:-.5s;animation-delay:-.5s}.load-container3 .circle3{-webkit-animation-delay:-.4s;-moz-animation-delay:-.4s;-o-animation-delay:-.4s;animation-delay:-.4s}.load-container1 .circle4{-webkit-animation-delay:-.3s;-moz-animation-delay:-.3s;-o-animation-delay:-.3s;animation-delay:-.3s}.load-container2 .circle4{-webkit-animation-delay:-.2s;-moz-animation-delay:-.2s;-o-animation-delay:-.2s;animation-delay:-.2s}.load-container3 .circle4{-webkit-animation-delay:-.1s;-moz-animation-delay:-.1s;-o-animation-delay:-.1s;animation-delay:-.1s}@-webkit-keyframes bouncedelay{0%,100%,80%{-webkit-transform:scale(0)}40%{-webkit-transform:scale(1)}}@-moz-keyframes bouncedelay{0%,100%,80%{-moz-transform:scale(0)}40%{-moz-transform:scale(1)}}@-o-keyframes bouncedelay{0%,100%,80%{-o-transform:scale(0)}40%{-o-transform:scale(1)}}@keyframes bouncedelay{0%,100%,80%{transform:scale(0)}40%{transform:scale(1)}}@-webkit-keyframes sk-circleFadeDelay{0%,100%,39%{opacity:0}40%{opacity:1}}@-moz-keyframes sk-circleFadeDelay{0%,100%,39%{opacity:0}40%{opacity:1}}@-o-keyframes sk-circleFadeDelay{0%,100%,39%{opacity:0}40%{opacity:1}}@keyframes sk-circleFadeDelay{0%,100%,39%{opacity:0}40%{opacity:1}}@-webkit-keyframes bouncedelay{0%,100%,80%{-webkit-transform:scale(0)}40%{-webkit-transform:scale(1)}}@-moz-keyframes bouncedelay{0%,100%,80%{-moz-transform:scale(0)}40%{-moz-transform:scale(1)}}@-o-keyframes bouncedelay{0%,100%,80%{-o-transform:scale(0)}40%{-o-transform:scale(1)}}@keyframes bouncedelay{0%,100%,80%{transform:scale(0)}40%{transform:scale(1)}}@-webkit-keyframes rotate{100%{-webkit-transform:rotate(360deg)}}@-moz-keyframes rotate{100%{-moz-transform:rotate(360deg)}}@-o-keyframes rotate{100%{-o-transform:rotate(360deg)}}@keyframes rotate{100%{transform:rotate(360deg)}}@-webkit-keyframes bounce{0%,100%{-webkit-transform:scale(0)}50%{-webkit-transform:scale(1)}}@-moz-keyframes bounce{0%,100%{-moz-transform:scale(0)}50%{-moz-transform:scale(1)}}@-o-keyframes bounce{0%,100%{-o-transform:scale(0)}50%{-o-transform:scale(1)}}@keyframes bounce{0%,100%{transform:scale(0)}50%{transform:scale(1)}}@-webkit-keyframes stretchdelay{0%,100%,40%{-webkit-transform:scaleY(.4)}20%{-webkit-transform:scaleY(1)}}@-moz-keyframes stretchdelay{0%,100%,40%{-moz-transform:scaleY(.4)}20%{-moz-transform:scaleY(1)}}@-o-keyframes stretchdelay{0%,100%,40%{-o-transform:scaleY(.4)}20%{-o-transform:scaleY(1)}}@keyframes stretchdelay{0%,100%,40%{transform:scaleY(.4)}20%{transform:scaleY(1)}}</style>`);load1.appendTo(this._anItem);this.addEle()},addEle(){this._anItem.appendTo(this._loadItem);if(this._showTip){this._tipItem.appendTo(this._loadItem)}this._loadItem.appendTo($('body'))},hideLoading(){this._loadItem.remove();this.resetData()},resetData(){this._loadItem=null;this._anItem=null;this._tipItem=null;this._showTip=true;this._type=1}}
})(jQuery);
var Openzc =new $.Openzc();
var securityToken=Openzc.token();
Openzc.Run();
Openzc.autoReload();