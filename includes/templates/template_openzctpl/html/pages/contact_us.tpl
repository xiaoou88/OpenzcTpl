<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Contact us</title>
<meta name="description" content="Use our form to contact us">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="contact" class="lang-en country-de currency-eur layout-left-column page-contact tax-display-enabled">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <aside id="notifications">
    <div class="container"> </div>
  </aside>
  <section id="wrapper">
    <nav data-depth="1" class="breadcrumb">
      <div class="container">
        <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php"> <span itemprop="name">Home</span> </a>
            <meta itemprop="position" content="1">
          </li>
        </ol>
      </div>
    </nav>
    <div class="container">
      <div id="columns_inner">
        <div id="left-column" class="col-xs-12 col-sm-3" style="width:21.4%">
          <div id="contact-rich" class="contact-rich block">
            <h4 class="block_title hidden-md-down">Store information</h4>
            <h4 class="block_title hidden-lg-up" data-target="#contact_rich_toggle" data-toggle="collapse"> Store information <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="fa-icon add"></i> <i class="fa-icon remove"></i> </span> </span> </h4>
            <div id="contact_rich_toggle" class="block_content collapse">
              <div class="">
                <div class="icon"><i class="fa fa-map-marker"></i></div>
                <div class="data">Maxcart - Mega Store<br>
                  United States</div>
              </div>
              <hr>
              <div class="">
                <div class="icon"><i class="fa fa-phone"></i></div>
                <div class="data"> Call us:<br>
                  <a href="tel:{openzc:define.STORE_TELEPHONE_CUSTSERVICE/}">{openzc:define.STORE_TELEPHONE_CUSTSERVICE/}</a> </div>
              </div>
              <hr>
              <div class="">
                <div class="icon"><i class="fa fa-fax"></i></div>
                <div class="data"> Fax:<br>
                  123456 </div>
              </div>
              <hr>
              <div class="">
                <div class="icon"><i class="fa fa-envelope-o"></i></div>
                <div class="data email"> Email us:<br>
                  <a href="mailto:sales@yourcompany.com">sales@yourcompany.com</a> </div>
              </div>
            </div>
          </div>
        </div>
        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9" style="width:78.6%">
          <section id="main">
            <section id="content" class="page-content card card-block">
             {openzc:msg name="contact_us" type="success"}
             <div class="alert alert-success">[field:text/]</div>
             {/openzc:msg}
              <section class="contact-form">
                <form action="/index.php?main_page=contact_us&action=send" method="post" enctype="multipart/form-data">
                 <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}">
                  <section class="form-fields">
                    <div class="form-group row">
                      <div class="col-md-9 col-md-offset-3">
                        <h3>Contact us</h3>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label">Full name</label>
                      <div class="col-md-6">
                         <input class="form-control" name="contactname" type="text" value="" placeholder="">
                      </div>
                    </div>
                    
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label">Email address</label>
                      <div class="col-md-6">
                       {openzc:if $_SESSION['customer_id']}
                        <input class="form-control" name="email" type="email" value="{openzc:account field='customers_email_address'/}" placeholder="{openzc:account field='customers_email_address'/}">
                        {else}
                         <input class="form-control" name="email" type="email" value="" placeholder="">
                       {/openzc:if}
                      </div>
                    </div>
                    
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label">Message</label>
                      <div class="col-md-9">
                        <textarea class="form-control" name="enquiry" placeholder="How can we help?" rows="3"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-md-3"> </div>
                    </div>
                  </section>
                  <footer class="form-footer text-sm-right">
                    <input class="btn btn-primary" type="submit" name="submitMessage" value="Send">
                  </footer>
                </form>
              </section>
            </section>
           
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>

</body>
</html>