<section class="cz-hometabcontent">
  <h2 class="h1 products-section-title text-uppercase">New Products</h2>
  <div class="tabs">
    <ul id="home-page-tabs" class="nav nav-tabs clearfix">
      <li class="nav-item"> <a data-toggle="tab" href="#newProduct" class="nav-link active" data-text="Latest"> <span>New Arrival</span> </a> </li>
      <li class="nav-item"> <a data-toggle="tab" href="#bestseller" class="nav-link" data-text="Best Sellers"> <span>Best Sellers</span> </a> </li>
    </ul>
    <div class="tab-content">
      <div id="newProduct" class="cz_productinner tab-pane active">
        <section class="newproducts clearfix">
          <h2 class="h1 products-section-title text-uppercase"> New products </h2>
          <div class="products"> 
            <!-- Define Number of product for SLIDER -->
            <ul id="newproduct-carousel" class="cz-carousel product_list">
              {openzc:prolist flag="new" row="10" imgsizer="252,239"}
              <li class="item">
                <div class="product-miniature js-product-miniature">
                  <div class="thumbnail-container"> <a href="[field:products_link/]" class="thumbnail product-thumbnail"> <img src = "[field:products_image/]" alt = "Accusantium Voluptatem" data-full-size-image-url = "[field:products_image/]"> </a>
                    <div class="outer-functional">
                      <div  class="functional-buttons"> <a href="#" class="quick-view" data-link-action="quickview"> <i class="material-icons search">&#xE417;</i> Quick view </a>
                        <div class="product-actions">
                          <form class="openzc-form add-to-cart-or-refresh">
                            <input type="hidden" name="products_id" value="[field:products_id/]" class="product_page_product_id">
                            <button class="btn btn-primary add-to-cart" type="submit" > Add to cart </button>
                          </form>
                        </div>
                      </div>
                    </div>
                    <ul class="product-flags">
                      <li class="on-sale">On sale!</li>
                      <li class="new">New</li>
                    </ul>
                  </div>
                  <div class="product-description">
                    <div class="comments_note">
                      <div class="star_content clearfix">
                        {openzc:php}
						   for($i=0;$i<5;$i++){
								if($i<$products_info['products_rating']){
									echo '<div class="star star_on"></div>'."\r\n";
								}else{
									echo '<div class="star"></div>'."\r\n";
								}   	
						   }
						{/openzc:php}
                      </div>
                    </div>
                    <span class="h3 product-title" itemprop="name"><a href="[field:products_link/]">[field:products_name/]</a></span>
                    <div class="product-price-and-shipping"> <span class="regular-price">[field:products_price/]</span> </div>
                  </div>
                </div>
              </li>
              {/openzc:prolist}
            </ul>
            <div class="customNavigation"> <a class="btn prev newproduct_prev">&nbsp;</a> <a class="btn next newproduct_next">&nbsp;</a> </div>
          </div>
        </section>
      </div>
      <div id="bestseller" class="cz_productinner tab-pane">
        <section class="bestseller-products">
          <h2 class="h1 products-section-title text-uppercase"> Best Sellers </h2>
          <div class="products"> 
            <!-- Define Number of product for SLIDER -->
            <ul id="bestseller-carousel" class="cz-carousel product_list">
              {openzc:prolist flag="bestsell" row="10" imgsizer="252,239"}
              <li class="item">
                <div class="product-miniature js-product-miniature" data-id-product="[field:products_id/]" itemscope itemtype="http://schema.org/Product">
                  <div class="thumbnail-container"> <a href="[field:products_link/]" class="thumbnail product-thumbnail"> <img src = "[field:products_image/]" alt = "Consectetur Hampden" data-full-size-image-url = "[field:products_image/]"/> <img class="fliper_image img-responsive" src="{openzc:field.template/}style/img/115-home_default.jpg" data-full-size-image-url="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/img/p/1/1/5/115-large_default.jpg" alt="" /> </a>
                    <div class="outer-functional">
                      <div  class="functional-buttons"> <a href="#" class="quick-view" data-link-action="quickview"> <i class="material-icons search">&#xE417;</i> Quick view </a>
                        <div class="product-actions">
                          <form action="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=cart" method="post" class="add-to-cart-or-refresh">
                            <input type="hidden" name="token" value="e4c275f0b19b4e8c4b073bc6daba85e9">
                            <input type="hidden" name="id_product" value="1" class="product_page_product_id">
                            <input type="hidden" name="id_customization" value="0" class="product_customization_id">
                            <button class="btn btn-primary add-to-cart" data-button-action="add-to-cart" type="submit" > Add to cart </button>
                          </form>
                        </div>
                      </div>
                    </div>
                    <ul class="product-flags">
                      <li class="on-sale">On sale!</li>
                      <li class="new">New</li>
                    </ul>
                  </div>
                  <div class="product-description">
                    <div class="comments_note">
                      <div class="star_content clearfix">
                        <div class="star star_on"></div>
                        <div class="star star_on"></div>
                        <div class="star star_on"></div>
                        <div class="star star_on"></div>
                        <div class="star star_on"></div>
                      </div>
                      <span class="total-rating">1 Review(s)&nbsp</span> </div>
                    <span class="h3 product-title" itemprop="name"> <a href="[field:products_link/]">[field:products_name/]</a> </span>
                    <div class="product-price-and-shipping"> <span itemprop="price" class="price">[field:products_price/]</span> </div>
                  </div>
                </div>
              </li>
              {/openzc:prolist}
            </ul>
            <div class="customNavigation"> <a class="btn prev bestseller_prev">&nbsp;</a> <a class="btn next bestseller_next">&nbsp;</a> </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</section>
