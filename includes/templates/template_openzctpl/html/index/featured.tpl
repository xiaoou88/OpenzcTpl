<section class="featured-products clearfix">
  <h2 class="h1 products-section-title text-uppercase"> Featured products </h2>
  <div class="featured-products-wrapper">
    <div class="products"> 
      <!-- Define Number of product for SLIDER -->
      <ul id="feature-carousel" class="cz-carousel product_list">
        {openzc:prolist flag="featured" row="10" imgsizer="252,239"}
        <li class="item">
          <div class="product-miniature js-product-miniature">
            <div class="thumbnail-container"> <a href="[field:products_link/]"> <img src = "[field:products_image/]" alt = "[field:products_name/]" data-full-size-image-url = "[field:products_image/]"> </a>
              <div class="outer-functional">
                <div  class="functional-buttons"> <a href="#" class="quick-view" data-link-action="quickview"> <i class="material-icons search">&#xE417;</i> Quick view </a>
                  <div class="product-actions">
                    <form class="openzc-form add-to-cart-or-refresh">
                      <input type="hidden" name="products_id" value="[field:products_id/]" class="product_page_product_id">
                      <button class="btn btn-primary add-to-cart" type="submit" > Add to cart </button>
                    </form>
                  </div>
                </div>
              </div>
              <ul class="product-flags">
                <li class="on-sale">On sale!</li>
                <li class="new">New</li>
              </ul>
            </div>
            <div class="product-description">
              <div class="comments_note">
                <div class="star_content clearfix"> {openzc:php}
                  for($i=0;$i<5;$i++){
                  if($i<$field['reviews_rating']){
                                  			echo '<div class="star star_on"></div>'."\r\n";
                                  		}else{
                                  			echo '<div class="star"></div>'."\r\n";
                                  		}
                                  		
                                  	}
                                  {/openzc:php}
                                </div>
                <span class="total-rating">1 Review(s)&nbsp</span> </div>
              <span class="h3 product-title" itemprop="name"> <a href="[field:products_link/]">[field:products_name/]</a></span>
              <div class="product-price-and-shipping"> <span itemprop="price" class="price">[field:products_price/]</span> </div>
            </div>
          </div>
        </li>
        {/openzc:prolist}
      </ul>
      <div class="customNavigation"> <a class="btn prev feature_prev">&nbsp;</a> <a class="btn next feature_next">&nbsp;</a> </div>
    </div>
  </div>
</section>
</div>
</section>