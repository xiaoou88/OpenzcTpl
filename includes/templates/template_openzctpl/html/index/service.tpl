<div id="czservicecmsblock">
                  <div class="service_container">
                    <div class="service-area box-content">
                      <div class="service-fourth service1">
                        <div class="service-icon icon1"></div>
                        <div class="service-content">
                          <div class="service-heading">Fast Delivery</div>
                          <div class="service-description">Cras Imperdiet Lorem Ipsum</div>
                        </div>
                      </div>
                      <div class="service-fourth service2">
                        <div class="service-icon icon2"></div>
                        <div class="service-content">
                          <div class="service-heading">Easy Return Policy</div>
                          <div class="service-description">Cras Imperdiet Lorem Ipsum</div>
                        </div>
                      </div>
                      <div class="service-fourth service3">
                        <div class="service-icon icon3"></div>
                        <div class="service-content">
                          <div class="service-heading">Online Support 24/7</div>
                          <div class="service-description">Cras Imperdiet Lorem Ipsum</div>
                        </div>
                      </div>
                      <div class="service-fourth service4">
                        <div class="service-icon icon4"></div>
                        <div class="service-content">
                          <div class="service-heading">Lowest Price Guarantee</div>
                          <div class="service-description">Cras Imperdiet Lorem Ipsum</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="czrightbannercmsblock" class="czrightbanner-block">
                  <div class="rightcmsbanners">
                    <div class="one-half rightcmsbanner-part1">
                      <div class="rightcmsbanner-inner">
                        <div class="rightcmsbanner rightcmsbanner1"><a href="#" class="rightbanner-anchor"><img src="{openzc:field.template/}style/img/rightcms-banner-1.jpg" alt="rightcms-banner1" class="rightcmsbanner-image1" /></a>
                          <div class="rightcmsbanner-text">
                            <div class="main-title">Get Discount<br />
                              <span>30% off</span></div>
                            <div class="shop-button"><a href="#" class="view-more">View More</a></div>
                          </div>
                        </div>
                        <div class="rightcmsbanner rightcmsbanner2"><a href="#" class="rightbanner-anchor"><img src="{openzc:field.template/}style/img/rightcms-banner-2.jpg" alt="rightcms-banner2" class="rightcmsbanner-image2" /></a>
                          <div class="rightcmsbanner-text">
                            <div class="main-title">Clothes<br />
                              <span>Sale 20% off</span></div>
                            <div class="shop-button"><a href="#" class="view-more">View More</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="one-half rightcmsbanner-part3">
                      <div class="rightcmsbanner-inner">
                        <div class="rightcmsbanner cmsbanner3"><a href="#" class="rightbanner-anchor"><img src="{openzc:field.template/}style/img/rightcms-banner-3.jpg" alt="rightcms-banner3" class="rightcmsbanner-image3" /></a>
                          <div class="rightcmsbanner-text">
                            <div class="main-title">Discount 20% Off</div>
                            <div class="sub-banner">Electronics Sale</div>
                            <div class="shop-button"><a href="#" class="view-more">Shop Now</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>