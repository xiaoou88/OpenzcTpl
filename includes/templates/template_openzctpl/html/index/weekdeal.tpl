<section class="special-products block">
  <h2 class="h1 products-section-title text-uppercase"> Big Deal </h2>
  <div class="special-products-wrapper">
    <div class="products">
      <ul id="special-carousel" class="cz-carousel product_list">
        {openzc:prolist flag="specials" desclen="200" row="6" imgsizer="344,352" showkeys="true"}
        <li class="item">
          <div class="product-miniature js-product-miniature" >
            <div class="thumbnail-container col-xs-12 col-md-6"> <a href="[field:products_link/]" class="thumbnail product-thumbnail"> <img src = "[field:products_image/]" alt = "[field:products_name/]"> </a>
              <ul class="product-flags">
                <li class="on-sale">On sale!</li>
                <li class="new">New</li>
              </ul>
            </div>
            <div class="product-description col-xs-12 col-md-6"> <span class="h3 product-title" itemprop="name"><a href="[field:products_link/]">[field:products_name/]</a></span>
              <div id="product-description-short-26" itemprop="description" class="deals-product-description">
				  <p>[field:products_description/]...</p>
              </div>
              <div class="comments_note">
                <div class="star_content clearfix">
                  {openzc:php}
                   for($i=0;$i<5;$i++){
                   		if($i<$products_info['products_rating']){
                        	echo '<div class="star star_on"></div>'."\r\n";
                        }else{
                            echo '<div class="star"></div>'."\r\n";
                        }   	
                   }
                   {/openzc:php}
                </div>
                <span class="total-rating">[field:products_rating/] Review(s)&nbsp</span> </div>
              <div class="product-price-and-shipping"> 
              	{openzc:if $field['productPriceDiscount']}
				  <span class="regular-price">[field:products_original_price/]</span> 
				  <span class="discount-amount discount-product">[field:productPriceDiscount/]</span> 
				  <span class="price">[field:products_price/]</span> 
            	{else}
            	  <span class="price">[field:products_price/]</span> 
             	{/openzc:if}
              </div>
              <div class="product-counter">
                <div class="psproductcountdown buttons_bottom_block pspc-inactive" data-to="[field:expires_date_secound/]">
					<div class="pspc-main days-diff-175 weeks-diff-25 "> </div>
                </div>
              </div>
              
              <div class="product-actions">
               
                  <a class="btn btn-primary add-to-cart" href="[field:products_link/]"> <span>Add to cart</span> </a>
               
              </div>
            </div>
          </div>
        </li>
        {/openzc:prolist}
      </ul>
      <div class="customNavigation"> <a class="btn prev special_prev">Previous Deal&nbsp;</a> <a class="btn next special_next">Next Deal&nbsp;</a> </div>
    </div>
  </div>
</section>
