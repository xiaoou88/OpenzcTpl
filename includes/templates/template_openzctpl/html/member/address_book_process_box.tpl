﻿<div class="address-form">
                <div class="js-address-form">
                  <form method="POST" action="">
                    <section class="form-fields">
                      <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
                      {openzc:if isset($_GET['edit'])}
                      <input type="hidden" name="action" value="update"/>
                      <input type="hidden" name="edit" value="{openzc:echo}$_GET['edit']{/openzc:echo}"/>
                      {else}
                      <input type="hidden" name="action" value="process"/>
                      {/openzc:if}
                      <div class="form-group row ">
						<label class="col-md-3 form-control-label"> Social title </label>
						<div class="col-md-6 form-control-valign">
					 
						  <label class="radio-inline"> <span class="custom-radio">
							<input name="gender" type="radio" value="m" {openzc:if $address_book['entry_gender']=='m'}checked=""{/openzc:if}>
							<span></span> </span> Mr. </label>
						  <label class="radio-inline"> <span class="custom-radio">
							<input name="gender" type="radio" value="f" {openzc:if $address_book['entry_gender']=='f'}checked=""{/openzc:if}>
							<span></span> </span> Mrs. </label>
						</div>
						<div class="col-md-3 form-control-comment"> </div>
					  </div>
                      <div class="form-group row ">
                        <label class="col-md-3 form-control-label required"> First name </label>
                        <div class="col-md-6">
                          <input class="form-control" name="firstname" type="text" value="{openzc:account item='address' field='firstname'/}" maxlength="32" required="">
                        </div>
                        <div class="col-md-3 form-control-comment"> </div>
                      </div>
                      <div class="form-group row ">
                        <label class="col-md-3 form-control-label required"> Last name </label>
                        <div class="col-md-6">
                          <input class="form-control" name="lastname" type="text" value="{openzc:account item='address' field='lastname'/}" maxlength="32" required="">
                        </div>
                        <div class="col-md-3 form-control-comment"> </div>
                      </div>
                      
                      <div class="form-group row ">
                        <label class="col-md-3 form-control-label required"> Address </label>
                        <div class="col-md-6">
                          <input class="form-control" name="street_address" type="text" value="{openzc:account item='address' field='street_address'/}" maxlength="128" required="">
                        </div>
                        <div class="col-md-3 form-control-comment"> </div>
                      </div>
                      <div class="form-group row ">
                        <label class="col-md-3 form-control-label"> Address Complement </label>
                        <div class="col-md-6">
                          <input class="form-control" name="suburb" type="text" value="{openzc:account item='address' field='suburb'/}" maxlength="128">
                        </div>
                        <div class="col-md-3 form-control-comment"> Optional </div>
                      </div>
                      <div class="form-group row ">
                        <label class="col-md-3 form-control-label required"> Zip/Postal Code </label>
                        <div class="col-md-6">
                          <input class="form-control" name="postcode" type="text" value="{openzc:account item='address' field='postcode'/}" maxlength="12" required="">
                        </div>
                        <div class="col-md-3 form-control-comment"> </div>
                      </div>
                      <div class="form-group row ">
                        <label class="col-md-3 form-control-label required"> City </label>
                        <div class="col-md-6">
                          <input class="form-control" name="city" type="text" value="{openzc:account item='address' field='city'/}" maxlength="64" required="">
                        </div>
                        <div class="col-md-3 form-control-comment"> </div>
                      </div>
                      <div class="form-group row ">
                        <label class="col-md-3 form-control-label required"> Country </label>
                        <div class="col-md-6">
                          <select class="form-control form-control-select js-country" name="zone_country_id" required>
                            <option value="" disabled="" selected="">-- please choose --</option>
                            {openzc:var name="countries"}
                            {openzc:if $field['status']=="active"}
                            	<option value="[field:id/]" selected="">[field:text/]</option>
                            {else}
                            	<option value="[field:id/]">[field:text/]</option>
                            {/openzc:if} 
                            {/openzc:var}
                          </select>
                        </div>
                        <div class="col-md-3 form-control-comment"> </div>
                      </div>
                      {openzc:if $address_book['primary']==false}
                      <div class="form-group row">
						  <div class="col-md-9 col-md-offset-3">
							<input name="primary" type="checkbox" value="on"/>
							<label>Set as Primary Address</label>
						  </div>
					  </div>
                   	  {/openzc:if}
                    </section>
                    <footer class="form-footer clearfix">
                      <input type="hidden" name="submitAddress" value="1">
                      <button class="btn btn-primary float-xs-right" type="submit"> Save </button>
                    </footer>
                  </form>
                </div>
              </div>