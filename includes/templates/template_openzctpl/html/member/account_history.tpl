﻿<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Order history</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="history" class="lang-en country-de currency-eur layout-left-column page-history tax-display-enabled page-customer-account">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <section id="wrapper">
    <nav data-depth="2" class="breadcrumb">
      <div class="container">
        <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php"> <span itemprop="name">Home</span> </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=my-account"> <span itemprop="name">Your account</span> </a>
            <meta itemprop="position" content="2">
          </li>
        </ol>
      </div>
    </nav>
    <div class="container">
      <div id="columns_inner">
        
        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9" style="width:78.6%">
          <section id="main">
            <header class="page-header">
              <h1> {openzc:define.HEADING_TITLE/}  </h1>
            </header>
            <section id="content" class="page-content">
              <aside id="notifications">
                <div class="container"> </div>
              </aside>
              <h6>Here are the orders you've placed since your account was created.</h6>
              <table class="table table-striped table-bordered table-labeled hidden-sm-down">
                <thead class="thead-default">
                  <tr>
                    <th>Order reference</th>
                    <th>Date</th>
                    <th>Total price</th>
                  
                    <th class="hidden-md-down">Status</th>
                    <th>Delivery name</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                 {openzc:account item="orders"}
                  <tr>
                    <th scope="row">[field:orders_id/]</th>
                    <td>[field:date_purchased/]</td>
                    <td class="text-xs-right">[field:order_total/]</td>
                   
                    <td><span class="label label-pill bright" style="background-color:#4169E1"> [field:orders_status_name/] </span></td>
                    <td class="text-sm-center hidden-md-down"> [field:order_name/] </td>
                    <td class="text-sm-center order-actions">
                    	<a href="[field:order_link/]"> Details </a> 
                    	
                    </td>
                  </tr>
                  {/openzc:account}
                </tbody>
              </table>
              <div class="orders hidden-md-up">
                <div class="order">
                  <div class="row">
                    <div class="col-xs-10"> <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=order-detail&amp;id_order=18">
                      <h3>HYDIWXCTI</h3>
                      </a>
                      <div class="date">03/26/2020</div>
                      <div class="total">€87.00</div>
                      <div class="status"> <span class="label label-pill bright" style="background-color:#4169E1"> Awaiting check payment </span> </div>
                    </div>
                    <div class="col-xs-2 text-xs-right">
                      <div> <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=order-detail&amp;id_order=18" data-link-action="view-order-details" title="Details"> <i class="material-icons">?</i> </a> </div>
                      <div> <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=order&amp;submitReorder=&amp;id_order=18" title="Reorder"> <i class="material-icons"></i> </a> </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <footer class="page-footer"> <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=my-account" class="account-link"> <i class="material-icons"></i> <span>Back to your account</span> </a> <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php" class="account-link"> <i class="material-icons">?</i> <span>Home</span> </a> </footer>
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>

</body>
</html>