﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Address</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="address" class="lang-en country-de currency-eur layout-left-column page-address tax-display-enabled page-customer-account">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <section id="wrapper">
    <nav data-depth="3" class="breadcrumb">
      <div class="container">
        <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php"> <span itemprop="name">Home</span> </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=my-account"> <span itemprop="name">Your account</span> </a>
            <meta itemprop="position" content="2">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=addresses"> <span itemprop="name">Addresses</span> </a>
            <meta itemprop="position" content="3">
          </li>
        </ol>
      </div>
    </nav>
    <div class="container">
      <div id="columns_inner">
        
        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9" style="width:78.6%">
          <section id="main">
            <header class="page-header">
              <h1> {openzc:define.HEADING_TITLE/} </h1>
            </header>
            <section id="content" class="page-content">
              <aside id="notifications">
                <div class="container"> </div>
              </aside>
              {openzc:if isset($_GET['delete'])}
              	{openzc:include filename="member/address_book_process_delete.tpl"/}
              {else}
              	{openzc:include filename="member/address_book_process_box.tpl"/}
              {/openzc:if}
              
            </section>
            <footer class="page-footer"> 
            <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=my-account" class="account-link"> <i class="material-icons"></i> <span>Back to your account</span> </a> 
            <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php" class="account-link"> <i class="material-icons">?</i> <span>Home</span> </a> </footer>
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>

</body>
</html>