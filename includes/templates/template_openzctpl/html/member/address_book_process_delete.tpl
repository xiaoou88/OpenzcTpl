﻿<div class="address-form">
  <div class="js-address-form">
    <form method="post" action="/index.php?main_page=address_book_process&action=deleteconfirm">
      <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
      <input type="hidden" name="delete" value="{openzc:echo}$_GET['delete']{/openzc:echo}"/>
      <section class="form-fields">
        <div class="alert alert-warning">{openzc:define.DELETE_ADDRESS_DESCRIPTION/}</div>
        <address>
        {openzc:account item="address" field="address"/}
        </address>
      </section>
      <footer class="form-footer clearfix">
      
        <button class="btn btn-primary float-xs-left" type="submit"> Delete </button>
      </footer>
    </form>
  </div>
</div>
