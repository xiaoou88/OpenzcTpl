﻿<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Maxcart - Mega Store</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="order-detail" class="lang-en country-de currency-eur layout-full-width page-order-detail tax-display-enabled">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <section id="wrapper">
    <nav data-depth="3" class="breadcrumb">
      <div class="container">
        <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php"> <span itemprop="name">Home</span> </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=my-account"> <span itemprop="name">Your account</span> </a>
            <meta itemprop="position" content="2">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=history"> <span itemprop="name">Order history</span> </a>
            <meta itemprop="position" content="3">
          </li>
        </ol>
      </div>
    </nav>
    <div class="container">
      <div id="columns_inner">
        <div id="content-wrapper">
          <section id="main">
            <header class="page-header">
              <h1> {openzc:define.HEADING_TITLE/} </h1>
            </header>
            <section id="content" class="page-content">
              <aside id="notifications">
                <div class="container"> </div>
              </aside>
              <div id="order-infos">
                <div class="box">
                  <div class="row">
                    <div class="col-xs-9"> <strong> Order Reference {openzc:account item="orders" field="orders_id"/} - placed on {openzc:account item="orders" field="date_purchased"/} </strong> </div>
                    
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="box">
                  <ul>
                    <li><strong>Carrier</strong> Maxcart - Mega Store</li>
                    <li><strong>Payment method</strong> {openzc:account item="orders" field="payment_method"/}</li>
                  </ul>
                </div>
              </div>
              <section id="order-history" class="box">
                <h3>Follow your order's status step-by-step</h3>
                <table class="table table-striped table-bordered table-labeled hidden-xs-down">
                  <thead class="thead-default">
                    <tr>
                      <th>Date</th>
                      <th>Status</th>
                      <th>Comments</th>
                    </tr>
                  </thead>
                  <tbody>
                   
                   {openzc:account item="orders" field="status_history"}
                    <tr>
                      <td>[field:date_added/]</td>
                      <td><span class="label label-pill bright" style="background-color:#4169E1"> [field:orders_status_name/] </span></td>
                      <td>[field:comments/]</td>
                    </tr>
                   {/openzc:account}
                  </tbody>
                </table>
                <div class="hidden-sm-up history-lines">
                 {openzc:account item="orders" field="status_history"}
                  <div class="history-line">
                    <div class="date">[field:date_added/]</div>
                    <div class="state"> <span class="label label-pill bright" style="background-color:#4169E1"> [field:orders_status_name/] </span> </div>
                  </div>
                  {/openzc:account}
                </div>
              </section>
              <div class="addresses">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <article id="delivery-address" class="box">
                    <h4>Delivery address My Address</h4>
                    <address>
                    {openzc:account item="orders" field="delivery"/}
                    </address>
                  </article>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <article id="invoice-address" class="box">
                    <h4>Invoice address My Address</h4>
                    <address>
                    {openzc:account item="orders" field="billing"/}
                    </address>
                  </article>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="box hidden-sm-down">
                <table id="order-products" class="table table-bordered">
                  <thead class="thead-default">
                    <tr>
                      <th>Product</th>
                      <th>Quantity</th>
                      <th>Unit price</th>
                      <th>Total price</th>
                    </tr>
                  </thead>
                  <tbody>
                   {openzc:account item="orders" field="products"}
                    <tr>
                      <td><strong> <a> [field:name/] </a> </strong><br>
                      {openzc:loopson field="attr"}
                      	[field:option/]:[field:value/] - 
                      {/openzc:loopson}
                      <br></td>
                      <td> [field:qty/] </td>
                      <td class="text-xs-right">[field:price/]</td>
                      <td class="text-xs-right">[field:final_price/]</td>
                    </tr>
                   {/openzc:account}
                  </tbody>
                  <tfoot>
                   {openzc:account item="orders" field="totals"}
                    <tr class="text-xs-right line-products">
                      <td colspan="3">[field:title/]</td>
                      <td>[field:text/]</td>
                    </tr>
                   {/openzc:account}
                   
                  </tfoot>
                </table>
              </div>
              <div class="order-items hidden-md-up box">
                <div class="order-item">
                 {openzc:account item="orders" field="products"}
                  <div class="row">
                    <div class="col-sm-5 desc">
                      <div class="name">[field:name/]</div>
                      <div class="ref">
                      {openzc:loopson field="attr"}
                      	[field:option/]:[field:value/] - 
                      {/openzc:loopson}
                      </div>
                    </div>
                    <div class="col-sm-7 qty">
                      <div class="row">
                        <div class="col-xs-4 text-sm-left text-xs-left"> [field:price/] </div>
                        <div class="col-xs-4"> [field:qty/] </div>
                        <div class="col-xs-4 text-xs-right"> [field:final_price/] </div>
                      </div>
                    </div>
                  </div>
                 {/openzc:account}
                </div>
              </div>
              <div class="order-totals hidden-md-up box">
               {openzc:account item="orders" field="totals"}
                <div class="order-total row">
                  <div class="col-xs-8"><strong>[field:title/]</strong></div>
                  <div class="col-xs-4 text-xs-right">[field:text/]</div>
                </div>
               {/openzc:account}
                
              </div>
              
            </section>
            <footer class="page-footer"> 
            <a href="/index.php?main_page=account" class="account-link"> <i class="material-icons"></i> <span>Back to your account</span> </a> 
            <a href="/" class="account-link"> <i class="material-icons">?</i> <span>Home</span> </a> 
            </footer>
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>

</body>
</html>