{openzc:cart "SourceString="PEY/u0kazP+Vl5GzMjm6laZB/qYj/rccE+vkX1IvOwY=" "imgsizer="80,76"}

                      <li class="cart-item">
                        <div class="product-line-grid"> 
                          <!--  product left content: image-->
                          <div class="product-line-grid-left col-md-3 col-xs-4"> <span class="product-image media-middle"> <img src="[field:products_image/]" alt="Laudant doloremque"> </span> </div>
                          
                          <!--  product left body: description -->
                          <div class="product-line-grid-body col-md-4 col-xs-8">
                            <div class="product-line-info"> <a class="label" href="[field:products_link/]" >[field:products_name/]</a> </div>
                            <div class="product-line-info product-price h5 has-discount">
							{openzc:if $field['productPriceDiscount']}
                              <div class="product-discount"> 
							  <span class="regular-price">[field:products_original_price/]</span> 
							  <span class="discount discount-percentage"> [field:productPriceDiscount/] </span> 
							  </div>
							 {/openzc:if}
                              <div class="current-price"> 
							  <span class="price">[field:products_price/]</span> 
							  </div>
                            </div>
                            <br>
							{openzc:loopson item="attr"}
								 <div class="product-line-info"> <span class="label">[field:products_options_name/]:</span> <span class="value">[field:products_options_values_name/]</span> </div>
							{/openzc:loopson}
                            
                          </div>
						  
                          
                          <!--  product left body: description -->
                          <div class="product-line-grid-right product-line-actions col-md-5 col-xs-12">
                            <div class="row">
                              <div class="col-xs-4 hidden-md-up"></div>
                              <div class="col-md-10 col-xs-6">
                                <div class="row">
                                  <div class="col-md-6 col-xs-6 qty">
                                    <div class="input-group bootstrap-touchspin">
                                     <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                      <input class="js-cart-line-product-quantity form-control openzc-input" value="[field:products_qty/]" name="product-quantity-spin" min="1" style="display: block;" data-action="quantity" data-id="[field:id/]" >
                                      <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical">
                                      <button class="btn btn-touchspin js-touchspin js-increase-product-quantity bootstrap-touchspin-up " data-id="[field:id/]" data-action="addOne" >
                                      <i class="material-icons touchspin-up"></i>
                                      </button>
                                      <button class="btn btn-touchspin js-touchspin js-decrease-product-quantity bootstrap-touchspin-down " data-id="[field:id/]" data-reload="cartlist">
                                      <i class="material-icons touchspin-down"></i>
                                      </button>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-xs-2 price"> <span class="product-price"> <strong> [field:productsPrice/]</strong> </span> </div>
                                </div>
                              </div>
                              <div class="col-md-2 col-xs-2 text-xs-right">
                                <div class="cart-line-product-actions"> <a class="remove-from-cart openzc-btn" rel="nofollow" data-action="delCart" data-id="[field:id/]"> <i class="material-icons float-xs-left">delete</i> </a> </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </li>
					  
{/openzc:cart}