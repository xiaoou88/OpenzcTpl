<div id="left-column" class="col-xs-12" style="width:21.4%">
  {openzc:include filename="public/category_tree.tpl"/}
  
  <div id="search_filters_wrapper" class="hidden-md-down block"> <!-- hidden-sm-down -->
    <div id="search_filter_controls" class="hidden-lg-up"> <!--  --> 
      <span id="_mobile_search_filters_clear_all"></span>
      <button class="btn btn-secondary ok"> <i class="material-icons">?</i> OK </button>
    </div>
    <div id="search_filters">
      <h4 class="block_title">Filter By</h4>
      <div id="leftfilter" class="block_content">
        {openzc:include filename="list/leftfilter.tpl" ajax="leftfilter"/}
      </div>
    </div>
  </div>
  <div id="czleftbanner">
    <ul>
      <li class="slide czleftbanner-container"> <a href="#" title="LeftBanner 1"> <img src="{openzc:field.template/}style/img/left-banner-1.jpg" alt="LeftBanner 1" title="LeftBanner 1"> </a> </li>
    </ul>
  </div>
  <div id="newproduct_block" class="block products-block">
    <h4 class="block_title hidden-md-down"> New products </h4>
    <h4 class="block_title hidden-lg-up" data-target="#newproduct_block_toggle" data-toggle="collapse"> New products <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="fa-icon add"></i> <i class="fa-icon remove"></i> </span> </span> </h4>
    <div id="newproduct_block_toggle" class="block_content  collapse">
      <ul class="products">
        {openzc:prolist cid="current" flag="new" row="5" imgsizer="80,76"}
        <li class="product_item">
          <div class="product-miniature js-product-miniature">
            <div class="product_thumbnail"> <a href="[field:products_link/]" class="thumbnail product-image"> <img src="[field:products_image/]" alt="[field:products_name/]"> </a> </div>
            <div class="product-info">
              <h1 class="h3 product-title" itemprop="name"><a href="[field:products_link/]">[field:products_name/]</a></h1>
              <div class="product-price-and-shipping"> {openzc:if $field['productPriceDiscount']} <span class="regular-price">[field:products_original_price/]</span> <span class="discount-amount discount-product">[field:productPriceDiscount/]</span> {/openzc:if} <span itemprop="price" class="price">[field:products_price/]</span> </div>
              <div class="comments_note">
                <div class="star_content clearfix"> {openzc:php}
                  for($i=0;$i<5;$i++){
                  if($i<$field['products_rating']){
                                  	echo '<div class="star star_on"></div>'."\r\n";
                                }else{
                                  	echo '<div class="star"></div>'."\r\n";
                              }   		
                            }
                        	{/openzc:php}
                          
                        </div>
                <span class="total-rating">1 Review(s)&nbsp;</span> </div>
            </div>
          </div>
        </li>
        {/openzc:prolist}
      </ul>
      <div class="view_more"> <a class="all-product-link btn btn-primary" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=new-products"> All new products </a> </div>
    </div>
  </div>
</div>