{openzc:filter item="clear"}
<div id="_desktop_search_filters_clear_all" class="hidden-md-down clear-all-wrapper"> <a href="[field:filter_link/]" class="btn btn-tertiary js-search-filters-clear-all"> <i class="material-icons"></i> Clear all </a> </div>
{/openzc:filter}
{openzc:filter item="brand" mul="true" }
<section class="facet">
  <h1 class="h6 facet-title hidden-md-down">[field:filter_item/]</h1>
  <div class="title hidden-lg-up" data-toggle="collapse">
    <h1 class="h6 facet-title">[field:filter_name/]</h1>
    <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="fa-icon add"></i> <i class="fa-icon remove"></i> </span> </span> </div>
  <ul class="collapse">
    {openzc:loopson type="son" }
    <li>
      <label class="facet-label" for="facet_input_33222_0"> 
        <span class="custom-radio">
        	{openzc:if $field['status']=="active"}
			<input type="radio"checked>
			{else}
			<input type="radio">
			{/openzc:if}
			<span class="ps-shown-by-js"></span>
		</span>
        <a data-url="[field:filter_link/]" class="openzc-btn _gray-darker search-link" data-reload="leftfilter,class:rightlist" data-action="getHtml"> [field:filter_name/] <span class="magnitude">([field:filter_count/])</span> </a> </label>
    </li>
    {/openzc:loopson}
  </ul>
</section>
{/openzc:filter} 
{openzc:filter item="price" space="100" }
<section class="facet">
  <h1 class="h6 facet-title hidden-md-down">[field:filter_item/]</h1>
  <div class="title hidden-lg-up" data-toggle="collapse">
    <h1 class="h6 facet-title">[field:filter_item/]</h1>
    <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="fa-icon add"></i> <i class="fa-icon remove"></i> </span> </span> </div>
  <ul id="facet_46479" class="collapse">
    {openzc:loopson type="son"}
    <li>
      <label class="facet-label" for="facet_input_33222_0"> <span class="custom-checkbox">
        <span class="custom-radio">
        	{openzc:if $field['status']=="active"}
			<input type="radio"checked>
			{else}
			<input type="radio">
			{/openzc:if}
			<span class="ps-shown-by-js"></span>
		</span>
        <a data-url="[field:filter_link/]" class="openzc-btn _gray-darker search-link" data-reload="leftfilter,class:rightlist" data-action="getHtml"> [field:filter_name/] <span class="magnitude">([field:filter_count/])</span> </a> </label>
    </li>
    {/openzc:loopson}
  </ul>
</section>
{/openzc:filter}

{openzc:filter item="attr" options_id="1,2" mul="true" }
<section class="facet">
  <h1 class="h6 facet-title hidden-md-down">[field:filter_item/]</h1>
  <div class="title hidden-lg-up" data-toggle="collapse">
    <h1 class="h6 facet-title">[field:filter_name/]</h1>
    <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="fa-icon add"></i> <i class="fa-icon remove"></i> </span> </span> </div>
  <ul id="facet_33222" class="collapse">
    {openzc:loopson type="son"}
    <li>
      <label class="facet-label"> <span class="custom-checkbox">
        <span class="custom-radio">
        	{openzc:if $field['status']=="active"}
			<input type="radio"checked>
			{else}
			<input type="radio">
			{/openzc:if}
			<span class="ps-shown-by-js"></span>
		</span>
        <a data-url="[field:filter_link/]" class="openzc-btn _gray-darker search-link" data-reload="leftfilter,class:rightlist" data-action="getHtml"> [field:filter_name/] <span class="magnitude">([field:filter_count/])</span> </a> </label>
    </li>
    {/openzc:loopson}
  </ul>
</section>
{/openzc:filter} 