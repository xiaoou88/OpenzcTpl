{openzc:s}

					<label class="col-md-3 form-control-label required"> {openzc:define.ENTRY_STATE/} </label>
					<div class="col-md-6">
					  <select class="form-control form-control-select js-country" name="zone_id" required>
						{openzc:var name="states"}
						{openzc:if $field['status']=="active"}
						<option value="[field:id/]" selected="">[field:text/]</option>
						{else}
						<option value="[field:id/]">[field:text/]</option>
						{/openzc:if}
						{/openzc:var}
					  </select>
					</div>
					<div class="col-md-3 form-control-comment"> </div>
                 	
{/openzc:s}