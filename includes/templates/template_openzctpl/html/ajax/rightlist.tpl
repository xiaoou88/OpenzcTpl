
  <div id="">
    <div id="js-product-list-top" class="products-selection">
      <div class="col-md-6 hidden-md-down total-products">
        <ul class="display hidden-xs grid_list">
          <li id="grid" class="selected"><a href="#" title="Grid">Grid</a></li>
          <li id="list"><a href="#" title="List">List</a></li>
        </ul>
        <p>{openzc:pagelist listitem="info"/}</p>
      </div>
      <div class="col-md-6">
        <div class="row sort-by-row"> <span class="col-sm-3 col-md-3 hidden-sm-down sort-by">Sort by:</span>
          <div class="col-sm-9 col-xs-8 col-md-9 products-sort-order dropdown"> {openzc:if !$_GET['orderby']} <a class="select-title" rel="nofollow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Relevance <i class="material-icons pull-xs-right"></i></a> {/openzc:if}
            {openzc:listbar item="orderby" flag="sell,price,view,name"}
            {openzc:if $field['status']=="active"} <a class="select-title" rel="nofollow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> [field:orderby_name/] <i class="material-icons pull-xs-right"></i> </a> {/openzc:if}
            {/openzc:listbar}
            <div class="dropdown-menu"> {openzc:listbar item="orderby" flag="sell,price,view,name"}
              
              {openzc:if $field['status']=="active"} <a rel="nofollow" href="[field:orderby_link/]" class="select-list current"> [field:orderby_name/] </a> {else} <a rel="nofollow" href="[field:orderby_link/]" class="select-list"> [field:orderby_name/] </a> {/openzc:if}
              
              {/openzc:listbar} </div>
          </div>
          <div class="col-sm-3 col-xs-4 hidden-lg-up filter-button">
            <button id="search_filter_toggler" class="btn btn-secondary"> Filter </button>
          </div>
        </div>
      </div>
      <div class="col-sm-12 hidden-lg-up showing"> Showing 1-12 of 15 item(s) </div>
    </div>
  </div>
  <div id="" class="hidden-sm-down">
    <section id="js-active-search-filters" class="hide"> </section>
  </div>
  <div id="">
    <div id="js-product-list">
      <div class="products row">
        <ul class="product_list grid gridcount">
          <!-- removed product_grid--> 
          {openzc:list pagesize="12" desclen="200" imgsizer="248,235"}
          <li class="product_item col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <div class="product-miniature js-product-miniature">
              <div class="thumbnail-container"> <a href="[field:products_link/]" class="thumbnail product-thumbnail"> <img src="[field:products_image/]" alt="[field:products_name/]" data-full-size-image-url="[field:products_image/]"> <img class="fliper_image img-responsive" src="[field:products_image/]" data-full-size-image-url="[field:products_image/]" alt=""> </a>
                <div class="outer-functional">
                  <div class="functional-buttons"> <a href="#" class="quick-view" data-link-action="quickview"> <i class="material-icons search"></i> Quick view </a>
                    <div class="product-actions">
                      <form method="post" class="openzc-form add-to-cart-or-refresh">
                        <input type="hidden" name="products_id" value="[field:products_id/]">
                        <button class="btn btn-primary add-to-cart" data-button-action="add-to-cart" type="submit"> Add to cart </button>
                      </form>
                    </div>
                  </div>
                </div>
                {openzc:if $field['productPriceDiscount']}
                <ul class="product-flags">
                  <li class="on-sale">On sale!</li>
                </ul>
                {/openzc:if} </div>
              <div class="product-description">
                <div class="comments_note">
                  <div class="star_content clearfix"> {openzc:php}
                    for($i=0;$i<5;$i++){
                    if($i<$field['products_rating']){
											echo '<div class="star star_on"></div>'."\r\n";
										}else{
											echo '<div class="star"></div>'."\r\n";
									  }   		
									}
                        		{/openzc:php}
                              </div>
                  <span class="total-rating">[field:products_reviews_count/] Review(s)&nbsp;</span> </div>
                <h3 class="h3 product-title" itemprop="name"> <a href="[field:products_link/]">[field:products_name/]</a></h3>
                <div class="product-price-and-shipping"> {openzc:if $field['productPriceDiscount']} <span class="regular-price">[field:products_original_price/]</span> {/openzc:if} <span itemprop="price" class="price">[field:products_price/]</span> </div>
                <div class="product-detail" itemprop="description">
                  <p>[field:products_description/]</p>
                </div>
                <div class="highlighted-informations hidden-sm-down">
                  <div class="variant-links"> {openzc:proattr options_id="1"}
                    {openzc:proattr type="son" values="15:#198af3,16:#fa073a,28:#775519,29:#000,30:#eae9e8"} <a class="color" title="[field:options_values_name/]" style="background-color:[field:options_values/]"><span class="sr-only">[field:options_values_name/]</span></a> {/openzc:proattr}
                    {/openzc:proattr} <span class="js-count count"></span> </div>
                  {openzc:if $field['products_quantity']>0} <span class="product-availability"> <span class="product-available"> <i class="material-icons"></i> In stock </span> </span> {else} <span class="product-availability"> <span class="product-available"> <i class="material-icons"></i> Oversell </span> </span> {/openzc:if} </div>
              </div>
            </div>
          </li>
          {/openzc:list}
        </ul>
      </div>
      <nav class="pagination">
        <div class="col-md-4"> {openzc:pagelist listitem="info"/} </div>
        <div class="col-md-8">
          <ul class="page-list clearfix text-xs-right">
            {openzc:pagelist listitem="prev,list,next" listsize="5"/}
          </ul>
        </div>
      </nav>
      
      <!--<div class="hidden-md-up text-xs-right up">
		<a href="#header" class="btn btn-secondary">
      		Back to top
			<i class="material-icons">&#xE316;</i>
		</a>
	</div>--> 
    </div>
  </div>
  <div id="js-product-list-bottom">
    <div id="js-product-list-bottom"></div>
  </div>
