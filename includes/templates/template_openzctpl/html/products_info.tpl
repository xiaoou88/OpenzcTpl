<!doctype html>
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<link rel="canonical" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_product=1&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1">
<title>Consectetur Hampden</title>
<meta name="description" content="At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi.">
<meta name="keywords" content="">

<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<meta property="og:type" content="product">
<meta property="og:url" content="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_product=1&amp;id_product_attribute=1&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1">
<meta property="og:title" content="Consectetur Hampden">
<meta property="og:site_name" content="Maxcart - Mega Store">
<meta property="og:description" content="At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi.">
<meta property="og:image" content="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/img/p/1/1/6/116-large_default.jpg">
<meta property="product:pretax_price:amount" content="98">
<meta property="product:pretax_price:currency" content="EUR">
<meta property="product:price:amount" content="104.86">
<meta property="product:price:currency" content="EUR">
<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="product" class="lang-en country-de currency-eur layout-full-width page-product tax-display-enabled product-id-1 product-consectetur-hampden product-id-category-3 product-id-manufacturer-3 product-id-supplier-0 product-on-sale product-available-for-order">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <aside id="notifications">
    <div class="container"> </div>
  </aside>
  <section id="wrapper">
    <nav data-depth="3" class="breadcrumb">
      <div class="container">
        <h1 class="h1 productpage_title" itemprop="name">{openzc:proinfo field="products_name"/}</h1>
        <ol>
          {openzc:field.position/}
        </ol>
      </div>
    </nav>
    <div class="container">
      <div id="columns_inner">
        <div id="content-wrapper">
          <section id="main" itemscope="" itemtype="https://schema.org/Product">
            <meta itemprop="url" >
            <div class="row">
              <div class="pp-left-column col-xs-12 col-sm-5 col-md-5">
                <section class="page-content" id="content">
                  <div class="product-leftside">
                    <ul class="product-flags">
                     
                      <li class="product-flag on-sale">On sale!</li>
                      
                    </ul>
                    <div class="images-container">
                      <div class="product-cover"> 
                       <a href="{openzc:proinfo field='products_image'/}"> 
                       <img class="js-qv-product-cover" src="{openzc:proinfo field='products_image'/}" alt="" title="" style="width:100%;" itemprop="image"> 
                       </a>
                        <div class="layer hidden-sm-down" data-toggle="modal" data-target="#product-modal"> <i class="fa fa-arrows-alt zoom-in"></i> </div>
                      </div>
                      
                      <!-- Define Number of product for SLIDER -->
                      
                      <div class="js-qv-mask mask additional_slider">
                        <ul class="cz-carousel product_list additional-carousel owl-carousel owl-theme" style="opacity: 1; display: block;">
                          
                             {openzc:mulimg imgsizer="95,95"}
                              <div class="owl-item" style="width: 127px;">
                                <li class="thumb-container item"> 
                                <img class="thumb js-thumb" data-image-medium-src="[field:products_image_detail/]" data-image-large-src="[field:products_image_detail_full/]" src="[field:products_image_detail/]" alt="" title="" width="95" itemprop="image"> 
                                </li>
                              </div>
                              {/openzc:mulimg}
                            
                          <div class="owl-controls clickable">
                            <div class="owl-pagination">
                              <div class="owl-page active"><span class=""></span></div>
                              <div class="owl-page"><span class=""></span></div>
                            </div>
                          </div>
                        </ul>
                        <div class="customNavigation"> <a class="btn prev additional_prev">&nbsp;</a> <a class="btn next additional_next">&nbsp;</a> </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
              <div class="pp-right-column col-xs-12  col-sm-7 col-md-7"> 
                
                <!-- Codezeel added -->
                
                <div class="comments_note">
                  <div class="star_content clearfix">
                   {openzc:php}
                   for($i=0;$i<5;$i++){
                   		if($i<$products_info['products_rating']){
                        	echo '<div class="star star_on"></div>'."\r\n";
                        }else{
                            echo '<div class="star"></div>'."\r\n";
                        }   	
                   }
                   {/openzc:php}
                    
                  </div>
                  <span class="total-rating">{openzc:proinfo field="products_reviews_count"/} Review(s)&nbsp;</span> </div>
                <div class="product-reference">
                  <label class="label">Reference: </label>
                  <span itemprop="sku">demo_1</span> </div>
                <div class="product-condition">
                  <label class="label">Model: </label>
                
                  <span>{openzc:proinfo field="products_model"/}</span> </div>
                <div class="product-information">
                  <div id="product-description-short-1" itemprop="description">
                    <p>{openzc:proinfo field="products_description" desclen="150"/}</p>
                  </div>
                  <div class="product-actions">
                    <form class="openzc-form">
                 
                      <input type="hidden" name="products_id" value="{openzc:proinfo field='products_id'/}">
                     
			  		  <input type="hidden" name="action" value="addCart"/>
                      <div class="product-variants">
                      	
                       	{openzc:proattr item="select"}
                        <div class="clearfix product-variants-item"> <span class="control-label">[field:options_name/]</span>
                          <select id="group_[field:options_id/]" name="id[[field:options_id/]]">
                            {openzc:proattr type="son"}
								<option value="[field:options_values_id/]" title="[field:options_values_name/]">[field:options_values_name/]</option>
							{/openzc:proattr}
                          </select>
                        </div>
                        {/openzc:proattr}
                        {openzc:proattr options_id="1"}
                      	<div class="clearfix product-variants-item">
                         	<span class="control-label">[field:options_name/]</span>
                          <ul id="group_[field:options_id/]">
                           	{openzc:proattr type="son" values="15:#198af3,16:#fa073a,28:#775519,29:#000,30:#eae9e8"}
                            <li class="pull-xs-left input-container">
                              <input class="input-color" type="radio" name="id[[field:options_id/]]" value="[field:options_values_id/]">
                              <span class="color" style="background-color:[field:options_values/]"><span class="sr-only">[field:options_values_name/]</span></span> 
                            </li>
                            {/openzc:proattr}
                          </ul>
                        </div>
                        {/openzc:proattr}
                      </div>
                      <section class="product-discounts"> </section>
                      <div class="product-prices">
                        <div class="product-price h5 ">
                          <div class="product-discount">
                          <span class="regular-price">{openzc:proinfo field="products_original_price"/}</span>
                          </div>
                          <div class="current-price">
                          <span content="104.86">{openzc:proinfo field="products_price"/}</span> 
                          <span class="discount discount-percentage">{openzc:proinfo field="productPriceDiscount"/}</span>
                          </div>
                        </div>
                       
                        {openzc:if $products_info["products_tax_class_id"]>0}
                        <div class="tax-shipping-delivery-label"> Tax included </div>
                        {/openzc:if}
                      </div>
                      <div class="product-add-to-cart"> 
                        <!-- <span class="control-label">Quantity</span>-->
                        
                        <div class="product-quantity">
                          <div class="qty">
                            <div class="input-group bootstrap-touchspin">
                              <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                              <input type="text" name="cart_quantity" id="quantity_wanted" value="1" class="input-group form-control" min="1" aria-label="Quantity" style="display: block;">
                             <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                              <span class="input-group-btn-vertical">
                              <button class="btn btn-touchspin js-touchspin bootstrap-touchspin-up" type="button"><i class="material-icons touchspin-up"></i></button>
                              <button class="btn btn-touchspin js-touchspin bootstrap-touchspin-down" type="button"><i class="material-icons touchspin-down"></i></button>
                              </span>
                              </div>
                          </div>
                          <div class="add">
                           
                           	<input type="hidden" name="reload" value="viewcart"/>
                            <button class="btn btn-primary add-to-cart" type="submit" > Add to cart </button>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <span id="product-availability"> <span class="product-available"> <i class="material-icons"></i> In stock </span> </span>
                        <p class="product-minimal-quantity"> </p>
                      </div>
                      <div class="product-additional-info">
                        <div class="social-sharing"> <span>Share</span>
                          <ul>
                            <li class="facebook icon-gray"><a href="http://www.facebook.com/sharer.php?u={openzc:field.weburl/}" class="" title="Share" target="_blank">&nbsp;</a></li>
                            <li class="twitter icon-gray"><a href="https://twitter.com/intent/tweet?text={openzc:field.weburl/}" class="" title="Tweet" target="_blank">&nbsp;</a></li>
                            <li class="googleplus icon-gray"><a href="https://plus.google.com/share?url={openzc:field.weburl/}" class="" title="Google+" target="_blank">&nbsp;</a></li>
                            <li class="pinterest icon-gray"><a href="http://www.pinterest.com/pin/create/button/?media={openzc:field.weburl/}" class="" title="Pinterest" target="_blank">&nbsp;</a></li>
                          </ul>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div id="block-reassurance">
                    <ul>
                      <li>
                        <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)"> <span class="h6">Security policy (edit with Customer reassurance module)</span> </div>
                      </li>
                      <li>
                        <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)"> <span class="h6">Delivery policy (edit with Customer reassurance module)</span> </div>
                      </li>
                      <li>
                        <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)"> <span class="h6">Return policy (edit with Customer reassurance module)</span> </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <section class="product-tabcontent">
              <div class="tabs">
                <ul class="nav nav-tabs">
                  <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#description">Description</a> </li>
                  <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#product-details">Product Details</a> </li>
                  <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#attachments">Attachments</a> </li>
                </ul>
                <div class="tab-content" id="tab-content">
                  <div class="tab-pane fade in active" id="description">
                    <div class="product-description">
                      {openzc:proinfo field="products_description" desc="150"/}
                    </div>
                  </div>
                  <div class="tab-pane fade" id="product-details">
                    Product Details
                  </div>
                  <div class="tab-pane fade in" id="attachments">
                    Attachments
                  </div>
                </div>
              </div>
            </section>
            
            <!-- Define Number of product for SLIDER -->
            
            <section class="product-accessories clearfix">
              <h2 class="h1 products-section-title text-uppercase"><span> <span>You might also like</span> </span></h2>
              <div class="product-accessories-wrapper">
                <div class="products">
                  <ul id="accessories-carousel" class="cz-carousel product_list owl-carousel owl-theme" style="opacity: 1; display: block;">
                    {openzc:prolist cid="current" row="10"}
                        <div class="owl-item" style="width: 286px;">
                          <li class="item">
                            <div class="product-miniature js-product-miniature">
                              <div class="thumbnail-container"> 
                               <a href="[field:products_link/]" class="thumbnail product-thumbnail"> 
                               <img src="[field:products_image/]" alt="Commodi consequatur" data-full-size-image-url="[field:products_image_full/]"> 
                               <img class="fliper_image img-responsive" src="[field:products_image_flip/]" data-full-size-image-url="[field:products_image_flip_full/]" alt=""> 
                               </a>
                                <div class="outer-functional">
                                  <div class="functional-buttons"> <a href="#" class="quick-view" data-link-action="quickview"> <i class="material-icons search"></i> Quick view </a>
                                    <div class="product-actions"> <a href="[field:products_link/]" class="btn btn-primary add-to-cart view_page"> View Detail </a> </div>
                                  </div>
                                </div>
                                <ul class="product-flags">
                                  <li class="new">New</li>
                                </ul>
                              </div>
                              <div class="product-description">
                                <div class="comments_note">
                                  <div class="star_content clearfix">
                                    {openzc:php}
										for($i=0;$i<5;$i++){
											if($i<$field['products_rating']){
												echo '<div class="star star_on"></div>'."\r\n";
											}else{
												echo '<div class="star"></div>'."\r\n";
										  }   		
										}
                        			{/openzc:php}
                                  </div>
                                  <span class="total-rating">[field:products_reviews_count/] Review(s)&nbsp;</span> </div>
                                <span class="h3 product-title" itemprop="name"><a href="[field:products_link/]">[field:products_name/]</a></span>
                                <div class="product-price-and-shipping"> <span itemprop="price" class="price">[field:products_price/]</span> </div>
                                <div class="highlighted-informations hidden-sm-down">
                                  <div class="variant-links"> 
                                  {openzc:proattr options_id="1"}
									{openzc:proattr type="son" values="15:#198af3,16:#fa073a,28:#775519,29:#000,30:#eae9e8"}
									<a class="color" title="[field:options_values_name/]" style="background-color:[field:options_values/]"><span class="sr-only">[field:options_values_name/]</span></a>
									{/openzc:proattr}
								  {/openzc:proattr}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        </div>
                     {/openzc:prolist}
                    
                    <div class="owl-controls clickable">
                      <div class="owl-pagination">
                        <div class="owl-page active"><span class=""></span></div>
                        <div class="owl-page"><span class=""></span></div>
                      </div>
                    </div>
                  </ul>
                  <div class="customNavigation"> <a class="btn prev accessories_prev">&nbsp;</a> <a class="btn next accessories_next">&nbsp;</a> </div>
                </div>
              </div>
            </section>
            <section class="productscategory-products clearfix">
              <h2 class="h1 products-section-title text-uppercase"><span> other products in the same category: </span></h2>
              <div class="productscategory-wrapper">
                <div class="products"> 
                  <!-- Define Number of product for SLIDER -->
                  
                  <ul id="productscategory-carousel" class="cz-carousel product_list owl-carousel owl-theme" style="opacity: 1; display: block;">
                  
                     
                       {openzc:prolist cid="current" row="20"}
                        <div class="owl-item" style="width: 286px;">
                          <li class="item">
                            <div class="product-miniature js-product-miniature">
                              <div class="thumbnail-container"> 
                               <a href="[field:products_link/]" class="thumbnail product-thumbnail"> 
                               <img src="[field:products_image/]" alt="[field:products_name/]" data-full-size-image-url="[field:products_image_full/]"> 
                               </a>
                                <div class="outer-functional">
                                  <div class="functional-buttons"> <a href="#" class="quick-view" data-link-action="quickview"> <i class="material-icons search"></i> Quick view </a>
                                    <div class="product-actions"> <a href="[field:products_link/]" class="btn btn-primary add-to-cart view_page"> View Detail </a> </div>
                                  </div>
                                </div>
                                <ul class="product-flags">
                                  <li class="on-sale">On sale!</li>
                                  <li class="new">New</li>
                                </ul>
                              </div>
                              <div class="product-description">
                                <div class="comments_note">
                                  <div class="star_content clearfix">
                                   {openzc:php}
										for($i=0;$i<5;$i++){
											if($i<$field['products_rating']){
												echo '<div class="star star_on"></div>'."\r\n";
											}else{
												echo '<div class="star"></div>'."\r\n";
										  }   		
										}
                        			{/openzc:php}
                                  </div>
                                  <span class="total-rating">{openzc:proinfo field="products_reviews_count"/} Review(s)&nbsp;</span> </div>
                                <span class="h3 product-title"><a href="[field:products_link/]">[field:products_name/]</a></span>
                                <div class="product-price-and-shipping"> 
                                <span class="regular-price">€91.00</span> 
                                <span class="discount-percentage discount-product">-7%</span> 
                                <span itemprop="price" class="price">[field:products_price/]</span> </div>
                                <div class="highlighted-informations">
                                  <div class="variant-links"> 
                                  {openzc:proattr options_id="1"}
									{openzc:proattr type="son" values="15:#198af3,16:#fa073a,28:#775519,29:#000,30:#eae9e8"}
									<a class="color" title="[field:options_values_name/]" style="background-color:[field:options_values/]"><span class="sr-only">[field:options_values_name/]</span></a>
									{/openzc:proattr}
								  {/openzc:proattr}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        </div>
                       {/openzc:prolist}
                 
                  
                    <div class="owl-controls clickable">
                      <div class="owl-pagination">
                        <div class="owl-page active"><span class=""></span></div>
                        <div class="owl-page"><span class=""></span></div>
                        <div class="owl-page"><span class=""></span></div>
                      </div>
                    </div>
                  </ul>
                  <div class="customNavigation"> <a class="btn prev productscategory_prev">&nbsp;</a> <a class="btn next productscategory_next">&nbsp;</a> </div>
                </div>
              </div>
            </section>
            
            <div id="productCommentsBlock">
              <div class="products-section-title-wrapper">
                <h1 class="products-section-title text-uppercase "><span> <span class="main-title"> Reviews </span> </span></h1>
              </div>
              <div class="tabs">
                <div class="clearfix pull-right"> </div>
                <div id="new_comment_form_ok" class="alert alert-success" style="display:none;padding:15px 25px"></div>
                <div id="product_comments_block_tab">
                 {openzc:reviews}
                  <div class="comment clearfix">
                    <div class="comment_author col-sm-2"> <span>Grade&nbsp;</span>
                      <div class="star_content clearfix">
                        {openzc:php}
                        	for($i=0;$i<5;$i++){
                                if($i<$field['reviews_rating']){
                                  	echo '<div class="star star_on"></div>'."\r\n";
                                }else{
                                  	echo '<div class="star"></div>'."\r\n";
                              }   		
                            }
                        {/openzc:php}
                      </div>
                      <div class="comment_author_infos"> <strong>[field:customers_name/]</strong><br>
                        <em>[field:date_added/]</em> </div>
                    </div>
                    <div class="comment_details col-sm-10">
                      <h4 class="title_block">Finibus</h4>
                      <p> [field:reviews_text/]</p>
                      <ul>
                      </ul>
                    </div>
                  </div>
                  {/openzc:reviews}
                </div>
              </div>
              
              <!-- Fancybox -->
              <div style="display:none">
                <div id="new_comment_form">
                  <form id="id_new_comment_form" action="#">
                    <h2 class="title">Write your review</h2>
                    <div class="product clearfix">
                      <div class="product_desc">
                        <p class="product_name"><strong>Praesentium voluptatum</strong></p>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi.</p>
                      </div>
                    </div>
                    <div class="new_comment_form_content">
                      <h2>Write your review</h2>
                      <div id="new_comment_form_error" class="error" style="display:none;padding:15px 25px">
                        <ul>
                        </ul>
                      </div>
                      <ul id="criterions_list">
                        <li>
                          <label>Quality</label>
                          <div class="star_content">
                            <input type="hidden" name="criterion[1]" value="5">
                            <div class="cancel"><a title="Cancel Rating"></a></div>
                            <div class="star star_on"><a title="1">1</a></div>
                            <div class="star star_on"><a title="2">2</a></div>
                            <div class="star star_on"><a title="3">3</a></div>
                            <div class="star star_on"><a title="4">4</a></div>
                            <div class="star star_on"><a title="5">5</a></div>
                          </div>
                          <div class="clearfix"></div>
                        </li>
                      </ul>
                      <label for="comment_title">Title for your review<sup class="required">*</sup></label>
                      <input id="comment_title" name="title" type="text" value="">
                      <label for="content">Your review<sup class="required">*</sup></label>
                      <textarea id="content" name="content"></textarea>
                      <div id="new_comment_form_footer">
                        <input id="id_product_comment_send" name="id_product" type="hidden" value="1">
                        <p class="fl required"><sup>*</sup> Required fields</p>
                        <p class="fr">
                          <button class="btn btn-primary" id="submitNewMessage" name="submitMessage" type="submit">Send</button>
                          &nbsp;
                          or&nbsp;<a href="#" onclick="$.fancybox.close();">Cancel</a> </p>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </form>
                  <!-- /end new_comment_form_content --> 
                </div>
              </div>
              <!-- End fancybox --> 
            </div>
            <div class="modal fade js-product-images-modal" id="product-modal">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <figure> <img class="js-modal-product-cover product-cover-modal" width="782" src="{openzc:proinfo field='products_image'/}" alt="" title="" itemprop="image">
                      <figcaption class="image-caption">
                        <div id="product-description-short" itemprop="description">
                          <p>{openzc:proinfo field="products_description" desclen="150"/}</p>
                        </div>
                      </figcaption>
                    </figure>
                    <aside id="thumbnails" class="thumbnails js-thumbnails text-xs-center">
                      <div class="js-modal-mask mask ">
                        <ul class="product-images js-modal-product-images">
                         {openzc:mulimg imgsizer="252,239"}
                          <li class="thumb-container"> 
                          <img data-image-large-src="[field:products_image_detail_full/]" class="thumb js-modal-thumb" src="[field:products_image_detail/]" alt="" title="" width="252" itemprop="image"> 
                          </li>
                          {/openzc:mulimg}
                        </ul>
                      </div>
                      <div class="arrows js-modal-arrows"> <i class="material-icons arrow-up js-modal-arrow-up"></i> <i class="material-icons arrow-down js-modal-arrow-down"></i> </div>
                    </aside>
                  </div>
                </div>
                <!-- /.modal-content --> 
              </div>
              <!-- /.modal-dialog --> 
            </div>
            <!-- /.modal -->
            
            <footer class="page-footer"> 
              
              <!-- Footer content --> 
              
            </footer>
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>
<script type="text/javascript" src="{openzc:field.template/}style/js/openzc.js"></script> 
<script>
Openzc.config={
	customReloadJS:{"no":"lightbox.js,custom.js,theme.js"},
	customReloadID:"viewcart,leftside",
	customAutoload:"viewcart"
};
Openzc.customReload=function(action,value){
	switch(action){
		case "viewcart":
			$("#viewcart").addClass("open");
			$(".cart_block").css('display',"block");
			$("#viewcart .shopping-cart").attr("aria-expanded","true");
		break;
	}
}
</script>
</body>
</html>