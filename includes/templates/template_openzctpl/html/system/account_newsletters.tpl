<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>

				<div class="card">
					<div class="card-body">
						<h4 class="card-title mb-3">{openzc:define.MY_NEWSLETTERS_GENERAL_NEWSLETTER/}</h4>
						<form action="/index.php?main_page=account_newsletters" method="post">
						<input type="hidden" name="action" value="process"/>
						<div class="form-check ">
							{openzc:if $newsletter->fields['customers_newsletter'] == '1'}
							<input type="checkbox" class="form-check-input" name="newsletter_general" id="newsletter_general" value="1" checked="">
							{else}
							<input type="checkbox" class="form-check-input" name="newsletter_general" id="newsletter_general" value="1">
							{/openzc:if}
							<label class="form-check-label" for="newsletter_general">{openzc:define.MY_NEWSLETTERS_GENERAL_NEWSLETTER_DESCRIPTION/}</label>
						</div>
						<button class="btn btn-warning waves-effect waves-light mt-3" type="submit">{openzc:define.BUTTON_SUBMIT_ALT/}</button>
						<a href="{openzc:link name='FILENAME_BACK'/}" class="btn btn-outline-light waves-effect float-right mt-3"><i class="fas fa-reply"></i>&nbsp;&nbsp;{openzc:define.BUTTON_BACK_ALT/}</a>
						</form>
					</div>
				
					</div>
				</div>
		
			
			
		</div>
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
    <script src="{openzc:field.assets/}libs/simplebar/simplebar.min.js"></script>
    <script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-validation.init.js"></script>
	<script src="{openzc:field.assets/}js/app.js"></script>
</body>
</html>