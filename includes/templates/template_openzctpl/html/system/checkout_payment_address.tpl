<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			{openzc:if strstr($referer,FILENAME_CHECKOUT_PAYMENT)}
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/}</h4>
			{else}
			<h4 class="mb-4">{openzc:define.TITLE_PAYMENT_ADDRESS/}</h4>
			{/openzc:if}
			
             
			<div class="row">
			<div class="col-xl-12 col-sm-12">
				
				<div class="card">
					<div class="card-body">
						
						<div class="row">
							<div class="col-xl-6 col-sm-6 mb-3">
							<h4 class="card-title mb-3"><i class="fas fa-flag"></i> {openzc:define.TITLE_PAYMENT_ADDRESS/}</h4>
							<address>{openzc:echo}zen_address_label($_SESSION['customer_id'], $_SESSION['billto'], true, ' ', '<br />'); {/openzc:echo}</address>
							</div>
							<p class="col-xl-6 col-sm-6 bg-light p-3">{openzc:define.TEXT_SELECTED_PAYMENT_DESTINATION/}</p>
							{openzc:if !strstr($referer,FILENAME_CHECKOUT_PAYMENT)}
							<button class="btn btn-outline-light waves-effect waves-light" type="button" data-toggle="collapse" data-target="#change_box" aria-expanded="false" aria-controls="change_box"> <i class="fas fa-exchange-alt"></i>&nbsp;&nbsp;{openzc:define.HEADING_TITLE/}</button> 
							{/openzc:if}
						</div>
					</div>
				</div>
			</div>
			<div id="change_box" class="collapse col-xl-12 col-sm-12 {openzc:if strstr($referer,FILENAME_CHECKOUT_PAYMENT)}show{/openzc:if}">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title mb-3"><i class="fab fa-wpforms"></i> &nbsp;&nbsp;{openzc:define.TITLE_PLEASE_SELECT/}</h4>
						<form action="{openzc:link name='FILENAME_CHECKOUT_PAYMENT_ADDRESS'/}" method="post" >
						
						{openzc:if ACCOUNT_GENDER == 'true'}
							<div class="form-group row">
                                <label for="gender" class="col-md-3 col-form-label">{openzc:define.ENTRY_GENDER/} <code>{openzc:define.ENTRY_GENDER_TEXT/}</code></label>
                                <div class="col-md-9 pt-2">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="MALE" name="gender"  value="m" class="custom-control-input">
										<label class="custom-control-label" for="MALE">{openzc:define.MALE/}</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline ">
										<input type="radio" id="FEMALE" name="gender" value="f" class="custom-control-input">
										<label class="custom-control-label" for="FEMALE">{openzc:define.FEMALE/}</label>
									</div>
                                </div>
                            </div>
						{/openzc:if}
						
						<div class="form-group row">
                            <label for="firstname" class="col-md-3 col-form-label">{openzc:define.ENTRY_FIRST_NAME/} <code>{openzc:define.ENTRY_FIRST_NAME_TEXT/}</code></label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="firstname">
                            </div>
                        </div>
						
						<div class="form-group row">
                            <label for="lastname" class="col-md-3 col-form-label">{openzc:define.ENTRY_LAST_NAME/} <code>{openzc:define.ENTRY_LAST_NAME_TEXT/}</code></label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="lastname">
                            </div>
                        </div>
						{openzc:if ACCOUNT_COMPANY == 'true'}
						<div class="form-group row">
                            <label for="company" class="col-md-3 col-form-label">{openzc:define.ENTRY_COMPANY/} <code>{openzc:define.ENTRY_COMPANY_TEXT/}</code></label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="company">
                            </div>
                        </div>
						{/openzc:if}
						<div class="form-group row">
                            <label for="stress_address" class="col-md-3 col-form-label">{openzc:define.ENTRY_STREET_ADDRESS/} <code>{openzc:define.ENTRY_STREET_ADDRESS_TEXT/}</code></label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="stress_address">
                            </div>
                        </div>
						{openzc:if ACCOUNT_SUBURB == 'true'}
						<div class="form-group row">
                            <label for="suburb" class="col-md-3 col-form-label">{openzc:define.ENTRY_SUBURB/} <code>{openzc:define.ENTRY_SUBURB_TEXT/}</code></label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="suburb">
                            </div>
                        </div>
						{/openzc:if}
						<div class="form-group row">
                            <label for="city" class="col-md-3 col-form-label">{openzc:define.ENTRY_CITY/} <code>{openzc:define.ENTRY_CITY_TEXT/}</code></label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" value="" id="city">
                            </div>
                        </div>
						<div id="states">
							{openzc:ajax filename="states" }
							{openzc:if ACCOUNT_STATE == 'true'}
							{openzc:if $flag_show_pulldown_states == true && count($states)>1}
							<div class="form-group row">
								<label for="zone_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_STATE/} <code>{openzc:define.ENTRY_STATE_TEXT/}</code></label>
								<div class="col-md-9">
								<select class="custom-select" name="zone_id">
                                    {openzc:var name="states"}
                                    <option value="[field:id/]" {openzc:if $field['status']=="active"}selected=""{/openzc:if}>[field:text/]</option>
									{/openzc:var}
                                </select>
								<input class="form-control d-none" type="text" name="state" value="{openzc:account item='address' field='state'/}" id="state">
								</div>
							</div>
							{else}
							<div class="form-group row">
								<label for="zone_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_STATE/} <code>{openzc:define.ENTRY_STATE_TEXT/}</code></label>
								<div class="col-md-9">
									<input class="form-control" type="text" name="state" value="{openzc:account item='address' field='state'/}" id="state">
									<input type="hidden" value="{openzc:var name='zone_name'/}" name="zone_id"/>
								</div>
							</div>
							{/openzc:if}
							{/openzc:if}
							{/openzc:ajax}
						</div>
						<div class="form-group row">
                            <label for="postcode" class="col-md-3 col-form-label">{openzc:define.ENTRY_POST_CODE/} <code>{openzc:define.ENTRY_POST_CODE_TEXT/}</code></label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="postcode" value="" id="postcode">
                            </div>
                        </div>
						<div class="form-group row">
								<label for="zone_country_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_COUNTRY/} <code>{openzc:define.ENTRY_COUNTRY_TEXT/}</code></label>
								<div class="col-md-9">
								{openzc:if $flag_show_pulldown_states == true}
								<select class="form-control openzc-select" name="zone_country_id" data-action="getState" data-reload="states" >
								{else}
								<select class="form-control select2" name="zone_country_id" >
								{/openzc:if}
									{openzc:var name="countries"}
                                    <option value="[field:id/]" {openzc:if $field['status']=="active"}selected=""{/openzc:if}>[field:text/]</option>
									{/openzc:var}
                                </select>
								</div>
						</div>
						<input type="hidden" name="action" value="submit"/>
						<button class="btn btn-light waves-effect waves-light float-right" type="submit"><i class="fas fa-redo-alt"></i>&nbsp;&nbsp;{openzc:define.BUTTON_UPDATE_ALT/}</button>
						</form>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
						<h4 class="card-title mb-3"><i class="fas fa-th-list"></i> &nbsp;&nbsp;{openzc:define.TABLE_HEADING_NEW_PAYMENT_ADDRESS/}</h4>
					
						<form action="{openzc:link name='FILENAME_CHECKOUT_PAYMENT_ADDRESS'/}" method="post" class="row">
						{openzc:account item="address"}
						
						<div class="col-xl-6 col-sm-6 mb-3">
							<span class="h5 font-weight-bold custom-control custom-radio">
								<input type="radio" id="address[field:address_book_id/]" name="address" class="custom-control-input" {openzc:if $field['address_book_id']==$_SESSION['billto']}checked=""{/openzc:if} value="[field:address_book_id/]">
								<label class="custom-control-label" for="address[field:address_book_id/]">[field:firstname/] [field:lastname/]</label>
							</span>
							<address class="mt-2">[field:address/]</address>
							<a class="btn btn-sm btn-outline-light waves-effect mr-2" href="[field:edit_link/]"> <i class="fas fa-pen"></i>&nbsp;&nbsp;[field:edit_text/]</a> 
						
						</div>
						{/openzc:account}
						<input type="hidden" name="action" value="submit"/>
						<div class="w-100">
						<button class="btn btn-light waves-effect waves-light float-right" type="submit"><i class="fas fa-redo-alt"></i>&nbsp;&nbsp;{openzc:define.BUTTON_UPDATE_ALT/}</button>
						</div>
						</form>
					</div>
				</div>
			</div>
			
			
			</div>
		</div>
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
    <script src="{openzc:field.assets/}libs/simplebar/simplebar.min.js"></script>
    <script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-validation.init.js"></script>
	<script src="{openzc:field.assets/}js/app.js"></script>
	<script src="{openzc:field.assets/}js/openzc.js"></script>
</body>
</html>