<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>
			{openzc:msg name="account_edit" type="error"}
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
                [field:text/]
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
			{/openzc:msg}
			<div class="row">
			<div class="col-xl-12 col-sm-12">
				<div class="card">
					<div class="card-body">
						<form action="/index.php?main_page=account_edit" method="post">
							
							<input type="hidden" name="action" value="process">
							{openzc:if ACCOUNT_GENDER=='true'}
							<div class="form-group row">
                                <label for="example-text-input" class="col-md-3 col-form-label">{openzc:define.ENTRY_GENDER/} <code>{openzc:define.ENTRY_GENDER_TEXT/}</code></label>
                                <div class="col-md-9">
									<div class="custom-control custom-radio custom-control-inline ">
										<input type="radio" id="MALE" name="gender"  value="m" class="custom-control-input" {openzc:if $male==true}checked=""{/openzc:if}>
										<label class="custom-control-label" for="MALE">{openzc:define.MALE/}</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline ">
										<input type="radio" id="FEMALE" name="gender" value="f" class="custom-control-input" {openzc:if $female==true}checked=""{/openzc:if}>
										<label class="custom-control-label" for="FEMALE">{openzc:define.FEMALE/}</label>
									</div>
                                </div>
                            </div>
							{/openzc:if}
							<div class="form-group row">
                                <label for="firstname" class="col-md-3 col-form-label">{openzc:define.ENTRY_FIRST_NAME/} <code>{openzc:define.ENTRY_FIRST_NAME_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="firstname" value="{openzc:account field='customers_firstname'/}" id="firstname">
                                </div>
                            </div>
							<div class="form-group row">
                                <label for="lastname" class="col-md-3 col-form-label">{openzc:define.ENTRY_LAST_NAME/} <code>{openzc:define.ENTRY_LAST_NAME_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="lastname" value="{openzc:account field='customers_lastname'/}" id="lastname">
                                </div>
                            </div>
							{openzc:if ACCOUNT_DOB == 'true'}
							<div class="form-group row">
                                <label for="dob" class="col-md-3 col-form-label">{openzc:define.ENTRY_DATE_OF_BIRTH/} <code>{openzc:define.ENTRY_DATE_OF_BIRTH_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="dob" value="{openzc:account field='customers_dob'/}" aria-describedby="option-format" placeholder="mm/dd/yyyy" id="dob">
                                </div>
                            </div>
							{/openzc:if}
							<div class="form-group row">
                                <label for="email_address" class="col-md-3 col-form-label">{openzc:define.ENTRY_EMAIL_ADDRESS/} <code>{openzc:define.ENTRY_EMAIL_ADDRESS_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="email" name="email_address" value="{openzc:account field='customers_email_address'/}" id="email_address">
                                </div>
                            </div>
							
							<div class="form-group row">
                                <label for="telephone" class="col-md-3 col-form-label">{openzc:define.ENTRY_TELEPHONE_NUMBER/} <code>{openzc:define.ENTRY_TELEPHONE_NUMBER_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="number" name="telephone" value="{openzc:account field='customers_telephone'/}" id="telephone">
                                </div>
                            </div>
							{openzc:if ACCOUNT_FAX_NUMBER=="true"}
							<div class="form-group row">
                                <label for="fax" class="col-md-3 col-form-label">{openzc:define.ENTRY_FAX_NUMBER/} <code>{openzc:define.ENTRY_FAX_NUMBER_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="number" name="fax" value="{openzc:account field='customers_fax'/}" id="fax">
                                </div>
                            </div>
							{/openzc:if}
							{openzc:if CUSTOMERS_REFERRAL_STATUS == 2 && $customers_referral == ''}
							<div class="form-group row">
                                <label for="customers_referral" class="col-md-3 col-form-label">{openzc:define.ENTRY_CUSTOMERS_REFERRAL/}</label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="customers_referral" value="{openzc:account field='customers_referral'/}" id="customers_referral">
                                </div>
                            </div>
							{/openzc:if}
							{openzc:if CUSTOMERS_REFERRAL_STATUS == 2 && $customers_referral != ''}
							<div class="form-group row">
                                <label for="customers_referral" class="col-md-3 col-form-label">{openzc:define.ENTRY_CUSTOMERS_REFERRAL/}</label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="customers_referral" value="{openzc:account field='customers_referral'/}" id="customers_referral">
                                </div>
                            </div>
							{/openzc:if}
							<div class="form-group row">
                                <label for="example-text-input" class="col-md-3 col-form-label">{openzc:define.ENTRY_EMAIL_PREFERENCE/}</label>
                                <div class="col-md-9">
									<div class="custom-control custom-radio custom-control-inline ">
										<input type="radio" id="HTML" name="email_format" value="HTML" class="custom-control-input" {openzc:if $email_pref_html}checked=""{/openzc:if}>
										<label class="custom-control-label" for="HTML">{openzc:define.ENTRY_EMAIL_HTML_DISPLAY/}</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline ">
										<input type="radio" id="TEXT" name="email_format" value="TEXT" class="custom-control-input" {openzc:if $email_pref_text}checked=""{/openzc:if}>
										<label class="custom-control-label" for="TEXT">{openzc:define.ENTRY_EMAIL_TEXT_DISPLAY/}</label>
									</div>
                                </div>
                            </div>
							<button class="btn btn-warning waves-effect waves-light" type="submit">{openzc:define.BUTTON_SUBMIT_ALT/}</button>
							<a href="{openzc:link name='FILENAME_ACCOUNT'/}" class="btn btn-outline-light waves-effect float-right"><i class="fas fa-reply"></i> &nbsp;&nbsp;{openzc:define.BUTTON_BACK_ALT/}</a>
						</form>
					</div>
				</div>
			</div>
			
			</div>
		</div>
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-validation.init.js"></script>
	
</body>
</html>