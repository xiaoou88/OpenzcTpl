<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}libs/select2/css/select2.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />

<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox">
	<div class="container">
	<div class="row mt-5">
		<div class="container">
			<h2 class="text-center">{openzc:define.HEADING_TITLE/}</h2>
			<h4 class="mt-4">{openzc:define.CATEGORY_PERSONAL/}</h4>
			<p>{openzc:var name="text_origin_login"/}</p>
			{openzc:msg name="create_account" type="error"}
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    [field:text/]
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
             {/openzc:msg}
			<form class="mb-4" action="/index.php?main_page=create_account" method="post">
				<input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
				<input type="hidden" name="action" value="process">
				<input type="hidden" name="email_pref_html" value="email_format">
				{openzc:if DISPLAY_PRIVACY_CONDITIONS=="true"}
				<!--Privacy Statement START-->
					<h3 class="card-title mt-4">{openzc:define.TABLE_HEADING_PRIVACY_CONDITIONS/}</h3>
					<p>{openzc:define.TEXT_PRIVACY_CONDITIONS_DESCRIPTION/}</p>
					<div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="privacy" value="1">
                            <label class="custom-control-label" for="privacy">{openzc:define.TEXT_PRIVACY_CONDITIONS_CONFIRM/}</label>
                        </div>
                    </div>
				<!--/Privacy Statement END-->
				{/openzc:if}
				{openzc:if ACCOUNT_COMPANY=="true"}
				<!--Company Details START-->
					<h4 class="mt-4">{openzc:define.CATEGORY_COMPANY/}</h4>
					<div class="form-group row">
                        <label for="account_company" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_COMPANY/} <code>{openzc:define.ENTRY_COMPANY_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="company" value="" id="account_company" {openzc:if ENTRY_COMPANY_TEXT}required{/openzc:if}>
                        </div>
                    </div>
				<!--Company Details END-->
				{/openzc:if}
				
			    <!--Address Details START-->
					<h4 class="mt-4">{openzc:define.TABLE_HEADING_ADDRESS_DETAILS/}</h4>
					{openzc:if ACCOUNT_GENDER=="true"}
						<div class="form-group row">
                            <label for="account_company" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_GENDER/} <code>{openzc:define.ENTRY_GENDER_TEXT/}</code></label>
                            <div class="custom-control custom-radio custom-control-inline ml-2">
                                <input type="radio" id="MALE" name="gender"  value="m" class="custom-control-input" {openzc:if ENTRY_GENDER_TEXT}required{/openzc:if}>
                                <label class="custom-control-label" for="MALE">{openzc:define.MALE/}</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="FEMALE" name="gender" value="f" class="custom-control-input" {openzc:if ENTRY_GENDER_TEXT}required{/openzc:if}>
                                <label class="custom-control-label" for="FEMALE">{openzc:define.FEMALE/}</label>
                            </div>
                        </div>
					{/openzc:if}
					
					<!--firstname-->
					<div class="form-group row">
                        <label for="firstname" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_FIRST_NAME/} <code>{openzc:define.ENTRY_FIRST_NAME_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="firstname" value="" id="firstname" {openzc:if ENTRY_FIRST_NAME_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					
					<!--lastname-->
					<div class="form-group row">
                        <label for="lastname" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_LAST_NAME/} <code>{openzc:define.ENTRY_LAST_NAME_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="lastname" value="" id="lastname" {openzc:if ENTRY_LAST_NAME_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					
					<!--street_address-->
					<div class="form-group row">
                        <label for="street_address" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_STREET_ADDRESS/} <code>{openzc:define.ENTRY_STREET_ADDRESS_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="street_address" value="" id="street_address" {openzc:if ENTRY_STREET_ADDRESS_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					
					<!--street_address-->
					{openzc:if ACCOUNT_SUBURB=="true"}
					<div class="form-group row">
                        <label for="suburb" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_SUBURB/} <code>{openzc:define.ENTRY_SUBURB_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="suburb" value="" id="suburb" {openzc:if ENTRY_SUBURB_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					{/openzc:if}
					
					<!--city-->
					<div class="form-group row">
                        <label for="city" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_CITY/} <code>{openzc:define.ENTRY_CITY_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="city" value="" id="city" {openzc:if ENTRY_CITY_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					
					<!--state-->
					<div id="res_states">
					{openzc:ajax filename="res_states"}
					{openzc:if ACCOUNT_STATE=="true"}
					{openzc:php}
						if(count($GLOBALS['states'])==1){$flag_show_pulldown_states=false;}
					{/openzc:php}
					<div class="form-group row">
                        <label for="state" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_STATE/} <code>{openzc:define.ENTRY_STATE_TEXT/}</code></label>
                        <div class="col-md-10">
							{openzc:if $flag_show_pulldown_states==true}
							<select class="form-control select2" name="zone_id">
								{openzc:var name="states"}
								<option value="[field:id/]">[field:text/]</option>
								{/openzc:var}
							</select>
							{else}
                            <input class="form-control" type="text" name="state" value="" id="state" {openzc:if ENTRY_STATE_TEXT}required{/openzc:if}>
							{/openzc:if}
                        </div>
                    </div>
					{/openzc:if}
					{/openzc:ajax}
					</div>
					<!--Post/Zip Code-->
					<div class="form-group row">
                        <label for="postcode" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_POST_CODE/} <code>{openzc:define.ENTRY_POST_CODE_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="postcode" value="" id="postcode" {openzc:if ENTRY_POST_CODE_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					
					<!--country-->
					<div class="form-group row">
                        <label for="country" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_COUNTRY/} <code>{openzc:define.ENTRY_COUNTRY_TEXT/}</code></label>
                        <div class="col-md-10">
							{openzc:if $flag_show_pulldown_states==true}
                            <select class="form-control select2 openzc-select" data-action="getState" data-reload="res_states" name="zone_country_id">
							{else}
							<select class="form-control select2" name="zone_country_id">
							{/openzc:if}
								{openzc:var name="countries"}
								<option value="[field:id/]">[field:text/]</option>
								{/openzc:var}
							</select>
                        </div>
                    </div>
				<!--Address Details END-->
				
				<!--Additional Contact Details START-->
					<h4 class="mt-4">{openzc:define.TABLE_HEADING_PHONE_FAX_DETAILS/}</h4>
					
					<!--telephone_number-->
					<div class="form-group row">
                        <label for="telephone_number" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_TELEPHONE_NUMBER/} <code>{openzc:define.ENTRY_TELEPHONE_NUMBER_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="telephone" value="" id="telephone_number" {openzc:if ENTRY_TELEPHONE_NUMBER_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					
					<!--Fax Number-->
					{openzc:if ACCOUNT_FAX_NUMBER=="true"}
					<div class="form-group row">
                        <label for="fax_number" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_FAX_NUMBER/} <code>{openzc:define.ENTRY_FAX_NUMBER_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="fax" value="" id="fax_number" {openzc:if ENTRY_FAX_NUMBER_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					{/openzc:if}
				<!--Additional Contact Details END-->
				
				<!--Verify Your Age START-->
					{openzc:if ACCOUNT_DOB=="true"}
					<h4 class="mt-4">{openzc:define.TABLE_HEADING_DATE_OF_BIRTH/}</h4>
					<div class="form-group row">
						<label for="fax_number" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_DATE_OF_BIRTH/} <code>{openzc:define.ENTRY_DATE_OF_BIRTH_TEXT/}</code></label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="dob" value="" aria-describedby="option-format" placeholder="mm/dd/yyyy" {openzc:if ENTRY_DATE_OF_BIRTH_TEXT}required{/openzc:if}>
						</div>
					</div>
					{/openzc:if}
				<!--Verify Your Age END-->
				
				<!--Login Details START-->
					<h4 class="mt-4">{openzc:define.TABLE_HEADING_LOGIN_DETAILS/}</h4>
					
					<!--Email Address-->
					<div class="form-group row">
                        <label for="email_address" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_EMAIL_ADDRESS/} <code>{openzc:define.ENTRY_EMAIL_ADDRESS_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="email" name="email_address" value="" id="email_address" {openzc:if ENTRY_EMAIL_ADDRESS_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					
					<!--Password-->
					<div class="form-group row">
                        <label for="password" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_PASSWORD/} <code>{openzc:define.ENTRY_PASSWORD_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="password" name="password" value="" id="password" {openzc:if ENTRY_PASSWORD_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="confirmation" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_PASSWORD_CONFIRMATION/} <code>{openzc:define.ENTRY_PASSWORD_CONFIRMATION_TEXT/}</code></label>
                        <div class="col-md-10">
                            <input class="form-control" type="password" name="confirmation" value="" id="confirmation" {openzc:if ENTRY_PASSWORD_CONFIRMATION_TEXT}required{/openzc:if}>
                        </div>
                    </div>
					
				
				<!--Login Details END-->
				
				<!--Newsletter and Email Details START-->
					<h4 class="mt-4">{openzc:define.ENTRY_EMAIL_PREFERENCE/}</h4>
					
					<!--Newsletter-->
					{openzc:if ACCOUNT_NEWSLETTER_STATUS=="1"}
					<p>{openzc:define.ENTRY_NEWSLETTER/}</p>
					<!--Email format-->
					<div class="form-group ">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="HTML" name="email_format"  value="HTML" class="custom-control-input" required>
                            <label class="custom-control-label" for="HTML">{openzc:define.ENTRY_EMAIL_HTML_DISPLAY/}</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="TEXT" name="email_format" value="TEXT" class="custom-control-input" required>
							<label class="custom-control-label" for="TEXT">{openzc:define.ENTRY_EMAIL_TEXT_DISPLAY/}</label>
                        </div>
                    </div>
					{/openzc:if}
					
					<!--/Newsletter and Email Details END-->
					{openzc:if CUSTOMERS_REFERRAL_STATUS=="2"}
						<h4 class="mt-4">{openzc:define.TABLE_HEADING_REFERRAL_DETAILS/}</h4>
						<div class="form-group row">
							<label for="customers_referral" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_CUSTOMERS_REFERRAL/}</label>
							<div class="col-md-10">
								<input class="form-control" type="text" name="customers_referral" value="" id="customers_referral">
							</div>
						</div>
					{/openzc:if}
					<!--Newsletter and Email Details END-->
				<button class="btn btn-lg btn-warning waves-effect waves-light" type="submit">{openzc:define.BUTTON_SUBMIT_ALT/}</button>
			</form>
		</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}js/openzc.js"></script>
</body>
</html>