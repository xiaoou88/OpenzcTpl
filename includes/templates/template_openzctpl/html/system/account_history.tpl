<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>
			<div class="row">
			<div class="col-xl-12 col-sm-12">
				<div class="card">
					<div class="card-body">
						{openzc:if ORDERS_COUNT>0}
						<div class="table-responsive">
                            <table class="table table-hover mb-0">
							<thead>
                                <tr>
                                    <th>Order #</th>
									<th>Date</th>
									<th>Order Total</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								{openzc:account item="orders"}
								<tr>
									<th class="table-row" scope="row">[field:orders_id/]</th>
									<td class="table-row">[field:date_purchased/]</td>
									<td class="inline-block">[field:order_total/]</td>
									<td class="inline-block">[field:orders_status_name/]</td>
									<td class="inline-block">
									<a class="text-reset waves-effect" href="[field:order_link/]"><i class="dripicons-clipboard"></i> View Order</a>
									</td>
								</tr>
								{/openzc:account}
								
							</tbody>
							</table>
						</div>
						{else}
						<div class="alert alert-warning" role="alert">
							<i class="fas fa-exclamation-triangle"></i> You have placed no orders.
                        </div>
						{/openzc:if}
					</div>
				</div>
			</div>
			
			</div>
		</div>
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-validation.init.js"></script>
	
</body>
</html>