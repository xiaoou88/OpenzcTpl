<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >

	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>
			{openzc:msg name="account" type="success"}
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					[field:text/]
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div>
             {/openzc:msg}
             {openzc:msg name="account" type="warning"}
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
					[field:text/]
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div>
             {/openzc:msg}
			<div class="row">
			<div class="col-xl-6 col-sm-6">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title mb-3"><i class="fas fa-address-card"></i> Contact Information</h4>
						<p>{openzc:account field='customers_firstname'/} {openzc:account field='customers_lastname'/}</p>
						<p>{openzc:account field='customers_email_address'/}</p>
						<p>{openzc:account item="nav" field="edit"}<a class="btn btn-outline-light waves-effect" href="[field:link/]"><i class="fas fa-pen"></i> &nbsp;Edit</a>{/openzc:account}</p>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-sm-6">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title mb-3"><i class="fas fa-rss-square"></i> Notifications Newsletters</h4>
						{openzc:account}
							<p>
							{openzc:if $field['customers_newsletter']}
								You are subscribed to "General Subscription".
							{else}
								You aren't subscribed to our newsletter.
							{/openzc:if}
							</p>
						{/openzc:account}
						<p>{openzc:account item="nav" field="notice_news"}<a class="btn btn-outline-light waves-effect" href="[field:link/]"><i class="fas fa-pen"></i> &nbsp;Edit</a>{/openzc:account}</p>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-sm-6">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title mb-5 d-block">
							<i class="fas fa-map-marker-alt"></i> Address Book 
							{openzc:account item="nav" field="address"}
								<a class="h6 btn-outline-light waves-effect float-right text-reset" href="[field:link/]"><i class="mdi mdi-cog-outline"></i>&nbsp;Manage Addresses</a>
							{/openzc:account}
						</h4>
						{openzc:account item="address" default=true}
							{openzc:if $field['status']=="off"}
							You have not set a default billing address.
							{else}
							<p class="font-weight-bold">Default Billing Address</p>
							<address>[field:address/]</address>
							{/openzc:if}
							<p class="mt-2"><a class="btn btn-outline-light waves-effect " href="[field:edit_link/]"><i class="fas fa-pen"></i>&nbsp;Edit Address</a></p>
						{/openzc:account}
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-sm-6">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title mb-3"><i class="fas fa-clipboard-list"></i> Order History</h4>
						<p>
						{openzc:if ORDERS_COUNT>0}
						See Your Order History.<br/>
						{openzc:account item="nav" field="orders"}
						<a class="btn btn-outline-light waves-effect mt-3" href="[field:link/]"><i class="fas fa-search"></i>&nbsp;[field:text/]</a>
						{/openzc:account}
						{else}
					    <span class="text-muted">You have not yet made any purchases.</span>
						{/openzc:if}
						</p>
						
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-validation.init.js"></script>
	
</body>
</html>