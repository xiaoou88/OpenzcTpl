<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>
	body #openzcBox {background-color:#fff;}
	#openzcBox #cart tr > th{font-weight: 300 !important;border-top:0}
</style>
</head>
<body>
<main id="openzcBox">
	<div class="container mt-4 ">
		<h3>Order Confirmation</h3>
		
			<div class="col-xs-12 col-sm-12 col-md-8 col-xl-8">
				<div class="card">
                    <div class="card-body row">
						<div class="col-xl-6 col-md-6 mb-3">
							<h3 class="card-title mt-0 mb-3">{openzc:define.HEADING_BILLING_ADDRESS/}</h3>
							<address>{openzc:echo} zen_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />');{/openzc:echo}</address>
							{openzc:if !$flagDisablePaymentAddressChange}
							<a class="btn btn-outline-light waves-effect" href="{openzc:link name='FILENAME_CHECKOUT_PAYMENT'/}"><i class="bx bx-highlight"></i> &nbsp;&nbsp;{openzc:define.BUTTON_EDIT_SMALL_ALT/}</a>
							{/openzc:if}
						</div>
						{openzc:if $_SESSION['sendto'] != false}
						<div class="col-xl-6 col-md-6 mb-3">
							<h3 class="card-title mt-0 mb-3">{openzc:define.HEADING_DELIVERY_ADDRESS/}</h3>
							<address>{openzc:echo}zen_address_format($order->delivery['format_id'], $order->delivery, 1, ' ', '<br />');{/openzc:echo}</address>
							{openzc:if !$flagDisablePaymentAddressChange}
							<a class="btn btn-outline-light waves-effect" href="{openzc:var name='editShippingButtonLink'/}"><i class="bx bx-highlight"></i> &nbsp;&nbsp;{openzc:define.BUTTON_EDIT_SMALL_ALT/}</a>
							{/openzc:if}
						</div>
						{/openzc:if}
						
                    </div>
                </div>
				<div class="card">
                    <div class="card-body row">
					<div class="col-xl-6 col-md-6">
							<h3 class="card-title mt-0 mb-3">{openzc:define.HEADING_ORDER_COMMENTS/}</h3>
							<div class="card-text">
								{openzc:echo}(empty($order->info['comments']) ? NO_COMMENTS_TEXT : nl2br(zen_output_string_protected($order->info['comments'])) . zen_draw_hidden_field('comments', $order->info['comments']));{/openzc:echo}
							</div>
							<a class="btn btn-outline-light waves-effect mt-2" href="{openzc:link name='FILENAME_CHECKOUT_PAYMENT'/}"><i class="bx bx-highlight"></i> &nbsp;&nbsp;{openzc:define.BUTTON_EDIT_SMALL_ALT/}</a>
						</div>
						{openzc:if $_SESSION['sendto'] != false}
						<div class="col-xl-6 col-md-6">
							<div class="row">
							<div class="col-xl-6 col-md-6 border-left">
							{openzc:if $order->info['shipping_method']}
							<h3 class="card-title mt-0 mb-3">{openzc:define.HEADING_SHIPPING_METHOD/}</h3>
							<div class="card-text">{openzc:echo}$order->info['shipping_method'];{/openzc:echo}</div>
							{/openzc:if}
							</div>
							<div class="col-xl-6 col-md-6 border-left">
							<h3 class="card-title mt-0 mb-3">{openzc:define.HEADING_PAYMENT_METHOD/}</h3>
							<div class="card-text">{openzc:echo} $GLOBALS[$_SESSION['payment']]->title;{/openzc:echo}</div>
							</div>
							</div>
						</div>
						{/openzc:if}
					</div>
				</div>
				<div class="card">
                    <div class="card-body">
						<h3 class="card-title mt-0 mb-3">{openzc:define.HEADING_PRODUCTS/}</h3>
						<div class="table-responsive">
							{openzc:if $_SESSION['cart']->total > 0}
								<table id="cart" class="table ">
									<thead >
										<tr>
											<th class="w-50">{openzc:define.TABLE_HEADING_PRODUCTS/}</th>
											<th class="w-25">{openzc:define.TABLE_HEADING_QUANTITY/}</th>
											
											<th>{openzc:define.TABLE_HEADING_TOTAL/}</th>
										
										</tr>
									</thead>
									<tbody >
										{openzc:cart ajax="cartlist" imgsizer="90,90"}
										<tr class="pt-4">
											<td>
												<img src="[field:products_image/]" class="float-left mr-4"/>
												<p class="mt-2 mb-2">
												<a class="h6" href="[field:linkProductsName/]" >[field:products_name/]</a>
												</p>
												<p class="text-black-50">
												{openzc:loopson item="attr"}
													<span class="font-weight-bold h7">[field:products_options_name/] :</span>&nbsp;&nbsp;<span class="value h7">[field:products_options_values_name/]</span></br>
												{/openzc:loopson}
												</p>
											</td>
											<td class="">
												<div>
													× [field:products_qty/]
												</div>
											</td>
											
											<td><p class="mt-2">[field:productsPrice/]</p></td>
										</tr>
										{/openzc:cart}
									</tbody>
									{openzc:if MODULE_ORDER_TOTAL_INSTALLED}
									{openzc:var name="order_total"}
										<tr>
											
                                            <td colspan="2" class="text-right">[field:title/]</td>
                                            <td>[field:text/]</td>
                                        </tr>
									{/openzc:var}
									{/openzc:if}
								</table>
							{else}
								<div class="alert alert-warning" role="alert">
									<i class="mdi mdi-cart-off"></i> {openzc:define.TEXT_CART_EMPTY/}
								</div>
							{/openzc:if}
						</div>
						<form action="{openzc:link name='FILENAME_CHECKOUT_PROCESS'/}" method="post">
							<button class="btn btn-warning waves-effect float-right" type="submit">Confirm Order &nbsp;&nbsp;<i class="fas fa-angle-right"></i></button>
						</form>
                    </div>
                </div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-xl-8">
				
			<div class="row mt-4">
			
			<div class="col-xl-6 col-md-6">
				
			</div>
		</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}libs/select2/js/select2.min.js"></script>
	<script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
	<script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-advanced.init.js"></script>
	
	<script src="{openzc:field.assets/}js/app.js"></script>
	<script src="{openzc:field.assets/}js/openzc.js"></script>
</body>
</html>