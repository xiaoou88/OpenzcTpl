<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->

<link href="{openzc:field.assets/}libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}libs/@chenfengyuan/datepicker/datepicker.min.css" rel="stylesheet" type="text/css" >
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
		<div class="row mt-5">
			<h4 class="mb-4 col-12">{openzc:define.HEADING_TITLE/} </h4>
			<p class="col-12">{openzc:define.TEXT_MAIN/}</p>
		</div>
	</div>

</main>

<!-- JAVASCRIPT -->
        <script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
       
		<script src="{openzc:field.assets/}js/openzc.js"></script>
       
</body>
</html>