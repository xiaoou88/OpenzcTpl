<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.CART_SHIPPING_OPTIONS/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>
	body #openzcBox {background-color:#fff;}
	#openzcBox #cart tr > th{font-weight: 300 !important;border-top:0}
</style>
</head>
<body>
<main id="openzcBox">
	<div class="container ">
		<h5>{openzc:define.CART_SHIPPING_OPTIONS/}</h5>
		<div class="row mt-3">
		{openzc:include filename="system/popup_shipping_estimator_box.tpl"/}
		</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}libs/select2/js/select2.min.js"></script>
	<script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
	<script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-advanced.init.js"></script>
	
	<script src="{openzc:field.assets/}js/app.js"></script>
	<script src="{openzc:field.assets/}js/openzc.js"></script>
</body>
</html>