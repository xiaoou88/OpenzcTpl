<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>
			<div class="row">
			<div class="col-xl-12 col-sm-12">
				<div class="card">
					<div class="card-body">
						<span class="h4 d-flex">#{openzc:account item="orders" field="orders_id"/}</span>
						<span class="h6 d-flex">{openzc:define.HEADING_ORDER_DATE/} {openzc:account item="orders" field="date_purchased"/}</span>
						<span class="h5 text-center mt-4 d-flex justify-content-center">{openzc:define.HEADING_TITLE/}{openzc:define.ORDER_HEADING_DIVIDER/}#{openzc:account item="orders" field="orders_id"/}</span>
						<div class="table-responsive">
                            <table class="table table-hover mb-0">
							<thead>
                                <tr>
                                    <th>{openzc:define.HEADING_QUANTITY/}</th>
									<th>{openzc:define.HEADING_PRODUCTS/}</th>
									{openzc:if sizeof($order->info['tax_groups'])>1}
									<th>{openzc:define.HEADING_TAX/}</th>
									{/openzc:if}
									<th>{openzc:define.HEADING_TOTAL/}</th>
								</tr>
							</thead>
							<tbody>
								{openzc:account item="orders" field="products"}
								<tr>
									<td>[field:qty/] {openzc:define.QUANTITY_SUFFIX/}</td>
									<td>
									<span class="h6">[field:name/]</span>
									<ul class="list-group list-group-flush">
									{openzc:loopson field="attr"}
										<li class="list-group-item">
										[field:option/] : [field:value/]
										</li>
									{/openzc:loopson}
									</ul>
									</td>
									{openzc:if sizeof($order->info['tax_groups'])>1}
									<td>[field:tax/]</td>
									{/openzc:if}
									<td>[field:final_price/]</td>
									<td></td>
								</tr>
								{/openzc:account}
								<tr><th colspan="4"></th></tr>
								{openzc:if sizeof($order->info['tax_groups'])>1}
									{openzc:account item="orders" field="totals"}
									<tr class="table-borderless table-sm"><th colspan="4" class="text-right">[field:title/] [field:text/]</th></tr>
									{/openzc:account}
								{else}
									{openzc:account item="orders" field="totals"}
									<tr class="table-borderless table-sm"><th colspan="3" class="text-right">[field:title/] [field:text/]</th></tr>
									{/openzc:account}
								{/openzc:if}
							</tbody>
							</table>
						</div>
						<span class="h5 text-center mt-4 d-flex justify-content-center">{openzc:define.HEADING_ORDER_HISTORY/}</span>
						<div class="table-responsive">
                            <table class="table table-hover mb-0">
							<thead>
                                <tr>
                                    <th>{openzc:define.TABLE_HEADING_STATUS_DATE/}</th>
									<th>{openzc:define.TABLE_HEADING_STATUS_ORDER_STATUS/}</th>
									<th>{openzc:define.TABLE_HEADING_STATUS_COMMENTS/}</th>
								</tr>
							</thead>
							<tbody>
								{openzc:account item="orders" field="status_history"}
								<tr>
									<td>[field:date_added/]</td>
									<td>[field:orders_status_name/]</td>
									<td>[field:comments/]</td>
								</tr>
								{/openzc:account}
							</tbody>
							</table>
						</div>
						<div class="row mt-5">
							<div class="col-xl-6 col-sm-6">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title mb-3">{openzc:define.HEADING_DELIVERY_ADDRESS/}</h4>
										<p>{openzc:account item="orders" field="delivery"/}</p>
									</div>
								</div>
							</div>
						
							<div class="col-xl-6 col-sm-6">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title mb-3">{openzc:define.HEADING_BILLING_ADDRESS/}</h4>
										<p>{openzc:account item="orders" field="billing"/}</p>
									</div>
								</div>
							</div>
						
							<div class="col-xl-6 col-sm-6">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title mb-3">{openzc:define.HEADING_SHIPPING_METHOD/}</h4>
										<p>{openzc:account item="orders" field="shipping_method"/}</p>
									</div>
								</div>
							</div>
						
							<div class="col-xl-6 col-sm-6">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title mb-3">{openzc:define.HEADING_PAYMENT_METHOD/}</h4>
										<p>{openzc:account item="orders" field="payment_method"/}</p>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
			</div>
		</div>
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-validation.init.js"></script>
	
</body>
</html>