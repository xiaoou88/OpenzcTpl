

			
			{openzc:if $_SESSION['cart']->count_contents()}
				{openzc:if !empty($totalsDisplay)}
				<div class="col-xl-12 col-md-12">
				<div class="alert alert-info mb-0" role="alert">
                    {openzc:var name="totalsDisplay"/}
                </div>
				</div>
				{/openzc:if}
				<div class="col-xl-6 col-md-6">
				{openzc:if $_GET['main_page']==FILENAME_POPUP_SHIPPING_ESTIMATOR}
					<form id="autoSubmit" class="mt-3" action="{openzc:link name='FILENAME_POPUP_SHIPPING_ESTIMATOR'/}" method="post">
				{/openzc:if}
				
				{openzc:if $_GET['main_page']==FILENAME_SHOPPING_CART}
					<form id="autoSubmit" class="mt-3" action="{openzc:link name='FILENAME_SHOPPING_CART'/}" method="post">
				{/openzc:if}
					<input type="hidden" name="scid" value="item_item">
					{openzc:if $_SESSION['customer_id']}
						{openzc:if $addresses->RecordCount() > 1}
						<div class="form-group row">
							<label for="address_id" class="col-md-3 col-form-label">{openzc:define.CART_SHIPPING_METHOD_ADDRESS/} </label>
							<div class="col-md-9">
								<select class="form-control openzc-select" data-action="autoSubmit" data-form="autoSubmit" name="address_id">
									{openzc:var name="addresses_array"}
									<option value="[field:id/]" {openzc:if $selected_address==$field['id']}selected=""{/openzc:if}>[field:text/]</option>
									{/openzc:var}
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="format_id" class="col-md-3 col-form-label">{openzc:define.CART_SHIPPING_METHOD_TO/} </label>
							<div class="col-md-9">
								<p>
									{openzc:php}echo zen_address_format($order->delivery['format_id'], $order->delivery, 1, ' ', '<br />');{/openzc:php}
								</p>
							</div>
						</div>
						{/openzc:if}
						
					{/openzc:if}
					{openzc:if !$_SESSION['customer_id']}
						{openzc:if $_SESSION['cart']->get_content_type() != 'virtual'}
							<div class="form-group row">
								<label for="zone_country_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_COUNTRY/} </label>
								<div class="col-md-9">
									<select class="form-control openzc-select" data-action="getState" data-reload="shipping_states" name="zone_country_id">
										{openzc:var name="countries"}
										<option value="[field:id/]">[field:text/]</option>
										{/openzc:var}
									</select>
								</div>
							</div>
							
							<div id="shipping_states">
							
							{openzc:ajax filename="shipping_states" }
							<div class="form-group row">
								<label for="zone_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_STATE/} </label>
								<div class="col-md-9">
									{openzc:if count($states)>1}
									<select class="form-control" name="zone_id">
										{openzc:var name="states"}
										<option value="[field:id/]">[field:text/]</option>
										{/openzc:var}
									</select>
								
									{/openzc:if}
								</div>
							</div>
							</div>
							
							{/openzc:ajax}
							{openzc:if CART_SHIPPING_METHOD_ZIP_REQUIRED == "true"}
							<div class="form-group row">
								<label for="postcode" class="col-md-3 col-form-label text-left">{openzc:define.ENTRY_POST_CODE/}</label>
								<div class="col-md-9">
									<input class="form-control" type="text" name="zip_code" value="" size="7" >
								</div>
							</div>
							{/openzc:if}
						{else}
						<div class="alert alert-info mb-0" role="alert">
							{openzc:define.CART_SHIPPING_METHOD_FREE_TEXT/} {openzc:define.CART_SHIPPING_METHOD_ALL_DOWNLOADS/}
						</div>
						{/openzc:if}
						{openzc:if $free_shipping==1}
							{openzc:php}sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER));{/openzc:php}
						{/openzc:if}
						<button class="btn btn-outline-light waves-effect" type="submit"><i class="fas fa-sync-alt"></i> &nbsp;{openzc:define.BUTTON_UPDATE_ALT/}</button>
					{/openzc:if}
					
				</form>
				</div>
			{/openzc:if}
			<div class="col-xl-6 col-md-6">
				<div class="table-responsive mt-3">
					<table class="table mb-0">
						<thead class="thead-light">
							{openzc:if $_SESSION['customer_id']<1}
							<tr class="table-success">
								<td colspan="2" class="text-center">
									{openzc:define.CART_SHIPPING_QUOTE_CRITERIA/}<br/>
									{openzc:php}echo '<span class="font-weight-bold">' . zen_get_zone_name($selected_country, $state_zone_id, '') . ($selectedState != '' ? ' ' . $selectedState : '') . ' ' . $order->delivery['postcode'] . ' ' . zen_get_country_name($order->delivery['country_id']) . '</span>';{/openzc:php}
								</td>
							</tr>
							{/openzc:if}
							<tr><th>{openzc:define.CART_SHIPPING_METHOD_TEXT/}</th><th>{openzc:define.CART_SHIPPING_METHOD_RATES/}</th></tr>
						</thead>
						<tbody>
							{openzc:var name="quotes"}
								{openzc:php}$thisquoteid = $field['id'].'_'.$field['methods'][0]['id'];{/openzc:php}
								{openzc:if sizeof($field['methods'])==1}
								<tr {openzc:if $selected_shipping['id'] == $thisquoteid}class="table-light"{/openzc:if}>
									{openzc:if $field['error']}
										<td colspan="2">[field:module/]&nbsp;([field:error/])</td>
									{else}
										{openzc:if $selected_shipping['id'] == $thisquoteid}
										<td class="font-weight-bold">
											[field:module/]  ({openzc:echo}$field['methods'][0]['title'];{/openzc:echo})
										</td>
										<td class="font-weight-bold">
											{openzc:echo}
												$currencies->format(zen_add_tax($field['methods'][0]['cost'], $field['tax']));
											{/openzc:echo}
										</td>
										{else}
										<td>
											[field:module/]  ({openzc:echo}$field['methods'][0]['title'];{/openzc:echo})
										</td>
										<td>
											{openzc:echo}
												$currencies->format(zen_add_tax($field['methods'][0]['cost'], $field['tax']));
											{/openzc:echo}
										</td>
										{/openzc:if}
									{/openzc:if}
								</tr>
								{else}
						
									{openzc:php}
										foreach($field['methods'] as $k => $v){
											$thisquoteid = $field['id'].'_'.$v['id'];
											if($selected_shipping['id'] == $thisquoteid){
												echo '<tr class="table-light">';
											}else{
												echo "<tr>";
											}
											if($field['error']){
												echo '<td colspan="2">'.$field['module']."&nbsp;(".$field['error'].")</td>";
											}else{
												if($selected_shipping['id'] == $thisquoteid){
													echo '<td class="font-weight-bold">'.$field['module']."&nbsp;(".$v['title'].")</td>";
													echo '<td class="font-weight-bold">'.$currencies->format(zen_add_tax($v['cost'], $field['tax']))."</td>";
												}else{
													echo '<td>'.$field['module']."&nbsp;(".$v['title'].")</td>";
													echo '<td>'.$currencies->format(zen_add_tax($v['cost'], $field['tax']))."</td>";
												}
											}
											echo "</tr>";
										}
									{/openzc:php}
								
								{/openzc:if}
								
							{/openzc:var}
						</tbody>
					</table>
				</div>
			
			
			
	</div>