<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>
			{openzc:msg name="addressbook" type="success"}
				<div class="alert alert-success">[field:text/]</div>
             {/openzc:msg}
             {openzc:msg name="addressbook" type="warning"}
				<div class="alert alert-warning">[field:text/]</div>
             {/openzc:msg}
			<div class="row">
			<div class="col-xl-12 col-sm-12">
				<div class="card">
					<div class="card-body">
						
						<div class="row">
							<div class="col-xl-6 col-sm-6 mb-3">
							<h4 class="card-title mb-3"><i class="fas fa-flag"></i> {openzc:define.PRIMARY_ADDRESS_TITLE/}</h4>
							{openzc:account item="address" default="true"}
							<address>[field:address/]</address>
							<a class="btn btn-outline-light waves-effect" href="[field:edit_link/]"> <i class="fas fa-pen"></i>&nbsp;&nbsp;[field:edit_text/]</a> 
							{/openzc:account}
							</div>
							<p class="col-xl-6 col-sm-6 bg-light p-3">{openzc:define.PRIMARY_ADDRESS_DESCRIPTION/}</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-12 col-sm-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title mb-3"><i class="fas fa-map-marker-alt"></i> {openzc:define.ADDRESS_BOOK_TITLE/}</h4>
						<div class="alert alert-info" role="alert">
						{openzc:php}echo sprintf(TEXT_MAXIMUM_ENTRIES, MAX_ADDRESS_BOOK_ENTRIES);{/openzc:php}
						</div>
						<div class="row">
						{openzc:account item="address"}
						<div class="col-xl-6 col-sm-6 mb-3">
							<span class="h5 font-weight-bold">
							[field:firstname/] [field:lastname/]
							</span>
							{openzc:if $field['status']=='active'}(primary address){/openzc:if}
							<address class="mt-2">[field:address/]</address>
							<a class="btn btn-outline-light waves-effect mr-2" href="[field:edit_link/]"> <i class="fas fa-pen"></i>&nbsp;&nbsp;[field:edit_text/]</a> 
							{openzc:if $field['status']!='active'}
							<a class="btn btn-outline-light waves-effect" href="[field:delete_link/]"> <i class="fas fa-trash-alt"></i>&nbsp;&nbsp;[field:delete_text/]</a> 
							{/openzc:if}
						</div>
						{/openzc:account}
						</div>
						
					</div>
					
				</div>
				<a class="btn btn-outline-light waves-effect float-right" href="{openzc:link name='FILENAME_ADDRESS_BOOK_PROCESS'/}"> <i class="fas fa-plus"></i>&nbsp;&nbsp;{openzc:define.BUTTON_ADD_ADDRESS_ALT/}</a> 
			</div>
			
			
			</div>
		</div>
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
    <script src="{openzc:field.assets/}libs/simplebar/simplebar.min.js"></script>
    <script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-validation.init.js"></script>
	<script src="{openzc:field.assets/}js/app.js"></script>
</body>
</html>