<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}libs/@chenfengyuan/datepicker/datepicker.min.css" rel="stylesheet" type="text/css" >
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>
			{openzc:msg name="addressbook" type="error"}
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    [field:text/]
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
             {/openzc:msg}
             
			<div class="row">
			<div class="col-xl-12 col-sm-12">
				
				<div class="card">
					
					<div class="card-body">
						<p>{openzc:define.MY_NOTIFICATIONS_DESCRIPTION/}</p>
						<form name="{openzc:define.FILENAME_ACCOUNT_NOTIFICATIONS/}" action="{openzc:link name='FILENAME_ACCOUNT_NOTIFICATIONS'/}" method="post">
							<input type="hidden" name="action" value="process"/>
							<input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
							<h5 class="mt-5"><i class="fas fa-caret-square-right"></i> {openzc:define.GLOBAL_NOTIFICATIONS_TITLE/} </h5>
							<div class="form-check custom-checkbox">
								{openzc:if $global->fields['global_product_notifications'] == '1'}
								<input type="checkbox" class="form-check-input" name="product_global" id="product_global" value="1" checked="">
								{else}
								<input type="checkbox" class="form-check-input" name="product_global" id="product_global" value="1">
								{/openzc:if}
								<label class="form-check-label" for="product_global">{openzc:define.GLOBAL_NOTIFICATIONS_DESCRIPTION/}</label>
							</div>
						
							{openzc:if $flag_global_notifications != '1'}
								<h5 class="mt-5"><i class="fas fa-caret-square-right"></i> {openzc:define.NOTIFICATIONS_TITLE/} </h5>
								{openzc:if $flag_products_check}
									<p>{openzc:define.NOTIFICATIONS_DESCRIPTION/}</p>
									{openzc:var name="notificationsArray"}
										<div class="form-check mt-1">
										<input type="checkbox" class="form-check-input" name="notify[]" id="notify[field:products_id/]" value="[field:products_id/]" checked="checked">
										<label class="form-check-label" for="notify[field:products_id/]">[field:products_name/]</label>
										</div>
									{/openzc:var}
								{else}
								<p> {openzc:define.NOTIFICATIONS_NON_EXISTING/} </p>
								{/openzc:if}
							
							{/openzc:if}
							<button class="btn btn-warning waves-effect waves-light mt-3" type="submit">{openzc:define.BUTTON_SUBMIT_ALT/}</button>
							<a href="{openzc:link name='FILENAME_BACK'/}" class="btn btn-outline-light waves-effect float-right mt-3"><i class="fas fa-reply"></i>&nbsp;&nbsp;{openzc:define.BUTTON_BACK_ALT/}</a>
						</form>
					</div>
				</div>
			</div>
			
			</div>
		</div>
	</div>
	</div>
</main>

<!-- JAVASCRIPT -->
        <script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
        <script src="{openzc:field.assets/}libs/simplebar/simplebar.min.js"></script>
        <script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>

        <script src="{openzc:field.assets/}libs/select2/js/select2.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
        <script src="{openzc:field.assets/}libs/@chenfengyuan/datepicker/datepicker.min.js"></script>

        <!-- form advanced init -->
        <script src="{openzc:field.assets/}js/pages/form-advanced.init.js"></script>

        <script src="{openzc:field.assets/}js/app.js"></script>
</body>
</html>