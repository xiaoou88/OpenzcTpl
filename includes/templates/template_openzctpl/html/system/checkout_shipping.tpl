<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>
	body #openzcBox {background-color:#fff;}
	#openzcBox .nav .nav-item,#openzcBox .nav,#openzcBox .nav .nav-link.active{border-color:#fff;font-weight:bold;}
	#openzcBox .nav .nav-link:hover,#openzcBox .nav .nav-link.active{border-color:#fff;}
</style>
</head>
<body>
<main id="openzcBox">
	<div class="container mt-4 w1200">
			
			<div class="row mt-4">
			<div class="col-xl-8 col-md-8">
				<div id="accordion">
                    <div class="card mb-1 shadow-none">
                        <div class="card-header" id="headinglogin">
                            <h6 class="m-0">
                                <a href="#login" class="text-dark d-flex h5 mb-0 {openzc:if $checkout_step!=1}collapsed{/openzc:if}" data-toggle="collapse" aria-expanded="{openzc:if $checkout_step==1}true{else}false{/openzc:if}" aria-controls="login">
								{openzc:if $checkout_step>1}<i class="fas fa-check text-success"></i>&nbsp;&nbsp;{/openzc:if}1. Personal Information
								</a>
                            </h6>
                        </div>
						<div id="login" class="border border-top-0 collapse {openzc:if $checkout_step==1}show{/openzc:if}" aria-labelledby="headinglogin" data-parent="#accordion">
                            <div class="card-body p-0">
								{openzc:if !$_SESSION['customer_id']}
								<!-- Nav tabs -->
                                <ul class="nav nav-tabs mt-2" role="tablist">
                                    <li class="nav-item ">
                                        <a class="nav-link" data-toggle="tab" href="#register" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> Create Account</span>
                                            <span class="d-none d-sm-block">Create Account</span> 
                                        </a>
                                    </li>
									<li class="nav-item"><a class="nav-link"><span>|</span></a></li>
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#login_box" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i> Login</span>
                                            <span class="d-none d-sm-block">Login</span> 
                                        </a>
                                    </li>      
                                </ul>
								<!-- Tab panes -->
                                <div class="tab-content p-3 text-muted">
                                    <div class="tab-pane" id="register" role="tabpanel">
                                        <form class="mb-4 pb-3" action="{openzc:link name='FILENAME_CREATE_ACCOUNT'/}" method="post">
											<input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
											<input type="hidden" name="action" value="process">
											<input type="hidden" name="email_pref_html" value="email_format">
											{openzc:if DISPLAY_PRIVACY_CONDITIONS=="true"}
											<!--Privacy Statement START-->
												<h3 class="card-title mt-4">{openzc:define.TABLE_HEADING_PRIVACY_CONDITIONS/}</h3>
												<p>{openzc:define.TEXT_PRIVACY_CONDITIONS_DESCRIPTION/}</p>
												<div class="form-group">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="privacy" value="1">
														<label class="custom-control-label" for="privacy">{openzc:define.TEXT_PRIVACY_CONDITIONS_CONFIRM/}</label>
													</div>
												</div>
											<!--/Privacy Statement END-->
											{/openzc:if}
											{openzc:if ACCOUNT_COMPANY=="true"}
											<!--Company Details START-->
												<h5 class="mt-4">{openzc:define.CATEGORY_COMPANY/}</h5>
												<div class="form-group row">
													<label for="account_company" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_COMPANY/} <code>{openzc:define.ENTRY_COMPANY_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="company" value="" id="account_company" {openzc:if ENTRY_COMPANY_TEXT}required{/openzc:if}>
													</div>
												</div>
											<!--Company Details END-->
											{/openzc:if}
											
											<!--Address Details START-->
												<h5 class="">{openzc:define.TABLE_HEADING_ADDRESS_DETAILS/}</h5>
												{openzc:if ACCOUNT_GENDER=="true"}
													<div class="form-group row">
														<label for="account_company" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_GENDER/} <code>{openzc:define.ENTRY_GENDER_TEXT/}</code></label>
														<div class="custom-control custom-radio custom-control-inline ml-2">
															<input type="radio" id="MALE" name="gender"  value="m" class="custom-control-input" {openzc:if ENTRY_GENDER_TEXT}required{/openzc:if}>
															<label class="custom-control-label" for="MALE">{openzc:define.MALE/}</label>
														</div>
														<div class="custom-control custom-radio custom-control-inline">
															<input type="radio" id="FEMALE" name="gender" value="f" class="custom-control-input" {openzc:if ENTRY_GENDER_TEXT}required{/openzc:if}>
															<label class="custom-control-label" for="FEMALE">{openzc:define.FEMALE/}</label>
														</div>
													</div>
												{/openzc:if}
												
												<!--firstname-->
												<div class="form-group row">
													<label for="firstname" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_FIRST_NAME/} <code>{openzc:define.ENTRY_FIRST_NAME_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="firstname" value="" id="firstname" {openzc:if ENTRY_FIRST_NAME_TEXT}required{/openzc:if}>
													</div>
												</div>
												
												<!--lastname-->
												<div class="form-group row">
													<label for="lastname" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_LAST_NAME/} <code>{openzc:define.ENTRY_LAST_NAME_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="lastname" value="" id="lastname" {openzc:if ENTRY_LAST_NAME_TEXT}required{/openzc:if}>
													</div>
												</div>
												
												<!--street_address-->
												<div class="form-group row">
													<label for="street_address" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_STREET_ADDRESS/} <code>{openzc:define.ENTRY_STREET_ADDRESS_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="street_address" value="" id="street_address" {openzc:if ENTRY_STREET_ADDRESS_TEXT}required{/openzc:if}>
													</div>
												</div>
												
												<!--street_address-->
												{openzc:if ACCOUNT_SUBURB=="true"}
												<div class="form-group row">
													<label for="suburb" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_SUBURB/} <code>{openzc:define.ENTRY_SUBURB_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="suburb" value="" id="suburb" {openzc:if ENTRY_SUBURB_TEXT}required{/openzc:if}>
													</div>
												</div>
												{/openzc:if}
												
												<!--city-->
												<div class="form-group row">
													<label for="city" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_CITY/} <code>{openzc:define.ENTRY_CITY_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="city" value="" id="city" {openzc:if ENTRY_CITY_TEXT}required{/openzc:if}>
													</div>
												</div>
												
												<!--state-->
												<div id="res_states">
												{openzc:ajax filename="res_states"}
												{openzc:if ACCOUNT_STATE=="true"}
												{openzc:php}
													if(count($GLOBALS['states'])==1){$flag_show_pulldown_states=false;}
												{/openzc:php}
												<div class="form-group row">
													<label for="state" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_STATE/} <code>{openzc:define.ENTRY_STATE_TEXT/}</code></label>
													<div class="col-md-10">
														{openzc:if $flag_show_pulldown_states==true}
														<select class="form-control" name="zone_id">
															{openzc:var name="states"}
															<option value="[field:id/]">[field:text/]</option>
															{/openzc:var}
														</select>
														{else}
														<input class="form-control" type="text" name="state" value="" id="state" {openzc:if ENTRY_STATE_TEXT}required{/openzc:if}>
														{/openzc:if}
													</div>
												</div>
												{/openzc:if}
												{/openzc:ajax}
												</div>
												<!--Post/Zip Code-->
												<div class="form-group row">
													<label for="postcode" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_POST_CODE/} <code>{openzc:define.ENTRY_POST_CODE_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="postcode" value="" id="postcode" {openzc:if ENTRY_POST_CODE_TEXT}required{/openzc:if}>
													</div>
												</div>
												
												<!--country-->
												<div class="form-group row">
													<label for="country" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_COUNTRY/} <code>{openzc:define.ENTRY_COUNTRY_TEXT/}</code></label>
													<div class="col-md-10">
														{openzc:if $flag_show_pulldown_states==true}
														<select class="form-control openzc-select" data-action="getState" data-reload="res_states" name="zone_country_id">
														{else}
														<select class="form-control" name="zone_country_id">
														{/openzc:if}
															{openzc:var name="countries"}
															<option value="[field:id/]">[field:text/]</option>
															{/openzc:var}
														</select>
													</div>
												</div>
											<!--Address Details END-->
											
											<!--Additional Contact Details START-->
												<h5 class="mt-4">{openzc:define.TABLE_HEADING_PHONE_FAX_DETAILS/}</h5>
												
												<!--telephone_number-->
												<div class="form-group row">
													<label for="telephone_number" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_TELEPHONE_NUMBER/} <code>{openzc:define.ENTRY_TELEPHONE_NUMBER_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="telephone" value="" id="telephone_number" {openzc:if ENTRY_TELEPHONE_NUMBER_TEXT}required{/openzc:if}>
													</div>
												</div>
												
												<!--Fax Number-->
												{openzc:if ACCOUNT_FAX_NUMBER=="true"}
												<div class="form-group row">
													<label for="fax_number" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_FAX_NUMBER/} <code>{openzc:define.ENTRY_FAX_NUMBER_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="text" name="fax" value="" id="fax_number" {openzc:if ENTRY_FAX_NUMBER_TEXT}required{/openzc:if}>
													</div>
												</div>
												{/openzc:if}
											<!--Additional Contact Details END-->
											
											<!--Verify Your Age START-->
												{openzc:if ACCOUNT_DOB=="true"}
												<h5 class="mt-4">{openzc:define.TABLE_HEADING_DATE_OF_BIRTH/}</h5>
												<div class="form-group row">
													<label for="fax_number" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_DATE_OF_BIRTH/} <code>{openzc:define.ENTRY_DATE_OF_BIRTH_TEXT/}</code></label>
													<div class="col-md-10">
														<input type="text" class="form-control" name="dob" value="" aria-describedby="option-format" placeholder="mm/dd/yyyy" {openzc:if ENTRY_DATE_OF_BIRTH_TEXT}required{/openzc:if}>
													</div>
												</div>
												{/openzc:if}
											<!--Verify Your Age END-->
											
											<!--Login Details START-->
												<h5 class="mt-4">{openzc:define.TABLE_HEADING_LOGIN_DETAILS/}</h5>
												
												<!--Email Address-->
												<div class="form-group row">
													<label for="email_address" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_EMAIL_ADDRESS/} <code>{openzc:define.ENTRY_EMAIL_ADDRESS_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="email" name="email_address" value="" id="email_address" {openzc:if ENTRY_EMAIL_ADDRESS_TEXT}required{/openzc:if}>
													</div>
												</div>
												
												<!--Password-->
												<div class="form-group row">
													<label for="password" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_PASSWORD/} <code>{openzc:define.ENTRY_PASSWORD_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="password" name="password" value="" id="password" {openzc:if ENTRY_PASSWORD_TEXT}required{/openzc:if}>
													</div>
												</div>
												<div class="form-group row">
													<label for="confirmation" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_PASSWORD_CONFIRMATION/} <code>{openzc:define.ENTRY_PASSWORD_CONFIRMATION_TEXT/}</code></label>
													<div class="col-md-10">
														<input class="form-control" type="password" name="confirmation" value="" id="confirmation" {openzc:if ENTRY_PASSWORD_CONFIRMATION_TEXT}required{/openzc:if}>
													</div>
												</div>
												
											
											<!--Login Details END-->
											
											<!--Newsletter and Email Details START-->
												<h5 class="mt-4">{openzc:define.ENTRY_EMAIL_PREFERENCE/}</h5>
												
												<!--Newsletter-->
												{openzc:if ACCOUNT_NEWSLETTER_STATUS=="1"}
												<p>{openzc:define.ENTRY_NEWSLETTER/}</p>
												<!--Email format-->
												<div class="form-group ">
													<div class="custom-control custom-radio custom-control-inline">
														<input type="radio" id="HTML" name="email_format"  value="HTML" class="custom-control-input" required>
														<label class="custom-control-label" for="HTML">{openzc:define.ENTRY_EMAIL_HTML_DISPLAY/}</label>
													</div>
													<div class="custom-control custom-radio custom-control-inline">
														<input type="radio" id="TEXT" name="email_format" value="TEXT" class="custom-control-input" required>
														<label class="custom-control-label" for="TEXT">{openzc:define.ENTRY_EMAIL_TEXT_DISPLAY/}</label>
													</div>
												</div>
												{/openzc:if}
												
												<!--/Newsletter and Email Details END-->
												{openzc:if CUSTOMERS_REFERRAL_STATUS=="2"}
													<h5 class="mt-4">{openzc:define.TABLE_HEADING_REFERRAL_DETAILS/}</h5>
													<div class="form-group row">
														<label for="customers_referral" class="col-md-2 col-form-label text-left">{openzc:define.ENTRY_CUSTOMERS_REFERRAL/}</label>
														<div class="col-md-10">
															<input class="form-control" type="text" name="customers_referral" value="" id="customers_referral">
														</div>
													</div>
												{/openzc:if}
												<!--Newsletter and Email Details END-->
											<button class="btn btn-warning waves-effect waves-light float-right" type="submit">{openzc:define.BUTTON_SUBMIT_ALT/}</button>
										</form>
                                    </div>
                                    <div class="tab-pane active" id="login_box" role="tabpanel">
                                        <form action="{openzc:link name='FILENAME_LOGIN' parameter='action=process'/}" method="post">
											<input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
											<div class="form-group row">
												<label class="col-md-3 col-form-label" for="email_address">{openzc:define.ENTRY_EMAIL_ADDRESS/}  <code>*</code></label>
												<input class="col-md-5 form-control" type="email" name="email_address" value="" required>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label" for="password">{openzc:define.ENTRY_PASSWORD/}  <code>*</code></label>
												<input class="col-md-5 form-control" type="password" name="password" value="" required>
											</div>
											<div class="col-md-9 float-right pb-3 pl-0">
												<a class="h6 float-left text-secondary pl-0" href="{openzc:link name='FILENAME_PASSWORD_FORGOTTEN'/}">{openzc:define.TEXT_PASSWORD_FORGOTTEN/}</a>
												<button type="submit" class="btn btn-warning waves-effect waves-light float-right">{openzc:define.BUTTON_LOGIN_ALT/}</button>
											</div>
										</form>
                                    </div>
                                </div>
								{else}
								<p class="p-3 line-height-2">
									Connected as <a href="{openzc:link name='FILENAME_ACCOUNT_EDIT'/}" target="_blank">{openzc:account field='customers_firstname'/}&nbsp;{openzc:account field='customers_lastname'/}</a><br/>
									Not you? <a class="text-reset" href="{openzc:link name='FILENAME_LOGOFF'/}">Log out</a><br/>
									If you sign out now, your cart will be emptied.
								</p>
								{/openzc:if}
                                
                            </div>
                        </div>
                    </div>
                    <div class="card mb-1 shadow-none">
                        <div class="card-header" id="headingshipping_address">
                            <h6 class="m-0">
								<a href="#shipping_address" class="text-dark d-flex mb-0 h5 {openzc:if $checkout_step!=2}collapsed{/openzc:if}" data-toggle="{openzc:if $checkout_step>=2}collapse{/openzc:if}" aria-expanded="{openzc:if $checkout_step==2}true{else}false{/openzc:if}" aria-controls="shipping_address">
								{openzc:if $checkout_step>2}<i class="fas fa-check text-success"></i>&nbsp;&nbsp;{/openzc:if}2. Shipping Addresses
								</a>
                            </h6>
                        </div>
                        <div id="shipping_address" class="border border-top-0 collapse {openzc:if $checkout_step==2}show{/openzc:if}" aria-labelledby="headingshipping_address" data-parent="#accordion">
                            <div class="card-body">
								{openzc:account item="address" default="true"}
								{openzc:if !$field['street_address'] || !$field['entry_city'] || !$field['entry_state'] || !$field['entry_country_id']}
									<div class="alert alert-warning" role="alert">
                                        <i class="mdi mdi-alert-outline mr-2"></i> No related information,Please add shipping address!
                                    </div>
									<a class="btn btn-outline-light waves-effect" href="[field:edit_link/]"><i class="fas fa-plus mr-2"></i>Add Address</a>
								{else}
								<form action="{openzc:link name='FILENAME_CHECKOUT_SHIPPING'/}" method="post">
								<input type="hidden" name="action" value="shipping_address"/>
								
									{openzc:if ACCOUNT_GENDER=='true'}
									<div class="form-group row">
										<label for="gender" class="col-md-3 col-form-label">{openzc:define.ENTRY_GENDER/} <code>{openzc:define.ENTRY_GENDER_TEXT/}</code></label>
										<div class="col-md-5">
											<div class="custom-control custom-radio custom-control-inline ">
												<input type="radio" id="MALE" class="custom-control-input" {openzc:if $field['entry_gender']=='m'}checked=""{/openzc:if} disabled>
												<label class="custom-control-label" for="MALE">{openzc:define.MALE/}</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline ">
												<input type="radio" id="FEMALE" class="custom-control-input" {openzc:if $field['entry_gender']=='f'}checked=""{/openzc:if} disabled>
												<label class="custom-control-label" for="FEMALE">{openzc:define.FEMALE/}</label>
											</div>
										</div>
									</div>
									{/openzc:if}
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="firstname">{openzc:define.ENTRY_FIRST_NAME/} <code>*</code></label>
										<input class="col-md-5 form-control" value="[field:firstname/]" disabled>
									</div>
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="lastname">{openzc:define.ENTRY_LAST_NAME/} <code>*</code></label>
										<input class="col-md-5 form-control" value="[field:lastname/]" disabled>
									</div>
									{openzc:if ACCOUNT_COMPANY == 'true'}
									<div class="form-group row">
										<label for="company" class="col-md-3 col-form-label">{openzc:define.ENTRY_COMPANY/} <code>{openzc:define.ENTRY_COMPANY_TEXT/}</code></label>
										<input class="col-md-5 form-control" value="[field:company/]" id="company" disabled>
									</div>
									{/openzc:if}
									<div class="form-group row">
										<label for="street_address" class="col-md-3 col-form-label">{openzc:define.ENTRY_STREET_ADDRESS/} <code>{openzc:define.ENTRY_STREET_ADDRESS_TEXT/}</code></label>
										<input class="col-md-5 form-control" value="[field:street_address/]" id="street_address" disabled>
									</div>
									{openzc:if ACCOUNT_SUBURB == 'true'}
									<div class="form-group row">
										<label for="entry_suburb" class="col-md-3 col-form-label">{openzc:define.ENTRY_SUBURB/} <code>{openzc:define.ENTRY_SUBURB_TEXT/}</code></label>
										<input class="col-md-5 form-control" value="[field:entry_suburb/]" id="entry_suburb" disabled>
									</div>
									{/openzc:if}
									<div class="form-group row">
										<label for="city" class="col-md-3 col-form-label">{openzc:define.ENTRY_CITY/} <code>{openzc:define.ENTRY_CITY_TEXT/}</code></label>
										<input class="col-md-5 form-control" value="[field:city/]" id="city" disabled>
									</div>
									<div class="form-group row">
										<label for="zone_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_STATE/} <code>{openzc:define.ENTRY_STATE_TEXT/}</code></label>
										<input class="form-control col-md-5" value="[field:state/]" id="state" disabled>
									</div>
								
									<div class="form-group row">
										<label for="postcode" class="col-md-3 col-form-label">{openzc:define.ENTRY_POST_CODE/} <code>{openzc:define.ENTRY_POST_CODE_TEXT/}</code></label>
										<input class="col-md-5 form-control" value="[field:postcode/]" id="postcode" disabled>
									</div>
									
									<div class="form-group row">
										<label for="zone_country_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_COUNTRY/} <code>{openzc:define.ENTRY_COUNTRY_TEXT/}</code></label>
										<select class="col-md-5 form-control" disabled>
											{openzc:var name="countries"}
											<option value="[field:id/]" {openzc:if $field['status']=="active"}selected=""{/openzc:if}>[field:text/]</option>
											{/openzc:var}
										</select>
									</div>
								
								<div class="col-md-12 mt-5 pb-3 pl-0 mb-3">
								
									<a class="btn btn-outline-light waves-effect float-left" href="{openzc:link name='FILENAME_CHECKOUT_SHIPPING_ADDRESS'/}"><i class="bx bx-highlight"></i> &nbsp;&nbsp;{openzc:define.BUTTON_CHANGE_ADDRESS_ALT/}</a>
						
									<button class="btn btn-warning waves-effect waves-light float-right align-middle" href="">Continue <i class="bx bx-chevron-right"></i></button>
								</div>
								</form>
								{/openzc:if}
								{/openzc:account}
                            </div>
                        </div>
                    </div>
					
                    <div class="card mb-1 shadow-none">
                        <div class="card-header" id="headingshipping_method">
                            <h6 class="m-0">
                                <a href="#shipping_method" class="text-dark d-flex mb-0 h5 {openzc:if $checkout_step!=3}collapsed{/openzc:if}" data-toggle="{openzc:if $checkout_step>=3}collapse{/openzc:if}" aria-expanded="{openzc:if $checkout_step==3}true{else}false{/openzc:if}" aria-controls="shipping_method">
                                    {openzc:if $checkout_step>3}<i class="fas fa-check text-success"></i>&nbsp;&nbsp;{/openzc:if}3. Shipping Method
                                </a>
                            </h6>
                        </div>
                        <div id="shipping_method" class="border border-top-0 collapse {openzc:if $checkout_step==3}show{/openzc:if}" aria-labelledby="headingshipping_method" data-parent="#accordion">
                            <div class="card-body">
								<form action="{openzc:link name='FILENAME_CHECKOUT_SHIPPING'/}" method="post">
								<input type="hidden" name="securityToken" value="{openzc:field.securityToken/}">
								<input type="hidden" name="action" value="process">
								{openzc:checkout item="shipping_method"}
                               
									{openzc:php}$module=$field['module'];{/openzc:php}
									{openzc:loopson type="son"}
									<div class="form-group custom-control custom-radio">
									<input type="radio" id="shipping[field:id/]" name="shipping" value="[field:pid/]_[field:id/]" class="custom-control-input" {openzc:if $field['status']=="active"}checked=""{/openzc:if}>
                                    <label class="custom-control-label mr-3" for="shipping[field:id/]">{openzc:echo}$module{/openzc:echo} ([field:title/])&nbsp;&nbsp;[field:cost/]</label>
									{openzc:if isset($field['icon'])}<img src="[field:icon/]"/>{/openzc:if}
									</div>
									{/openzc:loopson}
								{/openzc:checkout}
								<div class="form-group">
									<label for="comments">{openzc:define.TABLE_HEADING_COMMENTS/}</label>
									<textarea class="form-control" rows="4" placeholder="" name="comments">{openzc:echo}$_SESSION['comments'];{/openzc:echo}</textarea>
								</div>
								<div class="col-md-12 pb-3 pr-0 mb-3">
									<button type="submit" class="btn btn-warning waves-effect waves-light float-right align-middle">Continue <i class="bx bx-chevron-right"></i></button>
								</div>
								</form>
                            </div>
                        </div>
                    </div>
					
					<div class="card mb-0 shadow-none">
						<div class="card-header" id="headingPayment">
                            <h6 class="m-0">
                                <a href="#Payment" class="text-dark  d-flex mb-0 h5 {openzc:if $checkout_step!=4}collapsed{/openzc:if}" data-toggle="{openzc:if $checkout_step>=4}collapse{/openzc:if}" aria-expanded="{openzc:if $checkout_step==4}true{else}false{/openzc:if}" aria-controls="Payment">
                                    4. Payment
                                </a>
                            </h6>
                        </div>
						
						<div id="Payment" class="border border-top-0 collapse {openzc:if $checkout_step==4}show{/openzc:if}" aria-labelledby="headingPayment" data-parent="#accordion">
                            <div class="card-body">
							{openzc:if $checkout_step==4}
								<div class="row">
									{openzc:if sizeof($order_total_modules->credit_selection())>0}
									<div class="col-xl-12 col-md-12 border p-3 mb-3">
										{openzc:msg name="redemptions" type="warning"}
											<div class="alert alert-danger alert-dismissible fade show" role="alert">
												[field:text/]
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">×</span>
												</button>
											</div>
										 {/openzc:msg}
										
										{openzc:checkout item="order_total_modules"}
										<form action="/index.php?main_page=checkout_confirmation" method="post">
										
										<h4 class="w-100">[field:module/]</h4>
										[field:redeem_instructions/][field:checkbox/]
										{openzc:loopson type="son" field="fields"}
										<div class="form-group row">
                                            <label for="example-text-input" class="col-md-2 col-form-label">[field:title/]</label>
                                            <div class="col-md-10">
                                                [field:field/]
                                            </div>
                                        </div>
										{/openzc:loopson}
										<button class="btn btn-outline-light waves-effect float-right align-middle" type="submit">{openzc:define.BUTTON_UPDATE_ALT/} <i class="bx bx-chevron-right"></i></button>
										</form>
										{/openzc:checkout}
									</div>
									{/openzc:if}
									<div class="col-xl-12 col-md-12">
									{openzc:msg name="checkout_address" type="success"}
										<div class="alert alert-success">[field:text/]</div>
									{/openzc:msg}
									<h4 class="w-100 mt-3">{openzc:define.TITLE_BILLING_ADDRESS/}</h4>
									<address>
									{openzc:echo}zen_address_label($_SESSION['customer_id'], $_SESSION['billto'], true, ' ', '<br />');{/openzc:echo}
									</address>
									<a class="btn btn-outline-light waves-effect float-right" href="{openzc:link name='FILENAME_CHECKOUT_PAYMENT_ADDRESS'/}"><i class="bx bx-highlight"></i> &nbsp;&nbsp;{openzc:define.BUTTON_CHANGE_ADDRESS_ALT/}</a>
									</div>
								
								{openzc:if DISPLAY_CONDITIONS_ON_CHECKOUT == 'true'}
								{/openzc:if}
								{openzc:if MAX_ADDRESS_BOOK_ENTRIES >= 2}
								
								{/openzc:if}
								<div class="col-xl-12 col-md-12">
								{openzc:if MODULE_ORDER_TOTAL_INSTALLED}
								<h4 class="w-100 mt-4 mb-3">Summary</h4>
								<div class="table-responsive mb-3">
                                    <table class="table table-hover mb-0">
                                        <tbody>
											{openzc:var name="order_total"}
											<tr>
                                                <th>[field:title/]</th>
                                                <td>[field:text/]</td>
                                            </tr>
											{/openzc:var}
                                        </tbody>
                                   </table>
                                </div>
								{/openzc:if}
								
								<form action="{openzc:link name='FILENAME_CHECKOUT_CONFIRMATION'/}" method="post">
								<textarea class="d-none" name="comments">{openzc:echo}$_SESSION['comments'];{/openzc:echo}</textarea>
								<h4 class="w-100 mb-4">{openzc:define.TABLE_HEADING_PAYMENT_METHOD/}</h4>
								
								{openzc:checkout item="payment_method"}
								<div class="form-group custom-control custom-radio">
								<input type="radio" id="pmt[field:id/]" name="payment" value="[field:id/]" class="custom-control-input" required="" {openzc:if $_SESSION['payment']==$field['id']}checked="checked"{/openzc:if}>
								<label class="custom-control-label mr-3" for="pmt[field:id/]">[field:module/]</label>
								{openzc:if defined('MODULE_ORDER_TOTAL_COD_STATUS') && MODULE_ORDER_TOTAL_COD_STATUS == 'true' and $field['id'] == 'cod'}
								<span class="text-danger">{openzc:define.TEXT_INFO_COD_FEES/}</span>
								{/openzc:if}
								{openzc:if isset($field['error'])}[field:error/]{/openzc:if}
								{openzc:if isset($field['fields']) && is_array($field['fields'])}
									{openzc:loopson type="son" field="fields"}
									<label>[field:title/]</label>[field:field/]<br/>
									{/openzc:loopson}
								{/openzc:if}
								</div>
								{/openzc:checkout}
								
								
								
								{openzc:if !$payment_modules->in_special_checkout() }
									{openzc:if SHOW_ACCEPTED_CREDIT_CARDS!=0}
										{openzc:if SHOW_ACCEPTED_CREDIT_CARDS == '1'}
											{openzc:echo}TEXT_ACCEPTED_CREDIT_CARDS . zen_get_cc_enabled();{/openzc:echo}
										{/openzc:if}
										{openzc:if SHOW_ACCEPTED_CREDIT_CARDS == '2'}
											{openzc:echo}TEXT_ACCEPTED_CREDIT_CARDS . zen_get_cc_enabled('IMAGE_');{/openzc:echo}
										{/openzc:if}
										
									{/openzc:if}
								{else}
								<input type="hidden" name="payment" value="{openzc:echo}$_SESSION['payment'];{/openzc:echo}" />
								{/openzc:if}
								
								{openzc:if DISPLAY_CONDITIONS_ON_CHECKOUT=='true'}
								<div class="form-group">
									<p class="mb-0">{openzc:define.TEXT_CONDITIONS_DESCRIPTION/}</p>
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="conditions" id="privacy" value="1">
										<label class="custom-control-label" for="privacy">{openzc:define.TEXT_CONDITIONS_CONFIRM/}</label>
									</div>
									
								</div>
								
								{/openzc:if}
								<div class="col-md-12 pb-3 pr-0 mb-3">
								<button class="btn btn-warning waves-effect waves-light float-right align-middle" type="submit">{openzc:define.BUTTON_CONTINUE_ALT/} <i class="bx bx-chevron-right"></i></button>
								</div>
								</form>
								</div>
								</div>
								
								{/openzc:if}
								
                            </div>
                        </div>
						
					</div>
                </div>
			</div>
			
		
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}libs/select2/js/select2.min.js"></script>
	<script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
	<script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-advanced.init.js"></script>

	<script src="{openzc:field.assets/}js/app.js"></script>
	<script src="{openzc:field.assets/}js/openzc.js"></script>
</body>
</html>