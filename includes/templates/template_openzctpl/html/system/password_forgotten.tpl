<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		
		<div class="col-md-12">
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>
			{openzc:msg name="password_forgotten" type="error"}
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    [field:text/]
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
             {/openzc:msg}
            <p>{openzc:define.TEXT_MAIN/}</p>
			<form action="/index.php?main_page=password_forgotten&action=process" method="post">
			<div class="form-group row">
                <label for="postcode" class="col-md-2 col-form-label">{openzc:define.ENTRY_EMAIL_ADDRESS/} <code>{openzc:define.ENTRY_EMAIL_ADDRESS_TEXT/}</code></label>
                <div class="col-md-10">
					<input class="form-control" type="email" name="email_address" value="{openzc:account item='address' field='postcode'/}" id="postcode">
                </div>
            </div>
			<button class="btn btn-lg btn-warning waves-effect waves-light" type="submit">{openzc:define.BUTTON_SUBMIT_ALT/}</button>
			</form>
		</div>
	</div>
	</div>
</main>

<!-- JAVASCRIPT -->
        <script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
       
		<script src="{openzc:field.assets/}js/openzc.js"></script>
       
</body>
</html>