<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>
body #openzcBox {background-color:#fff;}
</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		<div class="col-md-12">
		{openzc:msg name="login" type="error"}
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    [field:text/]
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
             {/openzc:msg}
		</div>
		<div class="col-md-6">
		<div class="card">
            
            <div class="card-body">
				<h4 class="pb-2">{openzc:define.HEADING_TITLE/} </h4>
                <form action="/index.php?main_page=login&action=process" method="post">
					<input type="hidden" name="securityToken" value="{openzc:field.securityToken/}">
					<div class="form-group">
						<label for="email_address">{openzc:define.ENTRY_EMAIL_ADDRESS/} <code>*</code></label>
						<input type="email" name="email_address" class="form-control" value="" required>
					</div>
					<div class="form-group">
						<label for="password">{openzc:define.ENTRY_PASSWORD/} <code>*</code></label>
						<input type="password" name="password" class="form-control" value="" required>
					</div>
					
					<div class="">
                        <button type="submit" class="btn btn-warning waves-effect waves-light mr-1">{openzc:define.BUTTON_LOGIN_ALT/}</button>
						<a class="h6 float-right " href="{openzc:link name='FILENAME_PASSWORD_FORGOTTEN'/}">{openzc:define.TEXT_PASSWORD_FORGOTTEN/}</a>
                    </div>
			    </form>
            </div>
        </div>
		</div>
		<div class="col-md-6">
		<div class="card">
            
            <div class="card-body">
				<h4 class="pb-2"> New Customers </h4>
                <p>{openzc:define.TEXT_NEW_CUSTOMER_INTRODUCTION/}</p>
				<a class="btn btn-warning waves-effect waves-light" href="{openzc:link name='FILENAME_CREATE_ACCOUNT'/}">Create An Account</a>
            </div>
        </div>
		</div>
	
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-validation.init.js"></script>

</body>
</html>