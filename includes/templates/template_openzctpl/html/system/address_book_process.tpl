<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->

<link href="{openzc:field.assets/}libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
<link href="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}libs/@chenfengyuan/datepicker/datepicker.min.css" rel="stylesheet" type="text/css" >
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>body #openzcBox {background-color:#fff;}</style>
</head>
<body>
<main id="openzcBox" >
	<div class="container">
	<div class="row mt-5">
		
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title mb-3">Account Center</h4>
					<div class="table-responsive">
						<table class="table mb-0 table-hover">
							<tbody>
									{openzc:account item="nav"}
									<tr><td {openzc:if $field['status']=="active"}class="bg-light"{/openzc:if}><a class="text-reset d-lg-flex" href="[field:link/]" title="[field:text/]">[field:title/]</a></td></tr>
									{/openzc:account}
									<tr><td></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			{openzc:if isset($_GET['delete'])}
			<h4 class="mb-4">{openzc:define.HEADING_TITLE_DELETE_ENTRY/} </h4>
			<div class="alert alert-danger alert-dismissible fade show" role="alert">{openzc:define.DELETE_ADDRESS_DESCRIPTION/}</div>
			<address class="border p-4">
			{openzc:echo}zen_address_label($_SESSION['customer_id'], $_GET['delete'], true, ' ', '<br />');{/openzc:echo}
			<br/>
			<a href="{openzc:link name='FILENAME_ADDRESS_BOOK_PROCESS' parameter='delete=+$_GET[delete]+&action=deleteconfirm'/}" class="btn btn-outline-light waves-effect mt-3"><i class="far fa-trash-alt"></i>&nbsp;&nbsp;{openzc:define.BUTTON_DELETE_ALT/}</a>
			<a href="{openzc:link name='FILENAME_BACK'/}" class="btn btn-outline-light waves-effect mt-3 float-right"><i class="fas fa-reply"></i>&nbsp;&nbsp;{openzc:define.BUTTON_BACK_ALT/}</a>
			</address>
			{else}
			<h4 class="mb-4">{openzc:define.HEADING_TITLE/} </h4>
			{openzc:msg name="addressbook" type="error"}
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    [field:text/]
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
             {/openzc:msg}
             
			<div class="row">
			<div class="col-xl-12 col-sm-12">
				<div class="card">
					<div class="card-body">
						<form action="/index.php?main_page=address_book_process{openzc:if isset($_GET['edit'])}&edit={openzc:account item='address' field='address_book_id'/}{/openzc:if}" method="post">
							
							{openzc:if isset($_GET['edit'])}
							<input type="hidden" name="action" value="update">
							<input type="hidden" name="edit" value="{openzc:account item='address' field='address_book_id'/}">
							{else}
							<input type="hidden" name="action" value="process">
							{/openzc:if}
							
							{openzc:if ACCOUNT_GENDER=='true'}
							<div class="form-group row">
                                <label for="gender" class="col-md-3 col-form-label">{openzc:define.ENTRY_GENDER/} <code>{openzc:define.ENTRY_GENDER_TEXT/}</code></label>
                                <div class="col-md-9">
									<div class="custom-control custom-radio custom-control-inline ">
										<input type="radio" id="MALE" name="gender"  value="m" class="custom-control-input" {openzc:if $address_book['entry_gender']=='m'}checked=""{/openzc:if}>
										<label class="custom-control-label" for="MALE">{openzc:define.MALE/}</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline ">
										<input type="radio" id="FEMALE" name="gender" value="f" class="custom-control-input" {openzc:if $address_book['entry_gender']=='f'}checked=""{/openzc:if}>
										<label class="custom-control-label" for="FEMALE">{openzc:define.FEMALE/}</label>
									</div>
                                </div>
                            </div>
							{/openzc:if}
							
							<div class="form-group row">
                                <label for="firstname" class="col-md-3 col-form-label">{openzc:define.ENTRY_FIRST_NAME/} <code>{openzc:define.ENTRY_FIRST_NAME_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="firstname" value="{openzc:account item='address' field='firstname'/}" id="firstname">
                                </div>
                            </div>
							
							<div class="form-group row">
                                <label for="lastname" class="col-md-3 col-form-label">{openzc:define.ENTRY_LAST_NAME/} <code>{openzc:define.ENTRY_LAST_NAME_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="lastname" value="{openzc:account item='address' field='lastname'/}" id="lastname">
                                </div>
                            </div>
							
							{openzc:if ACCOUNT_COMPANY == 'true'}
							<div class="form-group row">
                                <label for="company" class="col-md-3 col-form-label">{openzc:define.ENTRY_COMPANY/} <code>{openzc:define.ENTRY_COMPANY_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="company" value="{openzc:account item='address' field='company'/}" id="company">
                                </div>
                            </div>
							{/openzc:if}
							
							<div class="form-group row">
                                <label for="street_address" class="col-md-3 col-form-label">{openzc:define.ENTRY_STREET_ADDRESS/} <code>{openzc:define.ENTRY_STREET_ADDRESS_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="street_address" value="{openzc:account item='address' field='street_address'/}" id="street_address">
                                </div>
                            </div>
							
							{openzc:if ACCOUNT_SUBURB == 'true'}
							<div class="form-group row">
                                <label for="entry_suburb" class="col-md-3 col-form-label">{openzc:define.ENTRY_SUBURB/} <code>{openzc:define.ENTRY_SUBURB_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="entry_suburb" value="{openzc:account item='address' field='entry_suburb'/}" id="entry_suburb">
                                </div>
                            </div>
							{/openzc:if}
							
							<div class="form-group row">
                                <label for="city" class="col-md-3 col-form-label">{openzc:define.ENTRY_CITY/} <code>{openzc:define.ENTRY_CITY_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="city" value="{openzc:account item='address' field='city'/}" id="city">
                                </div>
                            </div>
							<div id="states">
							
							{openzc:ajax filename="states" }
							{openzc:if ACCOUNT_STATE == 'true'}
							{openzc:if $flag_show_pulldown_states == true && count($states)>1}
							<div class="form-group row">
								<label for="zone_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_STATE/} <code>{openzc:define.ENTRY_STATE_TEXT/}</code></label>
								<div class="col-md-9">
								<select class="custom-select" name="zone_id">
                                    {openzc:var name="states"}
                                    <option value="[field:id/]" {openzc:if $field['status']=="active"}selected=""{/openzc:if}>[field:text/]</option>
									{/openzc:var}
                                </select>
								<input class="form-control d-none" type="text" name="state" value="{openzc:account item='address' field='state'/}" id="state">
								</div>
							</div>
							{else}
							<div class="form-group row">
								<label for="zone_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_STATE/} <code>{openzc:define.ENTRY_STATE_TEXT/}</code></label>
								<div class="col-md-9">
									<input class="form-control" type="text" name="state" value="{openzc:account item='address' field='state'/}" id="state">
									<input type="hidden" value="{openzc:var name='zone_name'/}" name="zone_id"/>
								</div>
							</div>
							{/openzc:if}
							{/openzc:if}
							{/openzc:ajax}
							</div>
							<div class="form-group row">
                                <label for="postcode" class="col-md-3 col-form-label">{openzc:define.ENTRY_POST_CODE/} <code>{openzc:define.ENTRY_POST_CODE_TEXT/}</code></label>
                                <div class="col-md-9">
									<input class="form-control" type="text" name="postcode" value="{openzc:account item='address' field='postcode'/}" id="postcode">
                                </div>
                            </div>
							
							<div class="form-group row">
								<label for="zone_country_id" class="col-md-3 col-form-label">{openzc:define.ENTRY_COUNTRY/} <code>{openzc:define.ENTRY_COUNTRY_TEXT/}</code></label>
								<div class="col-md-9">
								{openzc:if $flag_show_pulldown_states == true}
								<select class="form-control openzc-select" name="zone_country_id" data-action="getState" data-reload="states" >
								{else}
								<select class="form-control select2" name="zone_country_id" >
								{/openzc:if}
									{openzc:var name="countries"}
                                    <option value="[field:id/]" {openzc:if $field['status']=="active"}selected=""{/openzc:if}>[field:text/]</option>
									{/openzc:var}
                                </select>
								</div>
							</div>
							{openzc:if $address_book['primary']==false}
							<div class="form-group row">
								<label for="primary" class="col-md-3 col-form-label"></label>
								<div class="col-md-9">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="primary" id="primary" value="on">
									<label class="custom-control-label" for="primary">{openzc:define.SET_AS_PRIMARY/}</label>
								</div>
								</div>
							</div>
							{/openzc:if}
							<input type="hidden" name="submitAddress" value="1">
							<button class="btn btn-warning waves-effect waves-light mt-3" type="submit">{openzc:define.BUTTON_SUBMIT_ALT/}</button>
							<a href="{openzc:link name='FILENAME_BACK'/}" class="btn mt-3 btn-outline-light waves-effect float-right"><i class="fas fa-reply"></i>&nbsp;&nbsp;{openzc:define.BUTTON_BACK_ALT/}</a>
						</form>
					</div>
				</div>
			</div>
			
			</div>
			{/openzc:if}
			
		</div>
	</div>
	</div>
</main>

<!-- JAVASCRIPT -->
        <script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
        <script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
       
		<script src="{openzc:field.assets/}js/openzc.js"></script>
       
</body>
</html>