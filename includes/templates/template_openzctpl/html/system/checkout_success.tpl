<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>
	body #openzcBox {background-color:#fff;}
	#openzcBox #cart tr > th{font-weight: 300 !important;border-top:0}
</style>
</head>
<body>
<main id="openzcBox">
	<div class="container mt-4 ">
			
			
			<div class="col-xl-12 col-md-12 pr-4">
				<h1 class="text-center text-success m-5 p-5"><i class="bx bx-check-circle"></i> {openzc:define.HEADING_TITLE/}</h1>
				{openzc:if $customer_has_gv_balance}
				{openzc:php}
					require($template->get_template_dir('tpl_modules_send_or_spend.php',DIR_WS_TEMPLATE, $current_page_base,'templates'). '/tpl_modules_send_or_spend.php');
				{/openzc:php}
				{/openzc:if}
				
				<div class="h3"><i class="bx bx-receipt"></i> {openzc:echo}TEXT_YOUR_ORDER_NUMBER . $zv_orders_id;{/openzc:echo}</div>
				<div class="mt-2">
				{openzc:if DEFINE_CHECKOUT_SUCCESS_STATUS >= 1 and DEFINE_CHECKOUT_SUCCESS_STATUS <= 2}
					{openzc:php}require($define_page);{/openzc:php}
				{/openzc:if}
				</div>
				{openzc:if isset($_SESSION['payment_method_messages']) && $_SESSION['payment_method_messages'] != ''}
					{openzc:echo}$_SESSION['payment_method_messages'];{/openzc:echo}
				{/openzc:if}
				
				
				{openzc:if isset($_SESSION['customer_guest_id'])}
				<div class="alert alert-info mb-2" role="alert">
                    {openzc:define.TEXT_CHECKOUT_LOGOFF_CUSTOMER/}
                </div>
				{/openzc:if}
				{openzc:if isset($_SESSION['customer_id'])}
				<div class="alert alert-warning" role="alert">
					{openzc:define.TEXT_CHECKOUT_LOGOFF_GUEST/}
				</div>
				{/openzc:if}
				<a href="{openzc:link name='FILENAME_LOGOFF'/}" class="btn btn-outline-secondary waves-effect"><i class="fas fa-power-off"></i> {openzc:define.BUTTON_LOG_OFF_ALT/}</a>
				
				{openzc:if $flag_show_products_notification == true}
				<div class="border mt-3 p-3 mb-2">
					<h5>{openzc:define.TEXT_NOTIFY_PRODUCTS/}</h5>
					<form class="pt-3" action="{openzc:link name='FILENAME_CHECKOUT_SUCCESS' parameter='action=update'/}" method="post">
					<input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
					{openzc:var name="notificationsArray"}
					<div class="form-check mb-3">
                        <input type="checkbox" name="notify[]" value="[field:products_id/]" class="form-check-input" id="customCheck[field:products_id/]" checked="checked">
                        <label class="form-check-label" for="customCheck[field:products_id/]">[field:products_name/]</label>
                    </div>
					{/openzc:var}
					<button type="submit" class="btn btn-outline-light waves-effect">&nbsp;&nbsp;<i class="fas fa-redo-alt"></i>&nbsp;&nbsp;{openzc:define.BUTTON_UPDATE_ALT/}&nbsp;&nbsp;</button>
					</form>
				</div>
				{/openzc:if}
			
				{openzc:php}
					if( DOWNLOAD_ENABLED == 'true'){
					require($template->get_template_dir('tpl_modules_downloads.php',DIR_WS_TEMPLATE, $current_page_base,'templates'). '/tpl_modules_downloads.php');
					}
				{/openzc:php}
				<div class="border p-3 mt-3">
				<p>{openzc:define.TEXT_SEE_ORDERS/}</p>
				<p>{openzc:define.TEXT_CONTACT_STORE_OWNER/}</p>
				<p class="font-weight-bold text-success h4">{openzc:define.TEXT_THANKS_FOR_SHOPPING/}</p>
				</div>
			</div>
	
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}libs/select2/js/select2.min.js"></script>
	<script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
	<script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-advanced.init.js"></script>
	
	<script src="{openzc:field.assets/}js/app.js"></script>
	<script src="{openzc:field.assets/}js/openzc.js"></script>
</body>
</html>