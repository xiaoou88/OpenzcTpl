<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Css -->
<link href="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="{openzc:field.assets/}css/bootstrap.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/icons.css" rel="stylesheet" type="text/css" />
<link href="{openzc:field.assets/}css/app.css" id="app-style" rel="stylesheet" type="text/css" />
<style>
	body #openzcBox {background-color:#fff;}
	#openzcBox #cart tr > th{font-weight: 300 !important;border-top:0}
</style>
</head>
<body>
<main id="openzcBox">
	<div class="container mt-4 ">
			<h3>{openzc:define.HEADING_TITLE/}</h3>
			<div class="row mt-4">
			<div class="col-xl-9 col-md-9 pr-4">
			{openzc:msg name="shopping_cart" type="error"}
				<div class="alert alert-danger">[field:text/]</div>
			{/openzc:msg}
			{openzc:if $flagAnyOutOfStock}
				{openzc:if STOCK_ALLOW_CHECKOUT == 'true'}
					<div class="alert alert-danger">{openzc:define.OUT_OF_STOCK_CAN_CHECKOUT/}</div>
				{else}
					<div class="alert alert-danger">{openzc:define.OUT_OF_STOCK_CANT_CHECKOUT/}</div>
				{/openzc:if}
			{/openzc:if}
				<div class="table-responsive">
				{openzc:if $_SESSION['cart']->total > 0}
					<table id="cart" class="table ">
						<thead >
							<tr>
								<th class="w-50">{openzc:define.TABLE_HEADING_PRODUCTS/}</th>
								<th class="w-25">{openzc:define.TABLE_HEADING_QUANTITY/}</th>
								<th>{openzc:define.TABLE_HEADING_PRICE/}</th>
								<th>{openzc:define.TABLE_HEADING_TOTAL/}</th>
								<th></th>
							</tr>
						</thead>
						<tbody >
							{openzc:cart ajax="cartlist" imgsizer="90,90"}
							<tr class="pt-4">
								<td>
									<img src="[field:products_image/]" class="float-left mr-4"/>
									<p class="mt-2">
									<a class="h6" href="[field:linkProductsName/]" >[field:products_name/]</a>
									</p>
									<p class="text-black-50">
									{openzc:loopson item="attr"}
										<span class="font-weight-bold h7">[field:products_options_name/] :</span>&nbsp;&nbsp;<span class="value h7">[field:products_options_values_name/]</span></br>
									{/openzc:loopson}
									</p>
								</td>
								<td class="">
									<div style="width: 120px;">
                                        <input class="form-control openzc-input border-light rounded-right" size="1" name="demo_vertical" id="demo_vertical" data-action="quantity" data-id="[field:id/]" type="text" value="[field:products_qty/]">
                                    </div>
								</td>
								<td>
									<p class="mt-2">
									{openzc:if $field['productPriceDiscount']}
										<span class="text-black-50 text-decoration-line-through ">[field:products_original_price/]</span><br/>
										<span>[field:productsPriceEach/]</span><br/>
										<span class="badge badge-danger"> [field:productPriceDiscount/] </span> 
									{else}
										[field:productsPriceEach/]
									{/openzc:if}
									</p>
								</td>
								<td><p class="mt-2">[field:productsPrice/]</p></td>
								<td><p class="mt-2"><span class="btn text-black-50 pt-0"><i class="dripicons-trash"></i></span></p></td>
							</tr>
							{/openzc:cart}
						</tbody>
						
					</table>
				{else}
					<div class="alert alert-warning" role="alert">
                        <i class="mdi mdi-cart-off"></i> {openzc:define.TEXT_CART_EMPTY/}
                    </div>
				{/openzc:if}
				</div>
			</div>
			{openzc:if $_SESSION['cart']->total > 0}
			<div class="col-xl-3 col-md-3">
				<div class="p-3 bg-light2">
					<h3 class="text-dark border-grey border-bottom pb-2 mb-0">Summary</h3>
					<div class="pt-3 pb-3">
							<div class="d-flex justify-content-between bd-highlight">
								<div class="bd-highlight h4">{openzc:define.SUB_TITLE_SUB_TOTAL/}</div>
								<div class="bd-highlight h4">{openzc:var name="cartShowTotal"/}</div>
							</div>
						</div>  
					{openzc:if SHOW_SHIPPING_ESTIMATOR_BUTTON=='1'}
						<button class="btn btn-secondary btn-block waves-effect waves-light mb-2" data-toggle="modal" data-target="#estimator">
							{openzc:define.BUTTON_SHIPPING_ESTIMATOR_ALT/}
						</button>
						<div id="estimator" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title mt-0" id="myModalLabel">{openzc:define.BUTTON_SHIPPING_ESTIMATOR_ALT/}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
									<iframe class="border-0 w-100" style="height:520px" src="{openzc:link name='FILENAME_POPUP_SHIPPING_ESTIMATOR'/}"></iframe>
									
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->   
					{/openzc:if}
					<a class="btn btn-lg btn-warning waves-effe
					ct waves-light btn-block" href="{openzc:link name='FILENAME_CHECKOUT_SHIPPING'/}"><i class="fas fa-cart-arrow-down"></i> &nbsp;{openzc:define.BUTTON_CHECKOUT_ALT/}</a>
					<a class="btn btn-outline-light btn-block waves-effect waves-light" href="{openzc:link name='FILENAME_BACK'/}"><i class="fas fa-angle-left"></i> {openzc:define.BUTTON_CONTINUE_SHOPPING_ALT/}</a>
				</div>
			</div>
			{/openzc:if}
			{openzc:if SHOW_SHIPPING_ESTIMATOR_BUTTON==2}
			<div class="col-xl-12 col-md-12 mt-2">
			<h3 class="mt-3">{openzc:define.CART_SHIPPING_OPTIONS/}</h3>
				<div class="row">{openzc:include filename="system/popup_shipping_estimator_box.tpl"/}</div>
			</div>
			{/openzc:if}
	</div>
	</div>
</main>

 <!-- JAVASCRIPT -->
	<script src="{openzc:field.assets/}libs/jquery/jquery.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="{openzc:field.assets/}libs/select2/js/select2.min.js"></script>
	<script src="{openzc:field.assets/}libs/metismenu/metisMenu.min.js"></script>
	<script src="{openzc:field.assets/}libs/node-waves/waves.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
	<script src="{openzc:field.assets/}libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script src="{openzc:field.assets/}js/pages/form-advanced.init.js"></script>
	
	<script src="{openzc:field.assets/}js/app.js"></script>
	<script src="{openzc:field.assets/}js/openzc.js"></script>
</body>
</html>