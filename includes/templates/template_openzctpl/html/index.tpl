<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Maxcart - Mega Store</title>
<meta name="description" content="Shop powered by PrestaShop">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">var prestashop = {};</script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

</head>

<body id="index" class="lang-en country-de currency-eur layout-full-width page-index tax-display-enabled">
<main id="page"> 
 {openzc:include filename="public/header.tpl"/}
  
  <section id="wrapper">
    <nav data-depth="1" class="breadcrumb">
      <div class="container">
        <ol itemscope itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement"> 
            <a itemprop="item" href="/"> 
            	<span itemprop="name">Home</span> 
            </a>
            <meta itemprop="position" content="1">
          </li>
        </ol>
      </div>
    </nav>
    <div class="home-container">
      <div id="columns_inner">
        <div id="content-wrapper">
          <section id="main">
            <section id="content" class="page-home">
              <div class="display-top-inner">
                <div class="container">
                  {openzc:include filename="public/category_tree.tpl"/}
                  <div class="flexslider" data-interval="8000" data-pause="true">
                    <div class="loadingdiv spinner"></div>
                    <ul class="slides">
                      <li class="slide"> <a href="#" title="sample-1"> <img src="{openzc:field.template/}style/img/sample-1.jpg" alt="sample-1" title="Sample 1" /> </a> </li>
                      <li class="slide"> <a href="#" title="sample-2"> <img src="{openzc:field.template/}style/img/sample-2.jpg" alt="sample-2" title="Sample 2" /> </a> </li>
                    </ul>
                  </div>
                  <div id="czsubbannercmsblock" class="block czsubbanners">
                    <div class="czsubbanner_container">
                      <div class="subbanners">
                        <div class="subbanner subbanner1">
                          <div class="subbanner-inner"> 
                           {openzc:prolist pid="1"} 
                           <a href="[field:products_link/]" class="banner-anchor"> 
                           <img src="{openzc:field.template/}style/img/sub-banner-1.jpg" alt="sub-banner1" class="subbanner-image1" /></a>
                            <div class="subbanner-text">
                              <div class="main-title">Sale Up To 30% Off</div>
                              <div class="shopnow"><a href="[field:products_link/]" class="shop-now">Buy Now</a></div>
                            </div>
                            {/openzc:prolist} 
                          </div>
                        </div>
                        <div class="subbanner subbanner2">
                          <div class="subbanner-inner"><a href="#" class="banner-anchor"><img src="{openzc:field.template/}style/img/sub-banner-2.jpg" alt="sub-banner1" class="subbanner-image2" /></a>
                            <div class="subbanner-text">
                              <div class="main-title">Best Sound System</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="czbannercmsblock" class="block czbanners">
                    <div class="czbanner_container">
                      <div class="cmsbanners">
                        <div class="cmsbanner cmsbanner1">
                          <div class="cmsbanner-inner"><a href="#" class="banner-anchor"><img src="{openzc:field.template/}style/img/cms-banner-1.jpg" alt="cms-banner1" class="banner-image1" /></a>
                            <div class="cmsbanner-text">
                              <div class="cmsmain-title">New Collection 2018</div>
                              <div class="cmssub-title">Free Delivery on your first Order-only $70.00</div>
                              <div class="shop-button"><a href="#" class="view-offer">View Offer</a></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="container">
                <div id="czbannercmsblock1" class="block czbanners">
                  <div class="cmsbanners1">
                    <div class="left-cmsbanner"><a href="#" class="banner-anchor"><img src="{openzc:field.template/}style/img/left-cmsbanner1.jpg" alt="leftbanner1-image" class="leftbanner1-image" /></a></div>
                    <div class="leftbanner-text">
                      <div class="main-title">Only For To Days!</div>
                      <div class="sub-title">Grab Offer Up TO <br />
                        <span> 30% </span>Off</div>
                    </div>
                  </div>
                </div>
                {openzc:include filename="index/weekdeal.tpl"/} 
                <div id="czbannercmsblock2" class="block czbanners">
                  <div class="cmsbanners1">
                    <div class="right-cmsbanner"><a href="#" class="banner-anchor"><img src="{openzc:field.template/}style/img/right-cmsbanner1.jpg" alt="rightbanner1-image" class="rightbanner1-image" /></a></div>
                    <div class="rightbanner-text">
                      <div class="main-title">Cloth Big Offer <span> 20% Off </span></div>
                      <div class="shop-button"><a href="#" class="view-more">View More</a></div>
                    </div>
                  </div>
                </div>
                {openzc:include filename="index/newpro.tpl"/} 
                {openzc:include filename="index/service.tpl"/} 
                {openzc:include filename="index/featured.tpl"/} 
            <footer class="page-footer"> 
              
              <!-- Footer content --> 
              
            </footer>
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/} </main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js" ></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js" ></script>
<script type="text/javascript" src="{openzc:field.template/}style/js/openzc.js"></script> 
<script>
Openzc.config={
	customReloadJS:{"no":"lightbox.js,custom.js,theme.js"},
	customReloadID:"viewcart,leftside",
	customAutoload:"viewcart"
};

</script>
</body>
</html>