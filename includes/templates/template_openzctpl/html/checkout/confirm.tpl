<!doctype html>
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Order confirmation</title>
<meta name="description" content="">
<meta name="keywords" content="">

<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="order-confirmation" class="lang-en country-de currency-eur layout-full-width page-order-confirmation tax-display-enabled">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <aside id="notifications">
    <div class="container"> </div>
  </aside>
  <section id="wrapper">
    <nav data-depth="1" class="breadcrumb">
      <div class="container">
        <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php"> <span itemprop="name">Home</span> </a>
            <meta itemprop="position" content="1">
          </li>
        </ol>
      </div>
    </nav>
    <div class="container">
      <div id="columns_inner">
        <div id="content-wrapper">
          <section id="main">
            <section id="content-hook_order_confirmation" class="card">
              <div class="card-block">
                <div class="row">
                  <div class="col-md-12">
                    <h3 class="h1 card-title"> {openzc:define.HEADING_TITLE/} </h3>
                    <p> {openzc:define.TEXT_CONTINUE_CHECKOUT_PROCEDURE/} </p>
                  </div>
                </div>
              </div>
            </section>
            <section id="content-hook_order_confirmation" class="card">
              <div class="card-block">
				<div class="row">
					  <div class="col-md-12">
						<h3 class="h1 card-title"> {openzc:define.HEADING_BILLING_ADDRESS/} </h3>
						<p> {openzc:define.TEXT_BILLING_ADDRESS/} </p>
					  </div>
					</div>
				</div>
			</section>
           <section id="content-hook_order_confirmation" class="card">
              <div class="card-block">
				<div class="row">
					  <div class="col-md-12">
						<h3 class="h1 card-title"> {openzc:define.HEADING_PAYMENT_METHOD/} </h3>
						<p> {openzc:define.TEXT_PAYMENT_METHOD/} </p>
					  </div>
					</div>
				</div>
			</section>
           <section id="content-hook_order_confirmation" class="card">
              <div class="card-block">
				<div class="row">
					  <div class="col-md-12">
						<h3 class="h1 card-title"> {openzc:define.HEADING_DELIVERY_ADDRESS/} </h3>
						<p> {openzc:define.TEXT_DELIVERY_ADDRESS/} </p>
					  </div>
					</div>
				</div>
			</section>
          <section id="content-hook_order_confirmation" class="card">
              <div class="card-block">
				<div class="row">
					  <div class="col-md-12">
						<h3 class="h1 card-title"> {openzc:define.HEADING_SHIPPING_METHOD/} </h3>
						<p> {openzc:define.TEXT_SHIPPING_METHOD/} </p>
					  </div>
					</div>
				</div>
			</section>
          <section id="content-hook_order_confirmation" class="card">
              <div class="card-block">
				<div class="row">
					  <div class="col-md-12">
						<h3 class="h1 card-title"> {openzc:define.HEADING_ORDER_COMMENTS/} </h3>
						<p> {openzc:define.TEXT_ORDER_COMMENTS/} </p>
					  </div>
					</div>
				</div>
			</section>
           
            <section id="content" class="page-content page-order-confirmation card">
              <div class="card-block">
                <div class="row">
                  <div id="order-items" class="col-md-12">
                    <h3 class="card-title h3">Order items</h3>
                    <div class="order-confirmation-table">
                     {openzc:cart imgsizer="252,239"}
                      <div class="order-line row">
                        <div class="col-sm-2 col-xs-3"> <span class="image"> <img src="[field:products_image/]"> </span> </div>
                        <div class="col-sm-4 col-xs-9 details"> 
                        	<span>[field:products_name/]<br/>
                        	{openzc:loopson item="attr"}
                        	 - [field:products_options_name/] : [field:products_options_values_name/]<br/>
                        	{/openzc:loopson}
                        	</span> 
                        </div>
                        <div class="col-sm-6 col-xs-12 qty">
                          <div class="row">
                            <div class="col-xs-5 text-sm-right text-xs-left">[field:products_price/]</div>
                            <div class="col-xs-2">[field:products_qty/]</div>
                            <div class="col-xs-5 text-xs-right bold">[field:productsPrice/]</div>
                          </div>
                        </div>
                      </div>
                      <hr>
                      {/openzc:cart}
                      <table>
                        <tbody>
                          <tr>
                            <td>Subtotal</td>
                            <td>{openzc:cart item="subtotal"/}</td>
                          </tr>
                          <tr>
                            <td>Shipping and handling</td>
                            <td>{openzc:cart item="shipping_cost"/}</td>
                          </tr>
                          <tr class="font-weight-bold">
                            <td><span class="text-uppercase">Total</span> (tax incl.)</td>
                            <td>{openzc:cart item="total"/}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  
                </div>
              </div>
            </section>
            
            
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>

</body>
</html>