<section id="checkout-personal-information-step" class="checkout-step -reachable {openzc:if $checkout_step==1}-current js-current-step {/openzc:if} {openzc:if $checkout_status['logged']==true}-complete{/openzc:if} ">
  <h1 class="step-title h3"> <i class="material-icons rtl-no-flip done">&#xE876;</i> <span class="step-number">1</span> Personal Information <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i> Edit</span> </h1>
  <div class="content"> {openzc:if $checkout_status["logged"]==true}
    <p class="identity"> Connected as <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=identity">{openzc:account field='customers_firstname'/}&nbsp;{openzc:account field='customers_lastname'/}</a>. </p>
    <p> Not you? <a href="/index.php?main_page=logoff">Log out</a> </p>
    <p><small>If you sign out now, your cart will be emptied.</small></p>
    {else}
    <ul class="nav nav-inline my-2" role="tablist">
      <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#checkout-guest-form" role="tab" aria-controls="checkout-guest-form" aria-selected="true"> Order as a guest </a> </li>
      <li class="nav-item"> <span href="nav-separator"> | </span> </li>
      <li class="nav-item"> <a class="nav-link " data-link-action="show-login-form" data-toggle="tab" href="#checkout-login-form" role="tab" aria-controls="checkout-login-form"> Sign in </a> </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="checkout-guest-form" role="tabpanel"> 
       {openzc:php}echo $messageStack->output('create_account');{/openzc:php}
        {openzc:include filename="checkout/step1/checkout_reg_box.tpl"/} 
        </div>
      <div class="tab-pane " id="checkout-login-form" role="tabpanel" aria-hidden="true"> {openzc:include filename="checkout/step1/checkout_login_box.tpl"/} </div>
    </div>
    {/openzc:if} </div>
</section>
