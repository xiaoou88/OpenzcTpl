{openzc:if $checkout_step>=3 && $checkout_status['logged']==true}
 <section id="checkout-payment-step" class="checkout-step -reachable {openzc:if $checkout_step==4}-current js-current-step{/openzc:if}">
  <h1 class="step-title h3"> <i class="material-icons rtl-no-flip done">&#xE876;</i> <span class="step-number">4</span> Payment <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i> Edit</span> </h1>
  <div class="content">
   <form action="/index.php?main_page=checkout_confirmation" method="post">
   <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}">
   <input type="hidden" name="action" value="submit">
    <div class="payment-options ">
     {openzc:payment}
      <div>
        <div id="payment-option-[field:id/]-container" class="payment-option clearfix"> 
         <span class="custom-radio float-xs-left">
          <input class="ps-shown-by-js " id="payment-option-[field:id/]" name="payment" type="radio" value="[field:id/]" required="">
          <span></span>
			</span>
          <label for="payment-option-1"> [field:module/] </label>
        </div>
      </div>
      {/openzc:payment}
    </div>
    <div class="order-options clearfix">
            <div id="delivery">
              <label for="payment_message">{openzc:define.TABLE_HEADING_COMMENTS/}</label>
              <textarea rows="2" cols="120" id="payment_message" name="comments"></textarea>
            </div>
          </div>
    <br/>
    <div id="payment-confirmation">
      <div class="ps-shown-by-js clearfix">
        <button type="submit" class="btn btn-primary"> Order with an obligation to pay </button>
      </div>
     
    </div>
  </form>
  </div>
</section>
{else}
<section id="checkout-payment-step" class="checkout-step -unreachable">
  <h1 class="step-title "> <i class="material-icons rtl-no-flip done">&#xE876;</i> <span class="step-number">4</span> Payment <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i> Edit</span> </h1>
</section>
{/openzc:if}