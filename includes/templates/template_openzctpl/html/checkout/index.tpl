<!doctype html>
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Maxcart - Mega Store</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/vnd.microsoft.icon" href="/prestashop/PRS07/PRS070168/PRS03/img/favicon.ico?1549613849">
<link rel="shortcut icon" type="image/x-icon" href="/prestashop/PRS07/PRS070168/PRS03/img/favicon.ico?1549613849">

<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="checkout" class="lang-en country-de currency-eur layout-left-column page-order tax-display-enabled">
<main id="page"> {openzc:include filename="public/header.tpl"/}
  <aside id="notifications">
    <div class="container"> </div>
  </aside>
  <section id="wrapper">
    <div class="container ggdg">
      <section id="content">
        <div class="row">
          <div class="col-md-8">
            {openzc:include filename="checkout/step1.tpl"/} 
            {openzc:include filename="checkout/step2.tpl"/}
            {openzc:include filename="checkout/step3.tpl"/}
            {openzc:include filename="checkout/step4.tpl"/}
          </div>
          <div class="col-md-4">
            <section id="js-checkout-summary" class="card js-cart" data-refresh-url="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=cart?ajax=1&amp;action=refresh">
              <div class="card-block">
                <div class="cart-summary-products">
                  <p>{openzc:cart item="item"/} item</p>
                  <p> <a href="#" data-toggle="collapse" data-target="#cart-summary-product-list"> show details </a> </p>
                  <div class="collapse" id="cart-summary-product-list">
                    <ul class="media-list">
                      <li class="media">
                        <div class="media-left"> <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_product=21&amp;id_product_attribute=52&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1#/1-size-s/15-color-green" title="Exercitat Virginia"> <img class="media-object" src="{openzc:field.template/}style/img/140-cart_default.jpg" alt="Exercitat Virginia"> </a> </div>
                        <div class="media-body"> <span class="product-name">Exercitat Virginia</span> <span class="product-quantity">x1</span> <span class="product-price float-xs-right">€87.00</span>
                          <div class="product-line-info product-line-info-secondary text-muted"> <span class="label">Size:</span> <span class="value">S</span> </div>
                          <div class="product-line-info product-line-info-secondary text-muted"> <span class="label">Color:</span> <span class="value">Green</span> </div>
                          <br>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-products"> <span class="label">Subtotal</span> <span class="value">{openzc:cart item="total"/}</span> </div>
                <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-shipping"> <span class="label">Shipping</span> <span class="value">{openzc:cart item="shipping_cost"/}</span> </div>
              </div>
              <hr class="separator">
              <div class="card-block cart-summary-totals">
                <div class="cart-summary-line cart-total"> <span class="label">Total (tax incl.)</span> <span class="value">{openzc:cart item="total"/}</span> </div>
                <div class="cart-summary-line"> <span class="label sub"></span> <span class="value sub"></span> </div>
              </div>
            </section>
            <div id="block-reassurance">
              <ul>
                <li>
                  <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)"> <span class="h6">Security policy (edit with Customer reassurance module)</span> </div>
                </li>
                <li>
                  <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)"> <span class="h6">Delivery policy (edit with Customer reassurance module)</span> </div>
                </li>
                <li>
                  <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)"> <span class="h6">Return policy (edit with Customer reassurance module)</span> </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/} 
  <script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script> 
  <script type="text/javascript" src="{openzc:field.template/}style/js/openzc.js"></script> 
</main>
</body>
</html>