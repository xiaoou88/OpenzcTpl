<section class="form-fields">
 {openzc:account item="address" default="true"}
  <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
  <input type="hidden" name="sendto" value="[field:address_book_id/]"/>
  <div class="form-group row ">
    <label class="col-md-3 form-control-label required"> First name </label>
    <div class="col-md-6">
      <input class="form-control"type="text" disabled value="[field:firstname/]">
    </div>
    <div class="col-md-3 form-control-comment"> </div>
  </div>
  <div class="form-group row ">
    <label class="col-md-3 form-control-label required"> Last name </label>
    <div class="col-md-6">
      <input class="form-control" type="text" value="[field:lastname/]">
    </div>
    <div class="col-md-3 form-control-comment"> </div>
  </div>
  
  <div class="form-group row ">
    <label class="col-md-3 form-control-label required"> Address </label>
    <div class="col-md-6">
      <input class="form-control" type="text" value="[field:street_address/]" maxlength="128" required="">
    </div>
    <div class="col-md-3 form-control-comment"> </div>
  </div>
  <div class="form-group row ">
    <label class="col-md-3 form-control-label"> Address Complement </label>
    <div class="col-md-6">
      <input class="form-control" type="text" value="[field:suburb/]" maxlength="128">
    </div>
    <div class="col-md-3 form-control-comment"> Optional </div>
  </div>
  <div class="form-group row ">
    <label class="col-md-3 form-control-label required"> Zip/Postal Code </label>
    <div class="col-md-6">
      <input class="form-control" type="text" value="[field:postcode/]" maxlength="12" required="">
    </div>
    <div class="col-md-3 form-control-comment"> </div>
  </div>
  <div class="form-group row ">
    <label class="col-md-3 form-control-label required"> City </label>
    <div class="col-md-6">
      <input class="form-control" type="text" value="[field:city/]" maxlength="64" required="">
    </div>
    <div class="col-md-3 form-control-comment"> </div>
  </div>
  <div class="form-group row ">
    <label class="col-md-3 form-control-label required"> {openzc:define.ENTRY_STATE/} </label>
    <div class="col-md-6">
      <select class="form-control form-control-select js-country" name="id_country" required>
       	{openzc:var name="states"}
       	{openzc:if $field['status']=="active"}
        <option value="[field:id/]" selected="">[field:text/]</option>
        {else}
        <option value="[field:id/]">[field:text/]</option>
        {/openzc:if}
        {/openzc:var}
      </select>
    </div>
    <div class="col-md-3 form-control-comment"> </div>
  </div>
  <div class="form-group row ">
    <label class="col-md-3 form-control-label required"> Country </label>
    <div class="col-md-6">
      <select class="form-control form-control-select js-country" name="id_country" required>
       	{openzc:var name="countries"}
       	{openzc:if $field['status']=="active"}
        <option value="[field:id/]" selected="">[field:text/]</option>
        {else}
        <option value="[field:id/]">[field:text/]</option>
        {/openzc:if}
        {/openzc:var}
      </select>
    </div>
    <div class="col-md-3 form-control-comment"> </div>
  </div>
  <div class="form-group row ">
    <label class="col-md-3 form-control-label"> Phone </label>
    <div class="col-md-6">
      <input class="form-control" type="text" value="[field:telephone/]" maxlength="32">
    </div>
    <div class="col-md-3 form-control-comment"> Optional </div>
  </div>
  
 {/openzc:account}
</section>
<div class="clearfix">
          <button type="submit" class="btn btn-primary continue float-xs-right" name="confirm-addresses" value="1">
              Continue
          </button>
          <input type="hidden" id="not-valid-addresses" value="">
        </div>