{openzc:if $checkout_status['shipping_address']==true}
<section id="checkout-delivery-step" class="checkout-step -reachable {openzc:if $checkout_step==3}-current js-current-step{/openzc:if}{openzc:if $checkout_step>3}-complete{/openzc:if}">
  <h1 class="step-title h3"> <i class="material-icons rtl-no-flip done">&#xE876;</i> <span class="step-number">3</span> Shipping Method <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i> Edit</span> </h1>
  <div class="content">
    <div id="hook-display-before-carrier"> </div>
    <div class="delivery-options-list">
      <form class="clearfix" action="" method="post">
	    <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}">
		<input type="hidden" name="action" value="process">
        <div class="form-fields">
          <div class="delivery-options">
           	{openzc:shipping}
           	{openzc:loopson type="son"}
            <div class="row delivery-option">
              <div class="col-sm-1"> <span class="custom-radio float-xs-left">
                <input type="radio" name="shipping" value="[field:pid/]_[field:id/]" {openzc:if $field['status']=='active'}checked=""{/openzc:if}>
                <span></span> </span> </div>
              <label class="col-sm-11 delivery-option-2">
              <div class="row">
                <div class="col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="col-xs-9"> <span class="h6 carrier-name">[field:title/]</span> </div>
                  </div>
                </div>
                <div class="col-sm-4 col-xs-12"> <span class="carrier-delay">[field:title/]</span> </div>
                <div class="col-sm-3 col-xs-12"> <span class="carrier-price">[field:cost/]</span> </div>
              </div>
              </label>
            </div>
            <div class="clearfix"></div>
            {/openzc:loopson}
            {/openzc:shipping}
            
          </div>
          <div class="order-options">
            <div id="delivery">
              <label for="delivery_message">If you would like to add a comment about your order, please write it in the field below.</label>
              <textarea rows="2" cols="120" id="delivery_message" name="comments"></textarea>
            </div>
          </div>
        </div>
		<br/>
        <button type="submit" class="clearfix continue btn btn-primary float-xs-right" name="confirmDeliveryOption" value="1"> Continue </button>
      </form>
    </div>
    <div id="hook-display-after-carrier"> </div>
    <div id="extra_carrier"></div>
  </div>
</section>
{else}
<section id="checkout-delivery-step" class="checkout-step -unreachable">
  <h1 class="step-title "> <i class="material-icons rtl-no-flip done">&#xE876;</i> <span class="step-number">3</span> Shipping Method <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i> Edit</span> </h1>
</section>
{/openzc:if}