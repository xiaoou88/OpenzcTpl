{openzc:if $checkout_step>=1 && $checkout_status['logged']==true}
<section id="checkout-addresses-step" class="checkout-step {openzc:if $checkout_step==2} -current js-current-step {/openzc:if} -reachable {openzc:if $checkout_status['shipping_address']==true || $checkout_step>2} -complete{/openzc:if}">
  <h1 class="step-title h3"> <i class="material-icons rtl-no-flip done">&#xE876;</i> <span class="step-number">2</span>Shipping Addresses <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i> Edit</span> </h1>
  <div class="content">
   	
    {openzc:include filename="checkout/step2/checkout_shipping_address_default.tpl"/}
    
  </div>
</section>
{else}
<section id="checkout-addresses-step" class="checkout-step -unreachable">
  <h1 class="step-title "> <i class="material-icons rtl-no-flip done">&#xE876;</i> <span class="step-number">2</span>Shipping Addresses <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i> Edit</span> </h1>
</section>
{/openzc:if}