<form id="login-form" action="/index.php?main_page=login&action=process" method="post">
 <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}">
  <section>
 
    <div class="form-group row ">
      <label class="col-md-3 form-control-label required"> Email </label>
      <div class="col-md-6">
        <input class="form-control" name="email_address" type="email" value="" required="">
      </div>
      <div class="col-md-3 form-control-comment"> </div>
    </div>
    <div class="form-group row ">
      <label class="col-md-3 form-control-label required"> Password </label>
      <div class="col-md-6">
        <div class="input-group js-parent-focus">
          <input class="form-control js-child-focus js-visible-password" name="password" type="password" value="" pattern=".{5,}" required="">
          <span class="input-group-btn">
          <button class="btn" type="button" data-action="show-password" data-text-show="Show" data-text-hide="Hide"> Show </button>
          </span> </div>
      </div>
      <div class="col-md-3 form-control-comment"> </div>
    </div>
    <div class="forgot-password"> <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=password" rel="nofollow"> Forgot your password? </a> </div>
  </section>
  <footer class="form-footer text-sm-center clearfix">
    
    <button class="continue btn btn-primary float-xs-right" name="continue" data-link-action="sign-in" type="submit"> Continue </button>
  </footer>
</form>
