<form id="customer-form" action="" class="js-customer-form" method="post">
  <section>
    <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}">
    <input type="hidden" name="action" value="process">
    <input type="hidden" name="email_pref_html" value="email_format">
    <div class="form-group row ">
      <label class="col-md-3 form-control-label"> {openzc:define.ENTRY_GENDER/} </label>
      <div class="col-md-6 form-control-valign">
        <label class="radio-inline"> <span class="custom-radio">
          <input name="gender" type="radio" value="m">
          <span></span> </span> {openzc:define.MALE/} </label>
        <label class="radio-inline"> <span class="custom-radio">
          <input name="gender" type="radio" value="f">
          <span></span> </span> {openzc:define.FEMALE/} </label>
      </div>
      <div class="col-md-3 form-control-comment"> </div>
    </div>
    <div class="form-group row ">
      <label class="col-md-3 form-control-label required"> {openzc:define.ENTRY_FIRST_NAME/} </label>
      <div class="col-md-6">
        <input class="form-control" name="firstname" type="text" value="" required="">
      </div>
      <div class="col-md-3 form-control-comment"> </div>
    </div>
    <div class="form-group row ">
      <label class="col-md-3 form-control-label required"> {openzc:define.ENTRY_LAST_NAME/} </label>
      <div class="col-md-6">
        <input class="form-control" name="lastname" type="text" value="" required="">
      </div>
      <div class="col-md-3 form-control-comment"> </div>
    </div>
    <div class="form-group row ">
      <label class="col-md-3 form-control-label required"> {openzc:define.ENTRY_EMAIL_ADDRESS/} </label>
      <div class="col-md-6">
        <input class="form-control" name="email_address" type="email" value="" required="" placeholder="">
      </div>
      <div class="col-md-3 form-control-comment"> </div>
    </div>
    <p> <span class="font-weight-bold">Create an account</span> <span class="font-italic">(optional)</span> <br>
      <span class="text-muted">And save time on your next order!</span> </p>
    <div class="form-group row ">
      <label class="col-md-3 form-control-label"> {openzc:define.ENTRY_PASSWORD/} </label>
      <div class="col-md-6">
        <div class="input-group js-parent-focus">
          <input class="form-control js-child-focus js-visible-password" name="password" type="password" value="" pattern=".{5,}">
          <span class="input-group-btn">
          <button class="btn" type="button" data-action="show-password" data-text-show="Show" data-text-hide="Hide"> Show </button>
          </span> </div>
      </div>
      <div class="col-md-3 form-control-comment"> Optional </div>
    </div>
    <div class="form-group row ">
      <label class="col-md-3 form-control-label"> {openzc:define.ENTRY_PASSWORD_CONFIRMATION/} </label>
      <div class="col-md-6">
        <div class="input-group js-parent-focus">
          <input class="form-control js-child-focus js-visible-password" name="confirmation" type="password" value="" pattern=".{5,}">
          <span class="input-group-btn">
          <button class="btn" type="button" data-action="show-password" data-text-show="Show" data-text-hide="Hide"> Show </button>
          </span> </div>
      </div>
      <div class="col-md-3 form-control-comment"> Optional </div>
    </div>
    <div class="form-group row ">
      <label class="col-md-3 form-control-label"> {openzc:define.ENTRY_DATE_OF_BIRTH/} </label>
      <div class="col-md-6">
        <input class="form-control" name="dob" type="text" value="" placeholder="MM/DD/YYYY">
        <span class="form-control-comment"> (E.g.: 05/31/1970) </span> </div>
      <div class="col-md-3 form-control-comment"> Optional </div>
    </div>
    
    <div class="form-group row ">
      <label class="col-md-3 form-control-label"> </label>
      <div class="col-md-6"> <span class="custom-checkbox">
        <input name="newsletter" type="checkbox" value="1">
        <span><i class="material-icons checkbox-checked"></i></span>
        <label>Sign up for our newsletter<br>
          <em>You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.</em></label>
        </span> </div>
      <div class="col-md-3 form-control-comment"> </div>
    </div>
  </section>
  <footer class="form-footer clearfix">
    <button class="continue btn btn-primary float-xs-right" name="continue" data-link-action="register-new-customer" type="submit"> Continue </button>
  </footer>
</form>
