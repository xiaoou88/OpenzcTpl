<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{openzc:define.HEADING_TITLE/}</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">var prestashop = {};</script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="identity" class="lang-en country-de currency-eur layout-left-column page-identity tax-display-enabled page-customer-account">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <section id="wrapper">
    <nav data-depth="2" class="breadcrumb">
      <div class="container">
        <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php"> <span itemprop="name">Home</span> </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a itemprop="item" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=my-account"> <span itemprop="name">Your account</span> </a>
            <meta itemprop="position" content="2">
          </li>
        </ol>
      </div>
    </nav>
    <div class="container">
      <div id="columns_inner">
        <div class="col-md-8" >
          <section id="main">
            <header class="page-header">
              <h1> {openzc:define.HEADING_TITLE/} </h1>
            </header>
            
            <section id="content" class="page-content">
             <h3>{openzc:define.TITLE_SHIPPING_ADDRESS/}</h3>
             <address>{openzc:account item="address" default="true"}[field:address/]{/openzc:account}</address>
             <div class="alert alert-info">{openzc:define.TEXT_CREATE_NEW_SHIPPING_ADDRESS/}</div>
             <h3>{openzc:define.TITLE_PLEASE_SELECT/}</h3>
              <form action="/index.php?main_page=checkout_shipping_address" id="customer-form" method="post">
                <section>
                  <input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
                  <input type="hidden" name="action" value="submit">
                  <div class="form-group row ">
                    <label class="col-md-3 form-control-label"> Social title </label>
                    <div class="col-md-6 form-control-valign">
                      <label class="radio-inline"> <span class="custom-radio">
                        <input name="gender" type="radio" value="m" >
                        <span></span> </span> Mr. </label>
                      <label class="radio-inline"> <span class="custom-radio">
                        <input name="gender" type="radio" value="f" >
                        <span></span> </span> Mrs. </label>
                    </div>
                    <div class="col-md-3 form-control-comment"> * </div>
                  </div>
                  <div class="form-group row ">
                    <label class="col-md-3 form-control-label required"> First name </label>
                    <div class="col-md-6">
                      <input class="form-control" name="firstname" type="text" required="">
                    </div>
                    <div class="col-md-3 form-control-comment"> * </div>
                  </div>
                  <div class="form-group row ">
                    <label class="col-md-3 form-control-label required"> Last name </label>
                    <div class="col-md-6">
                      <input class="form-control" name="lastname" type="text" required="">
                    </div>
                    <div class="col-md-3 form-control-comment"> * </div>
                  </div>
                  <div class="form-group row ">
                    <label class="col-md-3 form-control-label required"> Strest Address </label>
                    <div class="col-md-6">
                      <input class="form-control" name="street_address" type="text" required="">
                    </div>
                    <div class="col-md-3 form-control-comment"> * </div>
                  </div>
                  <div class="form-group row ">
                    <label class="col-md-3 form-control-label required"> Address Line 2 </label>
                    <div class="col-md-6">
                      <input class="form-control" name="suburb" type="text" >
                    </div>
                    <div class="col-md-3 form-control-comment"> Optional </div>
                  </div>
                  <div class="form-group row ">
                    <label class="col-md-3 form-control-label required"> City </label>
                    <div class="col-md-6">
                      <input class="form-control" name="city" type="text" required="">
                    </div>
                    <div class="col-md-3 form-control-comment"> * </div>
                  </div>
                  <div class="form-group row ">
                    <label class="col-md-3 form-control-label required"> Post/Zip Code </label>
                    <div class="col-md-6">
                      <input class="form-control" name="postcode" type="text" required="">
                    </div>
                    <div class="col-md-3 form-control-comment"> * </div>
                  </div>
                  <div class="form-group row" id="states">
                  	{openzc:ajax filename="states"}
					<label class="col-md-3 form-control-label required"> {openzc:define.ENTRY_STATE/} </label>
					<div class="col-md-6">
					  <select class="form-control form-control-select js-country" name="zone_id" required>
						{openzc:var name="states"}
						{openzc:if $field['status']=="active"}
						<option value="[field:id/]" selected="">[field:text/]</option>
						{else}
						<option value="[field:id/]">[field:text/]</option>
						{/openzc:if}
						{/openzc:var}
					  </select>
					</div>
					<div class="col-md-3 form-control-comment"> </div>
                 	{/openzc:ajax}
                  </div>
                  <div class="form-group row ">
					<label class="col-md-3 form-control-label required"> Country </label>
					<div class="col-md-6">
					  <select class="form-control form-control-select openzc-select" data-action="getState" data-reload="states" name="zone_country_id"  required>
						{openzc:var name="countries"}
						{openzc:if $field['status']=="active"}
						<option value="[field:id/]" selected="">[field:text/]</option>
						{else}
						<option value="[field:id/]">[field:text/]</option>
						{/openzc:if}
						{/openzc:var}
					  </select>
					</div>
					<div class="col-md-3 form-control-comment"> </div>
				  </div>
                  
                </section>
                <footer class="form-footer clearfix">
                  <button class="btn btn-primary form-control-submit float-xs-right" type="submit"> Save </button>
                </footer>
              </form>
            </section>
            <section id="content" class="page-content">
            <h3>{openzc:define.TABLE_HEADING_ADDRESS_BOOK_ENTRIES/}</h3>
              <form action="/index.php?main_page=checkout_shipping_address" id="customer-form" method="post">
              <section class="row">
              	<input type="hidden" name="securityToken" value="{openzc:field.securityToken/}"/>
                  {openzc:account item="address"}
                    <div class="col-md-3 form-control-valign">
                      <label class="radio-inline"> 
                       <span class="custom-radio">
                        <input name="address" type="radio" value="[field:address_book_id/]" >
                        <span></span> </span> [field:address/] </label>
				  </div>
                  {/openzc:account}
                  </section>
                  <input type="hidden" name="action" value="submit">
                  <footer class="form-footer clearfix">
                  <button class="btn btn-primary form-control-submit float-xs-right" type="submit"> Save </button>
                	</footer>
              </form>
			</section>
            <footer class="page-footer clearfix"> 
            <a href="/index.php?main_page=account" class="account-link"> 
            <i class="material-icons">&#xE5CB;</i> <span>Back to your account</span> 
            </a> 
            <a href="/" class="account-link"> 
            <i class="material-icons">&#xE88A;</i> <span>Home</span> 
            </a> 
            </footer>
          </section>
        </div>
        <div class="col-md-4">
            <section id="js-checkout-summary" class="card js-cart" data-refresh-url="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=cart?ajax=1&amp;action=refresh">
              <div class="card-block">
                <div class="cart-summary-products">
                  <p>{openzc:cart item="item"/} item</p>
                  <p> <a href="#" data-toggle="collapse" data-target="#cart-summary-product-list"> show details </a> </p>
                  <div class="collapse" id="cart-summary-product-list">
                    <ul class="media-list">
                      <li class="media">
                        <div class="media-left"> <a href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_product=21&amp;id_product_attribute=52&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1#/1-size-s/15-color-green" title="Exercitat Virginia"> <img class="media-object" src="{openzc:field.template/}style/img/140-cart_default.jpg" alt="Exercitat Virginia"> </a> </div>
                        <div class="media-body"> <span class="product-name">Exercitat Virginia</span> <span class="product-quantity">x1</span> <span class="product-price float-xs-right">€87.00</span>
                          <div class="product-line-info product-line-info-secondary text-muted"> <span class="label">Size:</span> <span class="value">S</span> </div>
                          <div class="product-line-info product-line-info-secondary text-muted"> <span class="label">Color:</span> <span class="value">Green</span> </div>
                          <br>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-products"> <span class="label">Subtotal</span> <span class="value">{openzc:cart item="total"/}</span> </div>
                <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-shipping"> <span class="label">Shipping</span> <span class="value">{openzc:cart item="shipping_cost"/}</span> </div>
              </div>
              <hr class="separator">
              <div class="card-block cart-summary-totals">
                <div class="cart-summary-line cart-total"> <span class="label">Total (tax incl.)</span> <span class="value">{openzc:cart item="total"/}</span> </div>
                <div class="cart-summary-line"> <span class="label sub"></span> <span class="value sub"></span> </div>
              </div>
            </section>
            <div id="block-reassurance">
              <ul>
                <li>
                  <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)"> <span class="h6">Security policy (edit with Customer reassurance module)</span> </div>
                </li>
                <li>
                  <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)"> <span class="h6">Delivery policy (edit with Customer reassurance module)</span> </div>
                </li>
                <li>
                  <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)"> <span class="h6">Return policy (edit with Customer reassurance module)</span> </div>
                </li>
              </ul>
            </div>
          </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>
<script type="text/javascript" src="{openzc:field.template/}style/js/openzc.js"></script>
</body>
</html>