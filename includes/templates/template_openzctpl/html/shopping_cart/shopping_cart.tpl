<!doctype html>
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Cart</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body class="lang-en country-de currency-eur layout-full-width page-cart tax-display-enabled">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <aside id="notifications">
    <div class="container"> </div>
  </aside>
  <section id="wrapper">
    
    <div class="container">
      <div id="columns_inner">
        <div id="content-wrapper">
          <section id="main">
            <div class="cart-grid row"> 
              
              <!-- Left Block: cart product informations & shpping -->
              <div class="cart-grid-body col-xs-12 col-lg-8"> 
                {openzc:msg name="shopping_cart" type="error"}
				<div class="alert alert-danger">[field:text/]</div>
				{/openzc:msg}
                <!-- cart products detailed -->
                <div class="card cart-container">
                  <div class="card-block">
                    <h1 class="h3">Shopping Cart</h1>
                  </div>
                  <hr class="separator">
                  <div class="cart-overview js-cart">
                    <ul class="cart-items" id="cartlist">
					{openzc:cart ajax="cartlist" imgsizer="80,76"}
                      <li class="cart-item">
                        <div class="product-line-grid"> 
                          <!--  product left content: image-->
                          <div class="product-line-grid-left col-md-3 col-xs-4"> <span class="product-image media-middle"> <img src="[field:products_image/]" alt="Laudant doloremque"> </span> </div>
                          
                          <!--  product left body: description -->
                          <div class="product-line-grid-body col-md-4 col-xs-8">
                            <div class="product-line-info"> <a class="label" href="[field:products_link/]" >[field:products_name/]</a> </div>
                            <div class="product-line-info product-price h5 has-discount">
							{openzc:if $field['productPriceDiscount']}
                              <div class="product-discount"> 
							  <span class="regular-price">[field:products_original_price/]</span> 
							  <span class="discount discount-percentage"> [field:productPriceDiscount/] </span> 
							  </div>
							 {/openzc:if}
                              <div class="current-price"> 
							  <span class="price">[field:products_price/]</span> 
							  </div>
                            </div>
                            <br>
							{openzc:loopson item="attr"}
								 <div class="product-line-info"> <span class="label">[field:products_options_name/]:</span> <span class="value">[field:products_options_values_name/]</span> </div>
							{/openzc:loopson}
                            
                          </div>
						  
                          
                          <!--  product left body: description -->
                          <div class="product-line-grid-right product-line-actions col-md-5 col-xs-12">
                            <div class="row">
                              <div class="col-xs-4 hidden-md-up"></div>
                              <div class="col-md-10 col-xs-6">
                                <div class="row">
                                  <div class="col-md-6 col-xs-6 qty">
                                    <div class="input-group bootstrap-touchspin">
                                     <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                      <input class="js-cart-line-product-quantity form-control openzc-input" value="[field:products_qty/]" name="product-quantity-spin" min="1" style="display: block;" data-action="quantity" data-id="[field:id/]" >
                                      <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical">
                                      <button class="btn btn-touchspin js-touchspin js-increase-product-quantity bootstrap-touchspin-up " data-id="[field:id/]" data-action="addOne" >
                                      <i class="material-icons touchspin-up"></i>
                                      </button>
                                      <button class="btn btn-touchspin js-touchspin js-decrease-product-quantity bootstrap-touchspin-down " data-id="[field:id/]" data-reload="cartlist">
                                      <i class="material-icons touchspin-down"></i>
                                      </button>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-xs-2 price"> <span class="product-price"> <strong> [field:productsPrice/]</strong> </span> </div>
                                </div>
                              </div>
                              <div class="col-md-2 col-xs-2 text-xs-right">
                                <div class="cart-line-product-actions"> <a class="remove-from-cart openzc-btn" rel="nofollow" data-action="delCart" data-id="[field:id/]"> <i class="material-icons float-xs-left">delete</i> </a> </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </li>
					  {/openzc:cart}
                    </ul>
                  </div>
                </div>
                <a class="label" href="/"> <i class="material-icons">chevron_left</i>Continue shopping </a> 
                
                <!-- shipping informations --> 
                
              </div>
              
              <!-- Right Block: cart subtotal & cart total -->
              <div class="cart-grid-right col-xs-12 col-lg-4">
                <div class="card cart-summary">
                  <div class="cart-detailed-totals">
                    <div class="card-block">
                      <div class="cart-summary-line" id="cart-subtotal-products"> <span class="label js-subtotal"> {openzc:cart item="count"/} item </span> <span class="value">{openzc:cart item="total"/}</span> </div>
                      <div class="cart-summary-line" id="cart-subtotal-shipping"> <span class="label"> Shipping </span> <span class="value">Free</span>
                        <div><small class="value"></small></div>
                      </div>
                    </div>
                    <hr class="separator">
                    <div class="card-block">
                      <div class="cart-summary-line cart-total"> <span class="label">Total (tax incl.)</span> <span class="value">{openzc:cart item="total"/}</span> </div>
                      <div class="cart-summary-line"> <small class="label"></small> <small class="value"></small> </div>
                    </div>
                    <hr class="separator">
                  </div>
                  <div class="checkout cart-detailed-actions card-block">
                    <div class="text-sm-center"> <a href="/index.php?main_page=checkout_shipping" class="btn btn-primary">Proceed to checkout</a> </div>
                  </div>
                </div>
                <div id="block-reassurance">
                  <ul>
                    <li>
                      <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)"> <span class="h6">Security policy (edit with Customer reassurance module)</span> </div>
                    </li>
                    <li>
                      <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)"> <span class="h6">Delivery policy (edit with Customer reassurance module)</span> </div>
                    </li>
                    <li>
                      <div class="block-reassurance-item"> <img src="{openzc:field.template/}style/img/ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)"> <span class="h6">Return policy (edit with Customer reassurance module)</span> </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
  
 
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>
<script type="text/javascript" src="{openzc:field.template/}style/js/openzc.js"></script> 

<script>
Openzc.config={
	customReloadJS:{"yes":"all","no":"core.js"},
	//customReloadID:"viewcart,leftside"
};

</script>
</body>

</html>