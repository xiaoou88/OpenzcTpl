<!doctype html>
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Clothes &amp; Footwear</title>
<meta name="description" content="The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable that it has a more-or-less normal distribution of letters.">
<meta name="keywords" content="">
<link rel="canonical" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_category=3&amp;controller=category&amp;id_lang=1">
<link rel="alternate" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_category=3&amp;controller=category&amp;id_lang=1" hreflang="en-us">
<link rel="alternate" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_category=3&amp;controller=category&amp;id_lang=3" hreflang="fr-fr">
<link rel="alternate" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_category=3&amp;controller=category&amp;id_lang=4" hreflang="es-es">
<link rel="alternate" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_category=3&amp;controller=category&amp;id_lang=5" hreflang="de-de">
<link rel="alternate" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_category=3&amp;controller=category&amp;id_lang=6" hreflang="it-it">
<link rel="alternate" href="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?id_category=3&amp;controller=category&amp;id_lang=7" hreflang="ar-sa">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/vnd.microsoft.icon" href="/prestashop/PRS07/PRS070168/PRS03/img/favicon.ico?1549613849">
<link rel="shortcut icon" type="image/x-icon" href="/prestashop/PRS07/PRS070168/PRS03/img/favicon.ico?1549613849">

<!-- Codezeel added -->
<link href="{openzc:field.template/}style/css/css.css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{openzc:field.template/}style/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psproductcountdown.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/1-simple.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/productcomments.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/psblog.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/lightbox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.ui.theme.min.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/jquery.fancybox.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/cz_verticalmenu.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/flexslider.css" type="text/css" media="all">
<link rel="stylesheet" href="{openzc:field.template/}style/css/custom.css" type="text/css" media="all">
<script type="text/javascript">
        var prestashop = {};
      </script>

<!-- module psproductcountdown start -->
<script type="text/javascript">
        var pspc_labels = ['days', 'hours', 'minutes', 'seconds'];
    var pspc_labels_lang = {
        'days': 'days',
        'hours': 'hours',
        'minutes': 'minutes',
        'seconds': 'seconds'
    };
        var pspc_show_weeks = 0;
    var pspc_psv = 1.7;
</script>
<!-- module psproductcountdown end -->

<style type="text/css">
.fancybox-margin {
	margin-right: 17px;
}
</style>
</head>

<body id="category" class="lang-en country-de currency-eur layout-left-column page-category tax-display-enabled category-id-3 category-clothes-footwear category-id-parent-2 category-depth-level-2">
<main id="page">
  {openzc:include filename="public/header.tpl"/}
  <aside id="notifications">
    <div class="container"> </div>
  </aside>
  <section id="wrapper">
    <nav data-depth="2" class="breadcrumb">
      <div class="container">
       {openzc:catelist cid="current"}<h1 class="h1">[field:categories_name/]</h1>{/openzc:catelist}
       
        <ol itemscope="" >
          {openzc:field.position/}
        </ol>
      </div>
    </nav>
    <div class="container">
      <div id="columns_inner" class="leftside">
        {openzc:include filename="list/leftside.tpl" ajax="leftside"/}
        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9" style="width:78.6%">
          <section id="main">
            <input id="getCartLink" name="getCartLink" value="https://codezeel.com/prestashop/PRS07/PRS070168/PRS03/index.php?controller=cart" type="hidden">
            <input id="getTokenId" name="getTokenId" value="e4c275f0b19b4e8c4b073bc6daba85e9" type="hidden">
            {openzc:catelist cid="current"}
            <div class="block-category card card-block ">
              <div class="category-cover"> 
              <img src="[field:categories_image/]" alt="[field:categories_name/]"> 
              </div>
              <div id="category-description">
                <p>[field:categories_description/]</p>
              </div>
            </div>
            {/openzc:catelist}
            <div id="subcategories">
              <p class="subcategory-heading">Subcategories</p>
              <ul class="clearfix">
               {openzc:catelist cid="current" type="son"}
                <li>
                  <div class="subcategory-image"> 
                  <a href="[field:categories_link/]" title="[field:categories_name/]" class="img"> 
                  <img class="replace-2x" src="{openzc:field.template/}style/img/4-category_default.jpg" alt="[field:categories_name/]"> </a> 
                  </div>
                  <h5><a class="subcategory-name" href="[field:categories_link/]">[field:categories_name/]</a></h5>
                </li>
                {/openzc:catelist}
              </ul>
            </div>
            <section id="products" class="rightlist">
            {openzc:include filename="list/rightlist.tpl" ajax="rightlist"/}
			</section>
          </section>
        </div>
      </div>
    </div>
  </section>
  {openzc:include filename="public/footer.tpl"/}
</main>
<script type="text/javascript" src="{openzc:field.template/}style/js/core.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/theme.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/underscore.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/psproductcountdown.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.rating.pack.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.textareaCounter.plugin.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/productcomments.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_searchbar.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/ps_shoppingcart.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/owl.carousel.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/totalstorage.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/lightbox.js"></script> 
<script type="text/javascript" src="{openzc:field.template/}style/js/custom.js"></script>
<script type="text/javascript" src="{openzc:field.template/}style/js/openzc.js" id="openzc"></script>



</body>
</html>