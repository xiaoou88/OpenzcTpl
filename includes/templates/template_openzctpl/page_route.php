﻿<?php
/*自定义页面*/
$custom_page=[];
$tplpage_route=[
    /*常用模板页面*/
	"tpl_index_default.php"	=>	["index.tpl","cache"],		//网站首页
	"tpl_index_categories.php"	=>	["categories.tpl","cache"], 		//顶级分类页
	"tpl_index_product_list.php"	=>	["products_list.tpl","cache"], 		//产品列表页
	"tpl_featured_products_default.php"	=>	["featured_products.tpl"],		//特色产品页
	"tpl_product_info_display.php"	=>	["products_info.tpl"], 		//产品详情页
	"tpl_specials_default.php"	=>	["specials.tpl"], 		//特价页面
	"tpl_site_map_default.php"	=>	["site_map.tpl"],		//网站地图
	"tpl_gv_faq_default.php"	=>	["gv_faq.tpl"], 		//FAQ页面
	"tpl_contact_us_default.php"	=>	["pages/contact_us.tpl"],		//联系我们
	"tpl_shippinginfo_default.php"	=>	["shippinginfo.tpl"], 		//商店物流说明页面
	"tpl_product_preview_default.php"	=>	["product_preview.tpl"],		//商品预览页面
	"tpl_account_wishlist_default.php"	=>	["wishlist.tpl"],		//商品收藏页面
	"tpl_compare_default.php"	=>	["compare.tpl"],		//商品比较页面
					
	/*会员模板页*/				
	"tpl_account_default.php"	=>	["system/account.tpl"], 		//会员首页
	"tpl_account_edit_default.php"	=>	["system/account_edit.tpl"], 		//会员资料编辑
	"tpl_account_history_default.php"	=>	["system/account_history.tpl"],  		//会员历史
	"tpl_account_history_info_default.php"	=>	["system/account_history_info.tpl"],		//会员订单记录
	"tpl_account_newsletters_default.php"	=>	["system/account_newsletters.tpl"], 		//会员邮件订阅
	"tpl_account_notifications_default.php"	=>	["system/account_notifications.tpl"], 		//账户通知
	"tpl_account_password_default.php"	=>	["system/account_password.tpl"],		//账户密码修改
	"tpl_address_book_default.php"	=>	["system/address_book.tpl"], 		//账户收货地址列表
	"tpl_address_book_process_default.php"	=>	["system/address_book_process.tpl"],		//账户收货地址修改
	"tpl_login_default.php"	=>	["member/login.tpl"],		//会员登录页面
	"tpl_checkout_login_default.php"	=>	["checkout/index.tpl"],		//下单时登录页面
	"tpl_logoff_default.php"	=>	["member/logoff.tpl"],		//会员退出页面
	"tpl_password_forgotten_default.php"	=>	["member/password_forgotten.tpl"],		//会员密码忘记找回
	"tpl_create_account_default.php"	=>	["member/register.tpl"],   		//注册账号
	"tpl_create_account_success_default.php"	=>	["member/create_account_success.tpl"],		//注册账号成功
					
	/*购物相关页*/				
	"tpl_shopping_cart_default.php"	=>	["shopping_cart/shopping_cart.tpl"],		//购物车页面
	"tpl_advanced_search_default.php"	=>	["advanced_search.tpl"], 		//商品搜索页面
	"tpl_onepage_default.php"	=>	["checkout/onepage_test/index.tpl"], 		
	"tpl_advanced_search_result_default.php"	=>	["advanced_search.tpl"],  		//产品搜索
	"tpl_ajax_checkout_confirmation_default.php"	=>	["ajax_checkout_confirmation.tpl"], 		//ajax支付确认页
	"tpl_checkout_confirmation_default.php"	=>	["checkout/confirm.tpl"],    		//支付确认页
	"tpl_checkout_payment_address_default.php"	=>	["checkout_payment_address.tpl"],		//支付地址
	"tpl_checkout_payment_default.php"	=>	["checkout/index.tpl"],   		//支付页
	"tpl_checkout_shipping_address_default.php"	=>	["checkout/checkout_shipping_address.tpl"],		//支付收货地址
	"tpl_checkout_shipping_default.php"	=>	["checkout/index.tpl"],		//下单页
	"tpl_checkout_success_default.php"	=>	["checkout_success.tpl"], 		//支付成功
	"tpl_conditions_default.php"	=>	["conditions.tpl"],  		
					
	"tpl_cookie_usage_default.php"	=>	["cookie_usage.tpl"],		//COOKIE使用情况
	"tpl_discount_coupon_default.php"	=>	["discount_coupon.tpl"],		
	"tpl_document_general_info_displays.php"	=>	["document_general_info.tpl"],		
	"tpl_document_product_info_display.php"	=>	["document_product_info.tpl"],		
					
	"tpl_gv_redeem_default.php"	=>	["gv_redeem.tpl"], 		
	"tpl_gv_send_default.php"	=>	["gv_send.tpl"],		
	"tpl_info_shopping_cart_default.php"	=>	["info_shopping_cart.tpl"],		//购物车使用说明
					
	"tpl_message_stack_default.php"	=>	["message_stack.tpl"],		
	"tpl_page_2_default.php"	=>	["page2.tpl"], 		//单页2
	"tpl_page_3_default.php"	=>	["page3.tpl"], 		//单页3
	"tpl_page_4_default.php"	=>	["page4.tpl"], 		//单页4
	"tpl_page_default.php"	=>	["pages/page.tpl"],		//单页
	"tpl_page_not_found_default.php"	=>	["page_not_found.tpl"],		//404页面
					
	"tpl_payer_auth_frame_default.php"	=>	["payer_auth_frame.tpl"],		
	"tpl_privacy_default.php"	=>	["pages/privacy.tpl"],		
	"tpl_products_all_default.php"	=>	["products_list.tpl"],		//所有产品
	"tpl_products_new_default.php"	=>	["products_new.tpl"],		//新产品页面
	"tpl_products_next_previous.php"	=>	["products_next_previous.tpl"],		//下一个产品
	"tpl_product_free_shipping_info_display.php"	=>	["product_free_shipping_info_display.tpl"],		
					
	"tpl_product_info_noproduct.php"	=>	["product_info_noproduct.tpl"], 		//商品详情页，未知产品
					
	"tpl_product_reviews_info_default.php"	=>	["product_reviews.tpl"],		//商品评论列表
	"tpl_product_reviews_write_default.php"	=>	["product_reviews_write.tpl"],		//填写评论
	"tpl_reviews_default.php"	=>	["reviews.tpl"], 		//所有产品评论列表页面
					
	"tpl_ssl_check_default.php"	=>	["ssl_check.tpl"],		//安全检查
	"tpl_time_out_default.php"	=>	["time_out.tpl"],		//页面过期页
	"tpl_unsubscribe_default.php"	=>	["unsubscribe.tpl"],		//退订说明
	"tpl_zc_install_suggested_default.php"	=>	["zc_install_suggested.tpl"]
];




?>