<h1><a id="OpenzcTpl" class="anchor" href="#OpenzcTpl"></a>OpenzcTpl</h1>

<p>
<a href="http://openzc.cn">
    <img src="https://img.shields.io/badge/home-page-yellow.svg?style=flat" alt="Home page">
</a> 
<a href="http://openzc.cn">
    <img src="https://img.shields.io/badge/Licence-GPLV2-green.svg?style=flat" alt="GPLV2 License">
</a>

</p>

<h1><a id="description" class="anchor" href="#description"></a>介绍</h1>
<blockquote>
<p>OpenzcTPL 是基于 Zencart 开发的前端模板引擎，秉承极简、极速、极致的开发理念，集成了基于数据实时缓存机制并封装多种常用高效的模板标签，以方便开发者快速制作自己的跨境电商网站。</p>
</blockquote>


<h1><a id="path" class="anchor" href="#path"></a>软件架构</h1>
<pre deep="5">includes/
├─── openzctpl/         **模板引擎目录
│    ├─── classes/      **工具类目录
│    ├─── config/       **配置目录
│    ├─── functions/    **通用函数目录
│    ├─── install/      **安装目录
│    ├─── model/        **标签模型目录
│    └─── openzc.php    **OpenzcTPL引擎入口文件
│ 
│ 
├─── templates/      
│    └─── template_openzc/        **模板文件夹
│         ├─── page_route.php     **模板文件映射
│         ├─── template_info.php  **模板说明文档
│         ├─── html/              **模板文件.tpl
│         │    └─── modules/      **系统内置固定模板文件
│         │    │    └─── assets/  **系统内置固定模板样式库
│         │    │    └─── json/    **系统内置数据处理附件
│         │    │    └─── msgbox/  **系统内置信息提示框模板
│         │    │    └─── tpl/     **系统内置固定模板文件
│         │    └─── system/       **系统默认模板文件夹
│         ├─── languages/         **自定义define国家语言
│         ├─── templates/         **原始Zencart模板必须文件
│         └─── tplcache/          **模板文件临时缓存目录
</pre>


<h1><a id="install" class="anchor" href="#install"></a>安装教程</h1>
<div class="white"><div class="highlight"><pre><span id="LC1" class="line">git clone https://gitee.com/openzc/openzctpl/repository/archive/master.zip</span>
<span id="LC2" class="line">chmod -Rf 777 *</span></pre></div></div>

<blockquote>
<p>
下载压缩包文件,解压到已建好的Zen Cart网站根目录下。<br/>
访问安装地址:"http://www.yourdomain.com/openzctpl_install/"<br/>
按默认提示点击下一步直至安装完成。<br/>

进入后台选择模板<br/>
进入后台 > 模板选择,首次使用选择模板 "template_openzctpl" 仅供学习参考<br/>

其中两个重要文件：<br/>
page_route.php(模板文件映射:原生Zencart模板文件对应的.tpl静文件)<br/>
template_info.php(模板说明文档：在原有基础上添加一个变量$openzc_template_engine=true;,模板引擎开关)<br/><br/>
对原生Zencart无任何影响<br/>
Openzctpl模板引擎不影响原生模板使用机制,若要使用原生Zencart,可关掉开关$openzc_template_engine=false;
</p>
</blockquote>
<h1><a id="copyright" class="anchor" href="#copyright"></a>版权声明</h1>
<p>
OpenzcTpl使用 GPL v2 协议. license.License<a href="http://openzc.cn/openzctpl/license.txt">License</a><br>
Contact: hexipeng # openzc.cn & openzc.com<br>
Copyright (C) 2020 openzc.cn & openzc.com
</p>
